<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderAssignmentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->string('delivery_service_username')->nullable();
            $table->boolean('is_accepted')->default(false);
            $table->timestamp('acceptation_time')->nullable();
            $table->timestamp('rejection_time')->nullable();
            $table->boolean('is_waiting_acceptation')->default(true);
            $table->bigInteger('creator_id')->nullable();
            $table->string('creator')->nullable();
            $table->string('delivery_fees')->nullable();
            $table->bigInteger('task_id')->nullable();
            $table->string('currency_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_assignments');
    }
}
