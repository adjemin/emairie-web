<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTokenTransactionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('amount')->nullable();
            $table->bigInteger('source_id')->nullable();
            $table->string('source_name')->nullable();
            $table->string('transaction_type')->nullable();
            $table->bigInteger('receiver_id')->nullable();
            $table->string('receiver_name')->nullable();
            $table->string('designation')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('token_transactions');
    }
}
