<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('phone')->unique();
            $table->string('dial_code')->nullable();
            $table->string('country_code')->nullable();
            $table->string('email')->nullable();
            $table->bigInteger('photo_id')->nullable();
            $table->string('photo_url')->nullable();
            $table->string('photo_mime')->nullable();
            $table->string('photo_height')->nullable();
            $table->string('photo_width')->nullable();
            $table->string('photo_length')->nullable();
            $table->string('gender')->nullable();
            $table->string('bio')->nullable();
            $table->string('birthday')->nullable();
            $table->string('language')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('twitter_id')->nullable();
            $table->boolean('is_pro_seller')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
