<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable();
            $table->string('method_payment_id')->nullable();
            $table->string('payment_id')->nullable();
            $table->string('type_payment')->nullable();
            $table->string('amount')->nullable();
            $table->string('cpm_phone_prefix')->nullable();
            $table->string('cel_phone_num')->nullable();
            $table->string('cpm_ipn_ack')->nullable();
            $table->string('cpm_currency')->nullable();
            $table->string('status')->nullable();
            $table->boolean('is_transfer')->default(false);
            $table->string('cpm_result')->nullable();
            $table->string('commission')->nullable();
            $table->string('payid')->nullable();
            $table->string('buyername')->nullable();
            $table->string('transstatus')->nullable();
            $table->string('signature')->nullable();
            $table->string('cpm_designation')->nullable();
            $table->string('cpm_error_message')->nullable();
            $table->string('cpm_trans_date')->nullable();
            $table->date('cpm_payment_date')->nullable();
            $table->time('cpm_payment_time')->nullable();
            $table->text('sms')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->bigInteger('source_id')->nullable();
            $table->string('source_name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
