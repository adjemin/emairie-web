<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactInvitationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_invitations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('contact_id')->nullable();
            $table->text('message')->nullable();
            $table->boolean('is_sms')->default(false);
            $table->boolean('is_sent')->default(false);
            $table->string('receiver')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contact_invitations');
    }
}
