<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('description_metadata')->nullable();
            $table->text('medias')->nullable();
            $table->bigInteger('cover_id')->nullable();
            $table->string('cover_type')->nullable();
            $table->string('cover_url')->nullable();
            $table->string('cover_thumbnail')->nullable();
            $table->string('cover_duration')->nullable();
            $table->string('cover_width')->nullable();
            $table->string('cover_height')->nullable();
            $table->string('location_name')->nullable();
            $table->string('location_address')->nullable();
            $table->double('location_lat')->nullable();
            $table->double('location_lng')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->text('category_meta')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->boolean('is_sold')->default(false);
            $table->timestamp('sold_at')->nullable();
            $table->string('initiale_count')->default(1);
            $table->boolean('is_firm_price')->default(false);
            $table->integer('condition_id')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->boolean('is_published')->default(false);
            $table->bigInteger('deleted_by')->nullable();
            $table->string('deleted_creator')->nullable();
            $table->string('sold_count')->default(0);
            $table->text('delivery_services')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
