<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FlashSelling;
use Faker\Generator as Faker;

$factory->define(FlashSelling::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'flash_start_date' => $faker->date('Y-m-d H:i:s'),
        'flash_end_date' => $faker->date('Y-m-d H:i:s'),
        'delay' => $faker->word,
        'is_started' => $faker->word,
        'is_ended' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
