<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {

    return [
        'order_id' => $faker->randomDigitNotNull,
        'meta_data' => $faker->text,
        'quantity' => $faker->randomDigitNotNull,
        'quantity_unit' => $faker->word,
        'unit_price' => $faker->word,
        'currency_code' => $faker->word,
        'total_amount' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
