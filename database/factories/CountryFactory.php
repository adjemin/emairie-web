<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Country;
use Faker\Generator as Faker;

$factory->define(Country::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'code' => $faker->word,
        'flag' => $faker->word,
        'dial_code' => $faker->word,
        'language' => $faker->word,
        'location_lat' => $faker->word,
        'location_lng' => $faker->word,
        'currency_code' => $faker->word,
        'timezone' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
