<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductMedia;
use Faker\Generator as Faker;

$factory->define(ProductMedia::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'content_type' => $faker->word,
        'mime' => $faker->word,
        'url' => $faker->text,
        'thumbnail' => $faker->text,
        'duration' => $faker->word,
        'width' => $faker->word,
        'height' => $faker->word,
        'length' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
