<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'name' => $faker->word,
        'phone_number' => $faker->word,
        'dial_code' => $faker->word,
        'country_code' => $faker->word,
        'email' => $faker->word,
        'photo_id' => $faker->word,
        'photo_url' => $faker->word,
        'photo_mime' => $faker->word,
        'photo_height' => $faker->word,
        'photo_width' => $faker->word,
        'photo_length' => $faker->word,
        'gender' => $faker->word,
        'bio' => $faker->word,
        'birthday' => $faker->word,
        'language' => $faker->word,
        'facebook_id' => $faker->word,
        'twitter_id' => $faker->word,
        'is_pro_seller' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
