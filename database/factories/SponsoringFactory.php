<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Sponsoring;
use Faker\Generator as Faker;

$factory->define(Sponsoring::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'sponsoring_type' => $faker->word,
        'start_date' => $faker->date('Y-m-d H:i:s'),
        'end_date' => $faker->date('Y-m-d H:i:s'),
        'cost' => $faker->word,
        'currency_code' => $faker->word,
        'percentage' => $faker->word,
        'percentage_fees' => $faker->word,
        'visilibity' => $faker->word,
        'visibility_location' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
