<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TokenTransaction;
use Faker\Generator as Faker;

$factory->define(TokenTransaction::class, function (Faker $faker) {

    return [
        'amount' => $faker->word,
        'source_id' => $faker->word,
        'source_name' => $faker->word,
        'transaction_type' => $faker->word,
        'receiver_id' => $faker->word,
        'receiver_name' => $faker->word,
        'designation' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
