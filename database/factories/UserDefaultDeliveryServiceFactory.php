<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserDefaultDeliveryService;
use Faker\Generator as Faker;

$factory->define(UserDefaultDeliveryService::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'delivery_service_id' => $faker->word,
        'default_cost' => $faker->word,
        'cost_type' => $faker->word,
        'priority' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
