<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coupon;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {

    return [
        'code' => $faker->word,
        'is_used' => $faker->word,
        'percentage' => $faker->word,
        'delay' => $faker->word,
        'description' => $faker->word,
        'title' => $faker->word,
        'created_by' => $faker->word,
        'used_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
