<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Contact;
use Faker\Generator as Faker;

$factory->define(Contact::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'name' => $faker->word,
        'phone' => $faker->word,
        'email' => $faker->word,
        'has_email' => $faker->word,
        'has_phone' => $faker->word,
        'is_accessible' => $faker->word,
        'accessible_change' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
