<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderDelivery;
use Faker\Generator as Faker;

$factory->define(OrderDelivery::class, function (Faker $faker) {

    return [
        'order_id' => $faker->word,
        'delivery_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
