<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryService;
use Faker\Generator as Faker;

$factory->define(DeliveryService::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'smartlivraison_username' => $faker->word,
        'email' => $faker->word,
        'logo' => $faker->word,
        'delivery_level' => $faker->word,
        'phone' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
