<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeliveryAddress;
use Faker\Generator as Faker;

$factory->define(DeliveryAddress::class, function (Faker $faker) {

    return [
        'address_name' => $faker->word,
        'latitude' => $faker->word,
        'longitude' => $faker->word,
        'place' => $faker->word,
        'town' => $faker->word,
        'address_typed' => $faker->word,
        'address_type' => $faker->word,
        'customer_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
