<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderAssignment;
use Faker\Generator as Faker;

$factory->define(OrderAssignment::class, function (Faker $faker) {

    return [
        'order_id' => $faker->word,
        'delivery_service_username' => $faker->word,
        'is_accepted' => $faker->word,
        'acceptation_time' => $faker->date('Y-m-d H:i:s'),
        'rejection_time' => $faker->date('Y-m-d H:i:s'),
        'is_waiting_acceptation' => $faker->word,
        'creator_id' => $faker->word,
        'creator' => $faker->word,
        'delivery_fees' => $faker->word,
        'task_id' => $faker->word,
        'currency_code' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
