<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\WalletToken;
use Faker\Generator as Faker;

$factory->define(WalletToken::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'amount' => $faker->word,
        'is_actived' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
