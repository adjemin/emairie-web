<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductAudience;
use Faker\Generator as Faker;

$factory->define(ProductAudience::class, function (Faker $faker) {

    return [
        'product_id' => $faker->word,
        'view_count' => $faker->word,
        'last_read_date' => $faker->date('Y-m-d H:i:s'),
        'like_count' => $faker->word,
        'order_count' => $faker->word,
        'conversation_count' => $faker->word,
        'issue_count' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
