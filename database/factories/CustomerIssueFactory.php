<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerIssue;
use Faker\Generator as Faker;

$factory->define(CustomerIssue::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'created_by' => $faker->word,
        'description' => $faker->text,
        'issue_type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
