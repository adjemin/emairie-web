<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'methode_payment_id' => $faker->word,
        'payment_id' => $faker->word,
        'type_payment' => $faker->word,
        'payment_method' => $faker->word,
        'amount' => $faker->word,
        'cpm_phone_prefixe' => $faker->word,
        'cel_phone_num' => $faker->word,
        'cpm_ipn_ack' => $faker->word,
        'cpm_currency' => $faker->word,
        'status' => $faker->word,
        'is_transfer' => $faker->word,
        'cpm_result' => $faker->word,
        'commission' => $faker->word,
        'payid' => $faker->word,
        'buyername' => $faker->word,
        'transstatus' => $faker->word,
        'signature' => $faker->word,
        'cpm_designation' => $faker->word,
        'cpm_error_message' => $faker->word,
        'cpm_trans_date' => $faker->word,
        'cpm_payment_date' => $faker->word,
        'cpm_payment_time' => $faker->word,
        'sms' => $faker->text,
        'is_deleted' => $faker->word,
        'created_by' => $faker->word,
        'updated_by' => $faker->word,
        'deleted_by' => $faker->word,
        'source_id' => $faker->word,
        'source_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
