<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {

    return [
        'url' => $faker->word,
        'mime' => $faker->word,
        'height' => $faker->word,
        'width' => $faker->word,
        'length' => $faker->word,
        'customer�_id' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
