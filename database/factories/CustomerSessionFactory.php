<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerSession;
use Faker\Generator as Faker;

$factory->define(CustomerSession::class, function (Faker $faker) {

    return [
        'token' => $faker->text,
        'customer_id' => $faker->word,
        'is_active' => $faker->word,
        'location_address' => $faker->word,
        'location_latitude' => $faker->word,
        'location_longitude' => $faker->word,
        'battery' => $faker->word,
        'version' => $faker->word,
        'device' => $faker->text,
        'ip_address' => $faker->word,
        'network' => $faker->word,
        'isMobile' => $faker->word,
        'isTesting' => $faker->word,
        'deviceId' => $faker->word,
        'devicesOSVersion' => $faker->word,
        'devicesName' => $faker->word,
        'w' => $faker->word,
        'h' => $faker->word,
        'ms' => $faker->word,
        'idapp' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
