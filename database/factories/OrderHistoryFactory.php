<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderHistory;
use Faker\Generator as Faker;

$factory->define(OrderHistory::class, function (Faker $faker) {

    return [
        'order_id' => $faker->word,
        'status' => $faker->word,
        'creator' => $faker->word,
        'creator_id' => $faker->word,
        'creator_name' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
