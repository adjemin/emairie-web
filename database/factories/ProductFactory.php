<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'description_metadata' => $faker->text,
        'medias' => $faker->text,
        'cover_id' => $faker->word,
        'cover_type' => $faker->word,
        'cover_url' => $faker->word,
        'cover_thumbnail' => $faker->word,
        'cover_duration' => $faker->word,
        'cover_width' => $faker->word,
        'cover_height' => $faker->word,
        'location_name' => $faker->word,
        'location_address' => $faker->word,
        'location_lat' => $faker->word,
        'location_lng' => $faker->word,
        'category_id' => $faker->randomDigitNotNull,
        'category_meta' => $faker->text,
        'customer_id' => $faker->word,
        'is_sold' => $faker->word,
        'sold_at' => $faker->date('Y-m-d H:i:s'),
        'initiale_count' => $faker->word,
        'is_firm_price' => $faker->word,
        'condition_id' => $faker->randomDigitNotNull,
        'published_at' => $faker->date('Y-m-d H:i:s'),
        'is_published' => $faker->word,
        'deleted_by' => $faker->word,
        'deleted_creator' => $faker->word,
        'sold_count' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
