<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'is_waiting' => $faker->word,
        'current_status' => $faker->word,
        'rating' => $faker->word,
        'note' => $faker->text,
        'note_date' => $faker->date('Y-m-d H:i:s'),
        'payment_method_slug' => $faker->text,
        'delivery_fees' => $faker->word,
        'is_delivered' => $faker->word,
        'delivery_date' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
