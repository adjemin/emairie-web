<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EvaluateSeller;
use Faker\Generator as Faker;

$factory->define(EvaluateSeller::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'note' => $faker->randomDigitNotNull,
        'note_description' => $faker->text,
        'note_by' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
