<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ContactInvitation;
use Faker\Generator as Faker;

$factory->define(ContactInvitation::class, function (Faker $faker) {

    return [
        'contact_id' => $faker->word,
        'message' => $faker->text,
        'is_sms' => $faker->word,
        'is_sent' => $faker->word,
        'receiver' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
