<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $orderDelivery->order_id !!}</p>
</div>

<!-- Delivery Id Field -->
<div class="form-group">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    <p>{!! $orderDelivery->delivery_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $orderDelivery->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $orderDelivery->updated_at !!}</p>
</div>

