<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_id', 'Delivery Id:') !!}
    {!! Form::text('delivery_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orderDeliveries.index') !!}" class="btn btn-default">Cancel</a>
</div>
