<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Used Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_used', 'Is Used:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_used', 0) !!}
        {!! Form::checkbox('is_used', '1', null) !!}
    </label>
</div>


<!-- Percentage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('percentage', 'Percentage:') !!}
    {!! Form::text('percentage', null, ['class' => 'form-control']) !!}
</div>

<!-- Delay Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay', 'Delay:') !!}
    {!! Form::text('delay', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Used By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('used_by', 'Used By:') !!}
    {!! Form::text('used_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('coupons.index') !!}" class="btn btn-default">Cancel</a>
</div>
