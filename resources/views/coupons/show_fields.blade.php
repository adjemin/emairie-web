<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $coupon->code !!}</p>
</div>

<!-- Is Used Field -->
<div class="form-group">
    {!! Form::label('is_used', 'Is Used:') !!}
    <p>{!! $coupon->is_used !!}</p>
</div>

<!-- Percentage Field -->
<div class="form-group">
    {!! Form::label('percentage', 'Percentage:') !!}
    <p>{!! $coupon->percentage !!}</p>
</div>

<!-- Delay Field -->
<div class="form-group">
    {!! Form::label('delay', 'Delay:') !!}
    <p>{!! $coupon->delay !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $coupon->description !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $coupon->title !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $coupon->created_by !!}</p>
</div>

<!-- Used By Field -->
<div class="form-group">
    {!! Form::label('used_by', 'Used By:') !!}
    <p>{!! $coupon->used_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $coupon->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $coupon->updated_at !!}</p>
</div>

