<div class="table-responsive-sm">
    <table class="table table-striped" id="productConditions-table">
        <thead>
            <th>Name</th>
            <th>Name EN</th>
            <th>Slug</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($productConditions as $productCondition)
            <tr>
            <td>{!! $productCondition->name !!}</td>
            <td>{!! $productCondition->name_en !!}</td>
            <td>{!! $productCondition->slug !!}</td>
                <td>
                    {!! Form::open(['route' => ['productConditions.destroy', $productCondition->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('productConditions.show', [$productCondition->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('productConditions.edit', [$productCondition->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
