@extends('layoutFront.app_chield')
@section('detailProduit')
<div class="col">
    <div class="text-center">
      @if($customer->photo_url == NULL)
      <img src="{{asset('front/dist/images/avatar.png')}}" class="rounded-circle" alt="avatar">
      @else
      <img src="{{asset($customer->photo_url)}}" class="rounded-circle" alt="avatar" style="width: 70%">
      @endif
    </div></hr>
    <div class="form-group"><br>
      <form method="post" action="{{url('modifier',['id' =>Crypt::encrypt($customer->id)])}}" role='form' enctype="multipart/form-data">
          @csrf
          @method("PUT")
      <input class="form-control" onchange="this.form.submit()" name="photo" type="file" id="photo">
      </form>
    </div>
      <hr>
    <table class="table table-bordered">
      <tr>
        <th colspan="2">Mes transactions</th>
      </tr>
      <tr>
        <th>Mes Achats</th>
        @if($mont->count() > 0)
          @if($customer->order->count()>0)
        <td>CFA {{$achat}}</td>
          @else
        <td>CFA 0</td>
          @endif
        @endif
      </tr>
      <tr>
        <th>Mes Ventes</th>
        @if($mont->count()>0)
          @if($customer->product->count()>0)
        <td>CFA </td>
          @else
        <td>CFA 0</td>
          @endif
        @endif
      </tr>
    </table>
</div>
<div class="col-md-9">
  <div class="card">
    <!-- card body1 --->
     <!-- Nav tabs -->
     <ul class="nav nav-tabs">
      <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#infosCompte">
            <button style="border:none" type="button" class="btn btn-primary">
              Informations personelle
            </button>
          </a>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#pramCompte">
            <button style="border:none" type="button" class="btn btn-success">
            Parametre du compte
            </button>
          </a>
      </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div id="infosCompte" class="container tab-pane active"><br>
        <form method="post" action="{{url('modifier',['id' =>Crypt::encrypt($customer->id)])}}" role='form' enctype="multipart/form-data">
          @csrf
            @method("PUT")
       <div class="form-group">
        <label for="civilite" style="float:left">Civilité</label>
        <select class="form-control" name="civilite">
          <option value="Monsieur">Monsieur</option>
          <option value="Madame">Madame</option>
          <option value="Mademoiselle">Mademoiselle</option>
        </select>
       </div>

       <div class="form-group">
        <label for="nom" style="float:left">Nom</label>
        <input type="text" id="nom" name="nom" required class="form-control" value="{{$customer->last_name}}">
       </div>
       
        <div class="form-group">
          <label for="prenom" style="float:left;margin-top:10px;">Prénoms</label>
          <input type="text" name="prenom" required class="form-control" value="{{$customer->first_name}}">
        </div>
       
        <div class="form-group">
          <label for="dateN" style="float:left;margin-top:10px;">Date de Naissance</label>
          @if($customer->birthday == NULL)
          <input class="form-control" name="dateN" type="date" id="dateN">
          @else
          <input type="text" name="dateN" value="{{$customer->birthday}}" class="form-control">
          @endif
        </div>

       <br><button type="submit" class="btn btn-primary btn-block">Modifier</button>
       <a href="{{url('home')}}" class="btn btn-success btn-block">Annuler</a>
      </form>
        </div>
        <div id="pramCompte" class="container tab-pane fade"><br>
          <form method="post" action="{{url('modifier',['id' =>Crypt::encrypt($customer->id)])}}" role='form' enctype="multipart/form-data">
            @csrf
              @method("PUT")
          <div class="form-group">
            <label for="phone" style="float:left;">Numéro</label>
            <input type="text" readonly name="phone" class="form-control" value="{{$customer->phone}}">
           </div>
          
           <div class="form-group">
            <label for="email" style="float:left;">Mail</label>
            <input type="email" name="email" class="form-control" value="{{$customer->email}}" placeholder="123@xyz">
           </div>
           <br><button type="submit" class="btn btn-primary btn-block">Modifier</button>
           <a href="{{url('home')}}" class="btn btn-success btn-block">Annuler</a>
        </form>
      </div>
        </div><br>
    <!--End card body2 --->
  </div><br>
</div>
@endsection