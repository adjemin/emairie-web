<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $userDefaultDeliveryService->customer_id !!}</p>
</div>

<!-- Delivery Service Id Field -->
<div class="form-group">
    {!! Form::label('delivery_service_id', 'Delivery Service Id:') !!}
    <p>{!! $userDefaultDeliveryService->delivery_service_id !!}</p>
</div>

<!-- Default Cost Field -->
<div class="form-group">
    {!! Form::label('default_cost', 'Default Cost:') !!}
    <p>{!! $userDefaultDeliveryService->default_cost !!}</p>
</div>

<!-- Cost Type Field -->
<div class="form-group">
    {!! Form::label('cost_type', 'Cost Type:') !!}
    <p>{!! $userDefaultDeliveryService->cost_type !!}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{!! $userDefaultDeliveryService->priority !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $userDefaultDeliveryService->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $userDefaultDeliveryService->updated_at !!}</p>
</div>

