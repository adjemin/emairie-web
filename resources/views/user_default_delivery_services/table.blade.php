<div class="table-responsive-sm">
    <table class="table table-striped" id="userDefaultDeliveryServices-table">
        <thead>
            <th>Customer Id</th>
        <th>Delivery Service Id</th>
        <th>Default Cost</th>
        <th>Cost Type</th>
        <th>Priority</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($userDefaultDeliveryServices as $userDefaultDeliveryService)
            <tr>
                <td>{!! $userDefaultDeliveryService->customer_id !!}</td>
            <td>{!! $userDefaultDeliveryService->delivery_service_id !!}</td>
            <td>{!! $userDefaultDeliveryService->default_cost !!}</td>
            <td>{!! $userDefaultDeliveryService->cost_type !!}</td>
            <td>{!! $userDefaultDeliveryService->priority !!}</td>
                <td>
                    {!! Form::open(['route' => ['userDefaultDeliveryServices.destroy', $userDefaultDeliveryService->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('userDefaultDeliveryServices.show', [$userDefaultDeliveryService->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('userDefaultDeliveryServices.edit', [$userDefaultDeliveryService->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>