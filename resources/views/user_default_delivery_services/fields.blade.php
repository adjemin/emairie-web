<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_service_id', 'Delivery Service Id:') !!}
    {!! Form::text('delivery_service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Default Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('default_cost', 'Default Cost:') !!}
    {!! Form::text('default_cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost_type', 'Cost Type:') !!}
    {!! Form::text('cost_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::text('priority', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('userDefaultDeliveryServices.index') !!}" class="btn btn-default">Cancel</a>
</div>
