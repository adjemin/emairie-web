<table class="table table-responsive" id="invoicePayments-table">
    <thead>
        <tr>
            <th>Invoice Id</th>
        <th>Payment Method</th>
        <th>Payment Reference</th>
        <th>Amount</th>
        <th>Currency Code</th>
        <th>Coupon</th>
        <th>Creator Id</th>
        <th>Creator Name</th>
        <th>Creator</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($invoicePayments as $invoicePayment)
        <tr>
            <td>{!! $invoicePayment->invoice_id !!}</td>
            <td>{!! $invoicePayment->payment_method !!}</td>
            <td>{!! $invoicePayment->payment_reference !!}</td>
            <td>{!! $invoicePayment->amount !!}</td>
            <td>{!! $invoicePayment->currency_code !!}</td>
            <td>{!! $invoicePayment->coupon !!}</td>
            <td>{!! $invoicePayment->creator_id !!}</td>
            <td>{!! $invoicePayment->creator_name !!}</td>
            <td>{!! $invoicePayment->creator !!}</td>
            <td>
                {!! Form::open(['route' => ['invoicePayments.destroy', $invoicePayment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('invoicePayments.show', [$invoicePayment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('invoicePayments.edit', [$invoicePayment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>