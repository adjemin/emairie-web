<!-- Invoice Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_id', 'Invoice Id:') !!}
    {!! Form::text('invoice_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_reference', 'Payment Reference:') !!}
    {!! Form::text('payment_reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Coupon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coupon', 'Coupon:') !!}
    {!! Form::text('coupon', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    {!! Form::text('creator_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    {!! Form::text('creator_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator', 'Creator:') !!}
    {!! Form::text('creator', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('invoicePayments.index') !!}" class="btn btn-default">Cancel</a>
</div>
