<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $invoicePayment->id !!}</p>
</div>

<!-- Invoice Id Field -->
<div class="form-group">
    {!! Form::label('invoice_id', 'Invoice Id:') !!}
    <p>{!! $invoicePayment->invoice_id !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $invoicePayment->payment_method !!}</p>
</div>

<!-- Payment Reference Field -->
<div class="form-group">
    {!! Form::label('payment_reference', 'Payment Reference:') !!}
    <p>{!! $invoicePayment->payment_reference !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $invoicePayment->amount !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $invoicePayment->currency_code !!}</p>
</div>

<!-- Coupon Field -->
<div class="form-group">
    {!! Form::label('coupon', 'Coupon:') !!}
    <p>{!! $invoicePayment->coupon !!}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{!! $invoicePayment->creator_id !!}</p>
</div>

<!-- Creator Name Field -->
<div class="form-group">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    <p>{!! $invoicePayment->creator_name !!}</p>
</div>

<!-- Creator Field -->
<div class="form-group">
    {!! Form::label('creator', 'Creator:') !!}
    <p>{!! $invoicePayment->creator !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $invoicePayment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $invoicePayment->updated_at !!}</p>
</div>

