<!-- Contact Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_id', 'Contact Id:') !!}
    {!! Form::text('contact_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('message', 'Message:') !!}
    {!! Form::text('message', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Sms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sms', 'Is Sms:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_sms', 0) !!}
        {!! Form::checkbox('is_sms', '1', null) !!}
    </label>
</div>


<!-- Is Sent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sent', 'Is Sent:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_sent', 0) !!}
        {!! Form::checkbox('is_sent', '1', null) !!}
    </label>
</div>


<!-- Receiver Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver', 'Receiver:') !!}
    {!! Form::text('receiver', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('contactInvitations.index') !!}" class="btn btn-default">Cancel</a>
</div>
