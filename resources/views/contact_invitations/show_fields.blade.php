<!-- Contact Id Field -->
<div class="form-group">
    {!! Form::label('contact_id', 'Contact Id:') !!}
    <p>{!! $contactInvitation->contact_id !!}</p>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', 'Message:') !!}
    <p>{!! $contactInvitation->message !!}</p>
</div>

<!-- Is Sms Field -->
<div class="form-group">
    {!! Form::label('is_sms', 'Is Sms:') !!}
    <p>{!! $contactInvitation->is_sms !!}</p>
</div>

<!-- Is Sent Field -->
<div class="form-group">
    {!! Form::label('is_sent', 'Is Sent:') !!}
    <p>{!! $contactInvitation->is_sent !!}</p>
</div>

<!-- Receiver Field -->
<div class="form-group">
    {!! Form::label('receiver', 'Receiver:') !!}
    <p>{!! $contactInvitation->receiver !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $contactInvitation->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $contactInvitation->updated_at !!}</p>
</div>

