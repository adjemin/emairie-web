<div class="table-responsive-sm">
    <table class="table table-striped" id="contactInvitations-table">
        <thead>
            <th>Contact Id</th>
        <th>Message</th>
        <th>Is Sms</th>
        <th>Is Sent</th>
        <th>Receiver</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($contactInvitations as $contactInvitation)
            <tr>
                <td>{!! $contactInvitation->contact_id !!}</td>
            <td>{!! $contactInvitation->message !!}</td>
            <td>{!! $contactInvitation->is_sms !!}</td>
            <td>{!! $contactInvitation->is_sent !!}</td>
            <td>{!! $contactInvitation->receiver !!}</td>
                <td>
                    {!! Form::open(['route' => ['contactInvitations.destroy', $contactInvitation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('contactInvitations.show', [$contactInvitation->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('contactInvitations.edit', [$contactInvitation->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>