<!DOCTYPE html>
<html>
<head>
  <title>Adjemin</title>
  <link rel="stylesheet" href="{{asset('front/dist/productDetails.css')}}">

<link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">

<link rel="stylesheet" href="{{asset('front/dist/style.css')}}">
  <link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<script>
  (function ($) {
    $(document).ready(function() {
    $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', Xoffset: 15});
    $('.xzoom2, .xzoom-gallery2').xzoom({position: '#xzoom2-id', tint: '#ffa200'});
    $('.xzoom3, .xzoom-gallery3').xzoom({position: 'lens', lensShape: 'circle', sourceClass: 'xzoom-hidden'});
    $('.xzoom4, .xzoom-gallery4').xzoom({tint: '#006699', Xoffset: 15});
    $('.xzoom5, .xzoom-gallery5').xzoom({tint: '#006699', Xoffset: 15});
    
    //Integration with hammer.js
    var isTouchSupported = 'ontouchstart' in window;
    
    if (isTouchSupported) {
    //If touch adjemin product
    $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function(){
    var xzoom = $(this).data('xzoom');
    xzoom.eventunbind();
    });
    
    $('.xzoom, .xzoom2, .xzoom3').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    var s = 1, ls;
    
    xzoom.eventmove = function(element) {
    element.hammer().on('drag', function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    xzoom.movezoom(event);
    event.gesture.preventDefault();
    });
    }
    
    xzoom.eventleave = function(element) {
    element.hammer().on('tap', function(event) {
    xzoom.closezoom();
    });
    }
    xzoom.openzoom(event);
    });
    });
    
    $('.xzoom4').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    var s = 1, ls;
    
    xzoom.eventmove = function(element) {
    element.hammer().on('drag', function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    xzoom.movezoom(event);
    event.gesture.preventDefault();
    });
    }
    
    var counter = 0;
    xzoom.eventclick = function(element) {
    element.hammer().on('tap', function() {
    counter++;
    if (counter == 1) setTimeout(openfancy,300);
    event.gesture.preventDefault();
    });
    }
    
    function openfancy() {
    if (counter == 2) {
    xzoom.closezoom();
    $.fancybox.open(xzoom.gallery().cgallery);
    } else {
    xzoom.closezoom();
    }
    counter = 0;
    }
    xzoom.openzoom(event);
    });
    });
    
    $('.xzoom5').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    var s = 1, ls;
    
    xzoom.eventmove = function(element) {
    element.hammer().on('drag', function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    xzoom.movezoom(event);
    event.gesture.preventDefault();
    });
    }
    
    var counter = 0;
    xzoom.eventclick = function(element) {
    element.hammer().on('tap', function() {
    counter++;
    if (counter == 1) setTimeout(openmagnific,300);
    event.gesture.preventDefault();
    });
    }
    
    function openmagnific() {
    if (counter == 2) {
    xzoom.closezoom();
    var gallery = xzoom.gallery().cgallery;
    var i, images = new Array();
    for (i in gallery) {
    images[i] = {src: gallery[i]};
    }
    $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
    } else {
    xzoom.closezoom();
    }
    counter = 0;
    }
    xzoom.openzoom(event);
    });
    });
    
    } else {
    //If not touch adjemin product
    
    //Integration with fancybox plugin
    $('#xzoom-fancy').bind('click', function(event) {
    var xzoom = $(this).data('xzoom');
    xzoom.closezoom();
    $.fancybox.open(xzoom.gallery().cgallery, {padding: 0, helpers: {overlay: {locked: false}}});
    event.preventDefault();
    });
    
    //Integration with magnific popup plugin
    $('#xzoom-magnific').bind('click', function(event) {
    var xzoom = $(this).data('xzoom');
    xzoom.closezoom();
    var gallery = xzoom.gallery().cgallery;
    var i, images = new Array();
    for (i in gallery) {
    images[i] = {src: gallery[i]};
    }
    $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
    event.preventDefault();
    });
    }
    });
    })(jQuery);
</script>
</head>
<body>
        <header class="section-header">
        <section class="header-top-light border-bottom">
          <div class="container">
            <nav class="d-flex flex-column flex-md-row">
              <ul class="nav mr-auto d-none d-md-flex">
                <li><a href="#" class="nav-link px-2"> <i class="fab fa-facebook"></i> </a></li>
            <li><a href="#" class="nav-link px-2"> <i class="fab fa-instagram"></i> </a></li>
            <li><a href="#" class="nav-link px-2"> <i class="fab fa-twitter"></i> </a></li>
              </ul>
              <ul class="nav">
              
            <li class="nav-item"><a href="#" class="nav-link" style="color:inherit"> Aide </a></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit"> Devise </a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#">XOF</a></li>
                <li><a class="dropdown-item" href="#">EUR</a></li>
                <li><a class="dropdown-item" href="#">AED</a></li>
                <li><a class="dropdown-item" href="#">RUBL </a></li>
                    </ul>
            </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit">   Langue </a>
                <ul class="dropdown-menu dropdown-menu-right">     
                <li><a class="dropdown-item" href="#">Francais</a></li>
                <li><a class="dropdown-item" href="#">Anglais</a></li>
                <li><a class="dropdown-item" href="#">Arabe</a></li>
                    </ul>
                </li>
              </ul> <!-- navbar-nav.// -->
            </nav>
          </div>
        </section>
        
        <section class="header-main border-bottom">
          <div class="container">
        <div class="row align-items-center">
          <div class="col">
          <div class="brand-wrap">
              <a href="{{url('home')}}"><img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}" alt=""></a>

          </div> <!-- brand-wrap.// -->
          </div>
          <div class="col-8">
            <form action="{{url('resultat')}}" class="search">
              <div class="input-group w-100">
                  <input type="text" id="cherche" name="result" required class="form-control" style="width:55%;" placeholder="Search">
                  <div class="input-group-append">
                    <button class="btn btn-success" type="submit">
                      <i class="fa fa-search"></i>
                    </button>
                  </div>
                </div>
            </form> <!-- search-wrap .end// -->
          </div> <!-- col.// -->
          <div class="col-2"> 
              <div class="widget-header dropdown">
                  <a href="#" data-toggle="dropdown" data-offset="20,10" aria-expanded="false">
                      <div class="icontext">
                          <div class="col">
                              <small class="text-muted">Name</small><i class="fa fa-caret-down"></i>
                            </div>
                      </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-108px, 42px, 0px);">
                      <form class="px-4 py-3">
                          <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" placeholder="email@example.com">
                          </div>
                          <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password">
                          </div>
                          <button type="submit" class="btn btn-primary">Sign in</button>
                          <button class="mdl-button mdl-js-button mdl-button--raised" id="sign-out-button">Sign-out</button>
                          </form>
                          <hr class="dropdown-divider">
                          <a class="dropdown-item" href="#">Have account? Sign up</a>
                          <a class="dropdown-item" href="#">Forgot password?</a>
                  </div> <!--  dropdown-menu .// -->
              </div>  <!-- widget-header .// -->
         </div>
         <a class=" rounded-circle btn btn-primary">
          <i style="color:#fff;font-size:20px;" class="fa fa-bell"></i>
         </a>

        </div> <!-- row.// -->
          </div> <!-- container.// -->
        </section> <!-- header-main .// -->
        </header> <!-- section-header.// -->
        <br>
        <div class="col" style="background-color:#0e2b3b;padding-top:10px">
            <div class="row">
              <div class="col-1"></div>
              <div class="col-8">
                <p style="color:#fff;font-size:22px;">Télécharger notre application pour beneficier de plus de bonus</p>
              </div>
             <div class="col-3">
                 <a href="#"> <img  src="{{asset('front/dist/images/ios.png')}}" alt="" height="40"></a>
                 <a href="#"> <img  src="{{asset('front/dist/images/android.png')}}" alt="" height="40"> </a>            
             </div>
             
            </div>
           </div>
      <!-- script firebase déconnexion -->
      <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
      <script type="text/javascript">
  // Initialize Firebase
    var config = {
        apiKey: "AIzaSyAc91Vbr6ztfUxT9eDHejOP6QU-wQhStTo",
        authDomain: "adjemin-web.firebaseapp.com",
        databaseURL: "https://adjemin-web.firebaseio.com",
        projectId: "adjemin-web",
        storageBucket: "adjemin-web.appspot.com",
        messagingSenderId: "14203483930",
        appId: "1:14203483930:web:9dd798068dfe8c57b45ea3",
        measurementId: "G-DW262R5WYE"
    };
    firebase.initializeApp(config);

    var database = firebase.database();
  /**
   * Set up UI event listeners and registering Firebase auth listeners.
   */
  window.onload = function() {
    document.getElementById('sign-out-button').addEventListener('click', onSignOutClick);
    function onSignOutClick() {
    firebase.auth().signOut();
    window.location.href="{{url('/')}}";
  }
}
</script>
    <br>
<div class="col">
   <div class="row">
     <div class="col-1"></div>
      <div class="col-sm-6 col-s-6">
          <div class="card">
            <div class="card-body">
                <div class="container d-flex justify-content-center">
                    <section id="default" class="padding-top0">
                        <div class="row">
                            <div class="large-5 column">
                                <div class="xzoom-container"> 
                                  <div class="slider-banner-owl owl-carousel owl-theme">
                                      <div id="carousel1_indicator" class="carousel" style="margin-top:10px;">
                                          <!--<ol class="carousel-indicators">
                                            <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>
                                            <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="1"></li>
                                            <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="2"></li>
                                            <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="3"></li>

                                          </ol>-->
                                          <div class="carousel-inner" style="border:1px solid #e9e9e9;">
                                            @foreach($produit as $produits)
                                            <div class="carousel-item active">
                                                <img alt="First slide" class="xzoom" src="{{$produits->cover_url}}"/>
                                            </div>
                                          </div>
                                          <!--<a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
                                            <span id="slide-indicatorLeft" class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                          </a>
                                          <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
                                            <span id="slide-indicatorRight" class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                          </a>-->
                                        </div>
                                    </div>
                                  @endforeach
                                  <div class="row">
                                  @foreach($produitmedia as $produitmedias)
                                    <div class="col-md-2 col-s-2">
                                  <div class="xzoom-thumbs"><br>

                                      <a href="{{$produitmedias->url}}">
                                      <img class="xzoom-gallery" width="80" src="{{$produitmedias->url}}"></a>
                                      </div>
                                    </div>
                                  @endforeach
                                  </div>
                                </div>
                                <h5><i>Détails Produit</i></h5>
                                  @foreach($produit as $produits)
                                  <p>
                                    {{$produits->description}}
                                  </p>
                                  @endforeach
                            </div>
                        </div>
                    </section>
                </div>
            </div>
          </div>
        </div>
        <div class="col-sm-5 col-s-6">
          <div class="card">
            <div class="card-body">
              @foreach($produit as $produits)
                <article class="content-body" style="margin:15px;">
                    <h4 class="title"> <strong>{{$produits->title}}</strong>  <i style="font-size:2x;" class="far fa-heart"></i> </h4>
                    <hr>
                    <!-- rating-wrap.// -->
                    <dl class="row">
                      <div class="col-sm-6">
                        <button style="" type="button" class="btn btn-success btn-lg">
                          <strong>
                          {{$produits->price}} CFA
                        </strong>
                        </button>
            
                      </div>
                      <dd class="col-sm-6">
                        <i style="margin-right: 15px;" class="fa fa-calendar"></i>
                        Date de publicaton: {{$produits->created_at->format('d M Y')}}
                      </dd>
                      
                    </dl>
                    <hr>
                      <div class="col-8 col-s-8">
                            <div class="form-group col-md flex-grow-0">
                                <label>Quantity</label>
                                <div class="row input-group mb-3 input-spinner">
                                      <div class="input-group-prepend">
                                          <button onclick="incrementValue()" value="Increment Value" class="btn btn-primary" type="button"> + </button>
                                        </div>
                                        <input style="text-align:center" class="form-control" min="1" type="text" id="number" value="1"/>
                                        <div class="input-group-append">
                                          <button onclick="decrementValue()" value="Decrement Value"  class="btn btn-success" type="button" id="button-minus"> − </button>
                                    </div>
                                </div>

                                <p><br>
                                  @foreach($customer as $customers)
                                    <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                                    <b class="leftMargin">{{$customers->name}}</b>
                                    @endforeach
                                </p>
                                <p>
                                   <i class="fa fa-map-marker-alt"></i> Géolocalisation
                                   {{$produits->location_name}}
                                  </p> 
                              </div>
                      </div> <!-- row.// -->
                      <div class="col">
                          <button style="float:left" class="btn btn-secondary adjeminRadius">NEGOCIER</button>
                          <a style="float:right" href="{{url('payment')}}" class="btn btn-success adjeminRadius" >COMMANDER</a>

                      </div>
                    </article> <!-- product-info-aside .// -->
                    @endforeach
            </div>
          </div>
        </div>
   </div>
  </div>
    <script type="text/javascript">
     function incrementValue()
{   
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;
}
function decrementValue()
{
    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value--;
    document.getElementById('number').value = value;
}
      </script>
      <script type="text/javascript" src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1565190285/Scripts/xzoom.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1565190284/Scripts/xzoom.css" media="all" />
<footer class="section-footer border-top">
  <div class="col" style="background-color:#0e2b3b;color:#ffff;padding-top:15px;padding-bottom:15px;">
    <section class="footer-top padding-y">
      <div class="row">
        <aside class="col-md-4">
          <article class="mr-3">
                        <img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}">
            <p class="mt-3">
              Adjemin est une marketplace pour vendre, acheter, discuter gratuitement depuis votre mobile grâce à l'application adjemin disponible sur Google Play Store.
              içi, tout le monde est vendeur et acheteur grâce à son téléphone portable. 
            </p>
            <div>
                <a class="btn btn-icon btn-light" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-icon btn-light" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                <a class="btn btn-icon btn-light" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube"></i></a>
                <a class="btn btn-icon btn-light" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
            </div>
          </article>
        </aside>
        <aside class="col-sm-3 col-md-2">
          <h6 class="title" style="text-decoration:underline">A propos</h6>
        
        </aside>
        <aside class="col-sm-3 col-md-2">
          <h6 class="title" style="text-decoration:underline">Catégorie de produits</h6>
          <ul class="list-unstyled">
              <li> <a href="#" style="color:honeydew">Agriculture</a></li>
              <li> <a href="#" style="color:honeydew">Antiquités</a></li>
              <li> <a href="#" style="color:honeydew">Art et Bricolage</a></li>
              <li> <a href="#" style="color:honeydew">Accessoires de téléphone</a></li>
              <li> <a href="#" style="color:honeydew">Bateau et marine</a></li>
              <li> <a href="#" style="color:honeydew">Beauté et Santé</a></li>
              <li> <a href="#" style="color:honeydew">Bébé</a></li>
              <li> <a href="#" style="color:honeydew">Bijoux et Accessoires</a></li>
              <li> <a href="#" style="color:honeydew">Boisson</a></li>





            </ul>
        </aside>
        <aside class="col-sm-3  col-md-2">
          <h6 class="title" style="text-decoration:underline">Partenaire officiel</h6>
          <img src="{{asset('front/dist/images/mtn.jpg')}}" height="60">
        </aside>
        <aside class="col-sm-2  col-md-2">
          <h6 class="title" style="text-decoration:underline">Notre application mobile</h6>
          <a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/ios.png')}}" height="40"></a>
          <a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/android.png')}}" height="40"></a>
        </aside>
            </div> <!-- row.// -->
        </section>  <!-- footer-top.// -->  
    </div><!-- //container -->
    <div class="col" style="background-color:#0a4c71f5">
      <div class="col" style="color:#fff;flaot:right;text-align:center">
    <div>
      <a href="https://adjemin.com"></a>
      <i style="color:#fff" class="fa fa-lock"></i> Adjemin
      <span>&copy; 2019</span>
    </div>
      </div>

</div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <!-- recherche -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
    <script type="text/javascript">
  $(document).ready( function() {
    //$.noConflict()
    $( "#cherche" ).autocomplete({
      source: function(request,reponse){
        $.ajax({
            url: "{{url('recherche')}}",
            data: { term : request.term},
            dataType: "json",
            success: function(data){
                var resp = $.map(data, function(obj){
                    return obj.title;
                    });

                reponse(resp);
            }

        });
      },
      minLength:1
    });
  } );
  </script>
</body>
</html>

 