<div class="table-responsive-sm">
    <table class="table table-striped" id="deliveryAddresses-table">
        <thead>
            <th>Address Name</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Place</th>
        <th>Town</th>
        <th>Address Typed</th>
        <th>Address Type</th>
        <th>Customer Id</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($deliveryAddresses as $deliveryAddress)
            <tr>
                <td>{!! $deliveryAddress->address_name !!}</td>
            <td>{!! $deliveryAddress->latitude !!}</td>
            <td>{!! $deliveryAddress->longitude !!}</td>
            <td>{!! $deliveryAddress->place !!}</td>
            <td>{!! $deliveryAddress->town !!}</td>
            <td>{!! $deliveryAddress->address_typed !!}</td>
            <td>{!! $deliveryAddress->address_type !!}</td>
            <td>{!! $deliveryAddress->customer_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['deliveryAddresses.destroy', $deliveryAddress->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('deliveryAddresses.show', [$deliveryAddress->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('deliveryAddresses.edit', [$deliveryAddress->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>