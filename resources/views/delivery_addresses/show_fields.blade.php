<!-- Address Name Field -->
<div class="form-group">
    {!! Form::label('address_name', 'Address Name:') !!}
    <p>{!! $deliveryAddress->address_name !!}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{!! $deliveryAddress->latitude !!}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{!! $deliveryAddress->longitude !!}</p>
</div>

<!-- Place Field -->
<div class="form-group">
    {!! Form::label('place', 'Place:') !!}
    <p>{!! $deliveryAddress->place !!}</p>
</div>

<!-- Town Field -->
<div class="form-group">
    {!! Form::label('town', 'Town:') !!}
    <p>{!! $deliveryAddress->town !!}</p>
</div>

<!-- Address Typed Field -->
<div class="form-group">
    {!! Form::label('address_typed', 'Address Typed:') !!}
    <p>{!! $deliveryAddress->address_typed !!}</p>
</div>

<!-- Address Type Field -->
<div class="form-group">
    {!! Form::label('address_type', 'Address Type:') !!}
    <p>{!! $deliveryAddress->address_type !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $deliveryAddress->customer_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $deliveryAddress->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $deliveryAddress->updated_at !!}</p>
</div>

