<div class="table-responsive-sm">
    <table class="table table-striped" id="walletTokens-table">
        <thead>
            <th>Customer Id</th>
        <th>Amount</th>
        <th>Is Actived</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($walletTokens as $walletToken)
            <tr>
                <td>{!! $walletToken->customer_id !!}</td>
            <td>{!! $walletToken->amount !!}</td>
            <td>{!! $walletToken->is_actived !!}</td>
                <td>
                    {!! Form::open(['route' => ['walletTokens.destroy', $walletToken->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('walletTokens.show', [$walletToken->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('walletTokens.edit', [$walletToken->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>