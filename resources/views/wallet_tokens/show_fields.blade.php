<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $walletToken->customer_id !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $walletToken->amount !!}</p>
</div>

<!-- Is Actived Field -->
<div class="form-group">
    {!! Form::label('is_actived', 'Is Actived:') !!}
    <p>{!! $walletToken->is_actived !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $walletToken->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $walletToken->updated_at !!}</p>
</div>

