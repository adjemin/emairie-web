<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Actived Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_actived', 'Is Actived:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_actived', 0) !!}
        {!! Form::checkbox('is_actived', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('walletTokens.index') !!}" class="btn btn-default">Cancel</a>
</div>
