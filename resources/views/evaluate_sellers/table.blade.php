<div class="table-responsive-sm">
    <table class="table table-striped" id="evaluateSellers-table">
        <thead>
            <th>Customer Id</th>
        <th>Note</th>
        <th>Note Description</th>
        <th>Note By</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($evaluateSellers as $evaluateSeller)
            <tr>
                <td>{!! $evaluateSeller->customer_id !!}</td>
            <td>{!! $evaluateSeller->note !!}</td>
            <td>{!! $evaluateSeller->note_description !!}</td>
            <td>{!! $evaluateSeller->note_by !!}</td>
                <td>
                    {!! Form::open(['route' => ['evaluateSellers.destroy', $evaluateSeller->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('evaluateSellers.show', [$evaluateSeller->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('evaluateSellers.edit', [$evaluateSeller->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>