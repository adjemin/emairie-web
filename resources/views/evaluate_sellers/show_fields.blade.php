<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $evaluateSeller->customer_id !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $evaluateSeller->note !!}</p>
</div>

<!-- Note Description Field -->
<div class="form-group">
    {!! Form::label('note_description', 'Note Description:') !!}
    <p>{!! $evaluateSeller->note_description !!}</p>
</div>

<!-- Note By Field -->
<div class="form-group">
    {!! Form::label('note_by', 'Note By:') !!}
    <p>{!! $evaluateSeller->note_by !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $evaluateSeller->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $evaluateSeller->updated_at !!}</p>
</div>

