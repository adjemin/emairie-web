<!DOCTYPE html>
<html>
<head>
  <title>Adjemin</title>
  <link rel="stylesheet" href="{{asset('front/dist/productDetails.css')}}">

<link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">

<link rel="stylesheet" href="{{asset('front/dist/productDetails.css')}}">
<link rel="stylesheet" href="{{asset('front/dist/style.css')}}">
  <link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js'></script>
<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
</head>
<body>
        <header class="section-header">
        <section class="header-top-light border-bottom">
          <div class="container">
            <nav class="d-flex flex-column flex-md-row">
              <ul class="nav mr-auto d-none d-md-flex">
                <li><a href="#" class="nav-link px-2"> <i class="fab fa-facebook"></i> </a></li>
            <li><a href="#" class="nav-link px-2"> <i class="fab fa-instagram"></i> </a></li>
            <li><a href="#" class="nav-link px-2"> <i class="fab fa-twitter"></i> </a></li>
              </ul>
              <ul class="nav">
              
            <li class="nav-item"><a href="#" class="nav-link" style="color:inherit"> Aide </a></li>
            <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit"> Devise </a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#">XOF</a></li>
                <li><a class="dropdown-item" href="#">EUR</a></li>
                <li><a class="dropdown-item" href="#">AED</a></li>
                <li><a class="dropdown-item" href="#">RUBL </a></li>
                    </ul>
            </li>
                <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit">   Langue </a>
                <ul class="dropdown-menu dropdown-menu-right">     
                <li><a class="dropdown-item" href="#">Francais</a></li>
                <li><a class="dropdown-item" href="#">Anglais</a></li>
                <li><a class="dropdown-item" href="#">Arabe</a></li>
                    </ul>
                </li>
              </ul> <!-- navbar-nav.// -->
            </nav>
          </div>
        </section>
        
        <section class="header-main border-bottom">
          <div class="container">
        <div class="row align-items-center">
          <div class="col">
          <div class="brand-wrap">
              <a href="{{url('home')}}"><img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}" alt=""></a>

          </div> <!-- brand-wrap.// -->
          </div>
          <div class="col-8">
            <form action="{{url('resultat')}}" class="search">
              <div class="input-group w-100">
                  <input type="text" id="cherche" name="result" required class="form-control" style="width:55%;" placeholder="Search">
                  <div class="input-group-append">
                    <button class="btn btn-success" type="submit">
                      <i class="fa fa-search"></i>
                    </button>
                  </div>
                </div>
            </form> <!-- search-wrap .end// -->
          </div> <!-- col.// -->
          <div class="col-2"> 
              <div class="widget-header dropdown">
                  <a href="#" data-toggle="dropdown" data-offset="20,10" aria-expanded="false">
                      <div class="icontext">
                          <div class="col">
                              <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                              <small class="text-muted">Name</small><i class="fa fa-caret-down"></i>
                            </div>
                      </div>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-108px, 42px, 0px);">
                      <form class="px-4 py-3">
                          <div class="form-group">
                            <label>Email address</label>
                            <input type="email" class="form-control" placeholder="email@example.com">
                          </div>
                          <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password">
                          </div>
                          <button type="submit" class="btn btn-primary">Sign in</button>
                          <button class="mdl-button mdl-js-button mdl-button--raised" id="sign-out-button">Sign-out</button>
                          </form>
                          <hr class="dropdown-divider">
                          <a class="dropdown-item" href="#">Have account? Sign up</a>
                          <a class="dropdown-item" href="#">Forgot password?</a>
                  </div> <!--  dropdown-menu .// -->
              </div>  <!-- widget-header .// -->
         </div>
         <a class=" rounded-circle btn btn-primary">
          <i style="color:#fff;font-size:20px;" class="fa fa-bell"></i>
         </a>

        </div> <!-- row.// -->
          </div> <!-- container.// -->
        </section> <!-- header-main .// -->
        </header> <!-- section-header.// -->
        <br>
        <div class="col" style="background-color:#0e2b3b;padding-top:10px">
            <div class="row">
              <div class="col-1"></div>
              <div class="col-8">
                <p style="color:#fff;font-size:22px;">Télécharger notre application pour beneficier de plus de bonus</p>
              </div>
             <div class="col-3">
                 <a href="#"> <img  src="{{asset('front/dist/images/ios.png')}}" alt="" height="40"></a>
                 <a href="#"> <img  src="{{asset('front/dist/images/android.png')}}" alt="" height="40"> </a>            
             </div>
             
            </div>
           </div>
      <!-- script firebase déconnexion -->
      <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
      <script type="text/javascript">
  // Initialize Firebase
    var config = {
        apiKey: "AIzaSyAc91Vbr6ztfUxT9eDHejOP6QU-wQhStTo",
        authDomain: "adjemin-web.firebaseapp.com",
        databaseURL: "https://adjemin-web.firebaseio.com",
        projectId: "adjemin-web",
        storageBucket: "adjemin-web.appspot.com",
        messagingSenderId: "14203483930",
        appId: "1:14203483930:web:9dd798068dfe8c57b45ea3",
        measurementId: "G-DW262R5WYE"
    };
    firebase.initializeApp(config);

    var database = firebase.database();
  /**
   * Set up UI event listeners and registering Firebase auth listeners.
   */
  window.onload = function() {
    document.getElementById('sign-out-button').addEventListener('click', onSignOutClick);
    function onSignOutClick() {
    firebase.auth().signOut();
    window.location.replace="('/')";
  }
}
</script>
    <br>
    @yield('content')
 <footer class="section-footer border-top">
  <div class="col" style="background-color:#0e2b3b;color:#ffff;padding-top:15px;padding-bottom:15px;">
    <section class="footer-top padding-y">
      <div class="row">
        <aside class="col-md-4">
          <article class="mr-3">
                        <img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}">
            <p class="mt-3">
              Adjemin est une marketplace pour vendre, acheter, discuter gratuitement depuis votre mobile grâce à l'application adjemin disponible sur Google Play Store.
              içi, tout le monde est vendeur et acheteur grâce à son téléphone portable. 
            </p>
            <div>
                <a class="btn btn-icon btn-light" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-icon btn-light" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                <a class="btn btn-icon btn-light" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube"></i></a>
                <a class="btn btn-icon btn-light" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
            </div>
          </article>
        </aside>
        <aside class="col-sm-3 col-md-2">
          <h6 class="title" style="text-decoration:underline">A propos</h6>
        
        </aside>
        <aside class="col-sm-3 col-md-2">
          <h6 class="title" style="text-decoration:underline">Catégorie de produits</h6>
          <ul class="list-unstyled">
              <li> <a href="#" style="color:honeydew">Agriculture</a></li>
              <li> <a href="#" style="color:honeydew">Antiquités</a></li>
              <li> <a href="#" style="color:honeydew">Art et Bricolage</a></li>
              <li> <a href="#" style="color:honeydew">Accessoires de téléphone</a></li>
              <li> <a href="#" style="color:honeydew">Bateau et marine</a></li>
              <li> <a href="#" style="color:honeydew">Beauté et Santé</a></li>
              <li> <a href="#" style="color:honeydew">Bébé</a></li>
              <li> <a href="#" style="color:honeydew">Bijoux et Accessoires</a></li>
              <li> <a href="#" style="color:honeydew">Boisson</a></li>





            </ul>
        </aside>
        <aside class="col-sm-3  col-md-2">
          <h6 class="title" style="text-decoration:underline">Partenaire officiel</h6>
          <img src="{{asset('front/dist/images/mtn.jpg')}}" height="60">
        </aside>
        <aside class="col-sm-2  col-md-2">
          <h6 class="title" style="text-decoration:underline">Notre application mobile</h6>
          <a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/ios.png')}}" height="40"></a>
          <a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/android.png')}}" height="40"></a>
        </aside>
            </div> <!-- row.// -->
        </section>  <!-- footer-top.// -->  
    </div><!-- //container -->
    <div class="col" style="background-color:#0a4c71f5">
            <div class="col" style="color:#fff;flaot:right;text-align:center">
          <div>
            <a href="https://adjemin.com">Adjemin</a>
            <span>&copy; 2019 Adjemin.</span>
          </div>
            </div>

      </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- recherche -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
  $(document).ready( function() {
    //$.noConflict()
    $( "#cherche" ).autocomplete({
      source: function(request,reponse){
        $.ajax({
            url: "{{url('recherche')}}",
            data: { term : request.term},
            dataType: "json",
            success: function(data){
                var resp = $.map(data, function(obj){
                    return obj.title;
                    });

                reponse(resp);
            }

        });
      },
      minLength:1
    });
  } );
  </script>
</body>
</html>

 