<div class="table-responsive-sm">
    <table class="table table-striped" id="sponsorings-table">
        <thead>
            <th>Product Id</th>
        <th>Sponsoring Type</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Cost</th>
        <th>Currency Code</th>
        <th>Percentage</th>
        <th>Percentage Fees</th>
        <th>Visilibity</th>
        <th>Visibility Location</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($sponsorings as $sponsoring)
            <tr>
                <td>{!! $sponsoring->product_id !!}</td>
            <td>{!! $sponsoring->sponsoring_type !!}</td>
            <td>{!! $sponsoring->start_date !!}</td>
            <td>{!! $sponsoring->end_date !!}</td>
            <td>{!! $sponsoring->cost !!}</td>
            <td>{!! $sponsoring->currency_code !!}</td>
            <td>{!! $sponsoring->percentage !!}</td>
            <td>{!! $sponsoring->percentage_fees !!}</td>
            <td>{!! $sponsoring->visilibity !!}</td>
            <td>{!! $sponsoring->visibility_location !!}</td>
                <td>
                    {!! Form::open(['route' => ['sponsorings.destroy', $sponsoring->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('sponsorings.show', [$sponsoring->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('sponsorings.edit', [$sponsoring->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>