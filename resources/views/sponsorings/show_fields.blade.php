<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $sponsoring->product_id !!}</p>
</div>

<!-- Sponsoring Type Field -->
<div class="form-group">
    {!! Form::label('sponsoring_type', 'Sponsoring Type:') !!}
    <p>{!! $sponsoring->sponsoring_type !!}</p>
</div>

<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $sponsoring->start_date !!}</p>
</div>

<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $sponsoring->end_date !!}</p>
</div>

<!-- Cost Field -->
<div class="form-group">
    {!! Form::label('cost', 'Cost:') !!}
    <p>{!! $sponsoring->cost !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $sponsoring->currency_code !!}</p>
</div>

<!-- Percentage Field -->
<div class="form-group">
    {!! Form::label('percentage', 'Percentage:') !!}
    <p>{!! $sponsoring->percentage !!}</p>
</div>

<!-- Percentage Fees Field -->
<div class="form-group">
    {!! Form::label('percentage_fees', 'Percentage Fees:') !!}
    <p>{!! $sponsoring->percentage_fees !!}</p>
</div>

<!-- Visilibity Field -->
<div class="form-group">
    {!! Form::label('visilibity', 'Visilibity:') !!}
    <p>{!! $sponsoring->visilibity !!}</p>
</div>

<!-- Visibility Location Field -->
<div class="form-group">
    {!! Form::label('visibility_location', 'Visibility Location:') !!}
    <p>{!! $sponsoring->visibility_location !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sponsoring->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sponsoring->updated_at !!}</p>
</div>

