<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::text('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sponsoring Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sponsoring_type', 'Sponsoring Type:') !!}
    {!! Form::text('sponsoring_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Start Date:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'End Date:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cost', 'Cost:') !!}
    {!! Form::text('cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Percentage Field -->
<div class="form-group col-sm-6">
    {!! Form::label('percentage', 'Percentage:') !!}
    {!! Form::text('percentage', null, ['class' => 'form-control']) !!}
</div>

<!-- Percentage Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('percentage_fees', 'Percentage Fees:') !!}
    {!! Form::text('percentage_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Visilibity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('visilibity', 'Visilibity:') !!}
    {!! Form::text('visilibity', null, ['class' => 'form-control']) !!}
</div>

<!-- Visibility Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('visibility_location', 'Visibility Location:') !!}
    {!! Form::text('visibility_location', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sponsorings.index') !!}" class="btn btn-default">Cancel</a>
</div>
