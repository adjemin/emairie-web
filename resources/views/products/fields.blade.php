<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Metadata Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description_metadata', 'Description Metadata:') !!}
    {!! Form::text('description_metadata', null, ['class' => 'form-control']) !!}
</div>

<!-- Medias Field -->
<div class="form-group col-sm-6">
    {!! Form::label('medias', 'Medias:') !!}
    {!! Form::text('medias', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_id', 'Cover Id:') !!}
    {!! Form::text('cover_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_type', 'Cover Type:') !!}
    {!! Form::text('cover_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_url', 'Cover Url:') !!}
    {!! Form::text('cover_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Thumbnail Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_thumbnail', 'Cover Thumbnail:') !!}
    {!! Form::text('cover_thumbnail', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_duration', 'Cover Duration:') !!}
    {!! Form::text('cover_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_width', 'Cover Width:') !!}
    {!! Form::text('cover_width', null, ['class' => 'form-control']) !!}
</div>

<!-- Cover Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cover_height', 'Cover Height:') !!}
    {!! Form::text('cover_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_name', 'Location Name:') !!}
    {!! Form::text('location_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_address', 'Location Address:') !!}
    {!! Form::text('location_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    {!! Form::text('location_lat', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Lng Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    {!! Form::text('location_lng', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::text('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Meta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_meta', 'Category Meta:') !!}
    {!! Form::text('category_meta', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Sold Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sold', 'Is Sold:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_sold', 0) !!}
        {!! Form::checkbox('is_sold', '1', null) !!}
    </label>
</div>


<!-- Sold At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_at', 'Sold At:') !!}
    {!! Form::text('sold_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Initiale Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('initiale_count', 'Initiale Count:') !!}
    {!! Form::text('initiale_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Firm Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_firm_price', 'Is Firm Price:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_firm_price', 0) !!}
        {!! Form::checkbox('is_firm_price', '1', null) !!}
    </label>
</div>


<!-- Condition Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('condition_id', 'Condition Id:') !!}
    {!! Form::text('condition_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Published At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('published_at', 'Published At:') !!}
    {!! Form::text('published_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Published Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_published', 'Is Published:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_published', 0) !!}
        {!! Form::checkbox('is_published', '1', null) !!}
    </label>
</div>


<!-- Deleted By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    {!! Form::text('deleted_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted Creator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_creator', 'Deleted Creator:') !!}
    {!! Form::text('deleted_creator', null, ['class' => 'form-control']) !!}
</div>

<!-- Sold Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sold_count', 'Sold Count:') !!}
    {!! Form::text('sold_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>
