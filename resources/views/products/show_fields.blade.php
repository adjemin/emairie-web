<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $product->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $product->description !!}</p>
</div>

<!-- Description Metadata Field -->
<div class="form-group">
    {!! Form::label('description_metadata', 'Description Metadata:') !!}
    <p>{!! $product->description_metadata !!}</p>
</div>

<!-- Medias Field -->
<div class="form-group">
    {!! Form::label('medias', 'Medias:') !!}
    <p>{!! $product->medias !!}</p>
</div>

<!-- Cover Id Field -->
<div class="form-group">
    {!! Form::label('cover_id', 'Cover Id:') !!}
    <p>{!! $product->cover_id !!}</p>
</div>

<!-- Cover Type Field -->
<div class="form-group">
    {!! Form::label('cover_type', 'Cover Type:') !!}
    <p>{!! $product->cover_type !!}</p>
</div>

<!-- Cover Url Field -->
<div class="form-group">
    {!! Form::label('cover_url', 'Cover Url:') !!}
    <p>{!! $product->cover_url !!}</p>
</div>

<!-- Cover Thumbnail Field -->
<div class="form-group">
    {!! Form::label('cover_thumbnail', 'Cover Thumbnail:') !!}
    <p>{!! $product->cover_thumbnail !!}</p>
</div>

<!-- Cover Duration Field -->
<div class="form-group">
    {!! Form::label('cover_duration', 'Cover Duration:') !!}
    <p>{!! $product->cover_duration !!}</p>
</div>

<!-- Cover Width Field -->
<div class="form-group">
    {!! Form::label('cover_width', 'Cover Width:') !!}
    <p>{!! $product->cover_width !!}</p>
</div>

<!-- Cover Height Field -->
<div class="form-group">
    {!! Form::label('cover_height', 'Cover Height:') !!}
    <p>{!! $product->cover_height !!}</p>
</div>

<!-- Location Name Field -->
<div class="form-group">
    {!! Form::label('location_name', 'Location Name:') !!}
    <p>{!! $product->location_name !!}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{!! $product->location_address !!}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{!! $product->location_lat !!}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{!! $product->location_lng !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $product->category_id !!}</p>
</div>

<!-- Category Meta Field -->
<div class="form-group">
    {!! Form::label('category_meta', 'Category Meta:') !!}
    <p>{!! $product->category_meta !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $product->customer_id !!}</p>
</div>

<!-- Is Sold Field -->
<div class="form-group">
    {!! Form::label('is_sold', 'Is Sold:') !!}
    <p>{!! $product->is_sold !!}</p>
</div>

<!-- Sold At Field -->
<div class="form-group">
    {!! Form::label('sold_at', 'Sold At:') !!}
    <p>{!! $product->sold_at !!}</p>
</div>

<!-- Initiale Count Field -->
<div class="form-group">
    {!! Form::label('initiale_count', 'Initiale Count:') !!}
    <p>{!! $product->initiale_count !!}</p>
</div>

<!-- Is Firm Price Field -->
<div class="form-group">
    {!! Form::label('is_firm_price', 'Is Firm Price:') !!}
    <p>{!! $product->is_firm_price !!}</p>
</div>

<!-- Condition Id Field -->
<div class="form-group">
    {!! Form::label('condition_id', 'Condition Id:') !!}
    <p>{!! $product->condition_id !!}</p>
</div>

<!-- Published At Field -->
<div class="form-group">
    {!! Form::label('published_at', 'Published At:') !!}
    <p>{!! $product->published_at !!}</p>
</div>

<!-- Is Published Field -->
<div class="form-group">
    {!! Form::label('is_published', 'Is Published:') !!}
    <p>{!! $product->is_published !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    <p>{!! $product->deleted_by !!}</p>
</div>

<!-- Deleted Creator Field -->
<div class="form-group">
    {!! Form::label('deleted_creator', 'Deleted Creator:') !!}
    <p>{!! $product->deleted_creator !!}</p>
</div>

<!-- Sold Count Field -->
<div class="form-group">
    {!! Form::label('sold_count', 'Sold Count:') !!}
    <p>{!! $product->sold_count !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $product->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $product->updated_at !!}</p>
</div>

