<div class="table-responsive-sm">
    <table class="table table-striped" id="products-table">
        <thead>
            <th>Title</th>
        <th>Description</th>
        <th>Description Metadata</th>
        <th>Medias</th>
        <th>Cover Id</th>
        <th>Cover Type</th>
        <th>Cover Url</th>
        <th>Cover Thumbnail</th>
        <th>Cover Duration</th>
        <th>Cover Width</th>
        <th>Cover Height</th>
        <th>Location Name</th>
        <th>Location Address</th>
        <th>Location Lat</th>
        <th>Location Lng</th>
        <th>Category Id</th>
        <th>Category Meta</th>
        <th>Customer Id</th>
        <th>Is Sold</th>
        <th>Sold At</th>
        <th>Initiale Count</th>
        <th>Is Firm Price</th>
        <th>Condition Id</th>
        <th>Published At</th>
        <th>Is Published</th>
        <th>Deleted By</th>
        <th>Deleted Creator</th>
        <th>Sold Count</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{!! $product->title !!}</td>
            <td>{!! $product->description !!}</td>
            <td>{!! $product->description_metadata !!}</td>
            <td>{!! $product->medias !!}</td>
            <td>{!! $product->cover_id !!}</td>
            <td>{!! $product->cover_type !!}</td>
            <td>{!! $product->cover_url !!}</td>
            <td>{!! $product->cover_thumbnail !!}</td>
            <td>{!! $product->cover_duration !!}</td>
            <td>{!! $product->cover_width !!}</td>
            <td>{!! $product->cover_height !!}</td>
            <td>{!! $product->location_name !!}</td>
            <td>{!! $product->location_address !!}</td>
            <td>{!! $product->location_lat !!}</td>
            <td>{!! $product->location_lng !!}</td>
            <td>{!! $product->category_id !!}</td>
            <td>{!! $product->category_meta !!}</td>
            <td>{!! $product->customer_id !!}</td>
            <td>{!! $product->is_sold !!}</td>
            <td>{!! $product->sold_at !!}</td>
            <td>{!! $product->initiale_count !!}</td>
            <td>{!! $product->is_firm_price !!}</td>
            <td>{!! $product->condition_id !!}</td>
            <td>{!! $product->published_at !!}</td>
            <td>{!! $product->is_published !!}</td>
            <td>{!! $product->deleted_by !!}</td>
            <td>{!! $product->deleted_creator !!}</td>
            <td>{!! $product->sold_count !!}</td>
                <td>
                    {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('products.show', [$product->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('products.edit', [$product->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>