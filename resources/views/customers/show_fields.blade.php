<!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'First Name:') !!}
    <p>{!! $customer->first_name !!}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Last Name:') !!}
    <p>{!! $customer->last_name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $customer->name !!}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{!! $customer->phone_number !!}</p>
</div>

<!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{!! $customer->dial_code !!}</p>
</div>

<!-- Country Code Field -->
<div class="form-group">
    {!! Form::label('country_code', 'Country Code:') !!}
    <p>{!! $customer->country_code !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $customer->email !!}</p>
</div>

<!-- Photo Id Field -->
<div class="form-group">
    {!! Form::label('photo_id', 'Photo Id:') !!}
    <p>{!! $customer->photo_id !!}</p>
</div>

<!-- Photo Url Field -->
<div class="form-group">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    <p>{!! $customer->photo_url !!}</p>
</div>

<!-- Photo Mime Field -->
<div class="form-group">
    {!! Form::label('photo_mime', 'Photo Mime:') !!}
    <p>{!! $customer->photo_mime !!}</p>
</div>

<!-- Photo Height Field -->
<div class="form-group">
    {!! Form::label('photo_height', 'Photo Height:') !!}
    <p>{!! $customer->photo_height !!}</p>
</div>

<!-- Photo Width Field -->
<div class="form-group">
    {!! Form::label('photo_width', 'Photo Width:') !!}
    <p>{!! $customer->photo_width !!}</p>
</div>

<!-- Photo Length Field -->
<div class="form-group">
    {!! Form::label('photo_length', 'Photo Length:') !!}
    <p>{!! $customer->photo_length !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $customer->gender !!}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{!! $customer->bio !!}</p>
</div>

<!-- Birthday Field -->
<div class="form-group">
    {!! Form::label('birthday', 'Birthday:') !!}
    <p>{!! $customer->birthday !!}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{!! $customer->language !!}</p>
</div>

<!-- Facebook Id Field -->
<div class="form-group">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    <p>{!! $customer->facebook_id !!}</p>
</div>

<!-- Twitter Id Field -->
<div class="form-group">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    <p>{!! $customer->twitter_id !!}</p>
</div>

<!-- Is Pro Seller Field -->
<div class="form-group">
    {!! Form::label('is_pro_seller', 'Is Pro Seller:') !!}
    <p>{!! $customer->is_pro_seller !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customer->updated_at !!}</p>
</div>

