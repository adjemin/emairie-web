<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Dial Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    {!! Form::text('dial_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_code', 'Country Code:') !!}
    {!! Form::text('country_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_id', 'Photo Id:') !!}
    {!! Form::text('photo_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_url', 'Photo Url:') !!}
    {!! Form::text('photo_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Mime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_mime', 'Photo Mime:') !!}
    {!! Form::text('photo_mime', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_height', 'Photo Height:') !!}
    {!! Form::text('photo_height', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_width', 'Photo Width:') !!}
    {!! Form::text('photo_width', null, ['class' => 'form-control']) !!}
</div>

<!-- Photo Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_length', 'Photo Length:') !!}
    {!! Form::text('photo_length', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::text('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Facebook Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook_id', 'Facebook Id:') !!}
    {!! Form::text('facebook_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Twitter Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter_id', 'Twitter Id:') !!}
    {!! Form::text('twitter_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Pro Seller Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_pro_seller', 'Is Pro Seller:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_pro_seller', 0) !!}
        {!! Form::checkbox('is_pro_seller', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
</div>
