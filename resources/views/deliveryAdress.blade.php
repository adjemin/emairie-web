@extends('layoutFront.app_chield')
@section('detailProduit')
<div class="container">
  <div class="row">
      <div class="col-sm-12" style="font-family: montserrat;margin-top:15px;margin-bottom:15px;">
        <div class="card">
            <h5 class="card-header text-left" style="border:1px solid #f3f3f3;background-color:#fff;">
             <strong> Choisissez une adresse</strong>
            </h5>
          <form method="get" action="{{url('choix')}}" role="form">
            <div class="card-body">
              <!--Premiere adresse de livraison Maison-->
              <div class="card" id="maison" style="padding-top:10px;margin-bottom:15px;border-radius:0px;border-left:none;border-right:none">
                <div class="card-body" style="">
                  <div class="row">
                    <div class="col-sm-1">
                      <label style="display: none;" id="radmaison" class="custom-control custom-radio mb-3">
                        <input class="custom-control-input" value="house" name="myselection1" onclick="enlever();" type="radio">
                        <div class="custom-control-label"> &nbsp;</div>
                      </label>
                    </div>
                    <div class="col-sm-10">
                      <i class="fa fa-home"></i> &nbsp; Ajouter votre maison comme adresse de livraison : 
                      @foreach($address as $addresses)
                        @if($addresses->address_type == 'house')
                          {{$addresses->address_name}}
                    </div>
                    <div class="col-sm-1"> 
                      <a style="background: none; border: none;float:right !important" href="{{url('supprimeraddrese',['lieu'=>$addresses->address_name])}}" class="btn">
                        <i class="fa fa-minus-circle"></i>
                      </a> &nbsp;
                        @endif
                      @endforeach
                      <a style="background: none; border: none;float:right !important" class="btn" href="{{url('lieu',['type'=>'house'])}}" id="addhouse">
                        <i class="fa fa-plus-circle"></i>
                      </a> &nbsp;
                    </div>
                  </div>
                </div>
              </div>
                <!--Deuxième adresse de livraison Bureau-->
                <div class="card" id="bureau" style="margin-bottom:15px;border-radius:0px;border-left:none;border-right:none">
                  <div class="card-body" style="">
                    <div class="row">
                      <div class="col-sm-1">
                        <label class="custom-control custom-radio mb-3" style="display: none;" id="radbureau">
                          <input class="custom-control-input" onclick="enleverbureau();" value="office" name="myselection1" type="radio">
                          <div class="custom-control-label"> &nbsp;</div>
                        </label>
                      </div>
                      <div class="col-sm-10">
                        <i class="fas fa-chair"></i> &nbsp; Ajouter votre bureau comme adresse de livraison : 
                        @foreach($address as $addresses)
                         @if($addresses->address_type == 'office')
                        
                          {{$addresses->address_name}}
                       
                      </div>
                      <div class="col-sm-1">
                      <a style="background: none; border: none;float:right !important" href="{{url('supprimeraddrese',['lieu'=>$addresses->address_name])}}" class="btn">
                        <i class="fa fa-minus-circle"></i>
                      </a> &nbsp;
                      @endif
                        @endforeach
                      <a style="background: none; border: none;float:right !important" href="{{url('lieu',['type'=>'office'])}}" class="btn" id="addbureau">
                        <i class="fa fa-plus-circle"></i>
                      </a> &nbsp;
                    </div>              
                  </div>
                </div>
                </div>
                  <!--Troisième adresse de livraison Autre-->
              <div class="card" id="autre" style="display: block;margin-bottom:15px;border-radius:0px;border-left:none;border-right:none">
                <div class="card-body" style="">
                  <div class="row">
                    <div class="col-sm-1">
                      <label class="custom-control custom-radio mb-3" style="display: none;" id="radautre">
                        <input class="custom-control-input" onclick="enleverautre();" value="other" name="myselection1" type="radio">
                        <div class="custom-control-label"> &nbsp;</div>
                      </label>
                    </div>
                    <div class="col-sm-10">
                        <i class="fas fa-couch"></i>&nbsp; Definir autre adresse de livraison: 
                        @foreach($address as $addresses)
                          @if($addresses->address_type == 'other')
                        <p style="text-align:left;margin-left:15px;">
                            {{$addresses->address_name}}
                         </p>
                    </div>
                    <div class="col-sm-1">
                      <a style="background: none; border: none;float:right !important" href="{{url('supprimeraddrese',['lieu'=>$addresses->address_name])}}" class="btn">
                        <i class="fa fa-minus-circle"></i>
                      </a> &nbsp;
                      @endif
                        @endforeach
                      <a style="background: none; border: none;float:right !important" class="btn" href="{{url('lieu',['type'=>'other'])}}" id="addautre">
                        <i class="fa fa-plus-circle"></i>
                      </a> &nbsp;
                    </div>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Suivant</button>
          </div>
        </form>
      </div>
  </div>
  </div>
</div>

  <!-- script affichage des elements -->
  @foreach($address as $addresses)
    <!-- house -->
    @if($addresses->address_type == 'house')
      <script type="text/javascript">
        $(window).on('load',function(){
          document.getElementById('addhouse').style.display='none';
          document.getElementById('radmaison').style.display='block';
        });
        function enlever(){
            document.getElementById('maison').style.background='white';
            document.getElementById('bureau').style.background='#bfbebe57';
            document.getElementById('autre').style.background='#bfbebe57';
        }
      </script>
    @endif
    <!-- office -->
    @if($addresses->address_type == 'office')
      <script type="text/javascript">
        $(window).on('load',function(){
          document.getElementById('addbureau').style.display='none';
          document.getElementById('radbureau').style.display='block';
        });
        function enleverbureau(){
          document.getElementById('maison').style.background='#bfbebe57';
          document.getElementById('bureau').style.background='white';
          document.getElementById('autre').style.background='#bfbebe57';
        }
      </script>
    @endif
    <!-- autre -->
    @if($addresses->address_type == 'other')
      <script type="text/javascript">
        $(window).on('load',function(){
          document.getElementById('addautre').style.display='none';
          document.getElementById('radautre').style.display='block';
        });
        function enleverautre(){
          document.getElementById('maison').style.background='#bfbebe57';
          document.getElementById('bureau').style.background='#bfbebe57';
          document.getElementById('autre').style.background='white';
        }
      </script>
    @endif

  @endforeach


  @endsection