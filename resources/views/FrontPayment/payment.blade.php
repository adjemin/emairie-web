<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
            @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
              body {
                width: 100%;

              }
            }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!--sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="sweetalert2.all.min.js"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <link rel="stylesheet" href="sweetalert2.min.css">
    <!-- endsweetalert -->

    <script charset="utf-8" src="https://www.cinetpay.com/cdn/seamless_sdk/latest/cinetpay.prod.min.js" type="text/javascript"></script>
    <script type="text/javascript">

       function onSuccess(){

          Swal.fire({
          title: 'Félicitation',
          text: 'Votre commande à été éffectuée !',
          icon: 'success'
            }).then(function(){
                window.location.href="{{url('commande',['id'=> Crypt::encrypt($custom)])}}";

       });
        }

       function onError(){

           Swal.fire({
          title: 'Erreur',
          text: 'Commande non éffectuée !! ',
          icon: 'error'
            }).then(function(){
                window.location.href="{{url('cancel',['transactionId' =>$transactionId])}}";

       });
        }


    function init() {

    CinetPay.setConfig({
        apikey: document.getElementById('apikey').value,
        site_id: parseInt(document.getElementById('siteId').value),
        notify_url: "{{url('notify')}}"
    });
    //-------------Gestion des evenements
    //error
    CinetPay.on('error', function (e) {
      console.log('error found '+e);
    });
    //ajax
    CinetPay.on('ajaxStart', function () {
       console.log('ajaxStart');
    });
    CinetPay.on('ajaxStop', function () {
       console.log('ajaxStop');
    });
    //Lorsque la signature est généré
    CinetPay.on('signatureCreated', function (token) {
        console.log('Tocken généré: ' + token);

    });
    CinetPay.on('paymentPending', function (e) {


    });
    CinetPay.on('paymentSuccessfull', function (paymentInfo) {

            if(paymentInfo.cpm_result == '00'){
                onSuccess();
            }else{
                onError();
            }


    });

    //Application des méthodes

        CinetPay.setSignatureData({
                    amount: parseInt(document.getElementById('amount').value),
                    trans_id: document.getElementById('trans_id').value,
                    currency: document.getElementById('currency').value,
                    designation: document.getElementById('designation').value,
                    custom: document.getElementById('cpm_custom').value
                });
        CinetPay.getSignature();



   }



    </script>
</head>

<body onload="init()">


    <form id="info_paiement">

        <input type="hidden"  id="apikey" value="18122291995d9845221e7d53.99745292">
        <input type="hidden"  id="siteId" value="182538">
        <!--<input type="hidden"  id="notificationUrl" value="{{url('notify')}}">-->

        <input type="hidden"  id="amount" value="{{$amount}}">

        <input type="hidden" id="currency" value="CFA">

        <input type="hidden" id="trans_id" value="{{$transactionId}}">

        <input type="hidden" id="cpm_custom" value="{{$payment_method}}">

        <input type="hidden" id="designation" value="{{$transaction_designation}}">

    </form>


</body>
</html>