<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $payment->user_id !!}</p>
</div>

<!-- Methode Payment Id Field -->
<div class="form-group">
    {!! Form::label('methode_payment_id', 'Methode Payment Id:') !!}
    <p>{!! $payment->methode_payment_id !!}</p>
</div>

<!-- Payment Id Field -->
<div class="form-group">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    <p>{!! $payment->payment_id !!}</p>
</div>

<!-- Type Payment Field -->
<div class="form-group">
    {!! Form::label('type_payment', 'Type Payment:') !!}
    <p>{!! $payment->type_payment !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $payment->payment_method !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $payment->amount !!}</p>
</div>

<!-- Cpm Phone Prefixe Field -->
<div class="form-group">
    {!! Form::label('cpm_phone_prefixe', 'Cpm Phone Prefixe:') !!}
    <p>{!! $payment->cpm_phone_prefixe !!}</p>
</div>

<!-- Cel Phone Num Field -->
<div class="form-group">
    {!! Form::label('cel_phone_num', 'Cel Phone Num:') !!}
    <p>{!! $payment->cel_phone_num !!}</p>
</div>

<!-- Cpm Ipn Ack Field -->
<div class="form-group">
    {!! Form::label('cpm_ipn_ack', 'Cpm Ipn Ack:') !!}
    <p>{!! $payment->cpm_ipn_ack !!}</p>
</div>

<!-- Cpm Currency Field -->
<div class="form-group">
    {!! Form::label('cpm_currency', 'Cpm Currency:') !!}
    <p>{!! $payment->cpm_currency !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $payment->status !!}</p>
</div>

<!-- Is Transfer Field -->
<div class="form-group">
    {!! Form::label('is_transfer', 'Is Transfer:') !!}
    <p>{!! $payment->is_transfer !!}</p>
</div>

<!-- Cpm Result Field -->
<div class="form-group">
    {!! Form::label('cpm_result', 'Cpm Result:') !!}
    <p>{!! $payment->cpm_result !!}</p>
</div>

<!-- Commission Field -->
<div class="form-group">
    {!! Form::label('commission', 'Commission:') !!}
    <p>{!! $payment->commission !!}</p>
</div>

<!-- Payid Field -->
<div class="form-group">
    {!! Form::label('payid', 'Payid:') !!}
    <p>{!! $payment->payid !!}</p>
</div>

<!-- Buyername Field -->
<div class="form-group">
    {!! Form::label('buyername', 'Buyername:') !!}
    <p>{!! $payment->buyername !!}</p>
</div>

<!-- Transstatus Field -->
<div class="form-group">
    {!! Form::label('transstatus', 'Transstatus:') !!}
    <p>{!! $payment->transstatus !!}</p>
</div>

<!-- Signature Field -->
<div class="form-group">
    {!! Form::label('signature', 'Signature:') !!}
    <p>{!! $payment->signature !!}</p>
</div>

<!-- Cpm Designation Field -->
<div class="form-group">
    {!! Form::label('cpm_designation', 'Cpm Designation:') !!}
    <p>{!! $payment->cpm_designation !!}</p>
</div>

<!-- Cpm Error Message Field -->
<div class="form-group">
    {!! Form::label('cpm_error_message', 'Cpm Error Message:') !!}
    <p>{!! $payment->cpm_error_message !!}</p>
</div>

<!-- Cpm Trans Date Field -->
<div class="form-group">
    {!! Form::label('cpm_trans_date', 'Cpm Trans Date:') !!}
    <p>{!! $payment->cpm_trans_date !!}</p>
</div>

<!-- Cpm Payment Date Field -->
<div class="form-group">
    {!! Form::label('cpm_payment_date', 'Cpm Payment Date:') !!}
    <p>{!! $payment->cpm_payment_date !!}</p>
</div>

<!-- Cpm Payment Time Field -->
<div class="form-group">
    {!! Form::label('cpm_payment_time', 'Cpm Payment Time:') !!}
    <p>{!! $payment->cpm_payment_time !!}</p>
</div>

<!-- Sms Field -->
<div class="form-group">
    {!! Form::label('sms', 'Sms:') !!}
    <p>{!! $payment->sms !!}</p>
</div>

<!-- Is Deleted Field -->
<div class="form-group">
    {!! Form::label('is_deleted', 'Is Deleted:') !!}
    <p>{!! $payment->is_deleted !!}</p>
</div>

<!-- Created By Field -->
<div class="form-group">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{!! $payment->created_by !!}</p>
</div>

<!-- Updated By Field -->
<div class="form-group">
    {!! Form::label('updated_by', 'Updated By:') !!}
    <p>{!! $payment->updated_by !!}</p>
</div>

<!-- Deleted By Field -->
<div class="form-group">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    <p>{!! $payment->deleted_by !!}</p>
</div>

<!-- Source Id Field -->
<div class="form-group">
    {!! Form::label('source_id', 'Source Id:') !!}
    <p>{!! $payment->source_id !!}</p>
</div>

<!-- Source Name Field -->
<div class="form-group">
    {!! Form::label('source_name', 'Source Name:') !!}
    <p>{!! $payment->source_name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $payment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $payment->updated_at !!}</p>
</div>

