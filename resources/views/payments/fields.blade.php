<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Methode Payment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('methode_payment_id', 'Methode Payment Id:') !!}
    {!! Form::text('methode_payment_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_id', 'Payment Id:') !!}
    {!! Form::text('payment_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_payment', 'Type Payment:') !!}
    {!! Form::text('type_payment', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Phone Prefixe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_phone_prefixe', 'Cpm Phone Prefixe:') !!}
    {!! Form::text('cpm_phone_prefixe', null, ['class' => 'form-control']) !!}
</div>

<!-- Cel Phone Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cel_phone_num', 'Cel Phone Num:') !!}
    {!! Form::text('cel_phone_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Ipn Ack Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_ipn_ack', 'Cpm Ipn Ack:') !!}
    {!! Form::text('cpm_ipn_ack', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_currency', 'Cpm Currency:') !!}
    {!! Form::text('cpm_currency', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Transfer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_transfer', 'Is Transfer:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_transfer', 0) !!}
        {!! Form::checkbox('is_transfer', '1', null) !!}
    </label>
</div>


<!-- Cpm Result Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_result', 'Cpm Result:') !!}
    {!! Form::text('cpm_result', null, ['class' => 'form-control']) !!}
</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', 'Commission:') !!}
    {!! Form::text('commission', null, ['class' => 'form-control']) !!}
</div>

<!-- Payid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payid', 'Payid:') !!}
    {!! Form::text('payid', null, ['class' => 'form-control']) !!}
</div>

<!-- Buyername Field -->
<div class="form-group col-sm-6">
    {!! Form::label('buyername', 'Buyername:') !!}
    {!! Form::text('buyername', null, ['class' => 'form-control']) !!}
</div>

<!-- Transstatus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transstatus', 'Transstatus:') !!}
    {!! Form::text('transstatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Signature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('signature', 'Signature:') !!}
    {!! Form::text('signature', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Designation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_designation', 'Cpm Designation:') !!}
    {!! Form::text('cpm_designation', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Error Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_error_message', 'Cpm Error Message:') !!}
    {!! Form::text('cpm_error_message', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Trans Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_trans_date', 'Cpm Trans Date:') !!}
    {!! Form::text('cpm_trans_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Payment Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_payment_date', 'Cpm Payment Date:') !!}
    {!! Form::text('cpm_payment_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Cpm Payment Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cpm_payment_time', 'Cpm Payment Time:') !!}
    {!! Form::text('cpm_payment_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Sms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sms', 'Sms:') !!}
    {!! Form::text('sms', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Deleted Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_deleted', 'Is Deleted:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_deleted', 0) !!}
        {!! Form::checkbox('is_deleted', '1', null) !!}
    </label>
</div>


<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::text('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_by', 'Updated By:') !!}
    {!! Form::text('updated_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_by', 'Deleted By:') !!}
    {!! Form::text('deleted_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_id', 'Source Id:') !!}
    {!! Form::text('source_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_name', 'Source Name:') !!}
    {!! Form::text('source_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('payments.index') !!}" class="btn btn-default">Cancel</a>
</div>
