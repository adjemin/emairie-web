<div class="table-responsive-sm">
    <table class="table table-striped" id="payments-table">
        <thead>
            <th>User Id</th>
        <th>Methode Payment Id</th>
        <th>Payment Id</th>
        <th>Type Payment</th>
        <th>Payment Method</th>
        <th>Amount</th>
        <th>Cpm Phone Prefixe</th>
        <th>Cel Phone Num</th>
        <th>Cpm Ipn Ack</th>
        <th>Cpm Currency</th>
        <th>Status</th>
        <th>Is Transfer</th>
        <th>Cpm Result</th>
        <th>Commission</th>
        <th>Payid</th>
        <th>Buyername</th>
        <th>Transstatus</th>
        <th>Signature</th>
        <th>Cpm Designation</th>
        <th>Cpm Error Message</th>
        <th>Cpm Trans Date</th>
        <th>Cpm Payment Date</th>
        <th>Cpm Payment Time</th>
        <th>Sms</th>
        <th>Is Deleted</th>
        <th>Created By</th>
        <th>Updated By</th>
        <th>Deleted By</th>
        <th>Source Id</th>
        <th>Source Name</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($payments as $payment)
            <tr>
                <td>{!! $payment->user_id !!}</td>
            <td>{!! $payment->methode_payment_id !!}</td>
            <td>{!! $payment->payment_id !!}</td>
            <td>{!! $payment->type_payment !!}</td>
            <td>{!! $payment->payment_method !!}</td>
            <td>{!! $payment->amount !!}</td>
            <td>{!! $payment->cpm_phone_prefixe !!}</td>
            <td>{!! $payment->cel_phone_num !!}</td>
            <td>{!! $payment->cpm_ipn_ack !!}</td>
            <td>{!! $payment->cpm_currency !!}</td>
            <td>{!! $payment->status !!}</td>
            <td>{!! $payment->is_transfer !!}</td>
            <td>{!! $payment->cpm_result !!}</td>
            <td>{!! $payment->commission !!}</td>
            <td>{!! $payment->payid !!}</td>
            <td>{!! $payment->buyername !!}</td>
            <td>{!! $payment->transstatus !!}</td>
            <td>{!! $payment->signature !!}</td>
            <td>{!! $payment->cpm_designation !!}</td>
            <td>{!! $payment->cpm_error_message !!}</td>
            <td>{!! $payment->cpm_trans_date !!}</td>
            <td>{!! $payment->cpm_payment_date !!}</td>
            <td>{!! $payment->cpm_payment_time !!}</td>
            <td>{!! $payment->sms !!}</td>
            <td>{!! $payment->is_deleted !!}</td>
            <td>{!! $payment->created_by !!}</td>
            <td>{!! $payment->updated_by !!}</td>
            <td>{!! $payment->deleted_by !!}</td>
            <td>{!! $payment->source_id !!}</td>
            <td>{!! $payment->source_name !!}</td>
                <td>
                    {!! Form::open(['route' => ['payments.destroy', $payment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('payments.show', [$payment->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('payments.edit', [$payment->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>