<div class="table-responsive-sm">
    <table class="table table-striped" id="customerIssues-table">
        <thead>
            <th>Customer Id</th>
        <th>Created By</th>
        <th>Description</th>
        <th>Issue Type</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($customerIssues as $customerIssue)
            <tr>
                <td>{!! $customerIssue->customer_id !!}</td>
            <td>{!! $customerIssue->created_by !!}</td>
            <td>{!! $customerIssue->description !!}</td>
            <td>{!! $customerIssue->issue_type !!}</td>
                <td>
                    {!! Form::open(['route' => ['customerIssues.destroy', $customerIssue->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('customerIssues.show', [$customerIssue->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('customerIssues.edit', [$customerIssue->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>