<!-- Speakers Field -->
<div class="form-group">
    {!! Form::label('speakers', 'Speakers:') !!}
    <p>{!! $conversation->speakers !!}</p>
</div>

<!-- Is Group Field -->
<div class="form-group">
    {!! Form::label('is_group', 'Is Group:') !!}
    <p>{!! $conversation->is_group !!}</p>
</div>

<!-- Group Name Field -->
<div class="form-group">
    {!! Form::label('group_name', 'Group Name:') !!}
    <p>{!! $conversation->group_name !!}</p>
</div>

<!-- Admins Field -->
<div class="form-group">
    {!! Form::label('admins', 'Admins:') !!}
    <p>{!! $conversation->admins !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $conversation->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $conversation->updated_at !!}</p>
</div>

