<div class="table-responsive-sm">
    <table class="table table-striped" id="conversations-table">
        <thead>
            <th>Speakers</th>
        <th>Is Group</th>
        <th>Group Name</th>
        <th>Admins</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($conversations as $conversation)
            <tr>
                <td>{!! $conversation->speakers !!}</td>
            <td>{!! $conversation->is_group !!}</td>
            <td>{!! $conversation->group_name !!}</td>
            <td>{!! $conversation->admins !!}</td>
                <td>
                    {!! Form::open(['route' => ['conversations.destroy', $conversation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('conversations.show', [$conversation->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('conversations.edit', [$conversation->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>