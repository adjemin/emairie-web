<!-- Speakers Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speakers', 'Speakers:') !!}
    {!! Form::text('speakers', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Group Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_group', 'Is Group:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_group', 0) !!}
        {!! Form::checkbox('is_group', '1', null) !!}
    </label>
</div>


<!-- Group Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('group_name', 'Group Name:') !!}
    {!! Form::text('group_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Admins Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admins', 'Admins:') !!}
    {!! Form::text('admins', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('conversations.index') !!}" class="btn btn-default">Cancel</a>
</div>
