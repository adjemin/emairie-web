<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $customerDevice->customer_id !!}</p>
</div>

<!-- Firebase Id Field -->
<div class="form-group">
    {!! Form::label('firebase_id', 'Firebase Id:') !!}
    <p>{!! $customerDevice->firebase_id !!}</p>
</div>

<!-- Device Id Field -->
<div class="form-group">
    {!! Form::label('device_id', 'Device Id:') !!}
    <p>{!! $customerDevice->device_id !!}</p>
</div>

<!-- Device Model Field -->
<div class="form-group">
    {!! Form::label('device_model', 'Device Model:') !!}
    <p>{!! $customerDevice->device_model !!}</p>
</div>

<!-- Device Os Field -->
<div class="form-group">
    {!! Form::label('device_os', 'Device Os:') !!}
    <p>{!! $customerDevice->device_os !!}</p>
</div>

<!-- Device Os Version Field -->
<div class="form-group">
    {!! Form::label('device_os_version', 'Device Os Version:') !!}
    <p>{!! $customerDevice->device_os_version !!}</p>
</div>

<!-- Device Model Type Field -->
<div class="form-group">
    {!! Form::label('device_model_type', 'Device Model Type:') !!}
    <p>{!! $customerDevice->device_model_type !!}</p>
</div>

<!-- Device Meta Data Field -->
<div class="form-group">
    {!! Form::label('device_meta_data', 'Device Meta Data:') !!}
    <p>{!! $customerDevice->device_meta_data !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customerDevice->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customerDevice->updated_at !!}</p>
</div>

