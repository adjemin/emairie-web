<div class="table-responsive-sm">
    <table class="table table-striped" id="customerDevices-table">
        <thead>
            <th>Customer Id</th>
        <th>Firebase Id</th>
        <th>Device Id</th>
        <th>Device Model</th>
        <th>Device Os</th>
        <th>Device Os Version</th>
        <th>Device Model Type</th>
        <th>Device Meta Data</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($customerDevices as $customerDevice)
            <tr>
                <td>{!! $customerDevice->customer_id !!}</td>
            <td>{!! $customerDevice->firebase_id !!}</td>
            <td>{!! $customerDevice->device_id !!}</td>
            <td>{!! $customerDevice->device_model !!}</td>
            <td>{!! $customerDevice->device_os !!}</td>
            <td>{!! $customerDevice->device_os_version !!}</td>
            <td>{!! $customerDevice->device_model_type !!}</td>
            <td>{!! $customerDevice->device_meta_data !!}</td>
                <td>
                    {!! Form::open(['route' => ['customerDevices.destroy', $customerDevice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('customerDevices.show', [$customerDevice->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('customerDevices.edit', [$customerDevice->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>