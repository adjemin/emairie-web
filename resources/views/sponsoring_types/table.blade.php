<div class="table-responsive-sm">
    <table class="table table-striped" id="sponsoringTypes-table">
        <thead>
            <th>Name</th>
        <th>Slug</th>
        <th>Image</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($sponsoringTypes as $sponsoringType)
            <tr>
                <td>{!! $sponsoringType->name !!}</td>
            <td>{!! $sponsoringType->slug !!}</td>
            <td>{!! $sponsoringType->image !!}</td>
                <td>
                    {!! Form::open(['route' => ['sponsoringTypes.destroy', $sponsoringType->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('sponsoringTypes.show', [$sponsoringType->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('sponsoringTypes.edit', [$sponsoringType->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>