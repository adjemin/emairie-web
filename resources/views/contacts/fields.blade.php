<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Has Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_email', 'Has Email:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_email', 0) !!}
        {!! Form::checkbox('has_email', '1', null) !!}
    </label>
</div>


<!-- Has Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('has_phone', 'Has Phone:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('has_phone', 0) !!}
        {!! Form::checkbox('has_phone', '1', null) !!}
    </label>
</div>


<!-- Is Accessible Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_accessible', 'Is Accessible:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_accessible', 0) !!}
        {!! Form::checkbox('is_accessible', '1', null) !!}
    </label>
</div>


<!-- Accessible Change Field -->
<div class="form-group col-sm-6">
    {!! Form::label('accessible_change', 'Accessible Change:') !!}
    {!! Form::text('accessible_change', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('contacts.index') !!}" class="btn btn-default">Cancel</a>
</div>
