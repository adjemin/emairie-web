<div class="table-responsive-sm">
    <table class="table table-striped" id="contacts-table">
        <thead>
            <th>Customer Id</th>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Has Email</th>
        <th>Has Phone</th>
        <th>Is Accessible</th>
        <th>Accessible Change</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{!! $contact->customer_id !!}</td>
            <td>{!! $contact->name !!}</td>
            <td>{!! $contact->phone !!}</td>
            <td>{!! $contact->email !!}</td>
            <td>{!! $contact->has_email !!}</td>
            <td>{!! $contact->has_phone !!}</td>
            <td>{!! $contact->is_accessible !!}</td>
            <td>{!! $contact->accessible_change !!}</td>
                <td>
                    {!! Form::open(['route' => ['contacts.destroy', $contact->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('contacts.show', [$contact->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('contacts.edit', [$contact->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>