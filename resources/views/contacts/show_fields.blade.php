<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $contact->customer_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $contact->name !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $contact->phone !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $contact->email !!}</p>
</div>

<!-- Has Email Field -->
<div class="form-group">
    {!! Form::label('has_email', 'Has Email:') !!}
    <p>{!! $contact->has_email !!}</p>
</div>

<!-- Has Phone Field -->
<div class="form-group">
    {!! Form::label('has_phone', 'Has Phone:') !!}
    <p>{!! $contact->has_phone !!}</p>
</div>

<!-- Is Accessible Field -->
<div class="form-group">
    {!! Form::label('is_accessible', 'Is Accessible:') !!}
    <p>{!! $contact->is_accessible !!}</p>
</div>

<!-- Accessible Change Field -->
<div class="form-group">
    {!! Form::label('accessible_change', 'Accessible Change:') !!}
    <p>{!! $contact->accessible_change !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $contact->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $contact->updated_at !!}</p>
</div>

