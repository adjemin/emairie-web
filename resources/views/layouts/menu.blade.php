<li class="nav-item {{ Request::is('productCategories*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('productCategories.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Categories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('productConditions*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('productConditions.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Conditions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('countries*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('countries.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Countries</span>
    </a>
</li>
<li class="nav-item {{ Request::is('currencies*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('currencies.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Currencies</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('customers.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryAddresses*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('deliveryAddresses.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Addresses</span>
    </a>
</li>
<li class="nav-item {{ Request::is('products*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('products.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Products</span>
    </a>
</li>
<li class="nav-item {{ Request::is('photos*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('photos.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Photos</span>
    </a>
</li>
<li class="nav-item {{ Request::is('productMedia*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('productMedia.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Media</span>
    </a>
</li>
<li class="nav-item {{ Request::is('productMediaTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('productMediaTypes.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Media Types</span>
    </a>
</li>
<li class="nav-item {{ Request::is('flashSellings*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('flashSellings.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Flash Sellings</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderItems*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('orderItems.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Items</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('orders.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Orders</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderHistories*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('orderHistories.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Histories</span>
    </a>
</li>
<li class="nav-item {{ Request::is('userDefaultDeliveryServices*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('userDefaultDeliveryServices.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>User Default Delivery Services</span>
    </a>
</li>
<li class="nav-item {{ Request::is('deliveryServices*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('deliveryServices.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Delivery Services</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderAssignments*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('orderAssignments.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Assignments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderDeliveries*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('orderDeliveries.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Order Deliveries</span>
    </a>
</li>
<li class="nav-item {{ Request::is('paymentMethods*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('paymentMethods.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payment Methods</span>
    </a>
</li>
<li class="nav-item {{ Request::is('invoices*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('invoices.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Invoices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('favoriteProducts*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('favoriteProducts.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Favorite Products</span>
    </a>
</li>
<li class="nav-item {{ Request::is('follows*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('follows.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Follows</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerNotifications*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('customerNotifications.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Notifications</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerDevices*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('customerDevices.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Devices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerSessions*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('customerSessions.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Sessions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('coupons*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('coupons.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Coupons</span>
    </a>
</li>
<li class="nav-item {{ Request::is('conversations*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('conversations.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Conversations</span>
    </a>
</li>
<li class="nav-item {{ Request::is('messages*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('messages.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Messages</span>
    </a>
</li>
<li class="nav-item {{ Request::is('notificationTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('notificationTypes.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Notification Types</span>
    </a>
</li>
<li class="nav-item {{ Request::is('sponsoringTypes*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('sponsoringTypes.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sponsoring Types</span>
    </a>
</li>
<li class="nav-item {{ Request::is('sponsorings*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('sponsorings.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sponsorings</span>
    </a>
</li>
<li class="nav-item {{ Request::is('payments*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('payments.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('paymentSources*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('paymentSources.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payment Sources</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contacts*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('contacts.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Contacts</span>
    </a>
</li>
<li class="nav-item {{ Request::is('contactInvitations*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('contactInvitations.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Contact Invitations</span>
    </a>
</li>
<li class="nav-item {{ Request::is('walletTokens*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('walletTokens.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Wallet Tokens</span>
    </a>
</li>
<li class="nav-item {{ Request::is('tokenTransactions*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('tokenTransactions.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Token Transactions</span>
    </a>
</li>
<li class="nav-item {{ Request::is('evaluateSellers*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('evaluateSellers.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Evaluate Sellers</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerIssues*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('customerIssues.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Issues</span>
    </a>
</li>
<li class="nav-item {{ Request::is('productAudiences*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('productAudiences.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Product Audiences</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{!! route('users.index') !!}">
        <i class="nav-icon icon-cursor"></i>
        <span>Users</span>
    </a>
</li>
