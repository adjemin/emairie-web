<div class="table-responsive-sm">
    <table class="table table-striped" id="productMedia-table">
        <thead>
            <th>Product Id</th>
        <th>Content Type</th>
        <th>Mime</th>
        <th>Url</th>
        <th>Thumbnail</th>
        <th>Duration</th>
        <th>Width</th>
        <th>Height</th>
        <th>Length</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($productMedia as $productMedia)
            <tr>
                <td>{!! $productMedia->product_id !!}</td>
            <td>{!! $productMedia->content_type !!}</td>
            <td>{!! $productMedia->mime !!}</td>
            <td>{!! $productMedia->url !!}</td>
            <td>{!! $productMedia->thumbnail !!}</td>
            <td>{!! $productMedia->duration !!}</td>
            <td>{!! $productMedia->width !!}</td>
            <td>{!! $productMedia->height !!}</td>
            <td>{!! $productMedia->length !!}</td>
                <td>
                    {!! Form::open(['route' => ['productMedia.destroy', $productMedia->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('productMedia.show', [$productMedia->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('productMedia.edit', [$productMedia->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>