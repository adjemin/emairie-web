<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $productMedia->product_id !!}</p>
</div>

<!-- Content Type Field -->
<div class="form-group">
    {!! Form::label('content_type', 'Content Type:') !!}
    <p>{!! $productMedia->content_type !!}</p>
</div>

<!-- Mime Field -->
<div class="form-group">
    {!! Form::label('mime', 'Mime:') !!}
    <p>{!! $productMedia->mime !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $productMedia->url !!}</p>
</div>

<!-- Thumbnail Field -->
<div class="form-group">
    {!! Form::label('thumbnail', 'Thumbnail:') !!}
    <p>{!! $productMedia->thumbnail !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{!! $productMedia->duration !!}</p>
</div>

<!-- Width Field -->
<div class="form-group">
    {!! Form::label('width', 'Width:') !!}
    <p>{!! $productMedia->width !!}</p>
</div>

<!-- Height Field -->
<div class="form-group">
    {!! Form::label('height', 'Height:') !!}
    <p>{!! $productMedia->height !!}</p>
</div>

<!-- Length Field -->
<div class="form-group">
    {!! Form::label('length', 'Length:') !!}
    <p>{!! $productMedia->length !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $productMedia->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $productMedia->updated_at !!}</p>
</div>

