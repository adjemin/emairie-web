<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $order->customer_id !!}</p>
</div>

<!-- Is Waiting Field -->
<div class="form-group">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <p>{!! $order->is_waiting !!}</p>
</div>

<!-- Current Status Field -->
<div class="form-group">
    {!! Form::label('current_status', 'Current Status:') !!}
    <p>{!! $order->current_status !!}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{!! $order->rating !!}</p>
</div>

<!-- Note Field -->
<div class="form-group">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $order->note !!}</p>
</div>

<!-- Note Date Field -->
<div class="form-group">
    {!! Form::label('note_date', 'Note Date:') !!}
    <p>{!! $order->note_date !!}</p>
</div>

<!-- Payment Method Slug Field -->
<div class="form-group">
    {!! Form::label('payment_method_slug', 'Payment Method Slug:') !!}
    <p>{!! $order->payment_method_slug !!}</p>
</div>

<!-- Delivery Fees Field -->
<div class="form-group">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    <p>{!! $order->delivery_fees !!}</p>
</div>

<!-- Is Delivered Field -->
<div class="form-group">
    {!! Form::label('is_delivered', 'Is Delivered:') !!}
    <p>{!! $order->is_delivered !!}</p>
</div>

<!-- Delivery Date Field -->
<div class="form-group">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    <p>{!! $order->delivery_date !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $order->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $order->updated_at !!}</p>
</div>

