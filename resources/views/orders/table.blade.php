<div class="table-responsive-sm">
    <table class="table table-striped" id="orders-table">
        <thead>
            <th>Customer Id</th>
        <th>Is Waiting</th>
        <th>Current Status</th>
        <th>Rating</th>
        <th>Note</th>
        <th>Note Date</th>
        <th>Payment Method Slug</th>
        <th>Delivery Fees</th>
        <th>Is Delivered</th>
        <th>Delivery Date</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{!! $order->customer_id !!}</td>
            <td>{!! $order->is_waiting !!}</td>
            <td>{!! $order->current_status !!}</td>
            <td>{!! $order->rating !!}</td>
            <td>{!! $order->note !!}</td>
            <td>{!! $order->note_date !!}</td>
            <td>{!! $order->payment_method_slug !!}</td>
            <td>{!! $order->delivery_fees !!}</td>
            <td>{!! $order->is_delivered !!}</td>
            <td>{!! $order->delivery_date !!}</td>
                <td>
                    {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orders.show', [$order->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('orders.edit', [$order->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>