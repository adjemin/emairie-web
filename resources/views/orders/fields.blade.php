<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Waiting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting', 'Is Waiting:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting', 0) !!}
        {!! Form::checkbox('is_waiting', '1', null) !!}
    </label>
</div>


<!-- Current Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_status', 'Current Status:') !!}
    {!! Form::text('current_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::text('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note_date', 'Note Date:') !!}
    {!! Form::text('note_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    {!! Form::text('delivery_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Delivered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_delivered', 'Is Delivered:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_delivered', 0) !!}
        {!! Form::checkbox('is_delivered', '1', null) !!}
    </label>
</div>


<!-- Delivery Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_date', 'Delivery Date:') !!}
    {!! Form::text('delivery_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orders.index') !!}" class="btn btn-default">Cancel</a>
</div>
