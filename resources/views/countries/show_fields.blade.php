<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $country->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $country->code !!}</p>
</div>

<!-- Flag Field -->
<div class="form-group">
    {!! Form::label('flag', 'Flag:') !!}
    <p>{!! $country->flag !!}</p>
</div>

<!-- Dial Code Field -->
<div class="form-group">
    {!! Form::label('dial_code', 'Dial Code:') !!}
    <p>{!! $country->dial_code !!}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{!! $country->language !!}</p>
</div>

<!-- Location Lat Field -->
<div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{!! $country->location_lat !!}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{!! $country->location_lng !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $country->currency_code !!}</p>
</div>

<!-- Timezone Field -->
<div class="form-group">
    {!! Form::label('timezone', 'Timezone:') !!}
    <p>{!! $country->timezone !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $country->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $country->updated_at !!}</p>
</div>

