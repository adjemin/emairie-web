<div class="table-responsive-sm">
    <table class="table table-striped" id="countries-table">
        <thead>
            <th>Name</th>
        <th>Code</th>
        <th>Flag</th>
        <th>Dial Code</th>
        <th>Language</th>
        <th>Location Lat</th>
        <th>Location Lng</th>
        <th>Currency Code</th>
        <th>Timezone</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($countries as $country)
            <tr>
                <td>{!! $country->name !!}</td>
            <td>{!! $country->code !!}</td>
            <td>{!! $country->flag !!}</td>
            <td>{!! $country->dial_code !!}</td>
            <td>{!! $country->language !!}</td>
            <td>{!! $country->location_lat !!}</td>
            <td>{!! $country->location_lng !!}</td>
            <td>{!! $country->currency_code !!}</td>
            <td>{!! $country->timezone !!}</td>
                <td>
                    {!! Form::open(['route' => ['countries.destroy', $country->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('countries.show', [$country->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('countries.edit', [$country->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>