@extends('customer.layout.regi')
@section('content')
<div class="container" style="margin-top:1px">
    <div class="row">
        <h4 style="font-family: Montserrat !important;">Avec la communauté Adjemin, achetez, vendez faites vous livrer et gérer vos stocks</h4>
        <div class="col-md-12" style="border:1px solid #dfdfdf ;marging-right:50px;">
            <div class="panel panel-default">
                <div class="panel-heading" style="margin-top: 10px; text-align: center;"><h3 style="font-family: Montserrat !important;">Rejoignez nous</h3></div>
                <div class="panel-body">
                    <form class="form-horizontal" id="sign-in-form" role="form" method="POST" action="{{ url('/customer/register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
                            <label for="numero" class="col-md-12 control-label centrer">Numéro de Téléphone</label>
                            <div>
                                <input id="tele" type="text" class="form-control couleur" onkeyup="clik();" name="numero" value="{{ old('numero') }}" autofocus>
                                <input type="hidden" name="dial_code" id="dial_code">
                                <input type="hidden" name="country_code" id="pay">
                                <input type="hidden" name="phone-number" id="phone-number">
                                <script type="text/javascript">
                                function clik(){
                                var y= document.getElementById("tele").value;
                                var x= document.getElementById("dial_code").value;
                                document.getElementById("phone-number").value= "+".concat(x,y);
                                    }
                                </script>

                                @if ($errors->has('numero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                @endif
                            </div><br>
                            <button id="sign-in-button" type="button" style="background-color: #0e2b3b !important;color:#fff" class="btn btn-default btn-block">
                                Connexion
                            </button>
                        </div>
                    </form>
                    <script>
                        $(document).keypress(
                        function(event){
                            if (event.which == '13') {
                            event.preventDefault();
                            }
                        });
                    </script>

                  <form id="verification-code-form" style="display: none;" action="#" style="border:1px solid red">
                    <!-- Input to enter the verification code -->
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="col-md-12 mdl-textfield__input" type="text" id="verification-code"style="color:#0e2b3b !important;">
                      <label style="color:#0e2b3b !important;" class="mdl-textfield__label" for="verification-code">
                          <i style="color:#0e2b3b !important;">
                            Entrer le code de vérification...
                          </i>
                      </label>
                    </div><br>

                    <!-- Button that triggers code verification -->
                    <input style="background-color: #0e2b3b !important;color:#fff" type="submit" class="btn btn-default" id="verify-code-button" value="Vérifier le code"/>
                    <!-- Button to cancel code verification -->
                    <button style="float:right !important" type="button" class="btn btn-success" id="cancel-verify-code-button">Annuler</button>
                  </form>
            <!-- <p>Don't have an account!</p>  -->
            <!--<div class="form-group">
                <div class="col-md-12">
                    <a class="btn btn-primary btn-block" style="color: white; background-color: #298550 !important;" href="{{url('customer/register')}}" type="button" id="btn-signup"><i class="fas fa-user-plus"></i>Inscrivez-Vous</a>
                </div>
            </div>-->
            <br>
                </div>
            </div>  
        </div>
    </div>
</div>
@endsection
