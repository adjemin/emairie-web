@extends('customer.layout.regi')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Connexion</h1></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/customer/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('numero') ? ' has-error' : '' }}">
                            <label for="numero" class="col-md-12 control-label centrer">Numéro de Téléphone</label>

                            <div>
                                <input id="tele" type="text" class="form-control couleur" name="numero" value="{{ old('numero') }}" autofocus>

                                @if ($errors->has('numero'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-12 control-label centrer">Mot de passe</label>

                                <input type="password" class="form-control {{ $errors->has('password')?'is-invalid':'' }}" placeholder="Password" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                       <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                                
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" style="background-color: #298550 !important;" class="btn btn-primary btn-block">
                                    Connexion
                                </button>
                            </div>
                        </div>
                    </form>
                    <hr>
            <br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
