<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Adjemin marketplace vender nous faisons de vous les meilleur boutiques,Tchater,Acheter et Vender et faites vous livrer par nos service de livraison dynamiques">
  

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel Multi Auth Guard') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/intlTelInput.css') }}">

    <!-- vérification de numéro-->
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.orange-indigo.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

    <!-- Styles nouveau format -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!-- Scripts 
    <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
      ]); ?>
  </script>
  -->
<link rel="icon" href="{{asset('front/dist/images/icon.ico')}}" type="image/ico" />
</head>
<style>
  body, html{height:100%; font-family: Montserrat;}
#main{
min-height:100%;
margin:0 auto;
position:relative;

}
footer{
  background-color: #0e2b3b;
position:absolute;
bottom:0;
width:100%;
margin-top: 10px;
padding-bottom:5px;
height:inherit;
}
  </style>
<body>  
  <div id="main">
    <div class="container">
      <br><br><br>
      <div class="row">
        <div class="col-sm-6" style="">
          <div class="">
            <div class="">
            <img style="height:90%;" src="{{asset('front/dist/images/login.png')}}" class="card-img" alt="">
            <p></p>
            <div class="row" style="margin-left:100px;">
              <div class="card"style="border:none !important">
                 <div class="row">
                   <a href="#" class="btn btn-default">
                     <img style="border:1px solid #fff;border-radius:10px;" src="{{asset('front/dist/images/ios.png')}}" height="40">
                   </a>
                   <a href="#" class="btn btn-default">
                     <img style="border:1px solid #fff;border-radius:10px;" src="{{asset('front/dist/images/android.png')}}" height="40">
                   </a>
                  </div>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card" style="border:none !important">
            <div class="card-body">
              <p class="centrer">
                <img  style="margin-top:0px;height:100px;" src="{{asset('images/Admin/LOGO-ADJEMIN-150x150.png')}}" class="img-responsive">
              </p>
              @yield('content')
              <p></p>
            </div>
          </div>
        </div>
      </div>
    </div><br><br><br><br><br>
    <footer>
      <div class="d-flex justify-content-around" style="margin-top:15px;padding:5px;background-color: #0e2b3b">
        <div class="p-2" style="color:#fff !important">
          &copy; {{now()->format('Y')}} Adjemin, Tous les droits reservés
        </div>
        <div class="p-2" style="color:#fff !important">
          <a href="#" style="color:#fff !important">Produits &amp; Services</a> &nbsp;   &nbsp;
           <a href="#" style="color:#fff !important">Aides</a>
        </div>
        <div class="p-2" style="color:#fff !important">
          <a class="btn btn-icon btn-light" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
          <a class="btn btn-icon btn-light" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
          <a class="btn btn-icon btn-light" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube"></i></a>
          <a class="btn btn-icon btn-light" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
        </div>

        </div>
    </footer>
  
  </div>
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="{{asset('/js/intlTelInput.js')}}"></script>
  <script>
    // get the country data from the plugin
  var countryData = window.intlTelInputGlobals.getCountryData(),
  input = document.querySelector("#tele"),
  addressDropdown = document.querySelector("#pay");
  champ = document.querySelector("#dial_code");
  po = document.querySelector("#num");
  // init plugin
var iti = window.intlTelInput(input, {
preferredCountries: ['ci'],
    //allowDropdown: false,
       //autoHideDialCode: true,
       //autoPlaceholder: "off",
separateDialCode: true,
// excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
       //hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
  utilsScript: "{{asset('/js/utils.js?1537727621611')}}" // just for formatting/placeholders etc
});

// populate the country dropdown
for (var i = 0; i < countryData.length; i++) {
  var country = countryData[i];
  var optionNode = document.createElement("option");
  optionNode.value = country.iso2;
  var textNode = document.createTextNode(country.name);
  optionNode.appendChild(textNode);
  addressDropdown.appendChild(optionNode);
}
// set it's initial value
addressDropdown.value = iti.getSelectedCountryData().iso2;
//addressDropdown.value = iti.getSelectedCountryData().dialCode;
champ.value=iti.getSelectedCountryData().dialCode;
// listen to the telephone input for changes
input.addEventListener('countrychange', function(e) {
  addressDropdown.value = iti.getSelectedCountryData().iso2;
   //addressDropdown.value = iti.getSelectedCountryData().dialCode;
   champ.value = iti.getSelectedCountryData().dialCode;
});

// listen to the address dropdown for changes
addressDropdown.addEventListener('change', function() {
  iti.setCountry(this.value);
});
  </script>


  <!-- vérification de numéro -->
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script type="text/javascript">
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyAc91Vbr6ztfUxT9eDHejOP6QU-wQhStTo",
        authDomain: "adjemin-web.firebaseapp.com",
        databaseURL: "https://adjemin-web.firebaseio.com",
        projectId: "adjemin-web",
        storageBucket: "adjemin-web.appspot.com",
        messagingSenderId: "14203483930",
        appId: "1:14203483930:web:9dd798068dfe8c57b45ea3",
        measurementId: "G-DW262R5WYE"
    };
    firebase.initializeApp(config);
    //firebase.analytics();

    var database = firebase.database();
  /**
   * Set up UI event listeners and registering Firebase auth listeners.
   */
  window.onload = function() {
    // Listening for auth state changes.
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {

        var dial_code = document.getElementById("dial_code").value;
        var numero = document.getElementById("tele").value;
        var pay = document.getElementById("pay").value;
        var donnee =[dial_code,numero,pay];
        //window.location.href = "{{url('enre')}}";

        //alert(donnee);
       /*$.ajax({
            url:"{{ route('customers.store') }}", 
            method: "POST",  
            data:{data:donnee},  
            success:function(data){
                 //$('#show_cities').html(data);
                 alert('success'+data); 
           },
           error:function(data){
            alert(data);
           }

       });*/
        // User is signed in.
        //window.location.href = "layoutFront";
        //document.getElementsByName('formulaire').submit();
        document.getElementById('sign-in-form').submit();
        /*var uid = user.uid;
        var email = user.email;
        var photoURL = user.photoURL;
        var phoneNumber = user.phoneNumber;
        var isAnonymous = user.isAnonymous;
        var displayName = user.displayName;
        var providerData = user.providerData;
        var emailVerified = user.emailVerified;*/
      }
      /*updateSignInButtonUI();
      updateSignInFormUI();
      updateSignOutButtonUI();
      updateSignedInUserStatusUI();
      updateVerificationCodeFormUI();*/
      
    });

    // Event bindings.
    //document.getElementById('sign-out-button').addEventListener('click', onSignOutClick);
    document.getElementById('tele').addEventListener('keyup', updateSignInButtonUI);
    document.getElementById('tele').addEventListener('change', updateSignInButtonUI);
    document.getElementById('verification-code').addEventListener('keyup', updateVerifyCodeButtonUI);
    document.getElementById('verification-code').addEventListener('change', updateVerifyCodeButtonUI);
    document.getElementById('verification-code-form').addEventListener('submit', onVerifyCodeSubmit);
    document.getElementById('cancel-verify-code-button').addEventListener('click', cancelVerification);

    // [START appVerifier]
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        onSignInSubmit();
      }
    });
    // [END appVerifier]

    recaptchaVerifier.render().then(function(widgetId) {
      window.recaptchaWidgetId = widgetId;
      updateSignInButtonUI();
    });
  };

  /**
   * Function called when clicking the Login/Logout button.
   */
  function onSignInSubmit() {
    if (isPhoneNumberValid()) {
      window.signingIn = true;
      updateSignInButtonUI();
      var phoneNumber = getPhoneNumberFromUserInput();
      var appVerifier = window.recaptchaVerifier;
      firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
          .then(function (confirmationResult) {
            // SMS sent. Prompt user to type the code from the message, then sign the
            // user in with confirmationResult.confirm(code).
            window.confirmationResult = confirmationResult;
            window.signingIn = false;
            updateSignInButtonUI();
            updateVerificationCodeFormUI();
            updateVerifyCodeButtonUI();
            updateSignInFormUI();
          }).catch(function (error) {
            // Error; SMS not sent
            console.error('Error during signInWithPhoneNumber', error);
            window.alert('Error during signInWithPhoneNumber:\n\n'
                + error.code + '\n\n' + error.message);
            window.signingIn = false;
            updateSignInFormUI();
            updateSignInButtonUI();
          });
    }
  }

  /**
   * Function called when clicking the "Verify Code" button.
   */
  function onVerifyCodeSubmit(e) {
    e.preventDefault();
    if (!!getCodeFromUserInput()) {
      window.verifyingCode = true;
      updateVerifyCodeButtonUI();
      var code = getCodeFromUserInput();
      confirmationResult.confirm(code).then(function (result) {
        // User signed in successfully.
        var user = result.user;
        window.verifyingCode = false;
        window.confirmationResult = null;
        updateVerificationCodeFormUI();
      }).catch(function (error) {
        // User couldn't sign in (bad verification code?)
        console.error('Error while checking the verification code', error);
        window.alert('Error while checking the verification code:\n\n'
            + error.code + '\n\n' + error.message);
        window.verifyingCode = false;
        updateSignInButtonUI();
        updateVerifyCodeButtonUI();
      });
    }
  }

  /**
   * Cancels the verification code input.
   */
  function cancelVerification(e) {
    e.preventDefault();
    window.confirmationResult = null;
    updateVerificationCodeFormUI();
    updateSignInFormUI();
  }

  /**
   * Signs out the user when the sign-out button is clicked.
   */
  function onSignOutClick() {
    firebase.auth().signOut();
    window.location.replace('/');
  }

  /**
   * Reads the verification code from the user input.
   */
  function getCodeFromUserInput() {
    return document.getElementById('verification-code').value;
  }

  /**
   * Reads the phone number from the user input.
   */
  function getPhoneNumberFromUserInput() {
    return document.getElementById('phone-number').value;
  }

  /**
   * Returns true if the phone number is valid.
   */
  function isPhoneNumberValid() {
    var pattern = /^\+[0-9\s\-\(\)]+$/;
    var phoneNumber = getPhoneNumberFromUserInput();
    return phoneNumber.search(pattern) !== -1;
  }

  /**
   * Re-initializes the ReCaptacha widget.
   */
  function resetReCaptcha() {
    if (typeof grecaptcha !== 'undefined'
        && typeof window.recaptchaWidgetId !== 'undefined') {
      grecaptcha.reset(window.recaptchaWidgetId);
    }
  }

  /**
   * Updates the Sign-in button state depending on ReCAptcha and form values state.
   */
  function updateSignInButtonUI() {
    document.getElementById('sign-in-button').disabled =
        !isPhoneNumberValid()
        || !!window.signingIn;
  }

  /**
   * Updates the Verify-code button state depending on form values state.
   */
  function updateVerifyCodeButtonUI() {
    document.getElementById('verify-code-button').disabled =
        !!window.verifyingCode
        || !getCodeFromUserInput();
  }

  /**
   * Updates the state of the Sign-in form.
   */
  function updateSignInFormUI() {
    if (firebase.auth().currentUser || window.confirmationResult) {
      document.getElementById('sign-in-form').style.display = 'none';
    } else {
      resetReCaptcha();
      document.getElementById('sign-in-form').style.display = 'block';
    }
  }

  /**
   * Updates the state of the Verify code form.
   */
  function updateVerificationCodeFormUI() {
    if (!firebase.auth().currentUser && window.confirmationResult) {
      document.getElementById('verification-code-form').style.display = 'block';
    } else {
      document.getElementById('verification-code-form').style.display = 'none';
    }
  }

  /**
   * Updates the state of the Sign out button.
   */
  function updateSignOutButtonUI() {
    if (firebase.auth().currentUser) {
      document.getElementById('sign-out-button').style.display = 'block';
    } else {
      document.getElementById('sign-out-button').style.display = 'none';
    }
  }

  /**
   * Updates the Signed in user status panel.
   */
  function updateSignedInUserStatusUI() {
    var user = firebase.auth().currentUser;
    if (user) {
      document.getElementById('sign-in-status').textContent = 'Signed in';
      document.getElementById('account-details').textContent = JSON.stringify(user, null, '  ');
    } else {
      document.getElementById('sign-in-status').textContent = 'Signed out';
      document.getElementById('account-details').textContent = 'null';
    }
  }
</script>   
</body>
</html>
