@extends('layoutFront.app_chield')
@section('detailProduit')
@if(session('clef') != NULL)
<div class="container">
  <div class="row">
    <div class="col-sm-12" style="font-family: montserrat;margin-bottom:10px;">
      <div class="card">
          <h5 class="card-header text-left" style="border:1px solid #f3f3f3;background-color:#fff;">
            <strong>Passer une commande</strong>
           </h5>
           <form method="post" action="{{route('orders.store')}}" role="form" style="margin:15px;">
                @csrf
            <div class="card-body">
                @foreach($product as $products)
              <div class="row">
                <div class="col-sm-12">
                  <div class="card mb-3" style="border:none">
                    <div class="row no-gutters">
                      <div class="row">
                        <div class="col-md-4">
                          <img style="margin-bottom:15px !important;margin-left:15px !important" src="{{$products->cover_url}}" class="card-img" alt="...">
                          </div>
                          <div class="col-md-8">
                            <div class="card-body">
                              <h5 class="card-title"> <strong>{{$products->title}}</strong> </h5>
                              <p class="card-text-strong">{{$products->description}}</p>
                              <p class="card-text">
                                <strong class="text-muted">
                               <button type="button" class="btn btn-success">{{$products->price}} CFA</button>
                              </strong>
                              </p>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      Qauntité:
                    </div>
                    <div class="col-sm-6"></div>
                    <div class="col-sm-2">
                      <div class="col row input-group mb-3 input-spinner">
                        <div class="input-group-append">
                          <button style="border-radius:100%;" onclick="decrementValue(); final2(); buttonen();" value="Decrement Value"  class="btn btn-success" type="button" id="button-minus"> − </button>
                      </div>
                        <input style="text-align:center;border:none; background:none;" class="form-control" name="quantite" min="1" readonly type="text" id="number" value="1"/>
                      <div class="input-group-prepend">
                        <button style="border-radius:100%;" onclick="incrementValue(); final(); buttondis();" id="plus" value="Increment Value" class="btn btn-primary" type="button"> + </button>
                      </div>
                        </div>  <!-- input-spinner.// -->
                      </div> <!-- col.// -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12" style="margin-top:5px;margin-bottom:5px;">
                @if($choix && $choix->count()>0)
                <strong> Définir le lieu de livraison: &nbsp;</strong>
                <a href="#" id="definir" disabled style="color:#000 !important">
                  <i class="fa fa-edit"></i>
                  </a>
                  @else
                  <strong> Définir le lieu de livraison: &nbsp;</strong>
                  <a href="{{route('deliveryAddresses.index')}}" id="definir" disabled style="color:#000 !important">
                  
                  <i class="fa fa-edit"></i>
                  </a>
                  @endif
                  @if($choix && $choix->count()>0)
                    @foreach($choix as $choi)
                      <br><br>
                  <strong><i class="fa fa-map-marker" id="addresse" aria-hidden="true"></i><input id="addresse" type="text" readonly name="addresse" style="border: none; width: -moz-available" value="{{$choi->address_name}}"> &nbsp;</strong><button type="button" onclick="trash();" id="addresse" style="border: none; background: none;"><i id="addresse" class="fa fa-trash"></i></button><br>
                    @if($choi->address_type == 'house')
                      <input type="text" style="border: none; background: none;" readonly name="" id="addresse" value="Maison">
                      @elseif($choi->address_type == 'office')
                      <input type="text" style="border: none; background: none;" readonly name="" id="addresse" value="Bureau">
                      @else
                      <input type="text" style="border: none; background: none;" readonly name="" id="addresse" value="Autre">
                    @endif
                    @endforeach
                  @endif
              </div>
              <div style="margin-bottom:5px;font-weight:bold;">           
                <table class="table responsive">
                  <tbody>
                    @if($choix && $choix->count()>0)
                    <tr id="liv">
                      <td>Frais de livraison</td>
                      <td></td>
                      <td>CFA <input type="text" readonly id="frais" name="frais" value="{{10}}" style="border: none;"></td>
                    </tr>
                      @else
                    <tr id="liv" style="display: none;">
                      <td>Frais de livraison</td>
                      <td></td>
                      <td>CFA <input type="text" readonly id="frais" name="frais" value="{{0}}" style="border: none;"></td>
                    </tr>
                    @endif
                    
                    <tr>
                      <td>Sous-Total</td>
                      <td></td>
                      <td>CFA <input type="text" readonly name="unite" id="prix" value="{{$products->price}}" style="border: none;"></td>
                    </tr>
                    <tr>
                      <input type="hidden" id="tto" value="{{$total}}">
                      <td>Taxe</td>
                      <td></td>
                      <td>CFA <input type="text" readonly name="taxe" value="{{0}}" style="border: none;"></td>
                    </tr>
                    <tr>
                      <td>Total</td>
                      <td></td>
                      <td>
                        @if($choix && $choix->count()>0)
                        CFA <input type="text" readonly name="total" id="total" value="{{$total}}" style="border: none;">
                        @else
                        CFA <input type="text" readonly name="total" id="total" value="{{$products->price}}" style="border: none;">
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td colspan="3">Moyen de paiement <br>
                        <div class="col">
                          <div class="form-group">
                            <select class="form-control" name="Cinetpay" id="select">
                            <option data-thumbnail="{{$paymet->image}}" value="{{$paymet->slug}}">{{$paymet->name}}</option>   
                            </select>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <input type="hidden" name="quant" id="quant" value="{{$products->initiale_count}}">
              <button type="submit" class="btn btn-primary btn-block">Valider</button>
              <button type="button" class="btn btn-success btn-block">Annuler</button>
              @endforeach
              </form>
            </div>
          </div>
      </div>
  </div>
<!-- enlever la localisation -->
@if($choix && $choix->count()>0)
<script>
  function trash()
  {
    var myClasses = document.querySelectorAll('#addresse'),
                            i = 0,
                            l = myClasses.length;
                        for (i; i < l; i++) {
                            myClasses[i].value = '';
                            myClasses[i].style.display='none';
                        }
    var lieuu = document.querySelectorAll('#frais'),
                            i = 0,
                            l = lieuu.length;
                        for (i; i < l; i++) {
                            lieuu[i].value = '';
                            lieuu[i].style.display='none';
                            document.getElementById('liv').style.display='none';
                            document.getElementById('total').value="";
                        }
    document.getElementById('definir').href="{{route('deliveryAddresses.index')}}";
  }
</script>
@endif
<!-- gestion de la quantité -->
<script type="text/javascript">
  function incrementValue()
{
 var value = parseInt(document.getElementById('number').value, 10);
 value = isNaN(value) ? 0 : value;
 value++;
 document.getElementById('number').value = value;
}
function decrementValue()
{
 var value = parseInt(document.getElementById('number').value, 10);
 value = isNaN(value) ? 0 : value;
 value = value<=1 ? 2 : value;
 value--;
 document.getElementById('number').value = value;
}
   </script>
   <!-- calcul des totaux -->
   <script>
     $(window).on("load",function(){
        if(document.getElementById('number').value==document.getElementById('quant').value){
        document.getElementById('plus').disabled=true;
      }
     });
   </script>
   <script type="text/javascript">
    function buttondis()
    {
      if(document.getElementById('number').value==document.getElementById('quant').value){
        document.getElementById('plus').disabled=true;
      }
    }
    function buttonen()
        {
          if(document.getElementById('number').value !=1){
          document.getElementById('plus').disabled=false;
          }
        }
     function final()
     {
      var tot=Number(document.getElementById('total').value);
       var pri=Number(document.getElementById('prix').value);
      document.getElementById('total').value=tot+pri;
     }
     function final2()
     {
      if(document.getElementById('total').value != document.getElementById('prix').value && document.getElementById('total').value != document.getElementById('tto').value){
      document.getElementById('total').value=(document.getElementById('total').value)-(document.getElementById('prix').value);
      }
     }
   </script>
   @else
   <script>
  window.onload = function(){
    window.location.replace('/');
  }
</script>
   @endif
@endsection