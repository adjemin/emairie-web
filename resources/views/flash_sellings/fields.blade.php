<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::text('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Flash Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('flash_start_date', 'Flash Start Date:') !!}
    {!! Form::text('flash_start_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Flash End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('flash_end_date', 'Flash End Date:') !!}
    {!! Form::text('flash_end_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Delay Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delay', 'Delay:') !!}
    {!! Form::text('delay', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Started Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_started', 'Is Started:') !!}
    {!! Form::text('is_started', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Ended Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_ended', 'Is Ended:') !!}
    {!! Form::text('is_ended', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('flashSellings.index') !!}" class="btn btn-default">Cancel</a>
</div>
