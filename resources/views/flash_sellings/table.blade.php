<div class="table-responsive-sm">
    <table class="table table-striped" id="flashSellings-table">
        <thead>
            <th>Product Id</th>
        <th>Flash Start Date</th>
        <th>Flash End Date</th>
        <th>Delay</th>
        <th>Is Started</th>
        <th>Is Ended</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($flashSellings as $flashSelling)
            <tr>
                <td>{!! $flashSelling->product_id !!}</td>
            <td>{!! $flashSelling->flash_start_date !!}</td>
            <td>{!! $flashSelling->flash_end_date !!}</td>
            <td>{!! $flashSelling->delay !!}</td>
            <td>{!! $flashSelling->is_started !!}</td>
            <td>{!! $flashSelling->is_ended !!}</td>
                <td>
                    {!! Form::open(['route' => ['flashSellings.destroy', $flashSelling->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('flashSellings.show', [$flashSelling->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('flashSellings.edit', [$flashSelling->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>