<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $flashSelling->product_id !!}</p>
</div>

<!-- Flash Start Date Field -->
<div class="form-group">
    {!! Form::label('flash_start_date', 'Flash Start Date:') !!}
    <p>{!! $flashSelling->flash_start_date !!}</p>
</div>

<!-- Flash End Date Field -->
<div class="form-group">
    {!! Form::label('flash_end_date', 'Flash End Date:') !!}
    <p>{!! $flashSelling->flash_end_date !!}</p>
</div>

<!-- Delay Field -->
<div class="form-group">
    {!! Form::label('delay', 'Delay:') !!}
    <p>{!! $flashSelling->delay !!}</p>
</div>

<!-- Is Started Field -->
<div class="form-group">
    {!! Form::label('is_started', 'Is Started:') !!}
    <p>{!! $flashSelling->is_started !!}</p>
</div>

<!-- Is Ended Field -->
<div class="form-group">
    {!! Form::label('is_ended', 'Is Ended:') !!}
    <p>{!! $flashSelling->is_ended !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $flashSelling->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $flashSelling->updated_at !!}</p>
</div>

