<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Adjemin est une marketplace pour vendre, acheter, discuter gratuitement depuis votre mobile grâce à l'application adjemin disponible sur Google Play Store, chez Adjemin tout le monde est vendeur et acheteur grâce à son téléphone portable.">

<link rel="stylesheet" href="{{asset('front/dist/style.css')}}">
<link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<!-- Bootstrap JS -->
<script src="{{asset('js/app.js')}}"></script>

<!-- sweet -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- jQuery -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="icon" href="{{asset('front/dist/images/icon.ico')}}" type="image/ico"/>


  <title>Adjemin</title>
<style>
div.ui-slider-range.ui-widget-header {
background: #0000ff;
}
body, html{height:100%;}
#main{
min-height:100%;
margin:0 auto;
position:relative;

}
footer{
position:absolute;
bottom:0;
width:100%;
padding-top:50px;
height:50px;
}
</style>
</head>
<body>
  @if(session('clef') != NULL)
  <div id="main">
<!-- Content here -->
<!-- Header -->
@include('layoutFront.frontHeader')
<!-- Header  end-->
<!--modal-->
    <div class="container">
    <div class="modal fade hide" id="formulaire">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Renseignement supplémentaire</h4>              
            <!--<button type="button" class="close" data-dismiss="modal">-->
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body row">
            <form class="col" action="{{route('customers.store')}}" role="form" enctype="multipart/form-data" method="post">
                @csrf
                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="nom" class="form-control-label">Nom</label>
                <input type="text" required class="form-control" name ="last_name" id="nom" placeholder="Nom">
                @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="premons" class="form-control-label">Prénoms</label>
                <input type="text" required class="form-control" name ="first_name" id="prenoms" placeholder="Prénoms">
                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="form-control-label">Mail</label>
                <input type="email" class="form-control" name ="email" id="email" placeholder="123@xyz">
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                <label for="photo" class="form-control-label">photo</label>
                <input type="file" class="form-control" name ="photo" id="photo">
                @if ($errors->has('photo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                @endif
              </div>
              <div class="form-group">
                <label for="number" class="form-control-label">numéro</label>
                @foreach($cust as $custs)
                <input type="text" class="form-control" readonly name ="number" id="number" value="{{$custs->phone}}">
                @endforeach
              </div>
              <button type="submit" class="btn btn-primary pull-right" style="border-radius:10px; border: 0">Valider</button>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
<!-- end modal -->

   <!-- Category slide -->
 @include('layoutFront.FrontCategory')
  
            <!-- Category slide end -->
     <div class="container" style="margin-bottom:10px;">
         <div class="row">
             @include('layoutFront.frontSidebar')
             @include('flash-message')
             @yield('all_product')
           </div>
     </div>
     @include('layoutFront.frontFooter')
  </div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<!-- script modal -->
  @if(session('clef') != NULL)
    @foreach($cust as $customer)
      @if($customer->name == null)
        <script>
          $('#formulaire').modal({
              backdrop: 'static',
              keyboard: true, 
              show: true
            });
        </script>
      @endif
    @endforeach
  @endif
  


<!-- prix max -->
@if($maxi)
<input type="hidden" name="maximum" id="maximum" value="{{$maxi}}">
@endif

<!-- sweet -->
<script>
  $(document).ready(function(){
$.noConflict()
$('#rangeslider').slider({
  range: true,
  min: 0,
  max: document.getElementById('maximum').value,
  values: [ 0, document.getElementById('maximum').value ],
  slide: function( event, ui ) {
    $('#rangeval').html(ui.values[0]+" - "+ui.values[1]);

    document.getElementById('min').value=ui.values[0];
    document.getElementById('max').value=ui.values[1];

  }
});
});
</script>
@else
<script>
  window.onload = function(){
    window.location.replace('/');
  }
</script>
@endif
</body>
</html>