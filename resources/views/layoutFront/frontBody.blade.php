@extends('layoutFront.app')
@section('all_product')
<div class="col-md-9">
  <div class="card">
      <!-- card body1 --->
      <div class="card-body">
              <div class="card-deck">
                        <div class="row">
                          @foreach($prod as $prods)
                              <div class="col-md-3 hover-fade">
                                <form style="border:1px solid #e9e9e9;border-radius:10px;" method="get" action="{{route('products.index')}}" role="form">
                                  
                                  <a href="{{route('products.index',['identif'=>$prods->id])}}" style="text-decoration:none;">
                                    <img style="border-left:1px solid #e9e9e9;border-top-left-radius:10px !important;border-top-right-radius:10px !important;" src="{{$prods->cover_url}}" class="card-img-top img-fluid">
                                </a>
                                <div class="col">
                                    <input type="hidden" name="identif" value="{{$prods->id}}">
                                    <h5 class="card-title"> 
                                      <button type="submit" style="background: none; border: none;">
                                       <p style="text-align:left;float:left;font-size:15px;">
                                        <b>{{$prods->title}} </b>
                                       </p>
                                      </button>
                                    </h5>
                                         <p style="font-size:13px;">
                                          <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
                                          {{$prods->location_name}}
                                         </p>
                                          <b style="float:left">
                                            <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>{{$prods->price}} CFA</strong></button>
                                        </b>
                                      <br><br>
                                  </div> 
                                  <div class="col">
                                          <p>
                                              @if($prods->customer->photo_url != NULL)
                                              <img class="rounded-circle smal-avatar" src="{{asset($prods->customer->photo_url)}}" alt="">
                                              @else
                                              <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                                              @endif
                                              <b class="leftMargin"><br>{{$prods->customer->name}}</b>
                                          </p> 
                                  </div>
                                </form>
                              </div>
                          @endforeach 
                          </div>
                              
            </div>
      </div>
      <!--End card body2 --->
  </div><br>
</div>
@endsection