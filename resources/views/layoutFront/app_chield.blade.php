<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="La marketplace pour vendre, acheter, discuter gratuitement en côte d'ivoire c'est Adjemin,Rassurez vous de la qualité de votre article avant confirmation définitive de votre commande.">


<link rel="stylesheet" href="{{asset('front/dist/style.css')}}">
<link rel="stylesheet" href="{{asset('front/dist/bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<!-- Bootstrap JS -->
<script src="{{asset('js/app.js')}}"></script>

<!-- sweet -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- jQuery -->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="icon" href="{{asset('front/dist/images/icon.ico')}}" type="image/ico" />

    <title>Adjemin</title>
  </style>
  </head>
  <body>
  <!-- Content here -->
  <!-- Header -->
  <style>
    div.dropdown {
color: #555;
margin: 3px -22px 0 0;
width: 143px;
position: relative;
height: 17px;
text-align:left;
}
div.submenu
{
position: absolute;
z-index: 100;
width: 135px;
display: none;
margin-left: 10px;
padding: 40px 0 5px;
border-radius: 6px;
}
.dropdown  li a {
color: #555555;
display: block;
font-family: arial;
font-weight: bold;
padding: 6px 15px;
cursor: pointer;
text-decoration:none;
}
.dropdown li a:hover{
background:#155FB0;
color: #FFFFFF;
text-decoration: none;
}
a.account {
font-size: 11px;
line-height: 16px;
color: #555;
position: absolute;
z-index: 110;
display: block;
padding: 1px 0 0 2px;
height: 28px;
width: 150px;
margin: -11px 0 0 -10px;
text-decoration: none;
cursor:pointer;
}
.root
{
list-style: none;
font-size: 11px;
box-shadow: 0 2px 8px rgba(0, 0, 0, 0.45);
background: #fff;
padding: 10px 0 10px 0;
border-radius: 5px 5px 5px 5px;
margin: 21px 0 0 0;
}
.root:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
border-bottom-color: #ffffff;
position: absolute;
color: #ffffff;
top: 54px;
left: 17px;
}
.profile-circle {
width: 45px;
height: 44px;
background-size: cover;
background-repeat: no-repeat;
background-position: center center;
-webkit-border-radius: 99em;
-moz-border-radius: 99em;
border-radius: 99em;
border: 2px solid #eee;
box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);
vertical-align: middle;
}
.box {
  transition: box-shadow .3s;
  background: #fff;
}
.box:hover {
  box-shadow:0 1px 6px 0 rgba(32,33,36,0.28);
}
body, html{height:100%;}
#main{
min-height:100%;
margin:0 auto;
position:relative;

}
footer{
position:absolute;
bottom:0;
width:100%;
padding-top:50px;
height:50px;
}
</style>
   
      
  <!-- script firebase déconnexion -->
  <script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
  <script type="text/javascript">
// Initialize Firebase
var config = {
    apiKey: "AIzaSyAc91Vbr6ztfUxT9eDHejOP6QU-wQhStTo",
    authDomain: "adjemin-web.firebaseapp.com",
    databaseURL: "https://adjemin-web.firebaseio.com",
    projectId: "adjemin-web",
    storageBucket: "adjemin-web.appspot.com",
    messagingSenderId: "14203483930",
    appId: "1:14203483930:web:9dd798068dfe8c57b45ea3",
    measurementId: "G-DW262R5WYE"
};
firebase.initializeApp(config);

var database = firebase.database();
/**
* Set up UI event listeners and registering Firebase auth listeners.
*/
window.onload = function() {
document.getElementById('sign-out-button').addEventListener('click', onSignOutClick);
function onSignOutClick() {
firebase.auth().signOut();
window.location.href='deconnexion';
}
}

</script>


<!-- recherche -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready( function() {
$( "#cherche" ).autocomplete({
  source: function(request,reponse){
    $.ajax({
        url: "{{url('recherche')}}",
        data: { term : request.term},
        dataType: "json",
        success: function(data){
            var resp = $.map(data, function(obj){
                return obj.title;
                });

            reponse(resp);
        }

    });
  },
  minLength:1
});
} );
</script>
<div id="main">
  <header class="section-header">
    <section class="header-top-light border-bottom">
      <div class="col" style="border:1px solid #e9e9e9">
        <nav class="d-flex flex-column flex-md-row">
          <div class="col">
            <ul class="nav">
              <li><a href="#" class="nav-link px-2"> <i class="fab fa-facebook"></i> </a></li>
              <li><a href="#" class="nav-link px-2"> <i class="fab fa-instagram"></i> </a></li>
              <li><a href="#" class="nav-link px-2"> <i class="fab fa-twitter"></i> </a></li>
            </ul>
          </div>
          <div class="col">
            <ul class="nav" style="float:right !important">
              <li class="nav-item"><a href="#" class="nav-link" style="color:inherit"> Aide </a></li>
              <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit"> Devise </a>
                <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="dropdown-item" href="#">XOF</a></li>
                  <li><a class="dropdown-item" href="#">EUR</a></li>
                  <li><a class="dropdown-item" href="#">AED</a></li>
                  <li><a class="dropdown-item" href="#">RUBL </a></li>
                      </ul>
              </li>
                  <li class="nav-item dropdown"><a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" style="color:inherit">   Langue </a>
                  <ul class="dropdown-menu dropdown-menu-right">     
                  <li><a class="dropdown-item" href="#">Francais</a></li>
                  <li><a class="dropdown-item" href="#">Anglais</a></li>
                  <li><a class="dropdown-item" href="#">Arabe</a></li>
                      </ul>
                  </li>
                </ul> <!-- navbar-nav.// -->
          </div>
        </nav>
      </div>
      <div class="container">
        <nav class="d-flex flex-column flex-md-row">
         <div class="col-1" style="margin-top:5px;margin-bottom:5px;">
           <div class="brand-wrap">
            @if(session('clef') == NULL)
            <img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}" alt="">
                   @else
             <a href="{{url('home')}}"><img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}" alt=""></a>
             @endif
         </div> <!-- brand-wrap.// -->
         </div>
         <div class="col" style="margin-top:23px;">
           <form action="{{url('resultat')}}" class="search">
            @if(session('clef') == NULL)
            @else
             <div class="input-group w-100">
                 <input type="text" id="cherche" name="result" class="form-control" style="width:100%;" placeholder="Rechercher">
                 <div class="input-group-append">
                   <button class="btn btn-success" type="submit">
                    <i style="color:#fff !important" class="fa fa-search"></i>
                   </button>
                 </div>
               </div>
            @endif
           </form> <!-- search-wrap .end// -->
         </div>
         <div style="margin-top:15px;margin-left:0px;">
           <div class="d-flex flex-row-reverse bd-highlight">
             <div class="p-2 bd-highlight">
                <!-- Button trigger modal -->
                <button style="border-radius:20px;margin-right:10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                  Publier un article
                  </button>
                  <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header" style="background-color:#45c06f;color:#fff">
                            <h5 class="modal-title" id="exampleModalLabel">
                              Applications mobile adjemin
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <p style="border-bottom:gray">
                              <strong>
                                Pour publier un article, merci de téléchargez l'une de nos applications mobiles Adjemin !
                              </strong>
                            </p>
                            <p>
                              <div class="row" style="margin-left:10px;">
                                <div class="col-sm-6">
                                  <div class="card" style="border:none !important;">
                                    <div class="card-body">

                                        <a href="#"> <img style="border-radius:10px;"  src="{{asset('front/dist/images/ios.png')}}" alt="" height="40"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="card" style="border:none !important;">
                                    <div class="card-body">
                                        <a href="#"> <img style="border-radius:10px;"  src="{{asset('front/dist/images/android.png')}}" alt="" height="40"> </a>   
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non</button>
                            <button style="margin-left:347px;" type="button" class="btn btn-primary">Oui</button>
                          </div>
                        </div>
                      </div>
                    </div>
             </div>
             <div class="p-2 bd-highlight"><a class=" rounded-circle btn btn-primary">
               <i style="color:#fff;font-size:20px;" class="fa fa-bell"></i>
              </a>
             </div>
             @if(session('clef') !=NULL)
             <div class="p-2 bd-highlight" style="margin-left:1rem;width:200px !important;margin-right:2rem">
               <div class="dropdown">
                 <a href="#" class="account" style="width:250px !important">
                 @foreach($cust as $custs)
                      @if($custs->photo_url != NULL)
                  <img src="{{asset($custs->photo_url)}}" class="profile-circle"/>
                      @else
                  <img src="{{asset('front/dist/images/avatar.png')}}" class="profile-circle"/>
                      @endif
                  <i class="fa fa-caret-down" style="font-size:15px;color:#000"></i>
                     &nbsp; {{$custs->name}}                    
                  @endforeach
                 </a>
                 <div class="submenu" style="display: none; ">
                 <ul class="root">
                 @foreach($cust as $custs)
                 <li >
                    <a href="{{url('moi',['id'=> Crypt::encrypt($custs->id)])}}" >Mon Profil</a>
                 </li>
                 <li >
                 <a href="{{url('commande',['id'=> Crypt::encrypt($custs->id)])}}" >Mon Compte</a>
                 </li>
                 @endforeach
                 <li >
                  
                   <a  id="sign-out-button">Déconnexion</a>
                   
                 </li>
                 </ul>
                 </div>
                 </div>
             </div>
             @endif
           </div>
         </div>
       </nav>
     </div>
    </section>
    <div class="alert alert-primary" role="alert" style="border-radius:0px;border:none !important;background-color:#0e2b3b !important">
      <div class="row">
        <div class="col-1"></div>
        <div class="col-sm-7">
          <h3>
            <small style="color:#fff !important" class="text-muted">Télécharger notre application pour beneficier de plus de bonus</small>
          </h3>
        </div>
        <div class="col-sm-3">
          <a href="#"> <img style="border-radius:10px;"  src="{{asset('front/dist/images/ios.png')}}" alt="" height="40"></a>
          <a href="#"> <img style="border-radius:10px;"  src="{{asset('front/dist/images/android.png')}}" alt="" height="40"> </a>   
        </div> 
        <div class="col-1"></div>
      </div>         
    </div>
    </header> <!-- section-header.// -->
    <!-- Header  end-->
  <div class="container" style="margin-bottom:10px;">
    <div class="row">
      @include('flash-message')
    @yield('detailProduit')
    </div>
    </div>
    @include('layoutFront.frontFooter')
</div>
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script type="text/javascript" src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1565190285/Scripts/xzoom.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1565190284/Scripts/xzoom.css" media="all" />
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


    <!-- recherche -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function()
{
$(".account").click(function()
{
var X=$(this).attr('id');
if(X==1)
{
$(".submenu").hide();
$(this).attr('id', '0'); 
}
else
{
$(".submenu").show();
$(this).attr('id', '1');
}
});
$(".submenu").mouseup(function()
{
return false
});
$(".account").mouseup(function()
{
return false
});
$(document).mouseup(function()
{
$(".submenu").hide();
$(".account").attr('id', '');
});
});
  </script>


  </body>
</html>