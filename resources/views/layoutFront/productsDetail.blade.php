@extends('layoutFront.app_chield')
@section('detailProduit')
<!-- Mapbox Script et Css -->
<script src='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.js'></script>
<link href='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css' rel='stylesheet'/>
<script type='text/javascript' src="{{asset('front/dist/js/detailsjquery.min.js')}}"></script>

<script>
  (function ($) {
    $(document).ready(function() {
    $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 400, title: true, tint: '#333', Xoffset: 15});
    $('.xzoom2, .xzoom-gallery2').xzoom({position: '#xzoom2-id', tint: '#ffa200'});
    $('.xzoom3, .xzoom-gallery3').xzoom({position: 'lens', lensShape: 'circle', sourceClass: 'xzoom-hidden'});
    $('.xzoom4, .xzoom-gallery4').xzoom({tint: '#006699', Xoffset: 15});
    $('.xzoom5, .xzoom-gallery5').xzoom({tint: '#006699', Xoffset: 15});
    
    //Integration with hammer.js
    var isTouchSupported = 'ontouchstart' in window;
    
    if (isTouchSupported) {
    //If touch adjemin product
    $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function(){
    var xzoom = $(this).data('xzoom');
    xzoom.eventunbind();
    });

    $('.xzoom4').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    var s = 1, ls;
    
    xzoom.eventmove = function(element) {
    element.hammer().on('drag', function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    xzoom.movezoom(event);
    event.gesture.preventDefault();
    });
    }
    
    var counter = 0;
    xzoom.eventclick = function(element) {
    element.hammer().on('tap', function() {
    counter++;
    if (counter == 1) setTimeout(openfancy,300);
    event.gesture.preventDefault();
    });
    }
    
    function openfancy() {
    if (counter == 2) {
    xzoom.closezoom();
    $.fancybox.open(xzoom.gallery().cgallery);
    } else {
    xzoom.closezoom();
    }
    counter = 0;
    }
    xzoom.openzoom(event);
    });
    });
    
    $('.xzoom5').each(function() {
    var xzoom = $(this).data('xzoom');
    $(this).hammer().on("tap", function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    var s = 1, ls;
    
    xzoom.eventmove = function(element) {
    element.hammer().on('drag', function(event) {
    event.pageX = event.gesture.center.pageX;
    event.pageY = event.gesture.center.pageY;
    xzoom.movezoom(event);
    event.gesture.preventDefault();
    });
    }
    
    var counter = 0;
    xzoom.eventclick = function(element) {
    element.hammer().on('tap', function() {
    counter++;
    if (counter == 1) setTimeout(openmagnific,300);
    event.gesture.preventDefault();
    });
    }
    
    function openmagnific() {
    if (counter == 2) {
    xzoom.closezoom();
    var gallery = xzoom.gallery().cgallery;
    var i, images = new Array();
    for (i in gallery) {
    images[i] = {src: gallery[i]};
    }
    $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
    } else {
    xzoom.closezoom();
    }
    counter = 0;
    }
    xzoom.openzoom(event);
    });
    });
    
    } else {
    //If not touch adjemin product
    
    //Integration with fancybox plugin
    $('#xzoom-fancy').bind('click', function(event) {
    var xzoom = $(this).data('xzoom');
    xzoom.closezoom();
    $.fancybox.open(xzoom.gallery().cgallery, {padding: 0, helpers: {overlay: {locked: false}}});
    event.preventDefault();
    });
    
    //Integration with magnific popup plugin
    $('#xzoom-magnific').bind('click', function(event) {
    var xzoom = $(this).data('xzoom');
    xzoom.closezoom();
    var gallery = xzoom.gallery().cgallery;
    var i, images = new Array();
    for (i in gallery) {
    images[i] = {src: gallery[i]};
    }
    $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
    event.preventDefault();
    });
    }
    });
    })(jQuery);
</script>
<div class="col-md-12" style="border:1px solid #e9e9e9;padding:15px;">
  <div class="card-deck">
    <div>
      <div>
        <div class="container d-flex justify-content-center">
          <section id="default" class="padding-top0">
              <div class="row">
                  <div class="large-5 column" style="padding-left:10px;border:1px solid #e9e9e9;margin-left:15px;">
                      <div class="xzoom-container">
                        <div class="slider-banner-owl owl-carousel owl-theme">
                            <div id="carousel1_indicator" class="carousel" style="margin-top:10px;">
                                <!--<ol class="carousel-indicators">
                                  <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>
                                  <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="1"></li>
                                  <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="2"></li>
                                  <li style="background-color:#0e2b3b" data-target="#carousel1_indicator" data-slide-to="3"></li>
  
                                </ol>-->
                                <div class="carousel-inner" style="border:1px solid #e9e9e9;margin-left:15px;">
                                  @foreach($produit as $produits)
                                  <div class="carousel-item active">
                                      <img alt="First slide" class="xzoom" src="{{$produits->cover_url}}"/>
                                  </div>
                                </div>
                                <!--<a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
                                  <span id="slide-indicatorLeft" class="carousel-control-prev-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
                                  <span id="slide-indicatorRight" class="carousel-control-next-icon" aria-hidden="true"></span>
                                  <span class="sr-only">Next</span>
                                </a>-->
                              </div>
                          </div>
                        @endforeach
                       <div class="container">
                        <div class="row" style="padding-right:20px;margin-left:15px;">
                          @foreach($produitmedia as $produitmedias)
                            <div class="col-1.5">
                              <br>
                              <a href="{{$produitmedias->url}}">
                                <img class="xzoom-gallery" width="80" src="{{$produitmedias->url}}">
                              </a>
                            </div>
                          @endforeach
                          </div>
                       </div>
                      </div>
                      <h5><i>Détails Produit</i></h5>
                        @foreach($produit as $produits)
                        <p>
                          {{$produits->description}}
                        </p>
                        @endforeach
                  </div>
              </div>
          </section>
      </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        @foreach($produit as $produits)
        <form method="get" action="{{route('orders.index')}}">
          <article class="content-body" style="margin:5px;padding-bottom:15px">
              <div class="row">
                <div class="col-sm-10">
                  <div class="card" style="border:none">
                   
                      <h5 class="card-title">
                        <strong>{{$produits->title}}</strong> 
                      </h5>
                   
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="card" style="border:none">
                   @if(session('clef') != NULL)
                      <h5 class="card-title">
                          <a class="btn" style="font-size: 15px;" id="butt">🤍</a>
                      </h5>
                    @endif
                   
                  </div>
                </div>
              </div>
              <hr>
              <!-- rating-wrap.// -->
              <dl class="row">
                <div class="col-sm-6">
                  <button style="" type="button" class="btn btn-success btn-lg">
                    <strong>
                    {{$produits->price}} CFA
                  </strong>
                  </button>

                </div>
                <dd class="col-sm-6">
                  <i style="margin-right: 15px;" class="fa fa-calendar"></i>
                  Date de publicaton: {{$produits->created_at->format('d M Y')}}
                </dd>
              </dl>
              <hr>
                 <dd style="flaot:rigth">  
                   @if($customer->photo_url != NULL)
                      <img class="rounded-circle smal-avatar" src="{{asset($customer->photo_url)}}" alt="">
                      @else
                      <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                      @endif
                     <b class="leftMargin">{{$customer->name}}</b>
                     
                 </dd>
                  <input type="hidden" id="longi" value="{{$produits->location_lng}}">
                  <input type="hidden" id="lati" value="{{$produits->location_lat}}">
                  <input type="hidden" name="fav" id="fav" >
                  @if(session('clef') != NULL)
                  @if($favoris)
                  <input type="hidden" name="favid" value="{{$favoris->id}}">
                  @endif
                  @endif
                  <p>
                     <i class="fa fa-map-marker-alt"></i> :
                     {{$produits->location_name}}
                    </p>
                
                <dd class="responsive" id='map'style='width: 300px; height: 300px;'></dd>
                <br>
               
                 <input type="hidden" name="identif" value="{{$produits->id}}">
                    <button style="float:left" class="btn btn-warning adjeminRadius">NEGOCIER</button>
                    @if(session('clef') == NULL)
                    {{Session::put('identif', $produits->id)}}
                    <a href="" style="float:right" class="btn btn-primary adjeminRadius" >COMMANDER</a>
                    @else
                    <button style="float:right" type="submit" class="btn btn-primary adjeminRadius" >COMMANDER</button>
                    @endif
              </article> <!-- product-info-aside .// -->
             </form>
              @endforeach
              <br><br>
      </div>
    </div>
  </div> <br>
  <!--<h2>Articles Similaire : </h2>
  <div class="card-deck">
    <div class="card">
      <div class="hover-fade">
        <form method="get" action="" role="form">
          <a href="" style="text-decoration:none;padding:20px;">
            <img src="{{asset('front/dist/images/choose1.jpg')}}" class="card-img-top img-fluid">
        </a>
        <div class="col">
            <input type="hidden" name="identif" value="">
            <h5 class="card-title"> 
              <button type="submit" style="background: none; border: none;">
               <p style="text-align:left;float:left;font-size:15px;">
                <b>Product title</b>
               </p>
              </button>
            </h5>
                 <p style="font-size:13px;">
                  <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
                  location name
                 </p>
                  <b style="float:left">
                    <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>price CFA</strong></button>
                </b>
              <br><br>
          </div> 
          <div class="col">
                  <p>
                      <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                      <b class="leftMargin"><br>Name user</b>
                  </p> 
          </div>
        </form>
      </div>
  </div><div class="card">
    <div class="hover-fade">
      <form method="get" action="" role="form">
        <a href="" style="text-decoration:none;padding:20px;">
          <img src="{{asset('front/dist/images/choose1.jpg')}}" class="card-img-top img-fluid">
      </a>
      <div class="col">
          <input type="hidden" name="identif" value="">
          <h5 class="card-title"> 
            <button type="submit" style="background: none; border: none;">
             <p style="text-align:left;float:left;font-size:15px;">
              <b>Product title</b>
             </p>
            </button>
          </h5>
               <p style="font-size:13px;">
                <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
                location name
               </p>
                <b style="float:left">
                  <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>price CFA</strong></button>
              </b>
            <br><br>
        </div> 
        <div class="col">
                <p>
                    <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                    <b class="leftMargin"><br>Name user</b>
                </p> 
        </div>
      </form>
    </div>
</div>
<div class="card">
  <div class="hover-fade">
    <form method="get" action="" role="form">
      <a href="" style="text-decoration:none;padding:20px;">
        <img src="{{asset('front/dist/images/choose1.jpg')}}" class="card-img-top img-fluid">
    </a>
    <div class="col">
        <input type="hidden" name="identif" value="">
        <h5 class="card-title"> 
          <button type="submit" style="background: none; border: none;">
           <p style="text-align:left;float:left;font-size:15px;">
            <b>Product title</b>
           </p>
          </button>
        </h5>
             <p style="font-size:13px;">
              <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
              location name
             </p>
              <b style="float:left">
                <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>price CFA</strong></button>
            </b>
          <br><br>
      </div> 
      <div class="col">
              <p>
                  <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                  <b class="leftMargin"><br>Name user</b>
              </p> 
      </div>
    </form>
  </div>
</div>
    
    <div class="card">
        <div class="hover-fade">
          <form method="get" action="" role="form">
            <a href="" style="text-decoration:none;padding:20px;">
              <img src="{{asset('front/dist/images/choose1.jpg')}}" class="card-img-top img-fluid">
          </a>
          <div class="col">
              <input type="hidden" name="identif" value="">
              <h5 class="card-title"> 
                <button type="submit" style="background: none; border: none;">
                 <p style="text-align:left;float:left;font-size:15px;">
                  <b>Product title</b>
                 </p>
                </button>
              </h5>
                   <p style="font-size:13px;">
                    <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
                    location name
                   </p>
                    <b style="float:left">
                      <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>price CFA</strong></button>
                  </b>
                <br><br>
            </div> 
            <div class="col">
                    <p>
                        <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                        <b class="leftMargin"><br>Name user</b>
                    </p> 
            </div>
          </form>
        </div>
    </div>
  </div>-->
</div>
<!--script appel option Mapbox -->
<script>
  mapboxgl.accessToken = 'pk.eyJ1IjoicGlpbnpvIiwiYSI6ImNrNDl1Z3JiODA4MHUzb3AzNDZyM2o2eGQifQ.AsYteoZUXW3Lk9eQ2ZwBeQ';
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [document.getElementById('longi').value,document.getElementById('lati').value],
    zoom: 15,
  });
  var marker = new mapboxgl.Marker() // initialize a new marker
  .setLngLat([document.getElementById('longi').value,document.getElementById('lati').value]) // Marker [lng, lat] coordinates
  .addTo(map);
</script>

<!-- favoris -->
@if(session('clef') != NULL)
@if($favoris)
  <script>
    window.onload =function(){
var whiteHeart = '\ud83e\udd0d';
var blackHeart = '\u2764\ufe0f';
var button = document.getElementById('butt');
      button.textContent = blackHeart;
    }
  </script>
@endif
@endif

<script>
  const whiteHeart = '\ud83e\udd0d';
const blackHeart = '\u2764\ufe0f';
const button = document.getElementById('butt');
button.addEventListener('click', toggle);

function toggle() {
  const like = button.textContent;
  if(like==whiteHeart) {
    button.textContent = blackHeart;
    document.getElementById('fav').value=1;
  } else {
    button.textContent = whiteHeart;
    document.getElementById('fav').value=0;
  }
}
</script>
<!-- end favoris -->
@endsection
