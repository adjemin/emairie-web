@extends('layoutFront.app')
@section('all_product')
<div class="col-md-9">
  <div class="card">
      <!-- card body1 --->
      <div class="card-body">
		<div style="margin-top:15px;" class="alert alert-danger" role="alert">
			<h4 class="alert-heading">
				<strong>Aucun produit ne correspond aux termes de votre recherche !</strong>
			</h4>
			<hr>
			<p class="mb-0">
				<ul>
					<li>Vérifiez bien l’orthographe des termes de votre recherche.</li>
					<li>Essayez aussi d'autres mots pour affiner votre recherche.</li>
				</ul>
			</p>
		  </div>
      </div>
      <!--End card body2 --->
  </div>
</div>
@endsection