<style>	
	#myBtn {
	  display: none;
	  position: fixed;
	  bottom: 20px;
	  right: 30px;
	  z-index: 99;
	  font-size: 18px;
	  border: none;
	  outline: none;
	  background-color: #0e2b3b;
	  color: white;
	  cursor: pointer;
	  padding: 15px;
	  border-radius: 4px;
	}
	#myBtn:hover {
	  background-color: #45c06f;
	}
	</style>
<footer>
	<div class="col" style="background-color:#0e2b3b;color:#ffff;padding-top:15px;padding-bottom:15px;">
		<section class="footer-top padding-y">
			<div class="row">
				<aside class="col-md-4">
					<article class="mr-3">
                        <img class="adjemin-logo" src="{{asset('front/dist/images/logo.png')}}">
						<p class="mt-3">
							Adjemin est une marketplace pour vendre, acheter, discuter gratuitement depuis votre mobile grâce à l'application adjemin disponible sur Google Play Store.
							içi, tout le monde est vendeur et acheteur grâce à son téléphone portable. 
						</p>
						<div>
						    <a class="btn btn-icon btn-light" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
						    <a class="btn btn-icon btn-light" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
						    <a class="btn btn-icon btn-light" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube"></i></a>
						    <a class="btn btn-icon btn-light" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
						</div>
					</article>
				</aside>
				<aside class="col-sm-3 col-md-2">
					<h6 class="title" style="text-decoration:underline">A propos</h6>
				
				</aside>
				<aside class="col-sm-3 col-md-2">
					<h6 class="title" style="text-decoration:underline">Catégorie de produits</h6>
					<ul class="list-unstyled">
							<li> <a href="#" style="color:honeydew">Agriculture</a></li>
							<li> <a href="#" style="color:honeydew">Antiquités</a></li>
							<li> <a href="#" style="color:honeydew">Art et Bricolage</a></li>
							<li> <a href="#" style="color:honeydew">Accessoires de téléphone</a></li>
							<li> <a href="#" style="color:honeydew">Bateau et marine</a></li>
							<li> <a href="#" style="color:honeydew">Beauté et Santé</a></li>
							<li> <a href="#" style="color:honeydew">Bébé</a></li>
							<li> <a href="#" style="color:honeydew">Bijoux et Accessoires</a></li>
							<li> <a href="#" style="color:honeydew">Boisson</a></li>
						</ul>
				</aside>
				<button style="border:1px solid #fff !important" class="btn btn-success" onclick="topFunction()" id="myBtn" title="Remonter">
					<i style="color:#fff" class="fa fa-arrow-up"></i>
				</button>
				<aside class="col-sm-3  col-md-2">
					<h6 class="title" style="text-decoration:underline">Partenaire officiel</h6>
					<img src="{{asset('front/dist/images/mtn.png')}}" height="60">
				</aside>
				<aside class="col-sm-2  col-md-2">
					<h6 class="title" style="text-decoration:underline">Notre application mobile</h6>
					<a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/ios.png')}}" height="40"></a>
					<a href="#" class="d-block mb-2"><img src="{{asset('front/dist/images/android.png')}}" height="40"></a>
				</aside>
				<script>
					//Get the button
					var mybutton = document.getElementById("myBtn");
					
					// When the user scrolls down 20px from the top of the document, show the button
					window.onscroll = function() {scrollFunction()};
					
					function scrollFunction() {
					  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
						mybutton.style.display = "block";
					  } else {
						mybutton.style.display = "none";
					  }
					}
					
					// When the user clicks on the button, scroll to the top of the document
					function topFunction() {
					  document.body.scrollTop = 0;
					  document.documentElement.scrollTop = 0;
					}
					</script>
            </div> <!-- row.// -->
        </section>	<!-- footer-top.// -->	
    </div><!-- //container -->
    <div class="col" style="background-color:#0a4c71f5">
            <div class="col" style="color:#fff;flaot:right;text-align:center">
					<div>
						<a style="color: white;" href="https://adjemin.com">
						<span>&copy; 2019 Adjemin.</span>
						</a>
					</div>
            </div>

      </div>
</footer>