@extends('layoutFront.app_chield')

@section('detailProduit')
<div class="container">
	<div class="row">
		<div class="card">
			<!-- card body1 --->
			<div class="card-body">
					<div class="card-deck">
							  <div class="row">
								@foreach($result as $results)
									<div class="col-md-3 hover-fade">
									  <form style="border:1px solid #e9e9e9;border-radius:10px;" method="get" action="{{route('products.index')}}" role="form">
										
										<a href="#" style="text-decoration:none;padding-top:10px;">
										  <input type="image" name="submitbutton" class="card-img-top effet" src="{{$results->cover_url}}">
									  </a>
									  <div class="col">
										  <input type="hidden" name="identif" value="{{$results->id}}">
										  <h5 class="card-title"> 
											<button type="submit" style="background: none; border: none;">
											 <p style="text-align:left;float:left;font-size:15px;">
											  <b>{{$results->title}} </b>
											 </p>
											</button>
										  </h5>
											   <p style="font-size:13px;">
												<i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
												{{$results->location_name}}
											   </p>
												<b style="float:left">
												  <button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>{{$results->price}} CFA</strong></button>
											  </b>
											<br><br>
										</div> 
										<div class="col">
												<p>
													<img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
													<b class="leftMargin"><br>{{$results->customer->name}}</b>
												</p> 
										</div>
									  </form>
									</div>
								@endforeach 
								</div>	
				  </div>
			</div>
			<!--End card body2 --->
		</div>
	</div>
</div>

@endsection