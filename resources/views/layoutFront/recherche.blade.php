@extends('layoutFront.app')
@section('all_product')
<div class="col-md-9">
  <div class="card">
      <!-- card body1 --->
      <div class="card-body">
              <div class="card-deck">
                        <div class="row">
							@foreach($result as $results)
                              <div class="col-md-3 hover-fade">
								<form style="border:1px solid #e9e9e9;border-radius:10px;" method="get" action="{{route('products.index')}}" role="form">
							
							  <a href="{{route('products.index',['identif'=>$results->id])}}" style="text-decoration:none;">
								<img src="{{$results->cover_url}}" style="border-left:1px solid #e9e9e9;border-top-left-radius:10px !important;border-top-right-radius:10px !important;" class="card-img-top img-fluid">
							</a>
                               
							  <div class="col">
								<input type="hidden" name="identif" value="{{$results->id}}">
								<h5 class="card-title"> 
								  <button type="submit" style="background: none; border: none;">
								   <p style="text-align:left;float:left;font-size:15px;">
									<b>{{$results->title}} </b>
								   </p>
								  </button>
								</h5>
									 <p style="font-size:13px;">
									  <i style="margin-top:15px;" class="fa fa-map-marker-alt"></i>
									  {{$results->location_name}}
									 </p>
									  <b style="float:left">
										<button type="submit" class="col btn btn-success btn-sm adjeminRadius"><strong>{{$results->price}} CFA</strong></button>
									</b>
								  <br><br>
							  </div> 
							  <div class="col">
									  <p>
										  @if($results->customer->photo_url != NULL)
                                          <img class="rounded-circle smal-avatar" src="{{asset($results->customer->photo_url)}}" alt="">
                                          @else
                                          <img class="rounded-circle smal-avatar" src="{{asset('front/dist/images/avatar.png')}}" alt="">
                                          @endif
										  <b class="leftMargin"><br>{{$results->customer->name}}</b>
									  </p> 
							  </div>
                                </form>
                              </div>
                          @endforeach 
                          </div>
                              
            </div>
      </div>
      <!--End card body2 --->
  </div><br>
</div>
@endsection