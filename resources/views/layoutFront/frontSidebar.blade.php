<style>
    div.ui-slider-range.ui-widget-header {
  background: #0e2b3b;
  }
  </style>
<div class="col">
      <div class="card">
          <div class="row">
              <div class="card-body">
                  <h5 class="card-title">Filtrer par prix</h5>
                  <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" id="customCheck1">
                      <label class="custom-control-label" for="customCheck1"> 
                        <a style="text-decoration:none" href="{{('recent')}}"> <b style="color:#000">Plus récent</b></a> </label>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                        <label class="custom-control-label" for="customCheck2"> 
                          <a style="text-decoration:none" href="{{('vieux')}}"><b style="color:#000">Plus ancien</b></a> </label>
                      </div>
                      <div class="custom-control custom-checkbox">
                          <i class="fa fa-arrow-down"></i> 
                          <a style="text-decoration:none" href="{{url('prixbas')}}"><b style="color:#000">Prix Bas</b></a>
                      </div>
                      <div class="custom-control custom-checkbox">
                          <i class="fa fa-arrow-up"></i> 
                          <a style="text-decoration:none" href="{{url('prixhaut')}}"><b style="color:#000;">Prix Haut</b></a>
                      </div>
              </div>
          </div>
      </div><br>
      <div class="card">
        <div class="card-body">
            <h5 class="card-title">Filtrer par prix</h5>
            
            <form action="{{url('prix')}}">
                <div id="rangedval">
                   @if($maxi)<span class="custom-range" id="rangeval">0 - {{$maxi}}</span>
                  @endif
                </div>
                <div id="rangeslider"></div> 
                <br>
            
            <div class="form-row">
            <div class="form-group col-md-6">
              <label>Min</label>
              <input class="form-control" name="min" readonly placeholder="0" value="0" id="min" type="text">
            </div>
            <div class="form-group text-right col-md-6">
              <label>Max</label>
              <input class="form-control" name="max" readonly  id="max" value="{{$maxi}}" placeholder="{{$maxi}}" type="text">
            </div>
            </div> <!-- form-row.// -->
            <button type="submit" class="btn btn-block btn-primary">Rechercher</button>
            </form>
        </div>
    </div> <br><!-- card.// -->
          <!-- card.// -->
</div>
