
  <div class="card" style="border-radius:0px;margin-bottom:20px;border-left:none !important;border-right:none !important;">
    <div style="text-align:center;margin-top:8px;" class="col-md-12">
      @foreach($categorie as $categories)
      <a href="{{url('categorie',['cat'=>$categories->id])}}" class="btn" style="padding:1px !important">
        <button style="margin-top:6px;border: 1px solid #ccc;border-radius:20px;background:white !important;color:#000" type="button" class="box btn btn-secondary">
          {{$categories->name}}
        </button>
      </a>
    @endforeach
    <strong style="padding-left:none !important;margin-left:20px;text-align:center;float:left;margin-top:8px">
      {{$categorie->links()}}
    </strong>
    </div>
   </div>