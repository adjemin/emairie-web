@extends('layoutFront.app_chield')
@section('detailProduit')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx2lT5f7ZnPnXz1RBblsdTvFBM3H0Svhw&amp;libraries=places"></script>
<div class="container">
  <div class="row">
      <div class="col-sm-12" style="font-family: montserrat;margin-top:15px;margin-bottom:15px;">
        <div class="card">
          <h5 class="card-header text-left" style="border:1px solid #f3f3f3;background-color:#fff;">
           <strong> Détail de la localisation</strong>
          </h5>
          <div class="card-body">
            <form method="post" action="{{route('deliveryAddresses.store')}}" role='form'>
              @csrf
           <div class="form-group">
            <label for="Adresse" style="float:left">Adresse</label>
            <input type="text" id="autocomplete_search" onchange="show();" name="autocomplete_search" class="form-control inputColor" placeholder="Adresse">
           </div>
           <input type="text" id="lat" name="lat">
            <input type="text" id="long" name="long">
            <input type="hidden" name="type" value="{{$type}}">
           <div style="display: none;" id="elements">
            <div class="form-group">
              <label for="batiment" style="float:left;margin-top:10px;">Type de Bâtiment:</label>
              <select class="form-control inputColor" id="batiment" name="batiment">
                <option>Immeuble</option>
                <option>Villa</option>
              </select>
            </div>
           
            <div class="form-group">
              <label for="appart/villa" style="float:left;">Numéro Appart/Villa</label>
              <input type="text" name="num" class="form-control inputColor" placeholder="Appart/Villa">
             </div>
            
             <div class="form-group">
              <label for="Adresse" style="float:left;">Donnez un lieu comme repère</label>
              <input type="text" name="repere" class="form-control inputColor" placeholder="Adresse">
             </div>
            
            <div class="form-group">
              <label for="comment" style="float:left;margin-top:10px;">Adresse Suplémentaire:</label>
              <textarea class="form-control inputColor" name="supplement" rows="5" id="comment"></textarea>
            </div>

           <br><button type="submit" class="btn btn-primary btn-block">Valider</button>
          </div>
          </form>
          </div>
        </div>
      </div>
  </div>
  </div>

  <!-- script autocomplete -->
  <script>
  google.maps.event.addDomListener(window, 'load', initialize);
    function initialize() {
      var input = document.getElementById('autocomplete_search');
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#long').val(place.geometry['location'].lng());
    });
  }
</script>

  <!-- affichage des elements apres saisie -->
  <script>
    function show(){
      document.getElementById('elements').style.display='block';
    }
  </script>

  @endsection