<div class="table-responsive-sm">
    <table class="table table-striped" id="notificationTypes-table">
        <thead>
            <th>Name</th>
        <th>Slug</th>
        <th>Image</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($notificationTypes as $notificationType)
            <tr>
                <td>{!! $notificationType->name !!}</td>
            <td>{!! $notificationType->slug !!}</td>
            <td>{!! $notificationType->image !!}</td>
                <td>
                    {!! Form::open(['route' => ['notificationTypes.destroy', $notificationType->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('notificationTypes.show', [$notificationType->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('notificationTypes.edit', [$notificationType->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>