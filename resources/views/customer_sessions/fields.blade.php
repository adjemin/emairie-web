<!-- Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('token', 'Token:') !!}
    {!! Form::text('token', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Active Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_active', 'Is Active:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_active', 0) !!}
        {!! Form::checkbox('is_active', '1', null) !!}
    </label>
</div>


<!-- Location Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_address', 'Location Address:') !!}
    {!! Form::text('location_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_latitude', 'Location Latitude:') !!}
    {!! Form::text('location_latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_longitude', 'Location Longitude:') !!}
    {!! Form::text('location_longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Battery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('battery', 'Battery:') !!}
    {!! Form::text('battery', null, ['class' => 'form-control']) !!}
</div>

<!-- Version Field -->
<div class="form-group col-sm-6">
    {!! Form::label('version', 'Version:') !!}
    {!! Form::text('version', null, ['class' => 'form-control']) !!}
</div>

<!-- Device Field -->
<div class="form-group col-sm-6">
    {!! Form::label('device', 'Device:') !!}
    {!! Form::text('device', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    {!! Form::text('ip_address', null, ['class' => 'form-control']) !!}
</div>

<!-- Network Field -->
<div class="form-group col-sm-6">
    {!! Form::label('network', 'Network:') !!}
    {!! Form::text('network', null, ['class' => 'form-control']) !!}
</div>

<!-- Ismobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isMobile', 'Ismobile:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('isMobile', 0) !!}
        {!! Form::checkbox('isMobile', '1', null) !!}
    </label>
</div>


<!-- Istesting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isTesting', 'Istesting:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('isTesting', 0) !!}
        {!! Form::checkbox('isTesting', '1', null) !!}
    </label>
</div>


<!-- Deviceid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deviceId', 'Deviceid:') !!}
    {!! Form::text('deviceId', null, ['class' => 'form-control']) !!}
</div>

<!-- Devicesosversion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('devicesOSVersion', 'Devicesosversion:') !!}
    {!! Form::text('devicesOSVersion', null, ['class' => 'form-control']) !!}
</div>

<!-- Devicesname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('devicesName', 'Devicesname:') !!}
    {!! Form::text('devicesName', null, ['class' => 'form-control']) !!}
</div>

<!-- W Field -->
<div class="form-group col-sm-6">
    {!! Form::label('w', 'W:') !!}
    {!! Form::text('w', null, ['class' => 'form-control']) !!}
</div>

<!-- H Field -->
<div class="form-group col-sm-6">
    {!! Form::label('h', 'H:') !!}
    {!! Form::text('h', null, ['class' => 'form-control']) !!}
</div>

<!-- Ms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ms', 'Ms:') !!}
    {!! Form::text('ms', null, ['class' => 'form-control']) !!}
</div>

<!-- Idapp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('idapp', 'Idapp:') !!}
    {!! Form::text('idapp', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('customerSessions.index') !!}" class="btn btn-default">Cancel</a>
</div>
