<!-- Token Field -->
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    <p>{!! $customerSession->token !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $customerSession->customer_id !!}</p>
</div>

<!-- Is Active Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:') !!}
    <p>{!! $customerSession->is_active !!}</p>
</div>

<!-- Location Address Field -->
<div class="form-group">
    {!! Form::label('location_address', 'Location Address:') !!}
    <p>{!! $customerSession->location_address !!}</p>
</div>

<!-- Location Latitude Field -->
<div class="form-group">
    {!! Form::label('location_latitude', 'Location Latitude:') !!}
    <p>{!! $customerSession->location_latitude !!}</p>
</div>

<!-- Location Longitude Field -->
<div class="form-group">
    {!! Form::label('location_longitude', 'Location Longitude:') !!}
    <p>{!! $customerSession->location_longitude !!}</p>
</div>

<!-- Battery Field -->
<div class="form-group">
    {!! Form::label('battery', 'Battery:') !!}
    <p>{!! $customerSession->battery !!}</p>
</div>

<!-- Version Field -->
<div class="form-group">
    {!! Form::label('version', 'Version:') !!}
    <p>{!! $customerSession->version !!}</p>
</div>

<!-- Device Field -->
<div class="form-group">
    {!! Form::label('device', 'Device:') !!}
    <p>{!! $customerSession->device !!}</p>
</div>

<!-- Ip Address Field -->
<div class="form-group">
    {!! Form::label('ip_address', 'Ip Address:') !!}
    <p>{!! $customerSession->ip_address !!}</p>
</div>

<!-- Network Field -->
<div class="form-group">
    {!! Form::label('network', 'Network:') !!}
    <p>{!! $customerSession->network !!}</p>
</div>

<!-- Ismobile Field -->
<div class="form-group">
    {!! Form::label('isMobile', 'Ismobile:') !!}
    <p>{!! $customerSession->isMobile !!}</p>
</div>

<!-- Istesting Field -->
<div class="form-group">
    {!! Form::label('isTesting', 'Istesting:') !!}
    <p>{!! $customerSession->isTesting !!}</p>
</div>

<!-- Deviceid Field -->
<div class="form-group">
    {!! Form::label('deviceId', 'Deviceid:') !!}
    <p>{!! $customerSession->deviceId !!}</p>
</div>

<!-- Devicesosversion Field -->
<div class="form-group">
    {!! Form::label('devicesOSVersion', 'Devicesosversion:') !!}
    <p>{!! $customerSession->devicesOSVersion !!}</p>
</div>

<!-- Devicesname Field -->
<div class="form-group">
    {!! Form::label('devicesName', 'Devicesname:') !!}
    <p>{!! $customerSession->devicesName !!}</p>
</div>

<!-- W Field -->
<div class="form-group">
    {!! Form::label('w', 'W:') !!}
    <p>{!! $customerSession->w !!}</p>
</div>

<!-- H Field -->
<div class="form-group">
    {!! Form::label('h', 'H:') !!}
    <p>{!! $customerSession->h !!}</p>
</div>

<!-- Ms Field -->
<div class="form-group">
    {!! Form::label('ms', 'Ms:') !!}
    <p>{!! $customerSession->ms !!}</p>
</div>

<!-- Idapp Field -->
<div class="form-group">
    {!! Form::label('idapp', 'Idapp:') !!}
    <p>{!! $customerSession->idapp !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $customerSession->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $customerSession->updated_at !!}</p>
</div>

