<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $orderHistory->order_id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $orderHistory->status !!}</p>
</div>

<!-- Creator Field -->
<div class="form-group">
    {!! Form::label('creator', 'Creator:') !!}
    <p>{!! $orderHistory->creator !!}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{!! $orderHistory->creator_id !!}</p>
</div>

<!-- Creator Name Field -->
<div class="form-group">
    {!! Form::label('creator_name', 'Creator Name:') !!}
    <p>{!! $orderHistory->creator_name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $orderHistory->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $orderHistory->updated_at !!}</p>
</div>

