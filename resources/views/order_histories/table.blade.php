<div class="table-responsive-sm">
    <table class="table table-striped" id="orderHistories-table">
        <thead>
            <th>Order Id</th>
        <th>Status</th>
        <th>Creator</th>
        <th>Creator Id</th>
        <th>Creator Name</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($orderHistories as $orderHistory)
            <tr>
                <td>{!! $orderHistory->order_id !!}</td>
            <td>{!! $orderHistory->status !!}</td>
            <td>{!! $orderHistory->creator !!}</td>
            <td>{!! $orderHistory->creator_id !!}</td>
            <td>{!! $orderHistory->creator_name !!}</td>
                <td>
                    {!! Form::open(['route' => ['orderHistories.destroy', $orderHistory->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orderHistories.show', [$orderHistory->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('orderHistories.edit', [$orderHistory->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>