<div class="table-responsive-sm">
    <table class="table table-striped" id="paymentMethods-table">
        <thead>
            <th>Name</th>
        <th>Slug</th>
        <th>Image</th>
        <th>Is Actived</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($paymentMethods as $paymentMethod)
            <tr>
                <td>{!! $paymentMethod->name !!}</td>
            <td>{!! $paymentMethod->slug !!}</td>
            <td>{!! $paymentMethod->image !!}</td>
            <td>{!! $paymentMethod->is_actived !!}</td>
                <td>
                    {!! Form::open(['route' => ['paymentMethods.destroy', $paymentMethod->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('paymentMethods.show', [$paymentMethod->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('paymentMethods.edit', [$paymentMethod->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>