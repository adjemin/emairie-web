<div class="table-responsive-sm">
    <table class="table table-striped" id="orderItems-table">
        <thead>
            <th>Order Id</th>
        <th>Meta Data</th>
        <th>Quantity</th>
        <th>Quantity Unit</th>
        <th>Unit Price</th>
        <th>Currency Code</th>
        <th>Total Amount</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($orderItems as $orderItem)
            <tr>
                <td>{!! $orderItem->order_id !!}</td>
            <td>{!! $orderItem->meta_data !!}</td>
            <td>{!! $orderItem->quantity !!}</td>
            <td>{!! $orderItem->quantity_unit !!}</td>
            <td>{!! $orderItem->unit_price !!}</td>
            <td>{!! $orderItem->currency_code !!}</td>
            <td>{!! $orderItem->total_amount !!}</td>
                <td>
                    {!! Form::open(['route' => ['orderItems.destroy', $orderItem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orderItems.show', [$orderItem->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('orderItems.edit', [$orderItem->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>