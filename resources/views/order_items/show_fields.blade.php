<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $orderItem->order_id !!}</p>
</div>

<!-- Meta Data Field -->
<div class="form-group">
    {!! Form::label('meta_data', 'Meta Data:') !!}
    <p>{!! $orderItem->meta_data !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $orderItem->quantity !!}</p>
</div>

<!-- Quantity Unit Field -->
<div class="form-group">
    {!! Form::label('quantity_unit', 'Quantity Unit:') !!}
    <p>{!! $orderItem->quantity_unit !!}</p>
</div>

<!-- Unit Price Field -->
<div class="form-group">
    {!! Form::label('unit_price', 'Unit Price:') !!}
    <p>{!! $orderItem->unit_price !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $orderItem->currency_code !!}</p>
</div>

<!-- Total Amount Field -->
<div class="form-group">
    {!! Form::label('total_amount', 'Total Amount:') !!}
    <p>{!! $orderItem->total_amount !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $orderItem->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $orderItem->updated_at !!}</p>
</div>

