<div class="table-responsive-sm">
    <table class="table table-striped" id="paymentSources-table">
        <thead>
            <th>Name</th>
        <th>Slug</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($paymentSources as $paymentSource)
            <tr>
                <td>{!! $paymentSource->name !!}</td>
            <td>{!! $paymentSource->slug !!}</td>
                <td>
                    {!! Form::open(['route' => ['paymentSources.destroy', $paymentSource->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('paymentSources.show', [$paymentSource->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('paymentSources.edit', [$paymentSource->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>