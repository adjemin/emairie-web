<div class="table-responsive-sm">
    <table class="table table-striped" id="orderAssignments-table">
        <thead>
            <th>Order Id</th>
        <th>Delivery Service Username</th>
        <th>Is Accepted</th>
        <th>Acceptation Time</th>
        <th>Rejection Time</th>
        <th>Is Waiting Acceptation</th>
        <th>Creator Id</th>
        <th>Creator</th>
        <th>Delivery Fees</th>
        <th>Task Id</th>
        <th>Currency Code</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($orderAssignments as $orderAssignment)
            <tr>
                <td>{!! $orderAssignment->order_id !!}</td>
            <td>{!! $orderAssignment->delivery_service_username !!}</td>
            <td>{!! $orderAssignment->is_accepted !!}</td>
            <td>{!! $orderAssignment->acceptation_time !!}</td>
            <td>{!! $orderAssignment->rejection_time !!}</td>
            <td>{!! $orderAssignment->is_waiting_acceptation !!}</td>
            <td>{!! $orderAssignment->creator_id !!}</td>
            <td>{!! $orderAssignment->creator !!}</td>
            <td>{!! $orderAssignment->delivery_fees !!}</td>
            <td>{!! $orderAssignment->task_id !!}</td>
            <td>{!! $orderAssignment->currency_code !!}</td>
                <td>
                    {!! Form::open(['route' => ['orderAssignments.destroy', $orderAssignment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('orderAssignments.show', [$orderAssignment->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('orderAssignments.edit', [$orderAssignment->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>