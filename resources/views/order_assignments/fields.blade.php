<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Service Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_service_username', 'Delivery Service Username:') !!}
    {!! Form::text('delivery_service_username', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Accepted Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_accepted', 'Is Accepted:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_accepted', 0) !!}
        {!! Form::checkbox('is_accepted', '1', null) !!}
    </label>
</div>


<!-- Acceptation Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acceptation_time', 'Acceptation Time:') !!}
    {!! Form::text('acceptation_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Rejection Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rejection_time', 'Rejection Time:') !!}
    {!! Form::text('rejection_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Waiting Acceptation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting_acceptation', 'Is Waiting Acceptation:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting_acceptation', 0) !!}
        {!! Form::checkbox('is_waiting_acceptation', '1', null) !!}
    </label>
</div>


<!-- Creator Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    {!! Form::text('creator_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Creator Field -->
<div class="form-group col-sm-6">
    {!! Form::label('creator', 'Creator:') !!}
    {!! Form::text('creator', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    {!! Form::text('delivery_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Task Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('task_id', 'Task Id:') !!}
    {!! Form::text('task_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('orderAssignments.index') !!}" class="btn btn-default">Cancel</a>
</div>
