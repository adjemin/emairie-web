<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $orderAssignment->order_id !!}</p>
</div>

<!-- Delivery Service Username Field -->
<div class="form-group">
    {!! Form::label('delivery_service_username', 'Delivery Service Username:') !!}
    <p>{!! $orderAssignment->delivery_service_username !!}</p>
</div>

<!-- Is Accepted Field -->
<div class="form-group">
    {!! Form::label('is_accepted', 'Is Accepted:') !!}
    <p>{!! $orderAssignment->is_accepted !!}</p>
</div>

<!-- Acceptation Time Field -->
<div class="form-group">
    {!! Form::label('acceptation_time', 'Acceptation Time:') !!}
    <p>{!! $orderAssignment->acceptation_time !!}</p>
</div>

<!-- Rejection Time Field -->
<div class="form-group">
    {!! Form::label('rejection_time', 'Rejection Time:') !!}
    <p>{!! $orderAssignment->rejection_time !!}</p>
</div>

<!-- Is Waiting Acceptation Field -->
<div class="form-group">
    {!! Form::label('is_waiting_acceptation', 'Is Waiting Acceptation:') !!}
    <p>{!! $orderAssignment->is_waiting_acceptation !!}</p>
</div>

<!-- Creator Id Field -->
<div class="form-group">
    {!! Form::label('creator_id', 'Creator Id:') !!}
    <p>{!! $orderAssignment->creator_id !!}</p>
</div>

<!-- Creator Field -->
<div class="form-group">
    {!! Form::label('creator', 'Creator:') !!}
    <p>{!! $orderAssignment->creator !!}</p>
</div>

<!-- Delivery Fees Field -->
<div class="form-group">
    {!! Form::label('delivery_fees', 'Delivery Fees:') !!}
    <p>{!! $orderAssignment->delivery_fees !!}</p>
</div>

<!-- Task Id Field -->
<div class="form-group">
    {!! Form::label('task_id', 'Task Id:') !!}
    <p>{!! $orderAssignment->task_id !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $orderAssignment->currency_code !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $orderAssignment->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $orderAssignment->updated_at !!}</p>
</div>

