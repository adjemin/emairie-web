<div class="table-responsive-sm">
    <table class="table table-striped" id="deliveryServices-table">
        <thead>
            <th>Name</th>
        <th>Smartlivraison Username</th>
        <th>Email</th>
        <th>Logo</th>
        <th>Delivery Level</th>
        <th>Phone</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($deliveryServices as $deliveryService)
            <tr>
                <td>{!! $deliveryService->name !!}</td>
            <td>{!! $deliveryService->smartlivraison_username !!}</td>
            <td>{!! $deliveryService->email !!}</td>
            <td>{!! $deliveryService->logo !!}</td>
            <td>{!! $deliveryService->delivery_level !!}</td>
            <td>{!! $deliveryService->phone !!}</td>
                <td>
                    {!! Form::open(['route' => ['deliveryServices.destroy', $deliveryService->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('deliveryServices.show', [$deliveryService->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('deliveryServices.edit', [$deliveryService->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>