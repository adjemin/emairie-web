<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $deliveryService->name !!}</p>
</div>

<!-- Smartlivraison Username Field -->
<div class="form-group">
    {!! Form::label('smartlivraison_username', 'Smartlivraison Username:') !!}
    <p>{!! $deliveryService->smartlivraison_username !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $deliveryService->email !!}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{!! $deliveryService->logo !!}</p>
</div>

<!-- Delivery Level Field -->
<div class="form-group">
    {!! Form::label('delivery_level', 'Delivery Level:') !!}
    <p>{!! $deliveryService->delivery_level !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $deliveryService->phone !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $deliveryService->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $deliveryService->updated_at !!}</p>
</div>

