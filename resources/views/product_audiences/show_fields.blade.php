<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $productAudience->product_id !!}</p>
</div>

<!-- View Count Field -->
<div class="form-group">
    {!! Form::label('view_count', 'View Count:') !!}
    <p>{!! $productAudience->view_count !!}</p>
</div>

<!-- Last Read Date Field -->
<div class="form-group">
    {!! Form::label('last_read_date', 'Last Read Date:') !!}
    <p>{!! $productAudience->last_read_date !!}</p>
</div>

<!-- Like Count Field -->
<div class="form-group">
    {!! Form::label('like_count', 'Like Count:') !!}
    <p>{!! $productAudience->like_count !!}</p>
</div>

<!-- Order Count Field -->
<div class="form-group">
    {!! Form::label('order_count', 'Order Count:') !!}
    <p>{!! $productAudience->order_count !!}</p>
</div>

<!-- Conversation Count Field -->
<div class="form-group">
    {!! Form::label('conversation_count', 'Conversation Count:') !!}
    <p>{!! $productAudience->conversation_count !!}</p>
</div>

<!-- Issue Count Field -->
<div class="form-group">
    {!! Form::label('issue_count', 'Issue Count:') !!}
    <p>{!! $productAudience->issue_count !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $productAudience->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $productAudience->updated_at !!}</p>
</div>

