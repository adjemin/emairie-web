<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::text('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- View Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('view_count', 'View Count:') !!}
    {!! Form::text('view_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Read Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_read_date', 'Last Read Date:') !!}
    {!! Form::text('last_read_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Like Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('like_count', 'Like Count:') !!}
    {!! Form::text('like_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_count', 'Order Count:') !!}
    {!! Form::text('order_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Conversation Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('conversation_count', 'Conversation Count:') !!}
    {!! Form::text('conversation_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Issue Count Field -->
<div class="form-group col-sm-6">
    {!! Form::label('issue_count', 'Issue Count:') !!}
    {!! Form::text('issue_count', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('productAudiences.index') !!}" class="btn btn-default">Cancel</a>
</div>
