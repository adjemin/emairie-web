<div class="table-responsive-sm">
    <table class="table table-striped" id="productAudiences-table">
        <thead>
            <th>Product Id</th>
        <th>View Count</th>
        <th>Last Read Date</th>
        <th>Like Count</th>
        <th>Order Count</th>
        <th>Conversation Count</th>
        <th>Issue Count</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($productAudiences as $productAudience)
            <tr>
                <td>{!! $productAudience->product_id !!}</td>
            <td>{!! $productAudience->view_count !!}</td>
            <td>{!! $productAudience->last_read_date !!}</td>
            <td>{!! $productAudience->like_count !!}</td>
            <td>{!! $productAudience->order_count !!}</td>
            <td>{!! $productAudience->conversation_count !!}</td>
            <td>{!! $productAudience->issue_count !!}</td>
                <td>
                    {!! Form::open(['route' => ['productAudiences.destroy', $productAudience->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('productAudiences.show', [$productAudience->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('productAudiences.edit', [$productAudience->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>