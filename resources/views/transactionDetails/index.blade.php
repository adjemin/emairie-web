@extends('layoutFront.app_chield')
@section('detailProduit')
<div class="container">
<div class="row">
<div class="col-sm-12" style="font-family: montserrat;margin-top:15px;margin-bottom:15px;">
<div class="card text-center" style="padding:10px;">
<div class="card-body">
  <div class="container mt-3">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#history">
              <button style="border:none" type="button" class="btn btn-primary">
               Histotiques
              </input>
              </button>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#collection">
              <button style="border:none" type="button" class="btn btn-success">
               Ramassages
              </button>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#delivery">
              <button style="border:none" type="button" class="btn btn-info">
             Livraison
              </button>
            </a>
        </li>
      </ul>
      <!-- Tab panes -->
    <div class="tab-content">
        <div id="history" class="container tab-pane active"><br>
            <h3>Historique</h3>
                <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur maxime sed nihil? Fugit sequi nulla reiciendis culpa blanditiis iure 
                perspiciatis tempora, iste sed voluptatum! Consequuntur hic voluptatibus qui quod ullam!
                </div>
        </div>
            <div id="collection" class="container tab-pane fade"><br>
                <h3>Ramassages</h3>
                <div>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur maxime sed nihil? Fugit sequi nulla reiciendis culpa blanditiis iure 
                        perspiciatis tempora, iste sed voluptatum! Consequuntur hic voluptatibus qui quod ullam!
                </div>
            </div>
            <div id="delivery" class="container tab-pane fade"><br>
                <h3>Livraison</h3>
                <div>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur maxime sed nihil? Fugit sequi nulla reiciendis culpa blanditiis iure 
                        perspiciatis tempora, iste sed voluptatum! Consequuntur hic voluptatibus qui quod ullam!
                </div>
            </div>
    </div>       
</div>
</div>
</div>
</div>
</div>
@endsection
