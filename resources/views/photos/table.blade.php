<div class="table-responsive-sm">
    <table class="table table-striped" id="photos-table">
        <thead>
            <th>Url</th>
        <th>Mime</th>
        <th>Height</th>
        <th>Width</th>
        <th>Length</th>
        <th>Customer? Id</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($photos as $photo)
            <tr>
                <td>{!! $photo->url !!}</td>
            <td>{!! $photo->mime !!}</td>
            <td>{!! $photo->height !!}</td>
            <td>{!! $photo->width !!}</td>
            <td>{!! $photo->length !!}</td>
            <td>{!! $photo->customer�_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['photos.destroy', $photo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('photos.show', [$photo->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('photos.edit', [$photo->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>