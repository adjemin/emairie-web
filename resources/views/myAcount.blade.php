@extends('layoutFront.app_chield')
@section('detailProduit')
<div class="container">
<div class="row">
<div class="col-sm-12" style="font-family: montserrat;margin-top:15px;margin-bottom:15px;">
<div class="card text-center" style="padding:10px;">
<div class="card-body">
  <div class="container mt-3">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#mesAchat">
              <button style="border:none" type="button" class="btn btn-primary">
                Mes Achats
                <input value="{{$count_order}}" style="border:none;border-radius:90px;" type="button" class="btn btn-light btn-sm">
              </input>
              </button>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#mesVente">
              <button style="border:none" type="button" class="btn btn-success">
                Mes Ventes
                <input value="0" id="item" style="border:none;border-radius:90px;" type="button" class="btn btn-light btn-sm"></input>
              </button>
            </a>
        </li>
      </ul>
      @include('sweetalert::alert')
      <!-- Tab panes -->
      <div class="tab-content">
        <div id="mesAchat" class="container tab-pane active"><br>
          <h3>Mes Achats</h3>
        <div class="table-responsive-md"> 
          <table class="table">
            <thead>
            
              <tr class="table-bordered">
                <th scope="col">N°</th>
                <th scope="col">Images</th>
                <th scope="col">Nom</th>
                <th scope="col">Prix</th>
                <th scope="col">Quantité</th>
                <th scope="col">Frais livraison</th>
                <th scope="col">Total</th>
                <th scope="col">Date</th>
                <th scope="col">Statut</th>
                <th scope="col">Action</th>

              </tr>
            </thead>
            <tbody>
              @if($order->count())
              @foreach($order as $key => $orders)
                @foreach($orders->orderitem as $orderitem)
              
              <tr class="table-bordered">
              <th scope="row">{{++$key}}</th>
                <td>
                  <img src="{{$orderitem->product->cover_url}}" alt="" height="100">
                </td>
                <td>{{$orderitem->product->title}}</td>
                <td>CFA {{$orderitem->unit_price}}</td>
                <td>{{$orderitem->quantity}}</td>
                @if($orders->invoice)
                <td>CFA {{$orders->invoice->fees_delivery}}</td>
                <td>CFA {{$orders->invoice->total}}</td>
                @endif
                <td>{{$orders->created_at->format('d M Y H:m')}}</td>
                @if($orders->current_status == "waiting")
                <td>
                  <span class="badge badge-warning" style="color:#fff;padding:10px;">En attente</span>
                </td>
                @elseif($orders->current_status == "success")
                <td>
                  <span class="badge badge-success" style="color:#fff;padding:10px;">Succès</span>
                </td>
                @else
                <td>
                  <span class="badge badge-danger" style="color:#fff;padding:10px;">Annulé</span>
                </td>
                @endif
                <td>
                  <a href="#">
                    <span class="badge badge-info" style="color:#fff;padding:10px;">Détails</span>
                  </a>
                </td>
              </tr>
              @endforeach
              @endforeach

              @endif
            </tbody>
          </table>
            </div>
          <div style="float:right">
            {{ $order->links() }}
        </div>
          </div>
          <div id="mesVente" class="container tab-pane fade"><br>
          <h3>Mes Ventes</h3>
          <div class="table-responsive-md"> 
          <table class="table">
            <thead>
              <tr class="table-bordered">
                <th scope="col">N°</th>
                <th scope="col">Images</th>
                <th scope="col">Nom</th>
                <th scope="col">Prix</th>
                <th scope="col">Date</th>
                <th scope="col">Statut</th>
              </tr>
            </thead>
            <tbody>
              @if($sel->count()>0)
                @foreach($sel as $key => $sels)
                @foreach($sels->orderitem as $orderitem)
                @if($orderitem->product->customer_id == $customer_id)
              <tr class="table-bordered">
              <th scope="row">{{++$key}}</th>
                <td>
                  <img src="{{$orderitem->product->cover_url}}" alt="" height="100">
                </td>
                <td>{{$orderitem->product->title}}</td>
                <td>CFA {{$orderitem->product->price}}</td>
                <td>{{$sels->created_at->format('d M Y H:m')}}</td>
                @if($sels->current_status == "waiting")
                <td> <button style="color:#fff;border-radius:10px;" class="btn btn-warning btn-sm" type="button"> En attente</button> </td>
                @elseif($sels->current_status == "success")
                <td> <button style="color:#fff;border-radius:10px;" class="btn btn-primary btn-sm" type="button"> Succès</button> </td>
                @else
                <td> <button style="color:#fff;border-radius:10px;" class="btn btn-danger btn-sm" type="button"> Annuler</button> </td>
                @endif
              </tr>
              @endif
              @endforeach
              @endforeach
              @endif
            </tbody>
          </table>
          </div>
          <!--<div style="float:right">
            {{ $sel->links() }}
        </div>-->
          </div>
    </div>
  </div>       
</div>
</div>
</div>
</div>
</div>
@if($sel->count()>0)
@foreach($sel as $key => $sels)
@foreach($sels->orderitem as $orderitem)
@if($orderitem->product->customer_id == $customer_id)
  
<script>
  document.getElementById('item').value = document.getElementById('count').value;
</script>

@endif
@endforeach
@endforeach
@endif


  @endsection
