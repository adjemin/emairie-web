<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_id', 'Source Id:') !!}
    {!! Form::text('source_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Source Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('source_name', 'Source Name:') !!}
    {!! Form::text('source_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Transaction Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaction_type', 'Transaction Type:') !!}
    {!! Form::text('transaction_type', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_id', 'Receiver Id:') !!}
    {!! Form::text('receiver_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Receiver Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('receiver_name', 'Receiver Name:') !!}
    {!! Form::text('receiver_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Designation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('designation', 'Designation:') !!}
    {!! Form::text('designation', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tokenTransactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
