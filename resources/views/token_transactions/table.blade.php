<div class="table-responsive-sm">
    <table class="table table-striped" id="tokenTransactions-table">
        <thead>
            <th>Amount</th>
        <th>Source Id</th>
        <th>Source Name</th>
        <th>Transaction Type</th>
        <th>Receiver Id</th>
        <th>Receiver Name</th>
        <th>Designation</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($tokenTransactions as $tokenTransaction)
            <tr>
                <td>{!! $tokenTransaction->amount !!}</td>
            <td>{!! $tokenTransaction->source_id !!}</td>
            <td>{!! $tokenTransaction->source_name !!}</td>
            <td>{!! $tokenTransaction->transaction_type !!}</td>
            <td>{!! $tokenTransaction->receiver_id !!}</td>
            <td>{!! $tokenTransaction->receiver_name !!}</td>
            <td>{!! $tokenTransaction->designation !!}</td>
                <td>
                    {!! Form::open(['route' => ['tokenTransactions.destroy', $tokenTransaction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('tokenTransactions.show', [$tokenTransaction->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('tokenTransactions.edit', [$tokenTransaction->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>