<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $tokenTransaction->amount !!}</p>
</div>

<!-- Source Id Field -->
<div class="form-group">
    {!! Form::label('source_id', 'Source Id:') !!}
    <p>{!! $tokenTransaction->source_id !!}</p>
</div>

<!-- Source Name Field -->
<div class="form-group">
    {!! Form::label('source_name', 'Source Name:') !!}
    <p>{!! $tokenTransaction->source_name !!}</p>
</div>

<!-- Transaction Type Field -->
<div class="form-group">
    {!! Form::label('transaction_type', 'Transaction Type:') !!}
    <p>{!! $tokenTransaction->transaction_type !!}</p>
</div>

<!-- Receiver Id Field -->
<div class="form-group">
    {!! Form::label('receiver_id', 'Receiver Id:') !!}
    <p>{!! $tokenTransaction->receiver_id !!}</p>
</div>

<!-- Receiver Name Field -->
<div class="form-group">
    {!! Form::label('receiver_name', 'Receiver Name:') !!}
    <p>{!! $tokenTransaction->receiver_name !!}</p>
</div>

<!-- Designation Field -->
<div class="form-group">
    {!! Form::label('designation', 'Designation:') !!}
    <p>{!! $tokenTransaction->designation !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tokenTransaction->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tokenTransaction->updated_at !!}</p>
</div>

