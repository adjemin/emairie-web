<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::text('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::text('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Fees Delivery Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fees_delivery', 'Fees Delivery:') !!}
    {!! Form::text('fees_delivery', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::text('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Paid By Customer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_paid_by_customer', 'Is Paid By Customer:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_paid_by_customer', 0) !!}
        {!! Form::checkbox('is_paid_by_customer', '1', null) !!}
    </label>
</div>


<!-- Is Paid By Delivery Service Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_paid_by_delivery_service', 'Is Paid By Delivery Service:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_paid_by_delivery_service', 0) !!}
        {!! Form::checkbox('is_paid_by_delivery_service', '1', null) !!}
    </label>
</div>


<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('invoices.index') !!}" class="btn btn-default">Cancel</a>
</div>
