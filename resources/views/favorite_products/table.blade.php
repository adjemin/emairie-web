<div class="table-responsive-sm">
    <table class="table table-striped" id="favoriteProducts-table">
        <thead>
            <th>Product Id</th>
        <th>Customer Id</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($favoriteProducts as $favoriteProduct)
            <tr>
                <td>{!! $favoriteProduct->product_id !!}</td>
            <td>{!! $favoriteProduct->customer_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['favoriteProducts.destroy', $favoriteProduct->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('favoriteProducts.show', [$favoriteProduct->id]) !!}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{!! route('favoriteProducts.edit', [$favoriteProduct->id]) !!}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>