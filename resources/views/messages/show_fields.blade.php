<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $message->content !!}</p>
</div>

<!-- Sender Id Field -->
<div class="form-group">
    {!! Form::label('sender_id', 'Sender Id:') !!}
    <p>{!! $message->sender_id !!}</p>
</div>

<!-- Conversation Id Field -->
<div class="form-group">
    {!! Form::label('conversation_id', 'Conversation Id:') !!}
    <p>{!! $message->conversation_id !!}</p>
</div>

<!-- Is Read Field -->
<div class="form-group">
    {!! Form::label('is_read', 'Is Read:') !!}
    <p>{!! $message->is_read !!}</p>
</div>

<!-- Is Received Field -->
<div class="form-group">
    {!! Form::label('is_received', 'Is Received:') !!}
    <p>{!! $message->is_received !!}</p>
</div>

<!-- Is Sent Field -->
<div class="form-group">
    {!! Form::label('is_sent', 'Is Sent:') !!}
    <p>{!! $message->is_sent !!}</p>
</div>

<!-- Content Type Field -->
<div class="form-group">
    {!! Form::label('content_type', 'Content Type:') !!}
    <p>{!! $message->content_type !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $message->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $message->updated_at !!}</p>
</div>

