<!-- Follower Field -->
<div class="form-group col-sm-6">
    {!! Form::label('follower', 'Follower:') !!}
    {!! Form::text('follower', null, ['class' => 'form-control']) !!}
</div>

<!-- Followed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('followed', 'Followed:') !!}
    {!! Form::text('followed', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('follows.index') !!}" class="btn btn-default">Cancel</a>
</div>
