<!-- Follower Field -->
<div class="form-group">
    {!! Form::label('follower', 'Follower:') !!}
    <p>{!! $follow->follower !!}</p>
</div>

<!-- Followed Field -->
<div class="form-group">
    {!! Form::label('followed', 'Followed:') !!}
    <p>{!! $follow->followed !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $follow->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $follow->updated_at !!}</p>
</div>

