<?php


namespace App\Utils;


use App\Models\Message;
use App\Models\Customer;
use App\Models\CustomerDevice;
use App\Models\CustomerNotification;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class MessageNotificationUtils
{

    const MESSAGE_SENT = "message_sent";
    const MESSAGE_RECEIVED = "task_status_changed";

    /**
     * @param $customerId
     * @param $message @Message
     * @return bool|string
     */
    public static function notify($customerId, $message){
        $customer = Customer::where(["id" => $customerId])->first();

        $defaultLanguage = $customer->language;
        if($defaultLanguage == "fr"){
            $title = $message->getSenderAttribute()->name." vous a envoyé un message";
            $subtitle = Str::substr($message->content, 0, 250);
        }else{
            $title = $message->getSenderAttribute()->name." sent you a message";
            $subtitle = Str::substr($message->content, 0, 250);
        }

        $customerNotification  = new CustomerNotification();
        $customerNotification->title = $title;
        $customerNotification->title_en = $title;
        $customerNotification->subtitle = $subtitle;
        $customerNotification->subtitle_en = $subtitle;
        $customerNotification->customer_id = $customerId;
        $customerNotification->type_notification = self::MESSAGE_SENT;
        $customerNotification->meta_data_id = $message->id;
        $customerNotification->meta_data = json_encode($message);
        $customerNotification->data = json_encode($message->getConversation());
        $customerNotification->data_id = $message->getConversation()->id;
        $customerNotification->save();

        $customerDevices = CustomerDevice::where([
            "customer_id" => $customerId,
            "deleted_at" => null
        ])->orderBy('id', 'DESC')->take(1)->get();


        /*$devices = Collection::make([]);
        foreach ($agentDevices as $agentDevice){
            $devices->push($agentDevice->firebase_id);
        }*/

        $metadata = $message->toArray();
        if($customerDevices != null && count($customerDevices) >0){
            $agentDevice = $customerDevices[0];
            $device = "".$agentDevice->firebase_id;

            return FirebaseMessagingUtils::sendNotification($title, $subtitle,self::MESSAGE_SENT, $metadata, $device);
        }else{
            return "No Devices";
        }

    }

}
