<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSponsoringRequest;
use App\Http\Requests\UpdateSponsoringRequest;
use App\Repositories\SponsoringRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SponsoringController extends AppBaseController
{
    /** @var  SponsoringRepository */
    private $sponsoringRepository;

    public function __construct(SponsoringRepository $sponsoringRepo)
    {
        $this->sponsoringRepository = $sponsoringRepo;
    }

    /**
     * Display a listing of the Sponsoring.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sponsorings = $this->sponsoringRepository->all();

        return view('sponsorings.index')
            ->with('sponsorings', $sponsorings);
    }

    /**
     * Show the form for creating a new Sponsoring.
     *
     * @return Response
     */
    public function create()
    {
        return view('sponsorings.create');
    }

    /**
     * Store a newly created Sponsoring in storage.
     *
     * @param CreateSponsoringRequest $request
     *
     * @return Response
     */
    public function store(CreateSponsoringRequest $request)
    {
        $input = $request->all();

        $sponsoring = $this->sponsoringRepository->create($input);

        Flash::success('Sponsoring saved successfully.');

        return redirect(route('sponsorings.index'));
    }

    /**
     * Display the specified Sponsoring.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            Flash::error('Sponsoring not found');

            return redirect(route('sponsorings.index'));
        }

        return view('sponsorings.show')->with('sponsoring', $sponsoring);
    }

    /**
     * Show the form for editing the specified Sponsoring.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            Flash::error('Sponsoring not found');

            return redirect(route('sponsorings.index'));
        }

        return view('sponsorings.edit')->with('sponsoring', $sponsoring);
    }

    /**
     * Update the specified Sponsoring in storage.
     *
     * @param int $id
     * @param UpdateSponsoringRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSponsoringRequest $request)
    {
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            Flash::error('Sponsoring not found');

            return redirect(route('sponsorings.index'));
        }

        $sponsoring = $this->sponsoringRepository->update($request->all(), $id);

        Flash::success('Sponsoring updated successfully.');

        return redirect(route('sponsorings.index'));
    }

    /**
     * Remove the specified Sponsoring from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            Flash::error('Sponsoring not found');

            return redirect(route('sponsorings.index'));
        }

        $this->sponsoringRepository->delete($id);

        Flash::success('Sponsoring deleted successfully.');

        return redirect(route('sponsorings.index'));
    }
}
