<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFollowAPIRequest;
use App\Http\Requests\API\UpdateFollowAPIRequest;
use App\Models\Customer;
use App\Models\Follow;
use App\Repositories\FollowRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FollowController
 * @package App\Http\Controllers\API
 */

class FollowAPIController extends AppBaseController
{
    /** @var  FollowRepository */
    private $followRepository;

    public function __construct(FollowRepository $followRepo)
    {
        $this->followRepository = $followRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/follows",
     *      summary="Get a listing of the Follows.",
     *      tags={"Follow"},
     *      description="Get all Follows",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Follow")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $follows = $this->followRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($follows->toArray(), 'Follows retrieved successfully');
    }

    /**
     * @param CreateFollowAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/follows",
     *      summary="Store a newly created Follow in storage",
     *      tags={"Follow"},
     *      description="Store Follow",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Follow that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Follow")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFollowAPIRequest $request)
    {
        $input = $request->all();

        $follower = Customer::where(['id' => $request->follower])->first();
        $followed = Customer::where(['id' => $request->followed])->first();

        if (empty($follower)) {
            return $this->sendError('Follower not found');
        }

        if (empty($followed)) {
            return $this->sendError('Followed not found');
        }

        $follow = $this->followRepository->create($input);

        return $this->sendResponse($follow->toArray(), 'Follow saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/follows/{id}",
     *      summary="Display the specified Follow",
     *      tags={"Follow"},
     *      description="Get Follow",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        return $this->sendResponse($follow->toArray(), 'Follow retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFollowAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/follows/{id}",
     *      summary="Update the specified Follow in storage",
     *      tags={"Follow"},
     *      description="Update Follow",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Follow that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Follow")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Follow"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFollowAPIRequest $request)
    {
        $input = $request->all();

        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        $follow = $this->followRepository->update($input, $id);

        return $this->sendResponse($follow->toArray(), 'Follow updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/follows/{id}",
     *      summary="Remove the specified Follow from storage",
     *      tags={"Follow"},
     *      description="Delete Follow",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Follow",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Follow $follow */
        $follow = $this->followRepository->find($id);

        if (empty($follow)) {
            return $this->sendError('Follow not found');
        }

        $follow->delete();

        return $this->sendResponse($id, 'Follow deleted successfully');
    }

    public function unFollow(CreateFollowAPIRequest $request){
        $input = $request->all();

        $follower = Customer::where(['id' => $request->follower])->first();
        $followed = Customer::where(['id' => $request->followed])->first();

        if (empty($follower)) {
            return $this->sendError('Follower not found');
        }

        if (empty($followed)) {
            return $this->sendError('Followed not found');
        }

        $follows = $this->followRepository->all($input);

        if(count($follows) > 0){
           $follow = $follows[0];
           $follow->delete();
        }

        return $this->sendResponse($followed->toArray(), 'Follow deleted successfully');
    }

    public function checkFollowing(CreateFollowAPIRequest $request){
        $input = $request->all();

        $follower = Customer::where(['id' => $request->follower])->first();
        $followed = Customer::where(['id' => $request->followed])->first();

        if (empty($follower)) {
            return $this->sendError('Follower not found');
        }

        if (empty($followed)) {
            return $this->sendError('Followed not found');
        }

        $follows = $this->followRepository->all($input);

        if(count($follows) > 0){
            $follow = $follows[0];
            return $this->sendResponse(true, 'Follow deleted successfully');
        }else{
            return $this->sendResponse(false, 'Follow deleted successfully');
        }


    }
}
