<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFlashSellingAPIRequest;
use App\Http\Requests\API\UpdateFlashSellingAPIRequest;
use App\Models\FlashSelling;
use App\Repositories\FlashSellingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FlashSellingController
 * @package App\Http\Controllers\API
 */

class FlashSellingAPIController extends AppBaseController
{
    /** @var  FlashSellingRepository */
    private $flashSellingRepository;

    public function __construct(FlashSellingRepository $flashSellingRepo)
    {
        $this->flashSellingRepository = $flashSellingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/flashSellings",
     *      summary="Get a listing of the FlashSellings.",
     *      tags={"FlashSelling"},
     *      description="Get all FlashSellings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/FlashSelling")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $flashSellings = $this->flashSellingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($flashSellings->toArray(), 'Flash Sellings retrieved successfully');
    }

    /**
     * @param CreateFlashSellingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/flashSellings",
     *      summary="Store a newly created FlashSelling in storage",
     *      tags={"FlashSelling"},
     *      description="Store FlashSelling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FlashSelling that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FlashSelling")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FlashSelling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFlashSellingAPIRequest $request)
    {
        $input = $request->all();

        $flashSelling = $this->flashSellingRepository->create($input);

        return $this->sendResponse($flashSelling->toArray(), 'Flash Selling saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/flashSellings/{id}",
     *      summary="Display the specified FlashSelling",
     *      tags={"FlashSelling"},
     *      description="Get FlashSelling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FlashSelling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FlashSelling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var FlashSelling $flashSelling */
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            return $this->sendError('Flash Selling not found');
        }

        return $this->sendResponse($flashSelling->toArray(), 'Flash Selling retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFlashSellingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/flashSellings/{id}",
     *      summary="Update the specified FlashSelling in storage",
     *      tags={"FlashSelling"},
     *      description="Update FlashSelling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FlashSelling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FlashSelling that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FlashSelling")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FlashSelling"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFlashSellingAPIRequest $request)
    {
        $input = $request->all();

        /** @var FlashSelling $flashSelling */
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            return $this->sendError('Flash Selling not found');
        }

        $flashSelling = $this->flashSellingRepository->update($input, $id);

        return $this->sendResponse($flashSelling->toArray(), 'FlashSelling updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/flashSellings/{id}",
     *      summary="Remove the specified FlashSelling from storage",
     *      tags={"FlashSelling"},
     *      description="Delete FlashSelling",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FlashSelling",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var FlashSelling $flashSelling */
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            return $this->sendError('Flash Selling not found');
        }

        $flashSelling->delete();

        return $this->sendResponse($id, 'Flash Selling deleted successfully');
    }
}
