<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductConditionAPIRequest;
use App\Http\Requests\API\UpdateProductConditionAPIRequest;
use App\Models\ProductCondition;
use App\Repositories\ProductConditionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductConditionController
 * @package App\Http\Controllers\API
 */

class ProductConditionAPIController extends AppBaseController
{
    /** @var  ProductConditionRepository */
    private $productConditionRepository;

    public function __construct(ProductConditionRepository $productConditionRepo)
    {
        $this->productConditionRepository = $productConditionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/productConditions",
     *      summary="Get a listing of the ProductConditions.",
     *      tags={"ProductCondition"},
     *      description="Get all ProductConditions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ProductCondition")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $productConditions = $this->productConditionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productConditions->toArray(), 'Product Conditions retrieved successfully');
    }

    /**
     * @param CreateProductConditionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/productConditions",
     *      summary="Store a newly created ProductCondition in storage",
     *      tags={"ProductCondition"},
     *      description="Store ProductCondition",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductCondition that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductCondition")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductCondition"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProductConditionAPIRequest $request)
    {
        $input = $request->all();

        $productCondition = $this->productConditionRepository->create($input);

        return $this->sendResponse($productCondition->toArray(), 'Product Condition saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/productConditions/{id}",
     *      summary="Display the specified ProductCondition",
     *      tags={"ProductCondition"},
     *      description="Get ProductCondition",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductCondition",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductCondition"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ProductCondition $productCondition */
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            return $this->sendError('Product Condition not found');
        }

        return $this->sendResponse($productCondition->toArray(), 'Product Condition retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProductConditionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/productConditions/{id}",
     *      summary="Update the specified ProductCondition in storage",
     *      tags={"ProductCondition"},
     *      description="Update ProductCondition",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductCondition",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductCondition that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductCondition")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductCondition"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProductConditionAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductCondition $productCondition */
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            return $this->sendError('Product Condition not found');
        }

        $productCondition = $this->productConditionRepository->update($input, $id);

        return $this->sendResponse($productCondition->toArray(), 'ProductCondition updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/productConditions/{id}",
     *      summary="Remove the specified ProductCondition from storage",
     *      tags={"ProductCondition"},
     *      description="Delete ProductCondition",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductCondition",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ProductCondition $productCondition */
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            return $this->sendError('Product Condition not found');
        }

        $productCondition->delete();

        return $this->sendResponse($id, 'Product Condition deleted successfully');
    }
}
