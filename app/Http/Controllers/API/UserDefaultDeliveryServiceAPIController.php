<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateUserDefaultDeliveryServiceAPIRequest;
use App\Http\Requests\API\UpdateUserDefaultDeliveryServiceAPIRequest;
use App\Models\UserDefaultDeliveryService;
use App\Repositories\UserDefaultDeliveryServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class UserDefaultDeliveryServiceController
 * @package App\Http\Controllers\API
 */

class UserDefaultDeliveryServiceAPIController extends AppBaseController
{
    /** @var  UserDefaultDeliveryServiceRepository */
    private $userDefaultDeliveryServiceRepository;

    public function __construct(UserDefaultDeliveryServiceRepository $userDefaultDeliveryServiceRepo)
    {
        $this->userDefaultDeliveryServiceRepository = $userDefaultDeliveryServiceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/userDefaultDeliveryServices",
     *      summary="Get a listing of the UserDefaultDeliveryServices.",
     *      tags={"UserDefaultDeliveryService"},
     *      description="Get all UserDefaultDeliveryServices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserDefaultDeliveryService")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $userDefaultDeliveryServices = $this->userDefaultDeliveryServiceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($userDefaultDeliveryServices->toArray(), 'User Default Delivery Services retrieved successfully');
    }

    /**
     * @param CreateUserDefaultDeliveryServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/userDefaultDeliveryServices",
     *      summary="Store a newly created UserDefaultDeliveryService in storage",
     *      tags={"UserDefaultDeliveryService"},
     *      description="Store UserDefaultDeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserDefaultDeliveryService that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserDefaultDeliveryService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserDefaultDeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateUserDefaultDeliveryServiceAPIRequest $request)
    {
        $input = $request->all();

        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->create($input);

        return $this->sendResponse($userDefaultDeliveryService->toArray(), 'User Default Delivery Service saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/userDefaultDeliveryServices/{id}",
     *      summary="Display the specified UserDefaultDeliveryService",
     *      tags={"UserDefaultDeliveryService"},
     *      description="Get UserDefaultDeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserDefaultDeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserDefaultDeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var UserDefaultDeliveryService $userDefaultDeliveryService */
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            return $this->sendError('User Default Delivery Service not found');
        }

        return $this->sendResponse($userDefaultDeliveryService->toArray(), 'User Default Delivery Service retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateUserDefaultDeliveryServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/userDefaultDeliveryServices/{id}",
     *      summary="Update the specified UserDefaultDeliveryService in storage",
     *      tags={"UserDefaultDeliveryService"},
     *      description="Update UserDefaultDeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserDefaultDeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="UserDefaultDeliveryService that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/UserDefaultDeliveryService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserDefaultDeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateUserDefaultDeliveryServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var UserDefaultDeliveryService $userDefaultDeliveryService */
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            return $this->sendError('User Default Delivery Service not found');
        }

        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->update($input, $id);

        return $this->sendResponse($userDefaultDeliveryService->toArray(), 'UserDefaultDeliveryService updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/userDefaultDeliveryServices/{id}",
     *      summary="Remove the specified UserDefaultDeliveryService from storage",
     *      tags={"UserDefaultDeliveryService"},
     *      description="Delete UserDefaultDeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of UserDefaultDeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var UserDefaultDeliveryService $userDefaultDeliveryService */
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            return $this->sendError('User Default Delivery Service not found');
        }

        $userDefaultDeliveryService->delete();

        return $this->sendResponse($id, 'User Default Delivery Service deleted successfully');
    }
}
