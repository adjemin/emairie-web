<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateContactInvitationAPIRequest;
use App\Http\Requests\API\UpdateContactInvitationAPIRequest;
use App\Models\ContactInvitation;
use App\Repositories\ContactInvitationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ContactInvitationController
 * @package App\Http\Controllers\API
 */

class ContactInvitationAPIController extends AppBaseController
{
    /** @var  ContactInvitationRepository */
    private $contactInvitationRepository;

    public function __construct(ContactInvitationRepository $contactInvitationRepo)
    {
        $this->contactInvitationRepository = $contactInvitationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/contactInvitations",
     *      summary="Get a listing of the ContactInvitations.",
     *      tags={"ContactInvitation"},
     *      description="Get all ContactInvitations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ContactInvitation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $contactInvitations = $this->contactInvitationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($contactInvitations->toArray(), 'Contact Invitations retrieved successfully');
    }

    /**
     * @param CreateContactInvitationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/contactInvitations",
     *      summary="Store a newly created ContactInvitation in storage",
     *      tags={"ContactInvitation"},
     *      description="Store ContactInvitation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ContactInvitation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ContactInvitation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactInvitation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateContactInvitationAPIRequest $request)
    {
        $input = $request->all();

        $contactInvitation = $this->contactInvitationRepository->create($input);

        return $this->sendResponse($contactInvitation->toArray(), 'Contact Invitation saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/contactInvitations/{id}",
     *      summary="Display the specified ContactInvitation",
     *      tags={"ContactInvitation"},
     *      description="Get ContactInvitation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactInvitation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactInvitation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ContactInvitation $contactInvitation */
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            return $this->sendError('Contact Invitation not found');
        }

        return $this->sendResponse($contactInvitation->toArray(), 'Contact Invitation retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateContactInvitationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/contactInvitations/{id}",
     *      summary="Update the specified ContactInvitation in storage",
     *      tags={"ContactInvitation"},
     *      description="Update ContactInvitation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactInvitation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ContactInvitation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ContactInvitation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ContactInvitation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateContactInvitationAPIRequest $request)
    {
        $input = $request->all();

        /** @var ContactInvitation $contactInvitation */
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            return $this->sendError('Contact Invitation not found');
        }

        $contactInvitation = $this->contactInvitationRepository->update($input, $id);

        return $this->sendResponse($contactInvitation->toArray(), 'ContactInvitation updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/contactInvitations/{id}",
     *      summary="Remove the specified ContactInvitation from storage",
     *      tags={"ContactInvitation"},
     *      description="Delete ContactInvitation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ContactInvitation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ContactInvitation $contactInvitation */
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            return $this->sendError('Contact Invitation not found');
        }

        $contactInvitation->delete();

        return $this->sendResponse($id, 'Contact Invitation deleted successfully');
    }
}
