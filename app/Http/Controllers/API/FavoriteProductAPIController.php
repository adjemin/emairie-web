<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFavoriteProductAPIRequest;
use App\Http\Requests\API\UpdateFavoriteProductAPIRequest;
use App\Models\FavoriteProduct;
use App\Models\Product;
use App\Repositories\FavoriteProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Collection;
use Response;

/**
 * Class FavoriteProductController
 * @package App\Http\Controllers\API
 */

class FavoriteProductAPIController extends AppBaseController
{
    /** @var  FavoriteProductRepository */
    private $favoriteProductRepository;

    public function __construct(FavoriteProductRepository $favoriteProductRepo)
    {
        $this->favoriteProductRepository = $favoriteProductRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/favoriteProducts",
     *      summary="Get a listing of the FavoriteProducts.",
     *      tags={"FavoriteProduct"},
     *      description="Get all FavoriteProducts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/FavoriteProduct")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $favoriteProducts = $this->favoriteProductRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($favoriteProducts->toArray(), 'Favorite Products retrieved successfully');
    }

    /**
     * @param CreateFavoriteProductAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/favoriteProducts",
     *      summary="Store a newly created FavoriteProduct in storage",
     *      tags={"FavoriteProduct"},
     *      description="Store FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FavoriteProduct that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FavoriteProduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                    type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function isFavorite(CreateFavoriteProductAPIRequest $request)
    {
        $input = $request->all();

        $product = Product::where(["id" => $request->product_id])->first();

        if (empty($product)) {
            return $this->sendError('Favorite Product not found');
        }

        $favoriteProducts = $this->favoriteProductRepository->all($input);

        if(count($favoriteProducts) == 0){
            $favoriteProduct = false;
        }else{
            $favoriteProduct = true;
        }


        return $this->sendResponse($favoriteProduct, 'Favorite Product saved successfully');
    }

    /**
     * @param CreateFavoriteProductAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/favoriteProducts",
     *      summary="Store a newly created FavoriteProduct in storage",
     *      tags={"FavoriteProduct"},
     *      description="Store FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FavoriteProduct that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FavoriteProduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Product"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFavoriteProductAPIRequest $request)
    {
        $input = $request->all();

        $product = Product::where(["id" => $request->product_id])->first();

        if (empty($product)) {
            return $this->sendError('Favorite Product not found');
        }

        $favoriteProducts = $this->favoriteProductRepository->all($input);

        if(count($favoriteProducts) == 0){
            $favoriteProduct = $this->favoriteProductRepository->create($input);
        }


        return $this->sendResponse($product->toArray(), 'Favorite Product saved successfully');
    }

    /**
     * @param CreateFavoriteProductAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/favoriteProducts",
     *      summary="Store a newly created FavoriteProduct in storage",
     *      tags={"FavoriteProduct"},
     *      description="Store FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FavoriteProduct that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FavoriteProduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Product"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function unFavorite(CreateFavoriteProductAPIRequest $request)
    {
        $input = $request->all();

        $product = Product::where(["id" => $request->product_id])->first();

        if (empty($product)) {
            return $this->sendError('Favorite Product not found');
        }

        $favoriteProducts = $this->favoriteProductRepository->all($input);

        if(count($favoriteProducts) == 0){

        }else{
            $favoriteProduct =   $favoriteProducts[0];
            $favoriteProduct->delete();
        }

        return $this->sendResponse($product->toArray(), 'Favorite Product saved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/favoriteProducts/{id}",
     *      summary="Display the specified FavoriteProduct",
     *      tags={"FavoriteProduct"},
     *      description="Get FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FavoriteProduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FavoriteProduct"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var FavoriteProduct $favoriteProduct */
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            return $this->sendError('Favorite Product not found');
        }

        return $this->sendResponse($favoriteProduct->toArray(), 'Favorite Product retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFavoriteProductAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/favoriteProducts/{id}",
     *      summary="Update the specified FavoriteProduct in storage",
     *      tags={"FavoriteProduct"},
     *      description="Update FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FavoriteProduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FavoriteProduct that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FavoriteProduct")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FavoriteProduct"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFavoriteProductAPIRequest $request)
    {
        $input = $request->all();

        /** @var FavoriteProduct $favoriteProduct */
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            return $this->sendError('Favorite Product not found');
        }

        $favoriteProduct = $this->favoriteProductRepository->update($input, $id);

        return $this->sendResponse($favoriteProduct->toArray(), 'FavoriteProduct updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/favoriteProducts/{id}",
     *      summary="Remove the specified FavoriteProduct from storage",
     *      tags={"FavoriteProduct"},
     *      description="Delete FavoriteProduct",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FavoriteProduct",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var FavoriteProduct $favoriteProduct */
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            return $this->sendError('Favorite Product not found');
        }

        $favoriteProduct->delete();

        return $this->sendResponse($id, 'Favorite Product deleted successfully');
    }

    public function findProductByCustomer($customerId){

        $favoriteProducts = $this->favoriteProductRepository->all([
            'customer_id' => $customerId
        ]);

        $products = Collection::make([]);

        foreach ($favoriteProducts as $favoriteProduct){
            $product = Product::where(['id' => $favoriteProduct->product_id])->first();
           if($product != null){
               $products->push($product);
           }

        }


        return $this->sendResponse($products->toArray(), 'Products retrieved successfully');
    }
}
