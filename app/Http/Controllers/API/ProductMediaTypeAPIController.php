<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductMediaTypeAPIRequest;
use App\Http\Requests\API\UpdateProductMediaTypeAPIRequest;
use App\Models\ProductMediaType;
use App\Repositories\ProductMediaTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductMediaTypeController
 * @package App\Http\Controllers\API
 */

class ProductMediaTypeAPIController extends AppBaseController
{
    /** @var  ProductMediaTypeRepository */
    private $productMediaTypeRepository;

    public function __construct(ProductMediaTypeRepository $productMediaTypeRepo)
    {
        $this->productMediaTypeRepository = $productMediaTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/productMediaTypes",
     *      summary="Get a listing of the ProductMediaTypes.",
     *      tags={"ProductMediaType"},
     *      description="Get all ProductMediaTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ProductMediaType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $productMediaTypes = $this->productMediaTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productMediaTypes->toArray(), 'Product Media Types retrieved successfully');
    }

    /**
     * @param CreateProductMediaTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/productMediaTypes",
     *      summary="Store a newly created ProductMediaType in storage",
     *      tags={"ProductMediaType"},
     *      description="Store ProductMediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductMediaType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductMediaType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProductMediaTypeAPIRequest $request)
    {
        $input = $request->all();

        $productMediaType = $this->productMediaTypeRepository->create($input);

        return $this->sendResponse($productMediaType->toArray(), 'Product Media Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/productMediaTypes/{id}",
     *      summary="Display the specified ProductMediaType",
     *      tags={"ProductMediaType"},
     *      description="Get ProductMediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ProductMediaType $productMediaType */
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            return $this->sendError('Product Media Type not found');
        }

        return $this->sendResponse($productMediaType->toArray(), 'Product Media Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProductMediaTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/productMediaTypes/{id}",
     *      summary="Update the specified ProductMediaType in storage",
     *      tags={"ProductMediaType"},
     *      description="Update ProductMediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductMediaType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductMediaType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMediaType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProductMediaTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductMediaType $productMediaType */
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            return $this->sendError('Product Media Type not found');
        }

        $productMediaType = $this->productMediaTypeRepository->update($input, $id);

        return $this->sendResponse($productMediaType->toArray(), 'ProductMediaType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/productMediaTypes/{id}",
     *      summary="Remove the specified ProductMediaType from storage",
     *      tags={"ProductMediaType"},
     *      description="Delete ProductMediaType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMediaType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ProductMediaType $productMediaType */
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            return $this->sendError('Product Media Type not found');
        }

        $productMediaType->delete();

        return $this->sendResponse($id, 'Product Media Type deleted successfully');
    }
}
