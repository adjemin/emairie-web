<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTokenTransactionAPIRequest;
use App\Http\Requests\API\UpdateTokenTransactionAPIRequest;
use App\Models\TokenTransaction;
use App\Repositories\TokenTransactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TokenTransactionController
 * @package App\Http\Controllers\API
 */

class TokenTransactionAPIController extends AppBaseController
{
    /** @var  TokenTransactionRepository */
    private $tokenTransactionRepository;

    public function __construct(TokenTransactionRepository $tokenTransactionRepo)
    {
        $this->tokenTransactionRepository = $tokenTransactionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/tokenTransactions",
     *      summary="Get a listing of the TokenTransactions.",
     *      tags={"TokenTransaction"},
     *      description="Get all TokenTransactions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TokenTransaction")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $tokenTransactions = $this->tokenTransactionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($tokenTransactions->toArray(), 'Token Transactions retrieved successfully');
    }

    /**
     * @param CreateTokenTransactionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/tokenTransactions",
     *      summary="Store a newly created TokenTransaction in storage",
     *      tags={"TokenTransaction"},
     *      description="Store TokenTransaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TokenTransaction that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TokenTransaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TokenTransaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTokenTransactionAPIRequest $request)
    {
        $input = $request->all();

        $tokenTransaction = $this->tokenTransactionRepository->create($input);

        return $this->sendResponse($tokenTransaction->toArray(), 'Token Transaction saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/tokenTransactions/{id}",
     *      summary="Display the specified TokenTransaction",
     *      tags={"TokenTransaction"},
     *      description="Get TokenTransaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TokenTransaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TokenTransaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TokenTransaction $tokenTransaction */
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            return $this->sendError('Token Transaction not found');
        }

        return $this->sendResponse($tokenTransaction->toArray(), 'Token Transaction retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTokenTransactionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/tokenTransactions/{id}",
     *      summary="Update the specified TokenTransaction in storage",
     *      tags={"TokenTransaction"},
     *      description="Update TokenTransaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TokenTransaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TokenTransaction that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TokenTransaction")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TokenTransaction"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTokenTransactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var TokenTransaction $tokenTransaction */
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            return $this->sendError('Token Transaction not found');
        }

        $tokenTransaction = $this->tokenTransactionRepository->update($input, $id);

        return $this->sendResponse($tokenTransaction->toArray(), 'TokenTransaction updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/tokenTransactions/{id}",
     *      summary="Remove the specified TokenTransaction from storage",
     *      tags={"TokenTransaction"},
     *      description="Delete TokenTransaction",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TokenTransaction",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TokenTransaction $tokenTransaction */
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            return $this->sendError('Token Transaction not found');
        }

        $tokenTransaction->delete();

        return $this->sendResponse($id, 'Token Transaction deleted successfully');
    }
}
