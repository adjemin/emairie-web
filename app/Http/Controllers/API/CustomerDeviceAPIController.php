<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerDeviceAPIRequest;
use App\Http\Requests\API\UpdateCustomerDeviceAPIRequest;
use App\Models\CustomerDevice;
use App\Repositories\CustomerDeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerDeviceController
 * @package App\Http\Controllers\API
 */

class CustomerDeviceAPIController extends AppBaseController
{
    /** @var  CustomerDeviceRepository */
    private $customerDeviceRepository;

    public function __construct(CustomerDeviceRepository $customerDeviceRepo)
    {
        $this->customerDeviceRepository = $customerDeviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerDevices",
     *      summary="Get a listing of the CustomerDevices.",
     *      tags={"CustomerDevice"},
     *      description="Get all CustomerDevices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerDevice")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerDevices = $this->customerDeviceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerDevices->toArray(), 'Customer Devices retrieved successfully');
    }

    /**
     * @param CreateCustomerDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerDevices",
     *      summary="Store a newly created CustomerDevice in storage",
     *      tags={"CustomerDevice"},
     *      description="Store CustomerDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerDevice that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerDevice")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerDeviceAPIRequest $request)
    {
        $input = $request->all();

        $customerDevices = $this->customerDeviceRepository->all(['firebase_id' => $input['firebase_id']]);
        if(count($customerDevices) == 0){
            $customerDevice = $this->customerDeviceRepository->create($input);
        }else{
            $customerDevice = $customerDevices[0];
            $customerDevice->delete();

            $customerDevice = $this->customerDeviceRepository->create($input);
        }


        return $this->sendResponse($customerDevice->toArray(), 'Customer Device saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerDevices/{id}",
     *      summary="Display the specified CustomerDevice",
     *      tags={"CustomerDevice"},
     *      description="Get CustomerDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        return $this->sendResponse($customerDevice->toArray(), 'Customer Device retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerDevices/{id}",
     *      summary="Update the specified CustomerDevice in storage",
     *      tags={"CustomerDevice"},
     *      description="Update CustomerDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerDevice that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerDevice")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerDevice"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerDeviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        $customerDevice = $this->customerDeviceRepository->update($input, $id);

        return $this->sendResponse($customerDevice->toArray(), 'CustomerDevice updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerDevices/{id}",
     *      summary="Remove the specified CustomerDevice from storage",
     *      tags={"CustomerDevice"},
     *      description="Delete CustomerDevice",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerDevice",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerDevice $customerDevice */
        $customerDevice = $this->customerDeviceRepository->find($id);

        if (empty($customerDevice)) {
            return $this->sendError('Customer Device not found');
        }

        $customerDevice->delete();

        return $this->sendResponse($id, 'Customer Device deleted successfully');
    }
}
