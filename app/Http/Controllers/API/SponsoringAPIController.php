<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSponsoringAPIRequest;
use App\Http\Requests\API\UpdateSponsoringAPIRequest;
use App\Models\Sponsoring;
use App\Repositories\SponsoringRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SponsoringController
 * @package App\Http\Controllers\API
 */

class SponsoringAPIController extends AppBaseController
{
    /** @var  SponsoringRepository */
    private $sponsoringRepository;

    public function __construct(SponsoringRepository $sponsoringRepo)
    {
        $this->sponsoringRepository = $sponsoringRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/sponsorings",
     *      summary="Get a listing of the Sponsorings.",
     *      tags={"Sponsoring"},
     *      description="Get all Sponsorings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Sponsoring")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $sponsorings = $this->sponsoringRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sponsorings->toArray(), 'Sponsorings retrieved successfully');
    }

    /**
     * @param CreateSponsoringAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/sponsorings",
     *      summary="Store a newly created Sponsoring in storage",
     *      tags={"Sponsoring"},
     *      description="Store Sponsoring",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Sponsoring that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Sponsoring")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Sponsoring"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSponsoringAPIRequest $request)
    {
        $input = $request->all();

        $sponsoring = $this->sponsoringRepository->create($input);

        return $this->sendResponse($sponsoring->toArray(), 'Sponsoring saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/sponsorings/{id}",
     *      summary="Display the specified Sponsoring",
     *      tags={"Sponsoring"},
     *      description="Get Sponsoring",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Sponsoring",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Sponsoring"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Sponsoring $sponsoring */
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            return $this->sendError('Sponsoring not found');
        }

        return $this->sendResponse($sponsoring->toArray(), 'Sponsoring retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSponsoringAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/sponsorings/{id}",
     *      summary="Update the specified Sponsoring in storage",
     *      tags={"Sponsoring"},
     *      description="Update Sponsoring",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Sponsoring",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Sponsoring that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Sponsoring")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Sponsoring"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSponsoringAPIRequest $request)
    {
        $input = $request->all();

        /** @var Sponsoring $sponsoring */
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            return $this->sendError('Sponsoring not found');
        }

        $sponsoring = $this->sponsoringRepository->update($input, $id);

        return $this->sendResponse($sponsoring->toArray(), 'Sponsoring updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/sponsorings/{id}",
     *      summary="Remove the specified Sponsoring from storage",
     *      tags={"Sponsoring"},
     *      description="Delete Sponsoring",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Sponsoring",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Sponsoring $sponsoring */
        $sponsoring = $this->sponsoringRepository->find($id);

        if (empty($sponsoring)) {
            return $this->sendError('Sponsoring not found');
        }

        $sponsoring->delete();

        return $this->sendResponse($id, 'Sponsoring deleted successfully');
    }
}
