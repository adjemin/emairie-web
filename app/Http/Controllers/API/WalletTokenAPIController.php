<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateWalletTokenAPIRequest;
use App\Http\Requests\API\UpdateWalletTokenAPIRequest;
use App\Models\WalletToken;
use App\Repositories\WalletTokenRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class WalletTokenController
 * @package App\Http\Controllers\API
 */

class WalletTokenAPIController extends AppBaseController
{
    /** @var  WalletTokenRepository */
    private $walletTokenRepository;

    public function __construct(WalletTokenRepository $walletTokenRepo)
    {
        $this->walletTokenRepository = $walletTokenRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/walletTokens",
     *      summary="Get a listing of the WalletTokens.",
     *      tags={"WalletToken"},
     *      description="Get all WalletTokens",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/WalletToken")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $walletTokens = $this->walletTokenRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($walletTokens->toArray(), 'Wallet Tokens retrieved successfully');
    }

    /**
     * @param CreateWalletTokenAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/walletTokens",
     *      summary="Store a newly created WalletToken in storage",
     *      tags={"WalletToken"},
     *      description="Store WalletToken",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="WalletToken that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/WalletToken")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletToken"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWalletTokenAPIRequest $request)
    {
        $input = $request->all();

        $walletToken = $this->walletTokenRepository->create($input);

        return $this->sendResponse($walletToken->toArray(), 'Wallet Token saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/walletTokens/{id}",
     *      summary="Display the specified WalletToken",
     *      tags={"WalletToken"},
     *      description="Get WalletToken",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletToken",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletToken"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var WalletToken $walletToken */
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            return $this->sendError('Wallet Token not found');
        }

        return $this->sendResponse($walletToken->toArray(), 'Wallet Token retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWalletTokenAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/walletTokens/{id}",
     *      summary="Update the specified WalletToken in storage",
     *      tags={"WalletToken"},
     *      description="Update WalletToken",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletToken",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="WalletToken that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/WalletToken")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/WalletToken"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWalletTokenAPIRequest $request)
    {
        $input = $request->all();

        /** @var WalletToken $walletToken */
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            return $this->sendError('Wallet Token not found');
        }

        $walletToken = $this->walletTokenRepository->update($input, $id);

        return $this->sendResponse($walletToken->toArray(), 'WalletToken updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/walletTokens/{id}",
     *      summary="Remove the specified WalletToken from storage",
     *      tags={"WalletToken"},
     *      description="Delete WalletToken",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of WalletToken",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var WalletToken $walletToken */
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            return $this->sendError('Wallet Token not found');
        }

        $walletToken->delete();

        return $this->sendResponse($id, 'Wallet Token deleted successfully');
    }
}
