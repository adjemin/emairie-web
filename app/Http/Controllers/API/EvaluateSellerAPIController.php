<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEvaluateSellerAPIRequest;
use App\Http\Requests\API\UpdateEvaluateSellerAPIRequest;
use App\Models\EvaluateSeller;
use App\Repositories\EvaluateSellerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EvaluateSellerController
 * @package App\Http\Controllers\API
 */

class EvaluateSellerAPIController extends AppBaseController
{
    /** @var  EvaluateSellerRepository */
    private $evaluateSellerRepository;

    public function __construct(EvaluateSellerRepository $evaluateSellerRepo)
    {
        $this->evaluateSellerRepository = $evaluateSellerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/evaluateSellers",
     *      summary="Get a listing of the EvaluateSellers.",
     *      tags={"EvaluateSeller"},
     *      description="Get all EvaluateSellers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/EvaluateSeller")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $evaluateSellers = $this->evaluateSellerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($evaluateSellers->toArray(), 'Evaluate Sellers retrieved successfully');
    }

    /**
     * @param CreateEvaluateSellerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/evaluateSellers",
     *      summary="Store a newly created EvaluateSeller in storage",
     *      tags={"EvaluateSeller"},
     *      description="Store EvaluateSeller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EvaluateSeller that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EvaluateSeller")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EvaluateSeller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEvaluateSellerAPIRequest $request)
    {
        $input = $request->all();

        $evaluateSeller = $this->evaluateSellerRepository->create($input);

        return $this->sendResponse($evaluateSeller->toArray(), 'Evaluate Seller saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/evaluateSellers/{id}",
     *      summary="Display the specified EvaluateSeller",
     *      tags={"EvaluateSeller"},
     *      description="Get EvaluateSeller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EvaluateSeller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EvaluateSeller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var EvaluateSeller $evaluateSeller */
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            return $this->sendError('Evaluate Seller not found');
        }

        return $this->sendResponse($evaluateSeller->toArray(), 'Evaluate Seller retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEvaluateSellerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/evaluateSellers/{id}",
     *      summary="Update the specified EvaluateSeller in storage",
     *      tags={"EvaluateSeller"},
     *      description="Update EvaluateSeller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EvaluateSeller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EvaluateSeller that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EvaluateSeller")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EvaluateSeller"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEvaluateSellerAPIRequest $request)
    {
        $input = $request->all();

        /** @var EvaluateSeller $evaluateSeller */
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            return $this->sendError('Evaluate Seller not found');
        }

        $evaluateSeller = $this->evaluateSellerRepository->update($input, $id);

        return $this->sendResponse($evaluateSeller->toArray(), 'EvaluateSeller updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/evaluateSellers/{id}",
     *      summary="Remove the specified EvaluateSeller from storage",
     *      tags={"EvaluateSeller"},
     *      description="Delete EvaluateSeller",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EvaluateSeller",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var EvaluateSeller $evaluateSeller */
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            return $this->sendError('Evaluate Seller not found');
        }

        $evaluateSeller->delete();

        return $this->sendResponse($id, 'Evaluate Seller deleted successfully');
    }
}
