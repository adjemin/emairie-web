<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSponsoringTypeAPIRequest;
use App\Http\Requests\API\UpdateSponsoringTypeAPIRequest;
use App\Models\SponsoringType;
use App\Repositories\SponsoringTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SponsoringTypeController
 * @package App\Http\Controllers\API
 */

class SponsoringTypeAPIController extends AppBaseController
{
    /** @var  SponsoringTypeRepository */
    private $sponsoringTypeRepository;

    public function __construct(SponsoringTypeRepository $sponsoringTypeRepo)
    {
        $this->sponsoringTypeRepository = $sponsoringTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/sponsoringTypes",
     *      summary="Get a listing of the SponsoringTypes.",
     *      tags={"SponsoringType"},
     *      description="Get all SponsoringTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SponsoringType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $sponsoringTypes = $this->sponsoringTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sponsoringTypes->toArray(), 'Sponsoring Types retrieved successfully');
    }

    /**
     * @param CreateSponsoringTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/sponsoringTypes",
     *      summary="Store a newly created SponsoringType in storage",
     *      tags={"SponsoringType"},
     *      description="Store SponsoringType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SponsoringType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SponsoringType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SponsoringType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSponsoringTypeAPIRequest $request)
    {
        $input = $request->all();

        $sponsoringType = $this->sponsoringTypeRepository->create($input);

        return $this->sendResponse($sponsoringType->toArray(), 'Sponsoring Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/sponsoringTypes/{id}",
     *      summary="Display the specified SponsoringType",
     *      tags={"SponsoringType"},
     *      description="Get SponsoringType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SponsoringType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SponsoringType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SponsoringType $sponsoringType */
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            return $this->sendError('Sponsoring Type not found');
        }

        return $this->sendResponse($sponsoringType->toArray(), 'Sponsoring Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSponsoringTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/sponsoringTypes/{id}",
     *      summary="Update the specified SponsoringType in storage",
     *      tags={"SponsoringType"},
     *      description="Update SponsoringType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SponsoringType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SponsoringType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SponsoringType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SponsoringType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSponsoringTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var SponsoringType $sponsoringType */
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            return $this->sendError('Sponsoring Type not found');
        }

        $sponsoringType = $this->sponsoringTypeRepository->update($input, $id);

        return $this->sendResponse($sponsoringType->toArray(), 'SponsoringType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/sponsoringTypes/{id}",
     *      summary="Remove the specified SponsoringType from storage",
     *      tags={"SponsoringType"},
     *      description="Delete SponsoringType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SponsoringType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SponsoringType $sponsoringType */
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            return $this->sendError('Sponsoring Type not found');
        }

        $sponsoringType->delete();

        return $this->sendResponse($id, 'Sponsoring Type deleted successfully');
    }
}
