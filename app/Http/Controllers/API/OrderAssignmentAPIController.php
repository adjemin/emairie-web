<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderAssignmentAPIRequest;
use App\Http\Requests\API\UpdateOrderAssignmentAPIRequest;
use App\Models\OrderAssignment;
use App\Repositories\OrderAssignmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderAssignmentController
 * @package App\Http\Controllers\API
 */

class OrderAssignmentAPIController extends AppBaseController
{
    /** @var  OrderAssignmentRepository */
    private $orderAssignmentRepository;

    public function __construct(OrderAssignmentRepository $orderAssignmentRepo)
    {
        $this->orderAssignmentRepository = $orderAssignmentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderAssignments",
     *      summary="Get a listing of the OrderAssignments.",
     *      tags={"OrderAssignment"},
     *      description="Get all OrderAssignments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OrderAssignment")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orderAssignments = $this->orderAssignmentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderAssignments->toArray(), 'Order Assignments retrieved successfully');
    }

    /**
     * @param CreateOrderAssignmentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orderAssignments",
     *      summary="Store a newly created OrderAssignment in storage",
     *      tags={"OrderAssignment"},
     *      description="Store OrderAssignment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderAssignment that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderAssignment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderAssignment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderAssignmentAPIRequest $request)
    {
        $input = $request->all();

        $orderAssignment = $this->orderAssignmentRepository->create($input);

        return $this->sendResponse($orderAssignment->toArray(), 'Order Assignment saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderAssignments/{id}",
     *      summary="Display the specified OrderAssignment",
     *      tags={"OrderAssignment"},
     *      description="Get OrderAssignment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderAssignment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderAssignment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OrderAssignment $orderAssignment */
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            return $this->sendError('Order Assignment not found');
        }

        return $this->sendResponse($orderAssignment->toArray(), 'Order Assignment retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderAssignmentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orderAssignments/{id}",
     *      summary="Update the specified OrderAssignment in storage",
     *      tags={"OrderAssignment"},
     *      description="Update OrderAssignment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderAssignment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderAssignment that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderAssignment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderAssignment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrderAssignmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderAssignment $orderAssignment */
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            return $this->sendError('Order Assignment not found');
        }

        $orderAssignment = $this->orderAssignmentRepository->update($input, $id);

        return $this->sendResponse($orderAssignment->toArray(), 'OrderAssignment updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orderAssignments/{id}",
     *      summary="Remove the specified OrderAssignment from storage",
     *      tags={"OrderAssignment"},
     *      description="Delete OrderAssignment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderAssignment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OrderAssignment $orderAssignment */
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            return $this->sendError('Order Assignment not found');
        }

        $orderAssignment->delete();

        return $this->sendResponse($id, 'Order Assignment deleted successfully');
    }
}
