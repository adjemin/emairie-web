<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerNotificationAPIRequest;
use App\Http\Requests\API\UpdateCustomerNotificationAPIRequest;
use App\Models\CustomerNotification;
use App\Repositories\CustomerNotificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerNotificationController
 * @package App\Http\Controllers\API
 */

class CustomerNotificationAPIController extends AppBaseController
{
    /** @var  CustomerNotificationRepository */
    private $customerNotificationRepository;

    public function __construct(CustomerNotificationRepository $customerNotificationRepo)
    {
        $this->customerNotificationRepository = $customerNotificationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerNotifications",
     *      summary="Get a listing of the CustomerNotifications.",
     *      tags={"CustomerNotification"},
     *      description="Get all CustomerNotifications",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerNotification")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerNotifications = $this->customerNotificationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerNotifications->toArray(), 'Customer Notifications retrieved successfully');
    }

    /**
     * @param CreateCustomerNotificationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerNotifications",
     *      summary="Store a newly created CustomerNotification in storage",
     *      tags={"CustomerNotification"},
     *      description="Store CustomerNotification",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerNotification that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerNotification")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotification"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerNotificationAPIRequest $request)
    {
        $input = $request->all();

        $customerNotification = $this->customerNotificationRepository->create($input);

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerNotifications/{id}",
     *      summary="Display the specified CustomerNotification",
     *      tags={"CustomerNotification"},
     *      description="Get CustomerNotification",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotification",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotification"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerNotificationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerNotifications/{id}",
     *      summary="Update the specified CustomerNotification in storage",
     *      tags={"CustomerNotification"},
     *      description="Update CustomerNotification",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotification",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerNotification that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerNotification")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerNotification"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerNotificationAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification = $this->customerNotificationRepository->update($input, $id);

        return $this->sendResponse($customerNotification->toArray(), 'CustomerNotification updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerNotifications/{id}",
     *      summary="Remove the specified CustomerNotification from storage",
     *      tags={"CustomerNotification"},
     *      description="Delete CustomerNotification",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerNotification",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification->delete();

        return $this->sendResponse($id, 'Customer Notification deleted successfully');
    }

    public function makeRead($id){
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification->is_read  = true;
        $customerNotification->update();

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification deleted successfully');
    }

    public function makeReceive($id){
        /** @var CustomerNotification $customerNotification */
        $customerNotification = $this->customerNotificationRepository->find($id);

        if (empty($customerNotification)) {
            return $this->sendError('Customer Notification not found');
        }

        $customerNotification->is_received = true;
        $customerNotification->update();

        return $this->sendResponse($customerNotification->toArray(), 'Customer Notification deleted successfully');
    }
}
