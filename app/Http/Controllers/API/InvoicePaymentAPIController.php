<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateInvoicePaymentAPIRequest;
use App\Http\Requests\API\UpdateInvoicePaymentAPIRequest;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\OrderItem;
use App\Repositories\InvoicePaymentRepository;
use CinetPay\CinetPay;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class InvoicePaymentController
 * @package App\Http\Controllers\API
 */

class InvoicePaymentAPIController extends AppBaseController
{
    /** @var  InvoicePaymentRepository */
    private $invoicePaymentRepository;

    public function __construct(InvoicePaymentRepository $invoicePaymentRepo)
    {
        $this->invoicePaymentRepository = $invoicePaymentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/invoicePayments",
     *      summary="Get a listing of the InvoicePayments.",
     *      tags={"InvoicePayment"},
     *      description="Get all InvoicePayments",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/InvoicePayment")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $invoicePayments = $this->invoicePaymentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($invoicePayments->toArray(), 'Invoice Payments retrieved successfully');
    }

    /**
     * @param CreateInvoicePaymentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/invoicePayments",
     *      summary="Store a newly created InvoicePayment in storage",
     *      tags={"InvoicePayment"},
     *      description="Store InvoicePayment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="InvoicePayment that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/InvoicePayment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/InvoicePayment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        $invoicePayments = $this->invoicePaymentRepository->create($input);

        return $this->sendResponse($invoicePayments->toArray(), 'Invoice Payment saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/invoicePayments/{id}",
     *      summary="Display the specified InvoicePayment",
     *      tags={"InvoicePayment"},
     *      description="Get InvoicePayment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of InvoicePayment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/InvoicePayment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->findWithoutFail($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        return $this->sendResponse($invoicePayment->toArray(), 'Invoice Payment retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateInvoicePaymentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/invoicePayments/{id}",
     *      summary="Update the specified InvoicePayment in storage",
     *      tags={"InvoicePayment"},
     *      description="Update InvoicePayment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of InvoicePayment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="InvoicePayment that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/InvoicePayment")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/InvoicePayment"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateInvoicePaymentAPIRequest $request)
    {
        $input = $request->all();

        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->findWithoutFail($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment = $this->invoicePaymentRepository->update($input, $id);

        return $this->sendResponse($invoicePayment->toArray(), 'InvoicePayment updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/invoicePayments/{id}",
     *      summary="Remove the specified InvoicePayment from storage",
     *      tags={"InvoicePayment"},
     *      description="Delete InvoicePayment",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of InvoicePayment",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var InvoicePayment $invoicePayment */
        $invoicePayment = $this->invoicePaymentRepository->findWithoutFail($id);

        if (empty($invoicePayment)) {
            return $this->sendError('Invoice Payment not found');
        }

        $invoicePayment->delete();

        return $this->sendResponse($id, 'Invoice Payment deleted successfully');
    }

    public function notify(Request $request){

        if($request->has('cpm_trans_id')){

            try {
                $id_transaction = $request->cpm_trans_id;
                $apiKey = '18122291995d9845221e7d53.99745292';
                $site_id = '182538';
                $CinetPay = new CinetPay($site_id, $apiKey);
                // Reprise exacte des bonnes données chez CinetPay
                $CinetPay->setTransId($id_transaction)->getPayStatus();
                $cpm_site_id = $CinetPay->_cpm_site_id;
                $signature = $CinetPay->_signature;
                $cpm_amount = $CinetPay->_cpm_amount;
                $cpm_trans_id = $CinetPay->_cpm_trans_id;
                $cpm_custom = $CinetPay->_cpm_custom;
                $cpm_currency = $CinetPay->_cpm_currency;
                $cpm_payid = $CinetPay->_cpm_payid;
                $cpm_payment_date = $CinetPay->_cpm_payment_date;
                $cpm_payment_time = $CinetPay->_cpm_payment_time;
                $cpm_error_message = $CinetPay->_cpm_error_message;
                $payment_method = $CinetPay->_payment_method;
                $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
                $cel_phone_num = $CinetPay->_cel_phone_num;
                $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
                $created_at = $CinetPay->_created_at;
                $updated_at = $CinetPay->_updated_at;
                $cpm_result = $CinetPay->_cpm_result;
                $cpm_trans_status = $CinetPay->_cpm_trans_status;
                $cpm_designation = $CinetPay->_cpm_designation;
                $buyer_name = $CinetPay->_buyer_name;

                // Recuperation de la ligne de la transaction dans votre base de données
                $transaction = InvoicePayment::where(['payment_reference' => $id_transaction])->first();

                $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();

                $order = Order::where(['id' => $invoice->order_id])->first();
                // Verification de l'etat du traitement de la commande
                if($order->is_waiting_payment){

                    if($transaction != null){

                        $transaction->payment_gateway_trans_id = $cpm_trans_id;
                        $transaction->payment_gateway_custom = $cpm_custom;
                        $transaction->payment_gateway_currency = $cpm_currency;
                        $transaction->payment_gateway_amount = $cpm_amount;
                        $transaction->payment_gateway_payid = $cpm_payid;
                        $transaction->payment_gateway_payment_date = $cpm_payment_date;
                        $transaction->payment_gateway_payment_time = $cpm_payment_time;
                        $transaction->payment_gateway_error_message = $cpm_error_message;
                        $transaction->payment_gateway_payment_method = $payment_method;
                        $transaction->payment_gateway_phone_prefixe = $cpm_phone_prefixe;
                        $transaction->payment_gateway_cel_phone_num = $cel_phone_num;
                        $transaction->payment_gateway_ipn_ack = $cpm_ipn_ack;
                        $transaction->payment_gateway_created_at = $created_at;
                        $transaction->payment_gateway_updated_at = $updated_at;
                        $transaction->payment_gateway_cpm_result = $cpm_result;
                        $transaction->payment_gateway_trans_status = $cpm_trans_status;
                        $transaction->payment_gateway_designation = $cpm_designation;
                        $transaction->payment_gateway_buyer_name = $buyer_name;
                        $transaction->payment_gateway_signature = $signature;
                        $transaction->save();

                        // On verifie que le montant payé chez CinetPay correspond à notre montant en base de données pour cette transaction
                        if($order->amount == $cpm_amount ){

                            // On verifie que le paiement est valide
                            if ($CinetPay->isValidPayment()) {
                                $transaction->status = 'SUCCESSFUL';
                                $transaction->is_waiting = false;
                                $transaction->is_completed = true;
                                $transaction->save();

                                $this->validateTransaction($id_transaction);

                                echo 'Félicitation, votre paiement a été effectué avec succès';
                                die();
                            } else {

                                if($cpm_result == '623'){
                                    echo "".$CinetPay->_cpm_error_message;
                                    die();
                                }else{
                                   // $this->cancelTransaction($id_transaction);
                                    echo 'Echec, votre paiement a échoué pour cause : ' . $CinetPay->_cpm_error_message;
                                    die();
                                }


                            }

                        }else{
                            //Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande
                            $transaction->status = 'REFUSED';
                            $transaction->is_waiting = false;
                            $transaction->is_completed = true;
                            $transaction->save();
                            //$this->cancelTransaction($id_transaction);

                            echo 'Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande';
                            die();

                        }

                    }

                }else{
                    // Si le paiement est bon alors ne traitez plus cette transaction : die();
                    // La commande a été déjà traité
                    // Arret du script
                    die();
                }



            } catch (\Exception $e) {
                echo "Erreur :" . $e->getMessage();
                // Une erreur s'est produite
            }

        }else {
            // redirection vers la page d'accueil
            die();
        }

    }

    public function validateTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        if($transaction->status == 'SUCCESSFUL'){
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid_by_customer = true;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();

            $customer = Customer::where(['id' => $order->customer_id])->first();


            if($invoice->service == 'shop'){
                $orderItem = OrderItem::where(["order_id" => $order->id])->first();

                $order->current_status = 'customer_paid';
                $order->is_waiting_payment = false;
                $order->save();

                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = 'customer_paid';
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customer';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();

            }


        }

    }

    public function cancelTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
        $invoice->status = 'canceled';
        $invoice->is_paid = false;
        $invoice->save();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->status = 'canceled';
        $order->save();
    }
}
