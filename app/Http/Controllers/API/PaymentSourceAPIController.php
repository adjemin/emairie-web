<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePaymentSourceAPIRequest;
use App\Http\Requests\API\UpdatePaymentSourceAPIRequest;
use App\Models\PaymentSource;
use App\Repositories\PaymentSourceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PaymentSourceController
 * @package App\Http\Controllers\API
 */

class PaymentSourceAPIController extends AppBaseController
{
    /** @var  PaymentSourceRepository */
    private $paymentSourceRepository;

    public function __construct(PaymentSourceRepository $paymentSourceRepo)
    {
        $this->paymentSourceRepository = $paymentSourceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/paymentSources",
     *      summary="Get a listing of the PaymentSources.",
     *      tags={"PaymentSource"},
     *      description="Get all PaymentSources",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PaymentSource")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $paymentSources = $this->paymentSourceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($paymentSources->toArray(), 'Payment Sources retrieved successfully');
    }

    /**
     * @param CreatePaymentSourceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/paymentSources",
     *      summary="Store a newly created PaymentSource in storage",
     *      tags={"PaymentSource"},
     *      description="Store PaymentSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PaymentSource that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PaymentSource")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PaymentSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePaymentSourceAPIRequest $request)
    {
        $input = $request->all();

        $paymentSource = $this->paymentSourceRepository->create($input);

        return $this->sendResponse($paymentSource->toArray(), 'Payment Source saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/paymentSources/{id}",
     *      summary="Display the specified PaymentSource",
     *      tags={"PaymentSource"},
     *      description="Get PaymentSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PaymentSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PaymentSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PaymentSource $paymentSource */
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            return $this->sendError('Payment Source not found');
        }

        return $this->sendResponse($paymentSource->toArray(), 'Payment Source retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePaymentSourceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/paymentSources/{id}",
     *      summary="Update the specified PaymentSource in storage",
     *      tags={"PaymentSource"},
     *      description="Update PaymentSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PaymentSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PaymentSource that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PaymentSource")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PaymentSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePaymentSourceAPIRequest $request)
    {
        $input = $request->all();

        /** @var PaymentSource $paymentSource */
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            return $this->sendError('Payment Source not found');
        }

        $paymentSource = $this->paymentSourceRepository->update($input, $id);

        return $this->sendResponse($paymentSource->toArray(), 'PaymentSource updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/paymentSources/{id}",
     *      summary="Remove the specified PaymentSource from storage",
     *      tags={"PaymentSource"},
     *      description="Delete PaymentSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PaymentSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PaymentSource $paymentSource */
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            return $this->sendError('Payment Source not found');
        }

        $paymentSource->delete();

        return $this->sendResponse($id, 'Payment Source deleted successfully');
    }
}
