<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductAudienceAPIRequest;
use App\Http\Requests\API\UpdateProductAudienceAPIRequest;
use App\Models\ProductAudience;
use App\Repositories\ProductAudienceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductAudienceController
 * @package App\Http\Controllers\API
 */

class ProductAudienceAPIController extends AppBaseController
{
    /** @var  ProductAudienceRepository */
    private $productAudienceRepository;

    public function __construct(ProductAudienceRepository $productAudienceRepo)
    {
        $this->productAudienceRepository = $productAudienceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/productAudiences",
     *      summary="Get a listing of the ProductAudiences.",
     *      tags={"ProductAudience"},
     *      description="Get all ProductAudiences",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ProductAudience")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $productAudiences = $this->productAudienceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productAudiences->toArray(), 'Product Audiences retrieved successfully');
    }

    /**
     * @param CreateProductAudienceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/productAudiences",
     *      summary="Store a newly created ProductAudience in storage",
     *      tags={"ProductAudience"},
     *      description="Store ProductAudience",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductAudience that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductAudience")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductAudience"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProductAudienceAPIRequest $request)
    {
        $input = $request->all();

        $productAudience = $this->productAudienceRepository->create($input);

        return $this->sendResponse($productAudience->toArray(), 'Product Audience saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/productAudiences/{id}",
     *      summary="Display the specified ProductAudience",
     *      tags={"ProductAudience"},
     *      description="Get ProductAudience",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductAudience",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductAudience"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ProductAudience $productAudience */
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            return $this->sendError('Product Audience not found');
        }

        return $this->sendResponse($productAudience->toArray(), 'Product Audience retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProductAudienceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/productAudiences/{id}",
     *      summary="Update the specified ProductAudience in storage",
     *      tags={"ProductAudience"},
     *      description="Update ProductAudience",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductAudience",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductAudience that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductAudience")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductAudience"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProductAudienceAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductAudience $productAudience */
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            return $this->sendError('Product Audience not found');
        }

        $productAudience = $this->productAudienceRepository->update($input, $id);

        return $this->sendResponse($productAudience->toArray(), 'ProductAudience updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/productAudiences/{id}",
     *      summary="Remove the specified ProductAudience from storage",
     *      tags={"ProductAudience"},
     *      description="Delete ProductAudience",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductAudience",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ProductAudience $productAudience */
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            return $this->sendError('Product Audience not found');
        }

        $productAudience->delete();

        return $this->sendResponse($id, 'Product Audience deleted successfully');
    }
}
