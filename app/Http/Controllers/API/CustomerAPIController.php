<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Customer;
use App\Models\CustomerNotification;
use App\Models\CustomerSession;
use App\Models\DeliveryAddress;
use App\Models\PaymentMethod;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customers",
     *      summary="Get a listing of the Customers.",
     *      tags={"Customer"},
     *      description="Get all Customers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Customer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }


    /**
     * @param string $phone_with_prefix
     * @return Response
     *
     * @SWG\Get(
     *      path="/customers/{phone_with_prefix}",
     *      summary="Display the specified Customer",
     *      tags={"Customer"},
     *      description="Get Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function findByPhone($phone_with_prefix)
    {
        $customers = $this->customerRepository->all(
            ['phone' => $phone_with_prefix],
            null,
            1
        );

        if (empty($customers)) {
            return $this->sendError('Customer not found', 404);
        }


        return $this->sendResponse($customers[0]->toArray(), 'Customer retrieved successfully');
    }

    public function login(Request $request){

        /** @var string $phone_number */
        $phone_number = $request->json()->get('phone_number');

        /** @var string $dial_code */
        $dial_code = $request->json()->get('dial_code');

        /** @var  $session */
        $session = (array) $request->json()->get('session');

        $customers = $this->customerRepository->all(
            ['phone_number' => $phone_number, 'dial_code' => $dial_code],
            null,
            1
        );



        if (count($customers) == 0) {
            return $this->sendError('Customer not found', 404);
        }else{
            $customer = $customers[0];

            $session["customer_id"] = $customer->id;

            $customerSession = CustomerSession::create($session);

            $deliveryAddresses = DeliveryAddress::where(['customer_id' => $customer->id])->get();

            $customerNotifications = CustomerNotification::where(["customer_id" => $customer->id])->get();

            $payment_methods = PaymentMethod::where(['is_actived' => true])->get();
            $currencies = Currency::all();

            $data = [
                'user' => $customer,
                'deliveryAddresses' => $deliveryAddresses,
                'notifications' => $customerNotifications,
                'payment_methods' => $payment_methods,
                'currencies' => $currencies
            ];
            return $this->sendResponse($data, 'Customer retrieved successfully');

        }



    }

    /**
     * @param CreateCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customers",
     *      summary="Store a newly created Customer in storage",
     *      tags={"Customer"},
     *      description="Store Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Customer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerAPIRequest $request)
    {
        $input = $request->json()->all();

        /** @var string $phone_number */
        $phone_number = $request->json()->get('phone_number');

        /** @var string $dial_code */
        $dial_code = $request->json()->get('dial_code');

        $phone = $dial_code."".$phone_number;

        /** @var string $first_name */
        $first_name = $request->json()->get('first_name');
        $first_name = ucfirst(strtolower($first_name));

        /** @var string $last_name */
        $last_name = $request->json()->get('last_name');
        $last_name = ucfirst(strtolower($last_name));

        $name = $first_name." ".$last_name;

        /** @var string $email */
        $email = $request->json()->get('email');

        /** @var string $country_code */
        $country_code = $request->json()->get('country_code');

        $countryModel = Country::where(['code' => $country_code ])->first();
        if($countryModel != null){
            $language = $countryModel->language;
        }else{
            $language = 'fr';
        }

        /** @var  $session */
        $session = (array) $request->json()->get('session');

        $customer = $this->customerRepository->create([
            'dial_code' => $dial_code,
            'phone_number' => $phone_number,
            'phone' => $phone,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'name' => $name,
            'email' => $email,
            'country_code' => $country_code,
            'language' => $language
        ]);

        $session["customer_id"] = $customer->id;

        $customerSession = CustomerSession::create($session);

        $deliveryAddresses = DeliveryAddress::where(['customer_id' => $customer->id])->get();

        $payment_methods = PaymentMethod::where(['is_actived' => true])->get();
        $currencies = Currency::all();

        $data = [
            'user' => $customer,
            'deliveryAddresses' => $deliveryAddresses,
            'notifications' => [],
            'payment_methods' => $payment_methods,
            'currencies' => $currencies
        ];

        return $this->sendResponse($data, 'Customer saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customers/{id}",
     *      summary="Display the specified Customer",
     *      tags={"Customer"},
     *      description="Get Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        return $this->sendResponse($customer->toArray(), 'Customer retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customers/{id}",
     *      summary="Update the specified Customer in storage",
     *      tags={"Customer"},
     *      description="Update Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Customer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer = $this->customerRepository->update($input, $id);

        return $this->sendResponse($customer->toArray(), 'Customer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customers/{id}",
     *      summary="Remove the specified Customer from storage",
     *      tags={"Customer"},
     *      description="Delete Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer->delete();

        return $this->sendResponse($id, 'Customer deleted successfully');
    }
}
