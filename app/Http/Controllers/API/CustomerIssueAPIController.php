<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerIssueAPIRequest;
use App\Http\Requests\API\UpdateCustomerIssueAPIRequest;
use App\Models\CustomerIssue;
use App\Repositories\CustomerIssueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerIssueController
 * @package App\Http\Controllers\API
 */

class CustomerIssueAPIController extends AppBaseController
{
    /** @var  CustomerIssueRepository */
    private $customerIssueRepository;

    public function __construct(CustomerIssueRepository $customerIssueRepo)
    {
        $this->customerIssueRepository = $customerIssueRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerIssues",
     *      summary="Get a listing of the CustomerIssues.",
     *      tags={"CustomerIssue"},
     *      description="Get all CustomerIssues",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerIssue")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerIssues = $this->customerIssueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerIssues->toArray(), 'Customer Issues retrieved successfully');
    }

    /**
     * @param CreateCustomerIssueAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerIssues",
     *      summary="Store a newly created CustomerIssue in storage",
     *      tags={"CustomerIssue"},
     *      description="Store CustomerIssue",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerIssue that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerIssue")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerIssue"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerIssueAPIRequest $request)
    {
        $input = $request->all();

        $customerIssue = $this->customerIssueRepository->create($input);

        return $this->sendResponse($customerIssue->toArray(), 'Customer Issue saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerIssues/{id}",
     *      summary="Display the specified CustomerIssue",
     *      tags={"CustomerIssue"},
     *      description="Get CustomerIssue",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerIssue",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerIssue"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerIssue $customerIssue */
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            return $this->sendError('Customer Issue not found');
        }

        return $this->sendResponse($customerIssue->toArray(), 'Customer Issue retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerIssueAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerIssues/{id}",
     *      summary="Update the specified CustomerIssue in storage",
     *      tags={"CustomerIssue"},
     *      description="Update CustomerIssue",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerIssue",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerIssue that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerIssue")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerIssue"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerIssueAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerIssue $customerIssue */
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            return $this->sendError('Customer Issue not found');
        }

        $customerIssue = $this->customerIssueRepository->update($input, $id);

        return $this->sendResponse($customerIssue->toArray(), 'CustomerIssue updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerIssues/{id}",
     *      summary="Remove the specified CustomerIssue from storage",
     *      tags={"CustomerIssue"},
     *      description="Delete CustomerIssue",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerIssue",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerIssue $customerIssue */
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            return $this->sendError('Customer Issue not found');
        }

        $customerIssue->delete();

        return $this->sendResponse($id, 'Customer Issue deleted successfully');
    }
}
