<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderDeliveryAPIRequest;
use App\Http\Requests\API\UpdateOrderDeliveryAPIRequest;
use App\Models\OrderDelivery;
use App\Repositories\OrderDeliveryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderDeliveryController
 * @package App\Http\Controllers\API
 */

class OrderDeliveryAPIController extends AppBaseController
{
    /** @var  OrderDeliveryRepository */
    private $orderDeliveryRepository;

    public function __construct(OrderDeliveryRepository $orderDeliveryRepo)
    {
        $this->orderDeliveryRepository = $orderDeliveryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderDeliveries",
     *      summary="Get a listing of the OrderDeliveries.",
     *      tags={"OrderDelivery"},
     *      description="Get all OrderDeliveries",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OrderDelivery")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orderDeliveries = $this->orderDeliveryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderDeliveries->toArray(), 'Order Deliveries retrieved successfully');
    }

    /**
     * @param CreateOrderDeliveryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orderDeliveries",
     *      summary="Store a newly created OrderDelivery in storage",
     *      tags={"OrderDelivery"},
     *      description="Store OrderDelivery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderDelivery that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderDelivery")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDelivery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderDeliveryAPIRequest $request)
    {
        $input = $request->all();

        $orderDelivery = $this->orderDeliveryRepository->create($input);

        return $this->sendResponse($orderDelivery->toArray(), 'Order Delivery saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderDeliveries/{id}",
     *      summary="Display the specified OrderDelivery",
     *      tags={"OrderDelivery"},
     *      description="Get OrderDelivery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDelivery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDelivery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OrderDelivery $orderDelivery */
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            return $this->sendError('Order Delivery not found');
        }

        return $this->sendResponse($orderDelivery->toArray(), 'Order Delivery retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderDeliveryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orderDeliveries/{id}",
     *      summary="Update the specified OrderDelivery in storage",
     *      tags={"OrderDelivery"},
     *      description="Update OrderDelivery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDelivery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderDelivery that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderDelivery")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDelivery"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrderDeliveryAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderDelivery $orderDelivery */
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            return $this->sendError('Order Delivery not found');
        }

        $orderDelivery = $this->orderDeliveryRepository->update($input, $id);

        return $this->sendResponse($orderDelivery->toArray(), 'OrderDelivery updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orderDeliveries/{id}",
     *      summary="Remove the specified OrderDelivery from storage",
     *      tags={"OrderDelivery"},
     *      description="Delete OrderDelivery",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDelivery",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OrderDelivery $orderDelivery */
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            return $this->sendError('Order Delivery not found');
        }

        $orderDelivery->delete();

        return $this->sendResponse($id, 'Order Delivery deleted successfully');
    }
}
