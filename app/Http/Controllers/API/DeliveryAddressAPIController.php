<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryAddressAPIRequest;
use App\Http\Requests\API\UpdateDeliveryAddressAPIRequest;
use App\Models\DeliveryAddress;
use App\Models\Product;
use App\Repositories\DeliveryAddressRepository;
use App\Utils\CurrencyConverterUtils;
use App\Utils\GeoUtils;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DeliveryAddressController
 * @package App\Http\Controllers\API
 */

class DeliveryAddressAPIController extends AppBaseController
{
    /** @var  DeliveryAddressRepository */
    private $deliveryAddressRepository;

    public function __construct(DeliveryAddressRepository $deliveryAddressRepo)
    {
        $this->deliveryAddressRepository = $deliveryAddressRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/deliveryAddresses",
     *      summary="Get a listing of the DeliveryAddresses.",
     *      tags={"DeliveryAddress"},
     *      description="Get all DeliveryAddresses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/DeliveryAddress")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $deliveryAddresses = $this->deliveryAddressRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($deliveryAddresses->toArray(), 'Delivery Addresses retrieved successfully');
    }

    /**
     * @param CreateDeliveryAddressAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/deliveryAddresses",
     *      summary="Store a newly created DeliveryAddress in storage",
     *      tags={"DeliveryAddress"},
     *      description="Store DeliveryAddress",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DeliveryAddress that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DeliveryAddress")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryAddress"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDeliveryAddressAPIRequest $request)
    {
        $input = $request->all();

        $deliveryAddress = $this->deliveryAddressRepository->create($input);

        return $this->sendResponse($deliveryAddress->toArray(), 'Delivery Address saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/deliveryAddresses/{id}",
     *      summary="Display the specified DeliveryAddress",
     *      tags={"DeliveryAddress"},
     *      description="Get DeliveryAddress",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryAddress",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryAddress"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var DeliveryAddress $deliveryAddress */
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            return $this->sendError('Delivery Address not found');
        }

        return $this->sendResponse($deliveryAddress->toArray(), 'Delivery Address retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDeliveryAddressAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/deliveryAddresses/{id}",
     *      summary="Update the specified DeliveryAddress in storage",
     *      tags={"DeliveryAddress"},
     *      description="Update DeliveryAddress",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryAddress",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DeliveryAddress that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DeliveryAddress")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryAddress"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDeliveryAddressAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryAddress $deliveryAddress */
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            return $this->sendError('Delivery Address not found');
        }

        $deliveryAddress = $this->deliveryAddressRepository->update($input, $id);

        return $this->sendResponse($deliveryAddress->toArray(), 'DeliveryAddress updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/deliveryAddresses/{id}",
     *      summary="Remove the specified DeliveryAddress from storage",
     *      tags={"DeliveryAddress"},
     *      description="Delete DeliveryAddress",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryAddress",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var DeliveryAddress $deliveryAddress */
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            return $this->sendError('Delivery Address not found');
        }

        $deliveryAddress->delete();

        return $this->sendResponse($id, 'Delivery Address deleted successfully');
    }

    public function pricing(Request $request){

        /** @var Product $product */
        $product = Product::where(["id" => $request->product_id])->first();

        if(empty($product)){
            return $this->sendError('Product not found');
        }

        $customerLat = $request->customer_lat;
        $customerLng = $request->customer_lng;

        if($customerLat != null && $customerLng !=null){

            $productPrice = (int)$product->price;
            $currency = $product->currency_slug;

            $deliveryFees = 0;

            /** @var  $distance */
            $distance = GeoUtils::getDistanceBetweenPoints($product->location_lat,$product->location_lng, $customerLat, $customerLng );

            if($distance < 10){

                if($productPrice > 100000){
                    $deliveryFees = 5000 ;
                }else{
                    $deliveryFees = 1500;
                }

            }else{
                if($productPrice > 100000){
                    $deliveryFees = 5000 * 2 ;
                }else{
                    $deliveryFees = 1500 * 2;
                }
            }

            if($currency != 'XOF'){

                $from = 'XOF';
                $to = $currency;
                $deliveryFees = CurrencyConverterUtils::convert($deliveryFees, $from, $to);

            }

            return $this->sendResponse([
                    "price" => "0", //''.$deliveryFees,
                    "currency_slug" => $currency
                ], 'Delivery Fees');


        }else{
            return $this->sendError('Customer Latitude or Longitude not found');
        }

    }
}
