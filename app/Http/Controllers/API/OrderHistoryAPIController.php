<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderHistoryAPIRequest;
use App\Http\Requests\API\UpdateOrderHistoryAPIRequest;
use App\Models\OrderHistory;
use App\Repositories\OrderHistoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderHistoryController
 * @package App\Http\Controllers\API
 */

class OrderHistoryAPIController extends AppBaseController
{
    /** @var  OrderHistoryRepository */
    private $orderHistoryRepository;

    public function __construct(OrderHistoryRepository $orderHistoryRepo)
    {
        $this->orderHistoryRepository = $orderHistoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderHistories",
     *      summary="Get a listing of the OrderHistories.",
     *      tags={"OrderHistory"},
     *      description="Get all OrderHistories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OrderHistory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orderHistories = $this->orderHistoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderHistories->toArray(), 'Order Histories retrieved successfully');
    }

    /**
     * @param CreateOrderHistoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orderHistories",
     *      summary="Store a newly created OrderHistory in storage",
     *      tags={"OrderHistory"},
     *      description="Store OrderHistory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderHistory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderHistory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderHistory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderHistoryAPIRequest $request)
    {
        $input = $request->all();

        $orderHistory = $this->orderHistoryRepository->create($input);

        return $this->sendResponse($orderHistory->toArray(), 'Order History saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orderHistories/{id}",
     *      summary="Display the specified OrderHistory",
     *      tags={"OrderHistory"},
     *      description="Get OrderHistory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderHistory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderHistory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OrderHistory $orderHistory */
        $orderHistory = $this->orderHistoryRepository->find($id);

        if (empty($orderHistory)) {
            return $this->sendError('Order History not found');
        }

        return $this->sendResponse($orderHistory->toArray(), 'Order History retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderHistoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orderHistories/{id}",
     *      summary="Update the specified OrderHistory in storage",
     *      tags={"OrderHistory"},
     *      description="Update OrderHistory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderHistory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderHistory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderHistory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderHistory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrderHistoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderHistory $orderHistory */
        $orderHistory = $this->orderHistoryRepository->find($id);

        if (empty($orderHistory)) {
            return $this->sendError('Order History not found');
        }

        $orderHistory = $this->orderHistoryRepository->update($input, $id);

        return $this->sendResponse($orderHistory->toArray(), 'OrderHistory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orderHistories/{id}",
     *      summary="Remove the specified OrderHistory from storage",
     *      tags={"OrderHistory"},
     *      description="Delete OrderHistory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderHistory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OrderHistory $orderHistory */
        $orderHistory = $this->orderHistoryRepository->find($id);

        if (empty($orderHistory)) {
            return $this->sendError('Order History not found');
        }

        $orderHistory->delete();

        return $this->sendResponse($id, 'Order History deleted successfully');
    }
}
