<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProductMediaAPIRequest;
use App\Http\Requests\API\UpdateProductMediaAPIRequest;
use App\Models\ProductMedia;
use App\Repositories\ProductMediaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ProductMediaController
 * @package App\Http\Controllers\API
 */

class ProductMediaAPIController extends AppBaseController
{
    /** @var  ProductMediaRepository */
    private $productMediaRepository;

    public function __construct(ProductMediaRepository $productMediaRepo)
    {
        $this->productMediaRepository = $productMediaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/productMedia",
     *      summary="Get a listing of the ProductMedia.",
     *      tags={"ProductMedia"},
     *      description="Get all ProductMedia",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ProductMedia")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $productMedia = $this->productMediaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($productMedia->toArray(), 'Product Media retrieved successfully');
    }

    /**
     * @param CreateProductMediaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/productMedia",
     *      summary="Store a newly created ProductMedia in storage",
     *      tags={"ProductMedia"},
     *      description="Store ProductMedia",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductMedia that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductMedia")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateProductMediaAPIRequest $request)
    {
        $input = $request->all();

        $productMedia = $this->productMediaRepository->create($input);

        return $this->sendResponse($productMedia->toArray(), 'Product Media saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/productMedia/{id}",
     *      summary="Display the specified ProductMedia",
     *      tags={"ProductMedia"},
     *      description="Get ProductMedia",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMedia",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ProductMedia $productMedia */
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            return $this->sendError('Product Media not found');
        }

        return $this->sendResponse($productMedia->toArray(), 'Product Media retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateProductMediaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/productMedia/{id}",
     *      summary="Update the specified ProductMedia in storage",
     *      tags={"ProductMedia"},
     *      description="Update ProductMedia",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMedia",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ProductMedia that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ProductMedia")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ProductMedia"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateProductMediaAPIRequest $request)
    {
        $input = $request->all();

        /** @var ProductMedia $productMedia */
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            return $this->sendError('Product Media not found');
        }

        $productMedia = $this->productMediaRepository->update($input, $id);

        return $this->sendResponse($productMedia->toArray(), 'ProductMedia updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/productMedia/{id}",
     *      summary="Remove the specified ProductMedia from storage",
     *      tags={"ProductMedia"},
     *      description="Delete ProductMedia",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ProductMedia",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ProductMedia $productMedia */
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            return $this->sendError('Product Media not found');
        }

        $productMedia->delete();

        return $this->sendResponse($id, 'Product Media deleted successfully');
    }
}
