<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeliveryServiceAPIRequest;
use App\Http\Requests\API\UpdateDeliveryServiceAPIRequest;
use App\Models\DeliveryService;
use App\Repositories\DeliveryServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DeliveryServiceController
 * @package App\Http\Controllers\API
 */

class DeliveryServiceAPIController extends AppBaseController
{
    /** @var  DeliveryServiceRepository */
    private $deliveryServiceRepository;

    public function __construct(DeliveryServiceRepository $deliveryServiceRepo)
    {
        $this->deliveryServiceRepository = $deliveryServiceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/deliveryServices",
     *      summary="Get a listing of the DeliveryServices.",
     *      tags={"DeliveryService"},
     *      description="Get all DeliveryServices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/DeliveryService")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $deliveryServices = $this->deliveryServiceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($deliveryServices->toArray(), 'Delivery Services retrieved successfully');
    }

    /**
     * @param CreateDeliveryServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/deliveryServices",
     *      summary="Store a newly created DeliveryService in storage",
     *      tags={"DeliveryService"},
     *      description="Store DeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DeliveryService that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DeliveryService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDeliveryServiceAPIRequest $request)
    {
        $input = $request->all();

        $deliveryService = $this->deliveryServiceRepository->create($input);

        return $this->sendResponse($deliveryService->toArray(), 'Delivery Service saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/deliveryServices/{id}",
     *      summary="Display the specified DeliveryService",
     *      tags={"DeliveryService"},
     *      description="Get DeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var DeliveryService $deliveryService */
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            return $this->sendError('Delivery Service not found');
        }

        return $this->sendResponse($deliveryService->toArray(), 'Delivery Service retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDeliveryServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/deliveryServices/{id}",
     *      summary="Update the specified DeliveryService in storage",
     *      tags={"DeliveryService"},
     *      description="Update DeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="DeliveryService that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/DeliveryService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/DeliveryService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDeliveryServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var DeliveryService $deliveryService */
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            return $this->sendError('Delivery Service not found');
        }

        $deliveryService = $this->deliveryServiceRepository->update($input, $id);

        return $this->sendResponse($deliveryService->toArray(), 'DeliveryService updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/deliveryServices/{id}",
     *      summary="Remove the specified DeliveryService from storage",
     *      tags={"DeliveryService"},
     *      description="Delete DeliveryService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of DeliveryService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var DeliveryService $deliveryService */
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            return $this->sendError('Delivery Service not found');
        }

        $deliveryService->delete();

        return $this->sendResponse($id, 'Delivery Service deleted successfully');
    }


}
