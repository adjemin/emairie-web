<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNotificationTypeAPIRequest;
use App\Http\Requests\API\UpdateNotificationTypeAPIRequest;
use App\Models\NotificationType;
use App\Repositories\NotificationTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class NotificationTypeController
 * @package App\Http\Controllers\API
 */

class NotificationTypeAPIController extends AppBaseController
{
    /** @var  NotificationTypeRepository */
    private $notificationTypeRepository;

    public function __construct(NotificationTypeRepository $notificationTypeRepo)
    {
        $this->notificationTypeRepository = $notificationTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/notificationTypes",
     *      summary="Get a listing of the NotificationTypes.",
     *      tags={"NotificationType"},
     *      description="Get all NotificationTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/NotificationType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $notificationTypes = $this->notificationTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($notificationTypes->toArray(), 'Notification Types retrieved successfully');
    }

    /**
     * @param CreateNotificationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/notificationTypes",
     *      summary="Store a newly created NotificationType in storage",
     *      tags={"NotificationType"},
     *      description="Store NotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="NotificationType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/NotificationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateNotificationTypeAPIRequest $request)
    {
        $input = $request->all();

        $notificationType = $this->notificationTypeRepository->create($input);

        return $this->sendResponse($notificationType->toArray(), 'Notification Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/notificationTypes/{id}",
     *      summary="Display the specified NotificationType",
     *      tags={"NotificationType"},
     *      description="Get NotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var NotificationType $notificationType */
        $notificationType = $this->notificationTypeRepository->find($id);

        if (empty($notificationType)) {
            return $this->sendError('Notification Type not found');
        }

        return $this->sendResponse($notificationType->toArray(), 'Notification Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateNotificationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/notificationTypes/{id}",
     *      summary="Update the specified NotificationType in storage",
     *      tags={"NotificationType"},
     *      description="Update NotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="NotificationType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/NotificationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/NotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateNotificationTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var NotificationType $notificationType */
        $notificationType = $this->notificationTypeRepository->find($id);

        if (empty($notificationType)) {
            return $this->sendError('Notification Type not found');
        }

        $notificationType = $this->notificationTypeRepository->update($input, $id);

        return $this->sendResponse($notificationType->toArray(), 'NotificationType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/notificationTypes/{id}",
     *      summary="Remove the specified NotificationType from storage",
     *      tags={"NotificationType"},
     *      description="Delete NotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of NotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var NotificationType $notificationType */
        $notificationType = $this->notificationTypeRepository->find($id);

        if (empty($notificationType)) {
            return $this->sendError('Notification Type not found');
        }

        $notificationType->delete();

        return $this->sendResponse($id, 'Notification Type deleted successfully');
    }
}
