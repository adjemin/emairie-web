<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderHistory;
use App\Models\Invoice;
use App\Models\Customer;
use App\Models\DeliveryAddress;
use App\Models\InvoicePayment;
use App\Models\FavoriteProduct;
use Notification;
use App\Notifications\CommandeNotif;
use Flash;
use Response;
use Crypt;
use Session;
use Redirect;
use App\Models\PaymentMethod;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        Session::put('article', $request['identif']);
        $product=Product::where('id','=',$request['identif'])->get();
        $paymet= PaymentMethod::where('name','=','Cinetpay')->first();
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        foreach ($cust as $customer_id) {
            foreach ($product as $products) {
                $total=collect([(int)$products->price,10])->sum();
            }
            $choix= DeliveryAddress::where('customer_id','=',$customer_id->id)->where('address_type','=',$request['myselection1'])->get();
            if($request['fav'] == 1){
            $favoris = FavoriteProduct::Create([
                'product_id' => $request['identif'],
                'customer_id' => $customer_id->id,
            ]);
            }else{
                $favoris = FavoriteProduct::where('id','=',$request['favid'])->firstOrFail();
                $favoris->delete();
            }
            return view('FrontOrder.order',compact('product','cust','choix','total','paymet'));
        }
    }


    //choix du lieu
    public function choix(Request $request)
    {
        $article = session('article');
        $valeur = session('clef');
        $product=Product::where('id','=',$article)->get();
        $cust=Customer::where('phone_number','=',$valeur)->get();
        $paymet= PaymentMethod::where('name','=','Cinetpay')->first();
        foreach ($cust as $customer_id) {
            $choix= DeliveryAddress::where('customer_id','=',$customer_id->id)->where('address_type','=',$request['myselection1'])->get();
            foreach ($product as $products) {
                $total=collect([(int)$products->price,10])->sum();
            }
            return view('FrontOrder.order',compact('product','cust','choix','total','paymet'));
        }
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        if($request['addresse'] != NULL){
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->first();
        $customer_id=Customer::where('phone_number','=',$valeur)->first('id');
        $order = Order::Create([
            'customer_id' => $cust->id,
            'is_waiting' => true,
            'current_status' => 'waiting',
            'rating' => '',
            'note' => '',
            'payment_method_slug' => $request['Cinetpay'],
            'delivery_fees' => $request['frais'],
            'amount' => $request['total'],
            'is_delivered' => false,
            'is_waiting_payment' => true
        ]);

        $product=Product::where('id','=',session('article'))->first();
        $totalAmount= ((int)$product->price)*((int)$request['quantite']);
        $currencyCode=$product->currency_slug;

        $orderItem = OrderItem::create([
                    'order_id'=> $order->id,
                    'meta_data_id' => $product->id,
                    'meta_data' => json_encode($product),
                    'quantity' => $request['quantite'],
                    'quantity_unit'=> 'number',
                    'unit_price' => $product->price,
                    'currency_code' => $product->currency_slug,
                    'total_amount' => $totalAmount
                ]);

        $order->currency_code =  $currencyCode;
        $order->save();

        $tax = $request['total'] * Invoice::TAXES;

        $invoice = Invoice::create([
                'order_id' => $order->id,
                'customer_id' => $cust->id,
                'reference' => Invoice::generateID('SHOP', $order->id, $cust->id),
                'link' => '#',
                'subtotal' => $request['total'],
                'service' => 'shop',
                'tax' => $tax,
                'fees_delivery' => $request['frais'],
                'total' => $request['total'],
                'status' => 'unpaid',
                'is_paid_by_customer' => false,
                'is_paid_by_delivery_service' => false,
                'currency_code' => $currencyCode
            ]);

        //TODO Notify user
            $transactionId = self::generateTransId($cust->id, $invoice->id, $order->id);

            $transaction = InvoicePayment::create([
                'invoice_id' =>  $invoice->id,
                'payment_method'=> $request['Cinetpay'],
                'payment_reference' => $transactionId,
                'amount' => $invoice->total,
                'currency_code' => $invoice->currency_code,
                'creator_id' => $cust->id,
                'creator' => 'customer',
                'creator_name' => $cust->name

            ]);

            $orderHistory = new OrderHistory();
            $orderHistory->order_id = $order->id;
            $orderHistory->status = 'waiting_payment';
            $orderHistory->order_id = $order->id;
            $orderHistory->creator = 'customer';
            $orderHistory->creator_id = $cust->id;
            $orderHistory->creator_name = $cust->name;
            $saved = $orderHistory->save();

            $payment_method = $transaction->payment_method;
            $transaction_designation = 'Buying Product';
            $amount = $transaction->amount;
            $currency_code = $transaction->currency_code;
            $status = $transaction->status;
            $custom=$cust->id;

            return view('FrontPayment.payment',compact('payment_method','transactionId','transaction_designation','amount','status','custom'));

            /*return $this->sendResponse([
                'order' => $order,
                'transaction' => [
                    'payment_method'=> $transaction->payment_method,
                    'transaction_id' => $transactionId,
                    'transaction_designation' => 'Buying Product',
                    'amount' => $transaction->amount,
                    'currency_code' => $transaction->currency_code,
                    'status' => $transaction->status
                ]
            ], 'Order saved successfully');

            /*$input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));*/
        
       }else{
        return Redirect::back()->with('error','Adresse obligatoire');
       }
    }

        /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where(['payment_reference' => $transId])->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }


    }


    public function sendNotification()
    {
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        foreach ($cust as $customer_id) {
            $order = Order::where('customer_id','=',$customer_id->id)->get('id');
            $details= [
            'greeting' => 'Hi',
            'body' => 'You have received one order',
            'thanks' => 'Thank you for using Adjemin!',
            'actionText' => 'View your order',
            'actionURL' => url(''),
            'order_id' => $order
            ];
            Notification::send($cust, new CommandeNotif($details));
            return dd("Done");
        }
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //dd($id);
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('order', $order);
    }


    // Affiche les commandes et ventes dans le compte
    public function montrer($id)
    {
        $customer_id= Crypt::decrypt($id);
        $order=Order::with('orderitem','orderhistory','invoice')->where('customer_id','=',$customer_id)
        ->orderBy('created_at','DESC')->paginate(5);
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        $sel=Order::with('orderitem','orderhistory','invoice')->where('customer_id','=',$customer_id)
        ->orderBy('created_at','DESC')->paginate(5);

        $total_order=Order::with('orderitem','orderhistory','invoice')
        ->where('customer_id','=',$customer_id)->get();
        $count_order = count($total_order);
        return view('myAcount',compact('cust','order','sel','customer_id','count_order'));
        
    }



    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('orders.index'));
    }
}
