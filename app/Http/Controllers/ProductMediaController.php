<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductMediaRequest;
use App\Http\Requests\UpdateProductMediaRequest;
use App\Repositories\ProductMediaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProductMediaController extends AppBaseController
{
    /** @var  ProductMediaRepository */
    private $productMediaRepository;

    public function __construct(ProductMediaRepository $productMediaRepo)
    {
        $this->productMediaRepository = $productMediaRepo;
    }

    /**
     * Display a listing of the ProductMedia.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productMedia = $this->productMediaRepository->all();

        return view('product_media.index')
            ->with('productMedia', $productMedia);
    }

    /**
     * Show the form for creating a new ProductMedia.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_media.create');
    }

    /**
     * Store a newly created ProductMedia in storage.
     *
     * @param CreateProductMediaRequest $request
     *
     * @return Response
     */
    public function store(CreateProductMediaRequest $request)
    {
        $input = $request->all();

        $productMedia = $this->productMediaRepository->create($input);

        Flash::success('Product Media saved successfully.');

        return redirect(route('productMedia.index'));
    }

    /**
     * Display the specified ProductMedia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            Flash::error('Product Media not found');

            return redirect(route('productMedia.index'));
        }

        return view('product_media.show')->with('productMedia', $productMedia);
    }

    /**
     * Show the form for editing the specified ProductMedia.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            Flash::error('Product Media not found');

            return redirect(route('productMedia.index'));
        }

        return view('product_media.edit')->with('productMedia', $productMedia);
    }

    /**
     * Update the specified ProductMedia in storage.
     *
     * @param int $id
     * @param UpdateProductMediaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductMediaRequest $request)
    {
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            Flash::error('Product Media not found');

            return redirect(route('productMedia.index'));
        }

        $productMedia = $this->productMediaRepository->update($request->all(), $id);

        Flash::success('Product Media updated successfully.');

        return redirect(route('productMedia.index'));
    }

    /**
     * Remove the specified ProductMedia from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productMedia = $this->productMediaRepository->find($id);

        if (empty($productMedia)) {
            Flash::error('Product Media not found');

            return redirect(route('productMedia.index'));
        }

        $this->productMediaRepository->delete($id);

        Flash::success('Product Media deleted successfully.');

        return redirect(route('productMedia.index'));
    }
}
