<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaymentSourceRequest;
use App\Http\Requests\UpdatePaymentSourceRequest;
use App\Repositories\PaymentSourceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PaymentSourceController extends AppBaseController
{
    /** @var  PaymentSourceRepository */
    private $paymentSourceRepository;

    public function __construct(PaymentSourceRepository $paymentSourceRepo)
    {
        $this->paymentSourceRepository = $paymentSourceRepo;
    }

    /**
     * Display a listing of the PaymentSource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $paymentSources = $this->paymentSourceRepository->all();

        return view('payment_sources.index')
            ->with('paymentSources', $paymentSources);
    }

    /**
     * Show the form for creating a new PaymentSource.
     *
     * @return Response
     */
    public function create()
    {
        return view('payment_sources.create');
    }

    /**
     * Store a newly created PaymentSource in storage.
     *
     * @param CreatePaymentSourceRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentSourceRequest $request)
    {
        $input = $request->all();

        $paymentSource = $this->paymentSourceRepository->create($input);

        Flash::success('Payment Source saved successfully.');

        return redirect(route('paymentSources.index'));
    }

    /**
     * Display the specified PaymentSource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            Flash::error('Payment Source not found');

            return redirect(route('paymentSources.index'));
        }

        return view('payment_sources.show')->with('paymentSource', $paymentSource);
    }

    /**
     * Show the form for editing the specified PaymentSource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            Flash::error('Payment Source not found');

            return redirect(route('paymentSources.index'));
        }

        return view('payment_sources.edit')->with('paymentSource', $paymentSource);
    }

    /**
     * Update the specified PaymentSource in storage.
     *
     * @param int $id
     * @param UpdatePaymentSourceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentSourceRequest $request)
    {
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            Flash::error('Payment Source not found');

            return redirect(route('paymentSources.index'));
        }

        $paymentSource = $this->paymentSourceRepository->update($request->all(), $id);

        Flash::success('Payment Source updated successfully.');

        return redirect(route('paymentSources.index'));
    }

    /**
     * Remove the specified PaymentSource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $paymentSource = $this->paymentSourceRepository->find($id);

        if (empty($paymentSource)) {
            Flash::error('Payment Source not found');

            return redirect(route('paymentSources.index'));
        }

        $this->paymentSourceRepository->delete($id);

        Flash::success('Payment Source deleted successfully.');

        return redirect(route('paymentSources.index'));
    }
}
