<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductConditionRequest;
use App\Http\Requests\UpdateProductConditionRequest;
use App\Repositories\ProductConditionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProductConditionController extends AppBaseController
{
    /** @var  ProductConditionRepository */
    private $productConditionRepository;

    public function __construct(ProductConditionRepository $productConditionRepo)
    {
        $this->productConditionRepository = $productConditionRepo;
    }

    /**
     * Display a listing of the ProductCondition.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productConditions = $this->productConditionRepository->all();

        return view('product_conditions.index')
            ->with('productConditions', $productConditions);
    }

    /**
     * Show the form for creating a new ProductCondition.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_conditions.create');
    }

    /**
     * Store a newly created ProductCondition in storage.
     *
     * @param CreateProductConditionRequest $request
     *
     * @return Response
     */
    public function store(CreateProductConditionRequest $request)
    {
        $input = $request->all();

        $productCondition = $this->productConditionRepository->create($input);

        Flash::success('Product Condition saved successfully.');

        return redirect(route('productConditions.index'));
    }

    /**
     * Display the specified ProductCondition.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            Flash::error('Product Condition not found');

            return redirect(route('productConditions.index'));
        }

        return view('product_conditions.show')->with('productCondition', $productCondition);
    }

    /**
     * Show the form for editing the specified ProductCondition.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            Flash::error('Product Condition not found');

            return redirect(route('productConditions.index'));
        }

        return view('product_conditions.edit')->with('productCondition', $productCondition);
    }

    /**
     * Update the specified ProductCondition in storage.
     *
     * @param int $id
     * @param UpdateProductConditionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductConditionRequest $request)
    {
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            Flash::error('Product Condition not found');

            return redirect(route('productConditions.index'));
        }

        $productCondition = $this->productConditionRepository->update($request->all(), $id);

        Flash::success('Product Condition updated successfully.');

        return redirect(route('productConditions.index'));
    }

    /**
     * Remove the specified ProductCondition from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productCondition = $this->productConditionRepository->find($id);

        if (empty($productCondition)) {
            Flash::error('Product Condition not found');

            return redirect(route('productConditions.index'));
        }

        $this->productConditionRepository->delete($id);

        Flash::success('Product Condition deleted successfully.');

        return redirect(route('productConditions.index'));
    }
}
