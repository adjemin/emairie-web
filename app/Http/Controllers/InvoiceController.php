<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Repositories\InvoiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\InvoicePayment;
use App\Models\Order;
use App\Models\Invoice;
use App\Models\OrderItem;
use App\Models\OrderHistory;
use App\Models\Customer;
use Flash;
use Response;
use Alert;

class InvoiceController extends AppBaseController
{
    /** @var  InvoiceRepository */
    private $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepo)
    {
        $this->invoiceRepository = $invoiceRepo;
    }

    /**
     * Display a listing of the Invoice.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $invoices = $this->invoiceRepository->all();

        return view('invoices.index')
            ->with('invoices', $invoices);
    }

    /**
     * Show the form for creating a new Invoice.
     *
     * @return Response
     */
    public function create()
    {
        return view('invoices.create');
    }

    /**
     * Store a newly created Invoice in storage.
     *
     * @param CreateInvoiceRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoiceRequest $request)
    {
        $input = $request->all();

        $invoice = $this->invoiceRepository->create($input);

        Flash::success('Invoice saved successfully.');

        return redirect(route('invoices.index'));
    }

    /**
     * Display the specified Invoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (empty($invoice)) {
            Flash::error('Invoice not found');

            return redirect(route('invoices.index'));
        }

        return view('invoices.show')->with('invoice', $invoice);
    }

    /**
     * Show the form for editing the specified Invoice.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (empty($invoice)) {
            Flash::error('Invoice not found');

            return redirect(route('invoices.index'));
        }

        return view('invoices.edit')->with('invoice', $invoice);
    }

    /**
     * Update the specified Invoice in storage.
     *
     * @param int $id
     * @param UpdateInvoiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoiceRequest $request)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (empty($invoice)) {
            Flash::error('Invoice not found');

            return redirect(route('invoices.index'));
        }

        $invoice = $this->invoiceRepository->update($request->all(), $id);

        Flash::success('Invoice updated successfully.');

        return redirect(route('invoices.index'));
    }

    /**
     * Remove the specified Invoice from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (empty($invoice)) {
            Flash::error('Invoice not found');

            return redirect(route('invoices.index'));
        }

        $this->invoiceRepository->delete($id);

        Flash::success('Invoice deleted successfully.');

        return redirect(route('invoices.index'));
    }

    public function notify(Request $request){
        if($request->has('cpm_trans_id')){

            try {
                $id_transaction = $request->cpm_trans_id;
                $apiKey = '18122291995d9845221e7d53.99745292';
                $site_id = '182538';
                $CinetPay = new CinetPay($site_id, $apiKey);
                // Reprise exacte des bonnes données chez CinetPay
                $CinetPay->setTransId($id_transaction)->getPayStatus();
                $cpm_site_id = $CinetPay->_cpm_site_id;
                $signature = $CinetPay->_signature;
                $cpm_amount = $CinetPay->_cpm_amount;
                $cpm_trans_id = $CinetPay->_cpm_trans_id;
                $cpm_custom = $CinetPay->_cpm_custom;
                $cpm_currency = $CinetPay->_cpm_currency;
                $cpm_payid = $CinetPay->_cpm_payid;
                $cpm_payment_date = $CinetPay->_cpm_payment_date;
                $cpm_payment_time = $CinetPay->_cpm_payment_time;
                $cpm_error_message = $CinetPay->_cpm_error_message;
                $payment_method = $CinetPay->_payment_method;
                $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
                $cel_phone_num = $CinetPay->_cel_phone_num;
                $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
                $created_at = $CinetPay->_created_at;
                $updated_at = $CinetPay->_updated_at;
                $cpm_result = $CinetPay->_cpm_result;
                $cpm_trans_status = $CinetPay->_cpm_trans_status;
                $cpm_designation = $CinetPay->_cpm_designation;
                $buyer_name = $CinetPay->_buyer_name;

                // Recuperation de la ligne de la transaction dans votre base de données
                $transaction = InvoicePayment::where(['payment_reference' => $id_transaction])->first();

                $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();

                $order = Order::where(['id' => $invoice->order_id])->first();
                // Verification de l'etat du traitement de la commande
                if($order->is_waiting_payment){

                    if($transaction != null){

                        $transaction->payment_gateway_trans_id = $cpm_trans_id;
                        $transaction->payment_gateway_custom = $cpm_custom;
                        $transaction->payment_gateway_currency = $cpm_currency;
                        $transaction->payment_gateway_amount = $cpm_amount;
                        $transaction->payment_gateway_payid = $cpm_payid;
                        $transaction->payment_gateway_payment_date = $cpm_payment_date;
                        $transaction->payment_gateway_payment_time = $cpm_payment_time;
                        $transaction->payment_gateway_error_message = $cpm_error_message;
                        $transaction->payment_gateway_payment_method = $payment_method;
                        $transaction->payment_gateway_phone_prefixe = $cpm_phone_prefixe;
                        $transaction->payment_gateway_cel_phone_num = $cel_phone_num;
                        $transaction->payment_gateway_ipn_ack = $cpm_ipn_ack;
                        $transaction->payment_gateway_created_at = $created_at;
                        $transaction->payment_gateway_updated_at = $updated_at;
                        $transaction->payment_gateway_cpm_result = $cpm_result;
                        $transaction->payment_gateway_trans_status = $cpm_trans_status;
                        $transaction->payment_gateway_designation = $cpm_designation;
                        $transaction->payment_gateway_buyer_name = $buyer_name;
                        $transaction->payment_gateway_signature = $signature;
                        $transaction->save();

                        // On verifie que le montant payé chez CinetPay correspond à notre montant en base de données pour cette transaction
                        if($order->amount == $cpm_amount ){

                            // On verifie que le paiement est valide
                            if ($CinetPay->isValidPayment()) {
                                $transaction->status = 'SUCCESSFUL';
                                $transaction->is_waiting = false;
                                $transaction->is_completed = true;
                                $transaction->save();

                                $this->validateTransaction($id_transaction);
                                
                                $valeur = session('clef');
                                $cust=Customer::where('phone_number','=',$valeur)->get('id');
                                Alert::success('Félicitation','Votre commande à été éffectuée !');
                                return redirect('commande',['id'=> Crypt::encrypt($cust)]);

                                //echo 'Félicitation, votre paiement a été effectué avec succès';
                                die();
                            } else {

                                if($cpm_result == '623'){
                                    echo "".$CinetPay->_cpm_error_message;
                                    die();
                                }else{
                                   // $this->cancelTransaction($id_transaction);
                                    echo 'Echec, votre paiement a échoué pour cause : ' . $CinetPay->_cpm_error_message;
                                    die();
                                }


                            }

                        }else{
                            //Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande
                            $transaction->status = 'REFUSED';
                            $transaction->is_waiting = false;
                            $transaction->is_completed = true;
                            $transaction->save();
                            //$this->cancelTransaction($id_transaction);

                            echo 'Fraude : montant payé ' . $cpm_amount . ' ne correspond pas au montant de la commande';
                            die();

                        }

                    }

                }else{
                    // Si le paiement est bon alors ne traitez plus cette transaction : die();
                    // La commande a été déjà traité
                    // Arret du script
                    die();
                }



            } catch (\Exception $e) {
                echo "Erreur :" . $e->getMessage();
                // Une erreur s'est produite
            }

        }else {
            // redirection vers la page d'accueil
            die();
        }

    }

    public function validateTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        if($transaction->status == 'SUCCESSFUL'){
            $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
            $invoice->status = 'paid';
            $invoice->is_paid_by_customer = true;
            $invoice->save();

            $order = Order::where(['id' => $invoice->order_id])->first();

            $customer = Customer::where(['id' => $order->customer_id])->first();


            if($invoice->service == 'shop'){
                $orderItem = OrderItem::where(["order_id" => $order->id])->first();

                $order->current_status = 'customer_paid';
                $order->is_waiting_payment = false;
                $order->save();

                $orderHistory = new OrderHistory();
                $orderHistory->order_id = $order->id;
                $orderHistory->status = 'customer_paid';
                $orderHistory->order_id = $order->id;
                $orderHistory->creator = 'customer';
                $orderHistory->creator_id = $customer->id;
                $orderHistory->creator_name = $customer->name;
                $orderHistory->save();

            }


        }

    }

    public function cancelTransaction($transactionId){
        $transaction = InvoicePayment::where(['payment_reference' => $transactionId])->first();

        $invoice = Invoice::where(["id" => $transaction->invoice_id])->first();
        $invoice->status = 'canceled';
        $invoice->is_paid_by_customer = false;
        $invoice->save();

        $order = Order::where(['id' => $invoice->order_id])->first();
        $order->current_status = 'canceled';
        $order->save();

        return redirect('home');
    }
}
