<?php

namespace App\Http\Controllers\CustomerAuth;

use App\Customer;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use Cookie;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'home';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('customer.guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone_number' => 'unique:customers'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Customer
     */
    protected function create(array $data)
    {
        //dd(session('clef'));
        $cus= Customer::where('phone_number','=',$data['numero'])->get();
        if($cus->count()<=0){
            if($data['numero']!= NULL){
            Session::put('clef', $data['numero']);
            $save= Customer::create([
                'phone_number' => $data['numero'],
                'dial_code' => $data['dial_code'],
                'phone' => $data['dial_code'].''.$data['numero'],
                'country_code' => $data['country_code'] ,
            ]);
            Session::put('num',$save->phone);
            return redirect()->route('customers.index');
            }
        }
        elseif($data['numero']!=NULL and $data['numero']!=$data['dial_code']){
            Session::put('clef', $data['numero']);
            return redirect()->route('customers.index');
        }else{
            return back()->withInput();
        }
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('customer.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }
}
