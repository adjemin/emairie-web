<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFavoriteProductRequest;
use App\Http\Requests\UpdateFavoriteProductRequest;
use App\Repositories\FavoriteProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FavoriteProductController extends AppBaseController
{
    /** @var  FavoriteProductRepository */
    private $favoriteProductRepository;

    public function __construct(FavoriteProductRepository $favoriteProductRepo)
    {
        $this->favoriteProductRepository = $favoriteProductRepo;
    }

    /**
     * Display a listing of the FavoriteProduct.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $favoriteProducts = $this->favoriteProductRepository->all();

        return view('favorite_products.index')
            ->with('favoriteProducts', $favoriteProducts);
    }

    /**
     * Show the form for creating a new FavoriteProduct.
     *
     * @return Response
     */
    public function create()
    {
        return view('favorite_products.create');
    }

    /**
     * Store a newly created FavoriteProduct in storage.
     *
     * @param CreateFavoriteProductRequest $request
     *
     * @return Response
     */
    public function store(CreateFavoriteProductRequest $request)
    {
        $input = $request->all();

        $favoriteProduct = $this->favoriteProductRepository->create($input);

        Flash::success('Favorite Product saved successfully.');

        return redirect(route('favoriteProducts.index'));
    }

    /**
     * Display the specified FavoriteProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            Flash::error('Favorite Product not found');

            return redirect(route('favoriteProducts.index'));
        }

        return view('favorite_products.show')->with('favoriteProduct', $favoriteProduct);
    }

    /**
     * Show the form for editing the specified FavoriteProduct.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            Flash::error('Favorite Product not found');

            return redirect(route('favoriteProducts.index'));
        }

        return view('favorite_products.edit')->with('favoriteProduct', $favoriteProduct);
    }

    /**
     * Update the specified FavoriteProduct in storage.
     *
     * @param int $id
     * @param UpdateFavoriteProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFavoriteProductRequest $request)
    {
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            Flash::error('Favorite Product not found');

            return redirect(route('favoriteProducts.index'));
        }

        $favoriteProduct = $this->favoriteProductRepository->update($request->all(), $id);

        Flash::success('Favorite Product updated successfully.');

        return redirect(route('favoriteProducts.index'));
    }

    /**
     * Remove the specified FavoriteProduct from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $favoriteProduct = $this->favoriteProductRepository->find($id);

        if (empty($favoriteProduct)) {
            Flash::error('Favorite Product not found');

            return redirect(route('favoriteProducts.index'));
        }

        $this->favoriteProductRepository->delete($id);

        Flash::success('Favorite Product deleted successfully.');

        return redirect(route('favoriteProducts.index'));
    }
}
