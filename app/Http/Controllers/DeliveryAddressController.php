<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryAddressRequest;
use App\Http\Requests\UpdateDeliveryAddressRequest;
use App\Repositories\DeliveryAddressRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\DeliveryAddress;
use Flash;
use Response;

class DeliveryAddressController extends AppBaseController
{
    /** @var  DeliveryAddressRepository */
    private $deliveryAddressRepository;

    public function __construct(DeliveryAddressRepository $deliveryAddressRepo)
    {
        $this->deliveryAddressRepository = $deliveryAddressRepo;
    }

    /**
     * Display a listing of the DeliveryAddress.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryAddresses = $this->deliveryAddressRepository->all();
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        foreach ($cust as $customer_id) {
            $address= DeliveryAddress::where('customer_id','=',$customer_id->id)->get();
            return view('deliveryAdress',compact('cust','address'));
        }
            //->with('deliveryAddresses', $deliveryAddresses);
    }

    //type d'adresse
    public function details(Request $request)
    {
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        $type = $request['type'];
        return view('locationDetail',compact('type','cust'));
    }

    /**
     * Show the form for creating a new DeliveryAddress.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_addresses.create');
    }

    /**
     * Store a newly created DeliveryAddress in storage.
     *
     * @param CreateDeliveryAddressRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryAddressRequest $request)
    {
        //dd($request['type']);
        if($request['autocomplete_search'] != NULL){
            $valeur = session('clef');
            $cust=Customer::where('phone_number','=',$valeur)->first();
            $address=DeliveryAddress::Create([
                'address_name'=> $request['autocomplete_search'],
                'latitude' => $request['lat'],
                'longitude' => $request['long'],
                'place' => $request['num'],
                'town' => $request['repere'],
                'address_typed' => $request['supplement'],
                'address_type' => $request['type'],
                'customer_id' => $cust->id
            ]);

            return redirect(route('deliveryAddresses.index'));
        }
        else{
            return redirect(route('deliveryAddresses.index'));
        }
        /*$input = $request->all();

        $deliveryAddress = $this->deliveryAddressRepository->create($input);

        Flash::success('Delivery Address saved successfully.');

        return redirect(route('deliveryAddresses.index'));*/
    }

    /**
     * Display the specified DeliveryAddress.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            Flash::error('Delivery Address not found');

            return redirect(route('deliveryAddresses.index'));
        }

        return view('delivery_addresses.show')->with('deliveryAddress', $deliveryAddress);
    }

    /**
     * Show the form for editing the specified DeliveryAddress.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            Flash::error('Delivery Address not found');

            return redirect(route('deliveryAddresses.index'));
        }

        return view('delivery_addresses.edit')->with('deliveryAddress', $deliveryAddress);
    }

    /**
     * Update the specified DeliveryAddress in storage.
     *
     * @param int $id
     * @param UpdateDeliveryAddressRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryAddressRequest $request)
    {
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            Flash::error('Delivery Address not found');

            return redirect(route('deliveryAddresses.index'));
        }

        $deliveryAddress = $this->deliveryAddressRepository->update($request->all(), $id);

        Flash::success('Delivery Address updated successfully.');

        return redirect(route('deliveryAddresses.index'));
    }

    /**
     * Remove the specified DeliveryAddress from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryAddress = $this->deliveryAddressRepository->find($id);

        if (empty($deliveryAddress)) {
            Flash::error('Delivery Address not found');

            return redirect(route('deliveryAddresses.index'));
        }

        $this->deliveryAddressRepository->delete($id);

        Flash::success('Delivery Address deleted successfully.');

        return redirect(route('deliveryAddresses.index'));
    }

    // suupprimer une addresse
    public function supprimer(Request $request)
    {
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        foreach ($cust as $customer_id) {
            $address= DeliveryAddress::where('customer_id','=',$customer_id->id)->where('address_name','=',$request['lieu'])->get();
            foreach ($address as $addresses) {
                $ok=DeliveryAddress::findOrFail($addresses->id);
                $ok->delete();
                return redirect(route('deliveryAddresses.index'));
            }
        }
    }
}
