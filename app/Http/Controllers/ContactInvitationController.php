<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateContactInvitationRequest;
use App\Http\Requests\UpdateContactInvitationRequest;
use App\Repositories\ContactInvitationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ContactInvitationController extends AppBaseController
{
    /** @var  ContactInvitationRepository */
    private $contactInvitationRepository;

    public function __construct(ContactInvitationRepository $contactInvitationRepo)
    {
        $this->contactInvitationRepository = $contactInvitationRepo;
    }

    /**
     * Display a listing of the ContactInvitation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $contactInvitations = $this->contactInvitationRepository->all();

        return view('contact_invitations.index')
            ->with('contactInvitations', $contactInvitations);
    }

    /**
     * Show the form for creating a new ContactInvitation.
     *
     * @return Response
     */
    public function create()
    {
        return view('contact_invitations.create');
    }

    /**
     * Store a newly created ContactInvitation in storage.
     *
     * @param CreateContactInvitationRequest $request
     *
     * @return Response
     */
    public function store(CreateContactInvitationRequest $request)
    {
        $input = $request->all();

        $contactInvitation = $this->contactInvitationRepository->create($input);

        Flash::success('Contact Invitation saved successfully.');

        return redirect(route('contactInvitations.index'));
    }

    /**
     * Display the specified ContactInvitation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            Flash::error('Contact Invitation not found');

            return redirect(route('contactInvitations.index'));
        }

        return view('contact_invitations.show')->with('contactInvitation', $contactInvitation);
    }

    /**
     * Show the form for editing the specified ContactInvitation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            Flash::error('Contact Invitation not found');

            return redirect(route('contactInvitations.index'));
        }

        return view('contact_invitations.edit')->with('contactInvitation', $contactInvitation);
    }

    /**
     * Update the specified ContactInvitation in storage.
     *
     * @param int $id
     * @param UpdateContactInvitationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContactInvitationRequest $request)
    {
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            Flash::error('Contact Invitation not found');

            return redirect(route('contactInvitations.index'));
        }

        $contactInvitation = $this->contactInvitationRepository->update($request->all(), $id);

        Flash::success('Contact Invitation updated successfully.');

        return redirect(route('contactInvitations.index'));
    }

    /**
     * Remove the specified ContactInvitation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $contactInvitation = $this->contactInvitationRepository->find($id);

        if (empty($contactInvitation)) {
            Flash::error('Contact Invitation not found');

            return redirect(route('contactInvitations.index'));
        }

        $this->contactInvitationRepository->delete($id);

        Flash::success('Contact Invitation deleted successfully.');

        return redirect(route('contactInvitations.index'));
    }
}
