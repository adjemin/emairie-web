<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderDeliveryRequest;
use App\Http\Requests\UpdateOrderDeliveryRequest;
use App\Repositories\OrderDeliveryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderDeliveryController extends AppBaseController
{
    /** @var  OrderDeliveryRepository */
    private $orderDeliveryRepository;

    public function __construct(OrderDeliveryRepository $orderDeliveryRepo)
    {
        $this->orderDeliveryRepository = $orderDeliveryRepo;
    }

    /**
     * Display a listing of the OrderDelivery.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderDeliveries = $this->orderDeliveryRepository->all();

        return view('order_deliveries.index')
            ->with('orderDeliveries', $orderDeliveries);
    }

    /**
     * Show the form for creating a new OrderDelivery.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_deliveries.create');
    }

    /**
     * Store a newly created OrderDelivery in storage.
     *
     * @param CreateOrderDeliveryRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderDeliveryRequest $request)
    {
        $input = $request->all();

        $orderDelivery = $this->orderDeliveryRepository->create($input);

        Flash::success('Order Delivery saved successfully.');

        return redirect(route('orderDeliveries.index'));
    }

    /**
     * Display the specified OrderDelivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            Flash::error('Order Delivery not found');

            return redirect(route('orderDeliveries.index'));
        }

        return view('order_deliveries.show')->with('orderDelivery', $orderDelivery);
    }

    /**
     * Show the form for editing the specified OrderDelivery.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            Flash::error('Order Delivery not found');

            return redirect(route('orderDeliveries.index'));
        }

        return view('order_deliveries.edit')->with('orderDelivery', $orderDelivery);
    }

    /**
     * Update the specified OrderDelivery in storage.
     *
     * @param int $id
     * @param UpdateOrderDeliveryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderDeliveryRequest $request)
    {
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            Flash::error('Order Delivery not found');

            return redirect(route('orderDeliveries.index'));
        }

        $orderDelivery = $this->orderDeliveryRepository->update($request->all(), $id);

        Flash::success('Order Delivery updated successfully.');

        return redirect(route('orderDeliveries.index'));
    }

    /**
     * Remove the specified OrderDelivery from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderDelivery = $this->orderDeliveryRepository->find($id);

        if (empty($orderDelivery)) {
            Flash::error('Order Delivery not found');

            return redirect(route('orderDeliveries.index'));
        }

        $this->orderDeliveryRepository->delete($id);

        Flash::success('Order Delivery deleted successfully.');

        return redirect(route('orderDeliveries.index'));
    }
}
