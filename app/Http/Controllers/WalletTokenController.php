<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWalletTokenRequest;
use App\Http\Requests\UpdateWalletTokenRequest;
use App\Repositories\WalletTokenRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class WalletTokenController extends AppBaseController
{
    /** @var  WalletTokenRepository */
    private $walletTokenRepository;

    public function __construct(WalletTokenRepository $walletTokenRepo)
    {
        $this->walletTokenRepository = $walletTokenRepo;
    }

    /**
     * Display a listing of the WalletToken.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $walletTokens = $this->walletTokenRepository->all();

        return view('wallet_tokens.index')
            ->with('walletTokens', $walletTokens);
    }

    /**
     * Show the form for creating a new WalletToken.
     *
     * @return Response
     */
    public function create()
    {
        return view('wallet_tokens.create');
    }

    /**
     * Store a newly created WalletToken in storage.
     *
     * @param CreateWalletTokenRequest $request
     *
     * @return Response
     */
    public function store(CreateWalletTokenRequest $request)
    {
        $input = $request->all();

        $walletToken = $this->walletTokenRepository->create($input);

        Flash::success('Wallet Token saved successfully.');

        return redirect(route('walletTokens.index'));
    }

    /**
     * Display the specified WalletToken.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            Flash::error('Wallet Token not found');

            return redirect(route('walletTokens.index'));
        }

        return view('wallet_tokens.show')->with('walletToken', $walletToken);
    }

    /**
     * Show the form for editing the specified WalletToken.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            Flash::error('Wallet Token not found');

            return redirect(route('walletTokens.index'));
        }

        return view('wallet_tokens.edit')->with('walletToken', $walletToken);
    }

    /**
     * Update the specified WalletToken in storage.
     *
     * @param int $id
     * @param UpdateWalletTokenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWalletTokenRequest $request)
    {
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            Flash::error('Wallet Token not found');

            return redirect(route('walletTokens.index'));
        }

        $walletToken = $this->walletTokenRepository->update($request->all(), $id);

        Flash::success('Wallet Token updated successfully.');

        return redirect(route('walletTokens.index'));
    }

    /**
     * Remove the specified WalletToken from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $walletToken = $this->walletTokenRepository->find($id);

        if (empty($walletToken)) {
            Flash::error('Wallet Token not found');

            return redirect(route('walletTokens.index'));
        }

        $this->walletTokenRepository->delete($id);

        Flash::success('Wallet Token deleted successfully.');

        return redirect(route('walletTokens.index'));
    }
}
