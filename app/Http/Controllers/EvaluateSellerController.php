<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEvaluateSellerRequest;
use App\Http\Requests\UpdateEvaluateSellerRequest;
use App\Repositories\EvaluateSellerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class EvaluateSellerController extends AppBaseController
{
    /** @var  EvaluateSellerRepository */
    private $evaluateSellerRepository;

    public function __construct(EvaluateSellerRepository $evaluateSellerRepo)
    {
        $this->evaluateSellerRepository = $evaluateSellerRepo;
    }

    /**
     * Display a listing of the EvaluateSeller.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $evaluateSellers = $this->evaluateSellerRepository->all();

        return view('evaluate_sellers.index')
            ->with('evaluateSellers', $evaluateSellers);
    }

    /**
     * Show the form for creating a new EvaluateSeller.
     *
     * @return Response
     */
    public function create()
    {
        return view('evaluate_sellers.create');
    }

    /**
     * Store a newly created EvaluateSeller in storage.
     *
     * @param CreateEvaluateSellerRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluateSellerRequest $request)
    {
        $input = $request->all();

        $evaluateSeller = $this->evaluateSellerRepository->create($input);

        Flash::success('Evaluate Seller saved successfully.');

        return redirect(route('evaluateSellers.index'));
    }

    /**
     * Display the specified EvaluateSeller.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            Flash::error('Evaluate Seller not found');

            return redirect(route('evaluateSellers.index'));
        }

        return view('evaluate_sellers.show')->with('evaluateSeller', $evaluateSeller);
    }

    /**
     * Show the form for editing the specified EvaluateSeller.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            Flash::error('Evaluate Seller not found');

            return redirect(route('evaluateSellers.index'));
        }

        return view('evaluate_sellers.edit')->with('evaluateSeller', $evaluateSeller);
    }

    /**
     * Update the specified EvaluateSeller in storage.
     *
     * @param int $id
     * @param UpdateEvaluateSellerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluateSellerRequest $request)
    {
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            Flash::error('Evaluate Seller not found');

            return redirect(route('evaluateSellers.index'));
        }

        $evaluateSeller = $this->evaluateSellerRepository->update($request->all(), $id);

        Flash::success('Evaluate Seller updated successfully.');

        return redirect(route('evaluateSellers.index'));
    }

    /**
     * Remove the specified EvaluateSeller from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $evaluateSeller = $this->evaluateSellerRepository->find($id);

        if (empty($evaluateSeller)) {
            Flash::error('Evaluate Seller not found');

            return redirect(route('evaluateSellers.index'));
        }

        $this->evaluateSellerRepository->delete($id);

        Flash::success('Evaluate Seller deleted successfully.');

        return redirect(route('evaluateSellers.index'));
    }
}
