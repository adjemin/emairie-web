<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductMediaTypeRequest;
use App\Http\Requests\UpdateProductMediaTypeRequest;
use App\Repositories\ProductMediaTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProductMediaTypeController extends AppBaseController
{
    /** @var  ProductMediaTypeRepository */
    private $productMediaTypeRepository;

    public function __construct(ProductMediaTypeRepository $productMediaTypeRepo)
    {
        $this->productMediaTypeRepository = $productMediaTypeRepo;
    }

    /**
     * Display a listing of the ProductMediaType.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productMediaTypes = $this->productMediaTypeRepository->all();

        return view('product_media_types.index')
            ->with('productMediaTypes', $productMediaTypes);
    }

    /**
     * Show the form for creating a new ProductMediaType.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_media_types.create');
    }

    /**
     * Store a newly created ProductMediaType in storage.
     *
     * @param CreateProductMediaTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateProductMediaTypeRequest $request)
    {
        $input = $request->all();

        $productMediaType = $this->productMediaTypeRepository->create($input);

        Flash::success('Product Media Type saved successfully.');

        return redirect(route('productMediaTypes.index'));
    }

    /**
     * Display the specified ProductMediaType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            Flash::error('Product Media Type not found');

            return redirect(route('productMediaTypes.index'));
        }

        return view('product_media_types.show')->with('productMediaType', $productMediaType);
    }

    /**
     * Show the form for editing the specified ProductMediaType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            Flash::error('Product Media Type not found');

            return redirect(route('productMediaTypes.index'));
        }

        return view('product_media_types.edit')->with('productMediaType', $productMediaType);
    }

    /**
     * Update the specified ProductMediaType in storage.
     *
     * @param int $id
     * @param UpdateProductMediaTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductMediaTypeRequest $request)
    {
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            Flash::error('Product Media Type not found');

            return redirect(route('productMediaTypes.index'));
        }

        $productMediaType = $this->productMediaTypeRepository->update($request->all(), $id);

        Flash::success('Product Media Type updated successfully.');

        return redirect(route('productMediaTypes.index'));
    }

    /**
     * Remove the specified ProductMediaType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productMediaType = $this->productMediaTypeRepository->find($id);

        if (empty($productMediaType)) {
            Flash::error('Product Media Type not found');

            return redirect(route('productMediaTypes.index'));
        }

        $this->productMediaTypeRepository->delete($id);

        Flash::success('Product Media Type deleted successfully.');

        return redirect(route('productMediaTypes.index'));
    }
}
