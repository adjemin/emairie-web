<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFlashSellingRequest;
use App\Http\Requests\UpdateFlashSellingRequest;
use App\Repositories\FlashSellingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FlashSellingController extends AppBaseController
{
    /** @var  FlashSellingRepository */
    private $flashSellingRepository;

    public function __construct(FlashSellingRepository $flashSellingRepo)
    {
        $this->flashSellingRepository = $flashSellingRepo;
    }

    /**
     * Display a listing of the FlashSelling.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $flashSellings = $this->flashSellingRepository->all();

        return view('flash_sellings.index')
            ->with('flashSellings', $flashSellings);
    }

    /**
     * Show the form for creating a new FlashSelling.
     *
     * @return Response
     */
    public function create()
    {
        return view('flash_sellings.create');
    }

    /**
     * Store a newly created FlashSelling in storage.
     *
     * @param CreateFlashSellingRequest $request
     *
     * @return Response
     */
    public function store(CreateFlashSellingRequest $request)
    {
        $input = $request->all();

        $flashSelling = $this->flashSellingRepository->create($input);

        Flash::success('Flash Selling saved successfully.');

        return redirect(route('flashSellings.index'));
    }

    /**
     * Display the specified FlashSelling.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            Flash::error('Flash Selling not found');

            return redirect(route('flashSellings.index'));
        }

        return view('flash_sellings.show')->with('flashSelling', $flashSelling);
    }

    /**
     * Show the form for editing the specified FlashSelling.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            Flash::error('Flash Selling not found');

            return redirect(route('flashSellings.index'));
        }

        return view('flash_sellings.edit')->with('flashSelling', $flashSelling);
    }

    /**
     * Update the specified FlashSelling in storage.
     *
     * @param int $id
     * @param UpdateFlashSellingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlashSellingRequest $request)
    {
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            Flash::error('Flash Selling not found');

            return redirect(route('flashSellings.index'));
        }

        $flashSelling = $this->flashSellingRepository->update($request->all(), $id);

        Flash::success('Flash Selling updated successfully.');

        return redirect(route('flashSellings.index'));
    }

    /**
     * Remove the specified FlashSelling from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $flashSelling = $this->flashSellingRepository->find($id);

        if (empty($flashSelling)) {
            Flash::error('Flash Selling not found');

            return redirect(route('flashSellings.index'));
        }

        $this->flashSellingRepository->delete($id);

        Flash::success('Flash Selling deleted successfully.');

        return redirect(route('flashSellings.index'));
    }
}
