<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerSessionRequest;
use App\Http\Requests\UpdateCustomerSessionRequest;
use App\Repositories\CustomerSessionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerSessionController extends AppBaseController
{
    /** @var  CustomerSessionRepository */
    private $customerSessionRepository;

    public function __construct(CustomerSessionRepository $customerSessionRepo)
    {
        $this->customerSessionRepository = $customerSessionRepo;
    }

    /**
     * Display a listing of the CustomerSession.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerSessions = $this->customerSessionRepository->all();

        return view('customer_sessions.index')
            ->with('customerSessions', $customerSessions);
    }

    /**
     * Show the form for creating a new CustomerSession.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_sessions.create');
    }

    /**
     * Store a newly created CustomerSession in storage.
     *
     * @param CreateCustomerSessionRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerSessionRequest $request)
    {
        $input = $request->all();

        $customerSession = $this->customerSessionRepository->create($input);

        Flash::success('Customer Session saved successfully.');

        return redirect(route('customerSessions.index'));
    }

    /**
     * Display the specified CustomerSession.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            Flash::error('Customer Session not found');

            return redirect(route('customerSessions.index'));
        }

        return view('customer_sessions.show')->with('customerSession', $customerSession);
    }

    /**
     * Show the form for editing the specified CustomerSession.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            Flash::error('Customer Session not found');

            return redirect(route('customerSessions.index'));
        }

        return view('customer_sessions.edit')->with('customerSession', $customerSession);
    }

    /**
     * Update the specified CustomerSession in storage.
     *
     * @param int $id
     * @param UpdateCustomerSessionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerSessionRequest $request)
    {
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            Flash::error('Customer Session not found');

            return redirect(route('customerSessions.index'));
        }

        $customerSession = $this->customerSessionRepository->update($request->all(), $id);

        Flash::success('Customer Session updated successfully.');

        return redirect(route('customerSessions.index'));
    }

    /**
     * Remove the specified CustomerSession from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            Flash::error('Customer Session not found');

            return redirect(route('customerSessions.index'));
        }

        $this->customerSessionRepository->delete($id);

        Flash::success('Customer Session deleted successfully.');

        return redirect(route('customerSessions.index'));
    }
}
