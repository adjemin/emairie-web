<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductAudienceRequest;
use App\Http\Requests\UpdateProductAudienceRequest;
use App\Repositories\ProductAudienceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProductAudienceController extends AppBaseController
{
    /** @var  ProductAudienceRepository */
    private $productAudienceRepository;

    public function __construct(ProductAudienceRepository $productAudienceRepo)
    {
        $this->productAudienceRepository = $productAudienceRepo;
    }

    /**
     * Display a listing of the ProductAudience.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $productAudiences = $this->productAudienceRepository->all();

        return view('product_audiences.index')
            ->with('productAudiences', $productAudiences);
    }

    /**
     * Show the form for creating a new ProductAudience.
     *
     * @return Response
     */
    public function create()
    {
        return view('product_audiences.create');
    }

    /**
     * Store a newly created ProductAudience in storage.
     *
     * @param CreateProductAudienceRequest $request
     *
     * @return Response
     */
    public function store(CreateProductAudienceRequest $request)
    {
        $input = $request->all();

        $productAudience = $this->productAudienceRepository->create($input);

        Flash::success('Product Audience saved successfully.');

        return redirect(route('productAudiences.index'));
    }

    /**
     * Display the specified ProductAudience.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            Flash::error('Product Audience not found');

            return redirect(route('productAudiences.index'));
        }

        return view('product_audiences.show')->with('productAudience', $productAudience);
    }

    /**
     * Show the form for editing the specified ProductAudience.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            Flash::error('Product Audience not found');

            return redirect(route('productAudiences.index'));
        }

        return view('product_audiences.edit')->with('productAudience', $productAudience);
    }

    /**
     * Update the specified ProductAudience in storage.
     *
     * @param int $id
     * @param UpdateProductAudienceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductAudienceRequest $request)
    {
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            Flash::error('Product Audience not found');

            return redirect(route('productAudiences.index'));
        }

        $productAudience = $this->productAudienceRepository->update($request->all(), $id);

        Flash::success('Product Audience updated successfully.');

        return redirect(route('productAudiences.index'));
    }

    /**
     * Remove the specified ProductAudience from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $productAudience = $this->productAudienceRepository->find($id);

        if (empty($productAudience)) {
            Flash::error('Product Audience not found');

            return redirect(route('productAudiences.index'));
        }

        $this->productAudienceRepository->delete($id);

        Flash::success('Product Audience deleted successfully.');

        return redirect(route('productAudiences.index'));
    }
}
