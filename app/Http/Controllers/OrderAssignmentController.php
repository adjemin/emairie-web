<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderAssignmentRequest;
use App\Http\Requests\UpdateOrderAssignmentRequest;
use App\Repositories\OrderAssignmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderAssignmentController extends AppBaseController
{
    /** @var  OrderAssignmentRepository */
    private $orderAssignmentRepository;

    public function __construct(OrderAssignmentRepository $orderAssignmentRepo)
    {
        $this->orderAssignmentRepository = $orderAssignmentRepo;
    }

    /**
     * Display a listing of the OrderAssignment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orderAssignments = $this->orderAssignmentRepository->all();

        return view('order_assignments.index')
            ->with('orderAssignments', $orderAssignments);
    }

    /**
     * Show the form for creating a new OrderAssignment.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_assignments.create');
    }

    /**
     * Store a newly created OrderAssignment in storage.
     *
     * @param CreateOrderAssignmentRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderAssignmentRequest $request)
    {
        $input = $request->all();

        $orderAssignment = $this->orderAssignmentRepository->create($input);

        Flash::success('Order Assignment saved successfully.');

        return redirect(route('orderAssignments.index'));
    }

    /**
     * Display the specified OrderAssignment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            Flash::error('Order Assignment not found');

            return redirect(route('orderAssignments.index'));
        }

        return view('order_assignments.show')->with('orderAssignment', $orderAssignment);
    }

    /**
     * Show the form for editing the specified OrderAssignment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            Flash::error('Order Assignment not found');

            return redirect(route('orderAssignments.index'));
        }

        return view('order_assignments.edit')->with('orderAssignment', $orderAssignment);
    }

    /**
     * Update the specified OrderAssignment in storage.
     *
     * @param int $id
     * @param UpdateOrderAssignmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderAssignmentRequest $request)
    {
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            Flash::error('Order Assignment not found');

            return redirect(route('orderAssignments.index'));
        }

        $orderAssignment = $this->orderAssignmentRepository->update($request->all(), $id);

        Flash::success('Order Assignment updated successfully.');

        return redirect(route('orderAssignments.index'));
    }

    /**
     * Remove the specified OrderAssignment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderAssignment = $this->orderAssignmentRepository->find($id);

        if (empty($orderAssignment)) {
            Flash::error('Order Assignment not found');

            return redirect(route('orderAssignments.index'));
        }

        $this->orderAssignmentRepository->delete($id);

        Flash::success('Order Assignment deleted successfully.');

        return redirect(route('orderAssignments.index'));
    }
}
