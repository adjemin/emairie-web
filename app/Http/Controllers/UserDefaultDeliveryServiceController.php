<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserDefaultDeliveryServiceRequest;
use App\Http\Requests\UpdateUserDefaultDeliveryServiceRequest;
use App\Repositories\UserDefaultDeliveryServiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class UserDefaultDeliveryServiceController extends AppBaseController
{
    /** @var  UserDefaultDeliveryServiceRepository */
    private $userDefaultDeliveryServiceRepository;

    public function __construct(UserDefaultDeliveryServiceRepository $userDefaultDeliveryServiceRepo)
    {
        $this->userDefaultDeliveryServiceRepository = $userDefaultDeliveryServiceRepo;
    }

    /**
     * Display a listing of the UserDefaultDeliveryService.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $userDefaultDeliveryServices = $this->userDefaultDeliveryServiceRepository->all();

        return view('user_default_delivery_services.index')
            ->with('userDefaultDeliveryServices', $userDefaultDeliveryServices);
    }

    /**
     * Show the form for creating a new UserDefaultDeliveryService.
     *
     * @return Response
     */
    public function create()
    {
        return view('user_default_delivery_services.create');
    }

    /**
     * Store a newly created UserDefaultDeliveryService in storage.
     *
     * @param CreateUserDefaultDeliveryServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateUserDefaultDeliveryServiceRequest $request)
    {
        $input = $request->all();

        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->create($input);

        Flash::success('User Default Delivery Service saved successfully.');

        return redirect(route('userDefaultDeliveryServices.index'));
    }

    /**
     * Display the specified UserDefaultDeliveryService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            Flash::error('User Default Delivery Service not found');

            return redirect(route('userDefaultDeliveryServices.index'));
        }

        return view('user_default_delivery_services.show')->with('userDefaultDeliveryService', $userDefaultDeliveryService);
    }

    /**
     * Show the form for editing the specified UserDefaultDeliveryService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            Flash::error('User Default Delivery Service not found');

            return redirect(route('userDefaultDeliveryServices.index'));
        }

        return view('user_default_delivery_services.edit')->with('userDefaultDeliveryService', $userDefaultDeliveryService);
    }

    /**
     * Update the specified UserDefaultDeliveryService in storage.
     *
     * @param int $id
     * @param UpdateUserDefaultDeliveryServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserDefaultDeliveryServiceRequest $request)
    {
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            Flash::error('User Default Delivery Service not found');

            return redirect(route('userDefaultDeliveryServices.index'));
        }

        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->update($request->all(), $id);

        Flash::success('User Default Delivery Service updated successfully.');

        return redirect(route('userDefaultDeliveryServices.index'));
    }

    /**
     * Remove the specified UserDefaultDeliveryService from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $userDefaultDeliveryService = $this->userDefaultDeliveryServiceRepository->find($id);

        if (empty($userDefaultDeliveryService)) {
            Flash::error('User Default Delivery Service not found');

            return redirect(route('userDefaultDeliveryServices.index'));
        }

        $this->userDefaultDeliveryServiceRepository->delete($id);

        Flash::success('User Default Delivery Service deleted successfully.');

        return redirect(route('userDefaultDeliveryServices.index'));
    }
}
