<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDeliveryServiceRequest;
use App\Http\Requests\UpdateDeliveryServiceRequest;
use App\Repositories\DeliveryServiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DeliveryServiceController extends AppBaseController
{
    /** @var  DeliveryServiceRepository */
    private $deliveryServiceRepository;

    public function __construct(DeliveryServiceRepository $deliveryServiceRepo)
    {
        $this->deliveryServiceRepository = $deliveryServiceRepo;
    }

    /**
     * Display a listing of the DeliveryService.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $deliveryServices = $this->deliveryServiceRepository->all();

        return view('delivery_services.index')
            ->with('deliveryServices', $deliveryServices);
    }

    /**
     * Show the form for creating a new DeliveryService.
     *
     * @return Response
     */
    public function create()
    {
        return view('delivery_services.create');
    }

    /**
     * Store a newly created DeliveryService in storage.
     *
     * @param CreateDeliveryServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateDeliveryServiceRequest $request)
    {
        $input = $request->all();

        $deliveryService = $this->deliveryServiceRepository->create($input);

        Flash::success('Delivery Service saved successfully.');

        return redirect(route('deliveryServices.index'));
    }

    /**
     * Display the specified DeliveryService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            Flash::error('Delivery Service not found');

            return redirect(route('deliveryServices.index'));
        }

        return view('delivery_services.show')->with('deliveryService', $deliveryService);
    }

    /**
     * Show the form for editing the specified DeliveryService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            Flash::error('Delivery Service not found');

            return redirect(route('deliveryServices.index'));
        }

        return view('delivery_services.edit')->with('deliveryService', $deliveryService);
    }

    /**
     * Update the specified DeliveryService in storage.
     *
     * @param int $id
     * @param UpdateDeliveryServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDeliveryServiceRequest $request)
    {
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            Flash::error('Delivery Service not found');

            return redirect(route('deliveryServices.index'));
        }

        $deliveryService = $this->deliveryServiceRepository->update($request->all(), $id);

        Flash::success('Delivery Service updated successfully.');

        return redirect(route('deliveryServices.index'));
    }

    /**
     * Remove the specified DeliveryService from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryService = $this->deliveryServiceRepository->find($id);

        if (empty($deliveryService)) {
            Flash::error('Delivery Service not found');

            return redirect(route('deliveryServices.index'));
        }

        $this->deliveryServiceRepository->delete($id);

        Flash::success('Delivery Service deleted successfully.');

        return redirect(route('deliveryServices.index'));
    }
}
