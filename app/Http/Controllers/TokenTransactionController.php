<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTokenTransactionRequest;
use App\Http\Requests\UpdateTokenTransactionRequest;
use App\Repositories\TokenTransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TokenTransactionController extends AppBaseController
{
    /** @var  TokenTransactionRepository */
    private $tokenTransactionRepository;

    public function __construct(TokenTransactionRepository $tokenTransactionRepo)
    {
        $this->tokenTransactionRepository = $tokenTransactionRepo;
    }

    /**
     * Display a listing of the TokenTransaction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $tokenTransactions = $this->tokenTransactionRepository->all();

        return view('token_transactions.index')
            ->with('tokenTransactions', $tokenTransactions);
    }

    /**
     * Show the form for creating a new TokenTransaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('token_transactions.create');
    }

    /**
     * Store a newly created TokenTransaction in storage.
     *
     * @param CreateTokenTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTokenTransactionRequest $request)
    {
        $input = $request->all();

        $tokenTransaction = $this->tokenTransactionRepository->create($input);

        Flash::success('Token Transaction saved successfully.');

        return redirect(route('tokenTransactions.index'));
    }

    /**
     * Display the specified TokenTransaction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            Flash::error('Token Transaction not found');

            return redirect(route('tokenTransactions.index'));
        }

        return view('token_transactions.show')->with('tokenTransaction', $tokenTransaction);
    }

    /**
     * Show the form for editing the specified TokenTransaction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            Flash::error('Token Transaction not found');

            return redirect(route('tokenTransactions.index'));
        }

        return view('token_transactions.edit')->with('tokenTransaction', $tokenTransaction);
    }

    /**
     * Update the specified TokenTransaction in storage.
     *
     * @param int $id
     * @param UpdateTokenTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTokenTransactionRequest $request)
    {
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            Flash::error('Token Transaction not found');

            return redirect(route('tokenTransactions.index'));
        }

        $tokenTransaction = $this->tokenTransactionRepository->update($request->all(), $id);

        Flash::success('Token Transaction updated successfully.');

        return redirect(route('tokenTransactions.index'));
    }

    /**
     * Remove the specified TokenTransaction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tokenTransaction = $this->tokenTransactionRepository->find($id);

        if (empty($tokenTransaction)) {
            Flash::error('Token Transaction not found');

            return redirect(route('tokenTransactions.index'));
        }

        $this->tokenTransactionRepository->delete($id);

        Flash::success('Token Transaction deleted successfully.');

        return redirect(route('tokenTransactions.index'));
    }
}
