<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductMedia;
use App\Models\Customer;
use App\Models\ProductCategory;
use App\Models\FavoriteProduct;
use App\Notifications\ProductOrdered;
use Flash;
use Response;
use Cookie;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepository = $productRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $produit= Product::where('id','=',$request['identif'])->get();
        $produitmedia= ProductMedia::where('product_id','=',$request['identif'])->get();

        foreach ($produit as $produits) {
            $custid=$produits->customer_id;
            $customer=Customer::where('id','=',$custid)->first();
            $valeur = session('clef');
            $cust=Customer::where('phone_number','=',$valeur)->get();

            foreach ($cust as $customer) {
                $favoris=FavoriteProduct::where('product_id','=',$request['identif'])->where('customer_id','=',$customer->id)->first();
            }
            return view('layoutFront/productsDetail',
            compact('produit','produitmedia','customer','cust','favoris'));
            
        }
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        return view('products.create');
    }

    //recherche ajax
    public function recherche(Request $request){
    $search=$request->get('term');
    $result=Product::where('title','LIKE','%'.$search.'%')->get();
    return response()->json($result);
    }

    //résultat de la recerche
    public function resultat(Request $request){
    $valeur = session('clef');
    $cust=Customer::where('phone_number','=',$valeur)->get();
    $result=Product::where('title','LIKE','%'.$request->result.'%')->paginate(20);
    $categorie=ProductCategory::simplePaginate(5);
    $maxi=Product::max('price');


    if($result->count()>0){
    return view('layoutFront/recherche',compact('cust','categorie','maxi'))->with('result',$result);
    }else{
        return view('layoutFront/erreurrecherche',compact('cust','categorie','maxi'));
    }

    }

    //recherche par prix résultat
    public function prix(Request $request){
    $valeur = session('clef');
    $cust=Customer::where('phone_number','=',$valeur)->get();
    $categorie=ProductCategory::simplePaginate(5);
    $maxi=Product::max('price');

    
    $result=Product::where('price','<=',$request->max)->where('price','>=',$request->min)->paginate(20);
        if($result->count()>0){
            return view('layoutFront/recherche',compact('result','cust','categorie','maxi'));
        }else{
            return view('layoutFront/erreurrecherche',compact('categorie','maxi'));
        }
    }

    //recherche par catégorie
    public function cat($cat){

    $valeur = session('clef');
    $cust=Customer::where('phone_number','=',$valeur)->get();
    $result=Product::where('category_id','=',$cat)->paginate(20);
    $categorie=ProductCategory::simplePaginate(5);
    $maxi=Product::max('price');


    if($result->count()>0){
        return view('layoutFront/recherche',compact('cust','categorie','maxi'))->with('result',$result);
        }else{
            return view('layoutFront/erreurrecherche',compact('cust','categorie','maxi'));
        }
    }


    //Les detail d'un Vente/Achat: Historique-Ramassage-Livraison
    public function transactionDelail()
    {

        return view('layoutFront/');
    }



    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        $product = $this->productRepository->create($input);

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product);
    }


    //notification commande
    /*public function ordere(Request $request, Product $product){

        $this->ProductRepository->setProductOrder($product);
        // Notification
        $notificationRepository->deleteDuplicate($customer, $product);
        $product->customer->notify(new ProductOrdered($product, $request->value, $customer->id));
        return ...

    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product = $this->productRepository->update($request->all(), $id);

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }
}
