<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Customer;
use App\Models\ProductCategory;
use App\Models\OrderItem;
use App\Models\Order;
use Validator;
use Flash;
use Response;
use Session;
use Cookie;
use Image;
use Crypt;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //dd(session('clef'));
        $prod=Product::get();
        $cus=Customer::get('phone_number');
        $valeur = session('clef');
        //dd($valeur);
        $cust=Customer::where('phone_number','=',$valeur)->get();
        //dd($cust);
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return view('layoutFront/frontBody',compact('prod','categorie','maxi','cus','cust'));
    }


    //afficher prix bas
    public function prixbas(){
        $prod=Product::orderBy('price','ASC')->get();
        $cus=Customer::get('phone_number');
        $valeur = session('clef');
        //dd($valeur);
        $cust=Customer::where('phone_number','=',$valeur)->get();
        //dd($cust);
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return view('layoutFront/frontBody',compact('prod','categorie','maxi','cus','cust'));
        
    }

    //afficher prix haut
    public function prixhaut(){
        $prod=Product::orderBy('price','DESC')->get();
        $cus=Customer::get('phone_number');
        $valeur = session('clef');
        //dd($valeur);
        $cust=Customer::where('phone_number','=',$valeur)->get();
        //dd($cust);
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return view('layoutFront/frontBody',compact('prod','categorie','maxi','cus','cust'));
        
    }

    //afficher produit recent
    public function recent(){
        $prod=Product::orderBy('created_at','DESC')->get();
        $cus=Customer::get('phone_number');
        $valeur = session('clef');
        //dd($valeur);
        $cust=Customer::where('phone_number','=',$valeur)->get();
        //dd($cust);
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return view('layoutFront/frontBody',compact('prod','categorie','maxi','cus','cust'));
        
    }

    //afficher vieux produit
    public function vieux(){
        $prod=Product::orderBy('created_at','ASC')->get();
        $cus=Customer::get('phone_number');
        $valeur = session('clef');
        //dd($valeur);
        $cust=Customer::where('phone_number','=',$valeur)->get();
        //dd($cust);
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return view('layoutFront/frontBody',compact('prod','categorie','maxi','cus','cust'));
        
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $valeur = session('clef');
        //dd($valeur);
        $valid=Validator::make($request->all(),[
            'last_name'=>'required',
            'first_name'=>'required',
            'image' => 'image|max:1024',
        ]);
        if($valid){
            $cust=Customer::where('phone_number','=',$valeur)->first();
            //$trouv=Customer::findOrFail($cust);
            if($cust){
                $cust->first_name=ucfirst(strtolower($request['first_name']));
                $cust->last_name=ucfirst(strtolower($request['last_name']));
                $cust->name=$cust->first_name.' '.$cust->last_name;
                if($request['email']!=NULL){
                    $cust->email=$request['email'];
                }
                if($request['photo'] != NULL){
                    $photo=$request->file('photo');
                    //dd($photo);
                $filename=$request['last_name'].".".$photo->getClientOriginalExtension();
                $ph=Image::make($photo->getRealPath())->resize(304, 302)->save($photo->move('images/profile/'.$cust->phone.'/',$filename));
                $n='images/profile/'.$cust->phone.'/'.$filename;
                $cust->photo_url=$n;
                $mime = $photo->getClientMimeType();
                //dd($mime);
                $data = $ph->filesize();
                $width = $ph->width();
                $height = $ph->height();

                $cust->photo_url = $n;
                $cust->photo_mime = $mime;
                $cust->photo_height = $height;
                $cust->photo_width = $width;
                $cust->photo_length = $data;
                }
                $cust->save();
            }

        $prod=Product::get();
        $cus=Customer::get();
        $categorie=ProductCategory::simplePaginate(5);
        $maxi=Product::max('price');
        return back()->with('success','Donnee prise en compte');
        }
        else{
            return back()->with('error','Remplissez les champs requis');
        }
    }

    /**
     * Display the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer);
    }


    //utilisateur affiche son profil
    public function profil($id){
        $customer_id= Crypt::decrypt($id);
        $customer = $this->customerRepository->find($customer_id);
        $valeur = session('clef');
        $cust=Customer::where('phone_number','=',$valeur)->get();
        $mont = Order::with('orderitem')->where('current_status','=','customer_paid')->get();
        foreach ($customer->order as $orders) {
                if($mont->count()>0){
                foreach ($mont as $montach) {
                    $achat = $montach->sum('amount');
                }
            }
            if($mont->count()>0){
                foreach ($customer->product as $products) {
                    if($products->count()>0){
                        $orderitem=OrderItem::where('meta_data_id','=',$products->id)->get();
                        $vente= $orderitem->sum('total_amount');

                    }
                }
            }
        }
        return view('FrontCustomer.show',compact('customer','cust','achat','mont','vente','products'));
    }



    /**
     * Show the form for editing the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customer', $customer);
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param int $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    // Utilisateur modifie ses infos
    public function modifier($id, Request $request){
     $customer_id= Crypt::decrypt($id);
     $customer= $this->customerRepository->find($customer_id);
     $valeur = session('clef');
     $cust=Customer::where('phone_number','=',$valeur)->first();
     $valid=Validator::make($request->all(),[
            'photo' => 'image|max:1024',
            ]);
     if($valid->passes()){
        if($request['prenom'] != NULL and $request['nom'] != NULL){
        $customer->gender = $request['civilite'];
        $customer->first_name = ucfirst(strtolower($request['prenom']));
        $customer->last_name = ucfirst(strtolower($request['nom']));
        $customer->name = $customer->first_name.' '.$customer->last_name;
        $customer->birthday = $request['dateN'];
        $customer->save();
        }
        if($request['email'] != NULL){
        $customer->email = $request['email'];
        $customer->save();
        }
        if($request['photo'] != NULL){
            $photo=$request->file('photo');
            $filename=$request['nom'].".".$photo->getClientOriginalExtension();
            $ph=Image::make($photo->getRealPath())->resize(304, 302)->save($photo->move('images/profile/'.$cust->phone.'/',$filename));
            $n='images/profile/'.$cust->phone.'/'.$filename;
            $mime = $photo->getClientMimeType();
            $data = $ph->filesize();
            $width = $ph->width();
            $height = $ph->height();

            $customer->photo_url = $n;
            $customer->photo_mime = $mime;
            $customer->photo_height = $height;
            $customer->photo_width = $width;
            $customer->photo_length = $data;
            $customer->save();
            }

            return back()->with('success','Donnée modifié');
     }else{
        return $valid->errors()->all();
     }

    }


    /**
     * Remove the specified Customer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }

    //deconnexion session
    public function deco(Request $request){
        $request->session()->flush();
        return redirect('/');
    }
}
