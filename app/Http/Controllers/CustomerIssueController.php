<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerIssueRequest;
use App\Http\Requests\UpdateCustomerIssueRequest;
use App\Repositories\CustomerIssueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerIssueController extends AppBaseController
{
    /** @var  CustomerIssueRepository */
    private $customerIssueRepository;

    public function __construct(CustomerIssueRepository $customerIssueRepo)
    {
        $this->customerIssueRepository = $customerIssueRepo;
    }

    /**
     * Display a listing of the CustomerIssue.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerIssues = $this->customerIssueRepository->all();

        return view('customer_issues.index')
            ->with('customerIssues', $customerIssues);
    }

    /**
     * Show the form for creating a new CustomerIssue.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_issues.create');
    }

    /**
     * Store a newly created CustomerIssue in storage.
     *
     * @param CreateCustomerIssueRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerIssueRequest $request)
    {
        $input = $request->all();

        $customerIssue = $this->customerIssueRepository->create($input);

        Flash::success('Customer Issue saved successfully.');

        return redirect(route('customerIssues.index'));
    }

    /**
     * Display the specified CustomerIssue.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            Flash::error('Customer Issue not found');

            return redirect(route('customerIssues.index'));
        }

        return view('customer_issues.show')->with('customerIssue', $customerIssue);
    }

    /**
     * Show the form for editing the specified CustomerIssue.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            Flash::error('Customer Issue not found');

            return redirect(route('customerIssues.index'));
        }

        return view('customer_issues.edit')->with('customerIssue', $customerIssue);
    }

    /**
     * Update the specified CustomerIssue in storage.
     *
     * @param int $id
     * @param UpdateCustomerIssueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerIssueRequest $request)
    {
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            Flash::error('Customer Issue not found');

            return redirect(route('customerIssues.index'));
        }

        $customerIssue = $this->customerIssueRepository->update($request->all(), $id);

        Flash::success('Customer Issue updated successfully.');

        return redirect(route('customerIssues.index'));
    }

    /**
     * Remove the specified CustomerIssue from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerIssue = $this->customerIssueRepository->find($id);

        if (empty($customerIssue)) {
            Flash::error('Customer Issue not found');

            return redirect(route('customerIssues.index'));
        }

        $this->customerIssueRepository->delete($id);

        Flash::success('Customer Issue deleted successfully.');

        return redirect(route('customerIssues.index'));
    }
}
