<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSponsoringTypeRequest;
use App\Http\Requests\UpdateSponsoringTypeRequest;
use App\Repositories\SponsoringTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SponsoringTypeController extends AppBaseController
{
    /** @var  SponsoringTypeRepository */
    private $sponsoringTypeRepository;

    public function __construct(SponsoringTypeRepository $sponsoringTypeRepo)
    {
        $this->sponsoringTypeRepository = $sponsoringTypeRepo;
    }

    /**
     * Display a listing of the SponsoringType.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sponsoringTypes = $this->sponsoringTypeRepository->all();

        return view('sponsoring_types.index')
            ->with('sponsoringTypes', $sponsoringTypes);
    }

    /**
     * Show the form for creating a new SponsoringType.
     *
     * @return Response
     */
    public function create()
    {
        return view('sponsoring_types.create');
    }

    /**
     * Store a newly created SponsoringType in storage.
     *
     * @param CreateSponsoringTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateSponsoringTypeRequest $request)
    {
        $input = $request->all();

        $sponsoringType = $this->sponsoringTypeRepository->create($input);

        Flash::success('Sponsoring Type saved successfully.');

        return redirect(route('sponsoringTypes.index'));
    }

    /**
     * Display the specified SponsoringType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            Flash::error('Sponsoring Type not found');

            return redirect(route('sponsoringTypes.index'));
        }

        return view('sponsoring_types.show')->with('sponsoringType', $sponsoringType);
    }

    /**
     * Show the form for editing the specified SponsoringType.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            Flash::error('Sponsoring Type not found');

            return redirect(route('sponsoringTypes.index'));
        }

        return view('sponsoring_types.edit')->with('sponsoringType', $sponsoringType);
    }

    /**
     * Update the specified SponsoringType in storage.
     *
     * @param int $id
     * @param UpdateSponsoringTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSponsoringTypeRequest $request)
    {
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            Flash::error('Sponsoring Type not found');

            return redirect(route('sponsoringTypes.index'));
        }

        $sponsoringType = $this->sponsoringTypeRepository->update($request->all(), $id);

        Flash::success('Sponsoring Type updated successfully.');

        return redirect(route('sponsoringTypes.index'));
    }

    /**
     * Remove the specified SponsoringType from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sponsoringType = $this->sponsoringTypeRepository->find($id);

        if (empty($sponsoringType)) {
            Flash::error('Sponsoring Type not found');

            return redirect(route('sponsoringTypes.index'));
        }

        $this->sponsoringTypeRepository->delete($id);

        Flash::success('Sponsoring Type deleted successfully.');

        return redirect(route('sponsoringTypes.index'));
    }
}
