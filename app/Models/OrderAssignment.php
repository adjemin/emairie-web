<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="OrderAssignment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="delivery_service_username",
 *          description="delivery_service_username",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_accepted",
 *          description="is_accepted",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="acceptation_time",
 *          description="acceptation_time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="rejection_time",
 *          description="rejection_time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="is_waiting_acceptation",
 *          description="is_waiting_acceptation",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="creator_id",
 *          description="creator_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="creator",
 *          description="creator",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="delivery_fees",
 *          description="delivery_fees",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="task_id",
 *          description="task_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class OrderAssignment extends Model
{
    use SoftDeletes;

    public $table = 'order_assignments';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'delivery_service_username',
        'is_accepted',
        'acceptation_time',
        'rejection_time',
        'is_waiting_acceptation',
        'creator_id',
        'creator',
        'delivery_fees',
        'task_id',
        'currency_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'delivery_service_username' => 'string',
        'is_accepted' => 'boolean',
        'is_waiting_acceptation' => 'boolean',
        'creator_id' => 'integer',
        'creator' => 'string',
        'delivery_fees' => 'string',
        'task_id' => 'integer',
        'currency_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
