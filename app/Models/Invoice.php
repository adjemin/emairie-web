<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order;

/**
 * @SWG\Definition(
 *      definition="Invoice",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reference",
 *          description="reference",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="link",
 *          description="link",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtotal",
 *          description="subtotal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="tax",
 *          description="tax",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fees_delivery",
 *          description="fees_delivery",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="total",
 *          description="total",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_paid_by_customer",
 *          description="is_paid_by_customer",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_paid_by_delivery_service",
 *          description="is_paid_by_delivery_service",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';

    const TAXES = 0.18;

    protected $dates = ['deleted_at'];

    protected $appends = ['currency'];


    public $fillable = [
        'order_id',
        'customer_id',
        'reference',
        'link',
        'subtotal',
        'tax',
        'fees_delivery',
        'total',
        'status',
        'is_paid_by_customer',
        'is_paid_by_delivery_service',
        'currency_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'customer_id' => 'integer',
        'reference' => 'string',
        'link' => 'string',
        'subtotal' => 'string',
        'tax' => 'string',
        'fees_delivery' => 'string',
        'total' => 'string',
        'status' => 'string',
        'is_paid_by_customer' => 'boolean',
        'is_paid_by_delivery_service' => 'boolean',
        'currency_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * Generate ID
     * @return int|string
     */
    public static function generateID($service, $orderId, $customerId){
        //get last record
        $record = Invoice::count() + 1;

        return $service.'-'.$record.'-'.$orderId.'-'.$customerId.'-'.time();
    }

    public function getCurrencyAttribute(){
        return Currency::where(['slug' => $this->currency_code])->first();
    }

     public function order(){
        return $this->belongsTo('App\Models\Order');
    }


}
