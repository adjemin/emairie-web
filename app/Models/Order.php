<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\OrderItem;
use App\Models\OrderHistory;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Customer;

/**
 * @SWG\Definition(
 *      definition="Order",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_waiting",
 *          description="is_waiting",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="current_status",
 *          description="
 *     waiting_payment,
 *     waiting,
 *     confirmed,
 *     started,
 *     successful,
 *     failed,
 *     canceled,
 *     waiting_customer_pickup,
 *     waiting_customer_delivery,
 *     customer_paid,
 *     seller_paid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="rating",
 *          description="rating",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note",
 *          description="note",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="note_date",
 *          description="note_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="payment_method_slug",
 *          description="payment_method_slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="delivery_fees",
 *          description="delivery_fees",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_delivered",
 *          description="is_delivered",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="delivery_date",
 *          description="delivery_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';


    protected $dates = ['deleted_at'];

    protected  $appends = ["items", "invoice"];


    public $fillable = [
        'customer_id',
        'is_waiting',
        'is_waiting_payment',
        'current_status',
        'rating',
        'note',
        'note_date',
        'payment_method_slug',
        'delivery_fees',
        'is_delivered',
        'delivery_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'is_waiting' => 'boolean',
        'is_waiting_payment' => 'boolean',
        'current_status' => 'string',
        'rating' => 'string',
        'note' => 'string',
        'payment_method_slug' => 'string',
        'delivery_fees' => 'string',
        'is_delivered' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getItemsAttribute(){

        return OrderItem::where(["order_id" => $this->id])->get();
    }

    public function getInvoiceAttribute(){

        return Invoice::where(["order_id" => $this->id])->first();
    }

    public function orderitem(){
        return $this->hasMany('App\Models\OrderItem');
    }

    public function orderhistory(){
        return $this->hasMany('App\Models\OrderHistory');
    }

    public function invoice(){
        return $this->hasMany('App\Models\Invoice');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }


}
