<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Country",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="code",
 *          description="code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="flag",
 *          description="flag",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="dial_code",
 *          description="dial_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="language",
 *          description="language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lat",
 *          description="location_lat",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lng",
 *          description="location_lng",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="timezone",
 *          description="timezone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Country extends Model
{
    use SoftDeletes;

    public $table = 'countries';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'flag',
        'dial_code',
        'language',
        'location_lat',
        'location_lng',
        'currency_code',
        'timezone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'flag' => 'string',
        'dial_code' => 'string',
        'language' => 'string',
        'location_lat' => 'string',
        'location_lng' => 'string',
        'currency_code' => 'string',
        'timezone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
