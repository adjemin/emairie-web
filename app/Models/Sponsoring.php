<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Sponsoring",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sponsoring_type",
 *          description="sponsoring_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="start_date",
 *          description="start_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="end_date",
 *          description="end_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="cost",
 *          description="cost",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="percentage",
 *          description="percentage",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="percentage_fees",
 *          description="percentage_fees",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="visilibity",
 *          description="visilibity",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="visibility_location",
 *          description="visibility_location",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Sponsoring extends Model
{
    use SoftDeletes;

    public $table = 'sponsorings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'sponsoring_type',
        'start_date',
        'end_date',
        'cost',
        'currency_code',
        'percentage',
        'percentage_fees',
        'visilibity',
        'visibility_location'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'sponsoring_type' => 'string',
        'cost' => 'string',
        'currency_code' => 'string',
        'percentage' => 'string',
        'percentage_fees' => 'string',
        'visilibity' => 'string',
        'visibility_location' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
