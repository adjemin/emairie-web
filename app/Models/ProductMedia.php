<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ProductMedia",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="content_type",
 *          description="content_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="mime",
 *          description="mime",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="thumbnail",
 *          description="thumbnail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="duration",
 *          description="duration",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="width",
 *          description="width",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="height",
 *          description="height",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="length",
 *          description="length",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ProductMedia extends Model
{
    use SoftDeletes;

    public $table = 'product_media';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'content_type',
        'mime',
        'url',
        'thumbnail',
        'duration',
        'width',
        'height',
        'length'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'content_type' => 'string',
        'mime' => 'string',
        'url' => 'string',
        'thumbnail' => 'string',
        'duration' => 'string',
        'width' => 'string',
        'height' => 'string',
        'length' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
