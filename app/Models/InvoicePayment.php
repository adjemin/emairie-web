<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="InvoicePayment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_method",
 *          description="payment_method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_reference",
 *          description="payment_reference",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="coupon",
 *          description="coupon",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="creator_name",
 *          description="creator_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="creator",
 *          description="creator",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class InvoicePayment extends Model
{
    use SoftDeletes;

    public $table = 'invoice_payments';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'invoice_id',
        'payment_method',
        'payment_reference',
        'amount',
        'currency_code',
        'coupon',
        'creator_id',
        'creator_name',
        'creator',
        'status',
        'is_waiting',
        'is_completed',
        'payment_gateway_trans_id',
        'payment_gateway_custom',
        'payment_gateway_currency',
        'payment_gateway_amount',
        'payment_gateway_payment_date',
        'payment_gateway_payment_time',
        'payment_gateway_error_message',
        'payment_gateway_payment_method',
        'payment_gateway_phone_prefixe',
        'payment_gateway_cel_phone_num',
        'payment_gateway_ipn_ack',
        'payment_gateway_created_at',
        'payment_gateway_updated_at',
        'payment_gateway_cpm_result',
        'payment_gateway_trans_status',
        'payment_gateway_designation',
        'payment_gateway_buyer_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'payment_method' => 'string',
        'payment_reference' => 'string',
        'amount' => 'string',
        'currency_code' => 'string',
        'coupon' => 'string',
        'creator_name' => 'string',
        'creator' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
