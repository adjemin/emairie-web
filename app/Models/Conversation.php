<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @SWG\Definition(
 *      definition="Conversation",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="speakers",
 *          description="speakers",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_group",
 *          description="is_group",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="group_name",
 *          description="group_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="admins",
 *          description="admins",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="has_product",
 *          description="has_product",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Conversation extends Model
{
    use SoftDeletes;

    public $table = 'conversations';


    protected $dates = ['deleted_at'];

    protected  $appends = ['speaker_list', 'product', 'users', 'messages'];


    public $fillable = [
        'speakers',
        'is_group',
        'group_name',
        'admins',
        'product_id',
        'has_product'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'speakers' => 'array',
        'is_group' => 'boolean',
        'group_name' => 'string',
        'admins' => 'string',
        'product_id' => 'integer',
        'has_product' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getSpeakerListAttribute(){
        $speakerList = Collection::make([]);

        $speakers = $this->speakers;

        if(is_array($speakers)){
            foreach ($speakers as $speaker){
                $speakerList->push((int)$speaker);
            }
        }

        return $speakerList;
    }

    public function getUsersAttribute(){

        $speakerList = Collection::make([]);
        $speakers = $this->speakers;

        if(is_array($speakers)){
            foreach ($speakers as $speaker){
                $user = Customer::where(['id' => $speaker])->first();
                if($user != null){
                    $speakerList->push($user);
                }

            }
        }

        return $speakerList;
    }

    public function getMessagesAttribute(){

        return Message::where(["conversation_id" => $this->id])->get();

    }

    public function getProductAttribute(){
        return Product::where(['id' => $this->product_id])->first();
    }


}
