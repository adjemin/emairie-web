<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CustomerNotification",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title_en",
 *          description="title_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtitle",
 *          description="subtitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtitle_en",
 *          description="subtitle_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="action",
 *          description="action",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="action_by",
 *          description="action_by",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="meta_data",
 *          description="meta_data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type_notification",
 *          description="type_notification",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_read",
 *          description="is_read",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_received",
 *          description="is_received",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="data",
 *          description="data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="data_id",
 *          description="data_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CustomerNotification extends Model
{
    use SoftDeletes;

    public $table = 'customer_notifications';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'title_en',
        'subtitle',
        'subtitle_en',
        'action',
        'action_by',
        'meta_data',
        'meta_data_id',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'customer_id',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_en' => 'string',
        'subtitle' => 'string',
        'subtitle_en' => 'string',
        'action' => 'string',
        'action_by' => 'string',
        'meta_data' => 'string',
        'meta_data_id' => 'integer',
        'type_notification' => 'string',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'data' => 'string',
        'customer_id' => 'integer',
        'data_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
