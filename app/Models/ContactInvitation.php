<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ContactInvitation",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="contact_id",
 *          description="contact_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_sms",
 *          description="is_sms",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_sent",
 *          description="is_sent",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="receiver",
 *          description="receiver",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ContactInvitation extends Model
{
    use SoftDeletes;

    public $table = 'contact_invitations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'contact_id',
        'message',
        'is_sms',
        'is_sent',
        'receiver'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contact_id' => 'integer',
        'message' => 'string',
        'is_sms' => 'boolean',
        'is_sent' => 'boolean',
        'receiver' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
