<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="DeliveryAddress",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="address_name",
 *          description="address_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="place",
 *          description="place",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="town",
 *          description="town",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_typed",
 *          description="address_typed",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address_type",
 *          description="address_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class DeliveryAddress extends Model
{
    use SoftDeletes;

    public $table = 'delivery_addresses';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'address_name',
        'latitude',
        'longitude',
        'place',
        'town',
        'address_typed',
        'address_type',
        'customer_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'address_name' => 'string',
        'latitude' => 'double',
        'longitude' => 'double',
        'place' => 'string',
        'town' => 'string',
        'address_typed' => 'string',
        'address_type' => 'string',
        'customer_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
