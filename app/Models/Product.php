<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Customer;
use App\Models\Product;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @SWG\Definition(
 *      definition="Product",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_metadata",
 *          description="description_metadata",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="medias",
 *          description="medias",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_id",
 *          description="cover_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="cover_type",
 *          description="cover_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_url",
 *          description="cover_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_thumbnail",
 *          description="cover_thumbnail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_duration",
 *          description="cover_duration",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_width",
 *          description="cover_width",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cover_height",
 *          description="cover_height",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_name",
 *          description="location_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_address",
 *          description="location_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lat",
 *          description="location_lat",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="location_lng",
 *          description="location_lng",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="category_id",
 *          description="category_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="category_meta",
 *          description="category_meta",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_sold",
 *          description="is_sold",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="sold_at",
 *          description="sold_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="initiale_count",
 *          description="initiale_count",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_firm_price",
 *          description="is_firm_price",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="condition_id",
 *          description="condition_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="published_at",
 *          description="published_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="is_published",
 *          description="is_published",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="deleted_creator",
 *          description="deleted_creator",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sold_count",
 *          description="sold_count",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';


    protected $dates = ['deleted_at'];

    protected $appends = ['category', 'media_list', 'currency', 'seller', 'condition', 'favorite'];


    public $fillable = [
        'title',
        'description',
        'description_metadata',
        'price',
        'currency_slug',
        'medias',
        'cover_id',
        'cover_type',
        'cover_url',
        'cover_thumbnail',
        'cover_duration',
        'cover_width',
        'cover_height',
        'location_name',
        'location_address',
        'location_lat',
        'location_lng',
        'category_id',
        'category_meta',
        'customer_id',
        'is_sold',
        'sold_at',
        'initiale_count',
        'is_firm_price',
        'condition_id',
        'published_at',
        'is_published',
        'deleted_by',
        'deleted_creator',
        'sold_count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'description_metadata' => 'string',
        'price' => 'string',
        'currency_slug' => 'string',
        'has_promo' => 'boolean',
        'promo_percentage' => 'string',
        'medias' => 'string',
        'cover_id' => 'integer',
        'cover_type' => 'string',
        'cover_url' => 'string',
        'cover_thumbnail' => 'string',
        'cover_duration' => 'string',
        'cover_width' => 'string',
        'cover_height' => 'string',
        'location_name' => 'string',
        'location_address' => 'string',
        'location_lat' => 'double',
        'location_lng' => 'double',
        'category_id' => 'integer',
        'category_meta' => 'string',
        'customer_id' => 'integer',
        'is_sold' => 'boolean',
        'initiale_count' => 'integer',
        'is_firm_price' => 'boolean',
        'condition_id' => 'integer',
        'is_published' => 'boolean',
        'deleted_by' => 'integer',
        'deleted_creator' => 'string',
        'sold_count' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getCategoryAttribute(){
        return ProductCategory::where(['id' => $this->category_id])->first();
    }

    public function getCurrencyAttribute(){
        return Currency::where(['slug' => $this->currency_slug])->first();
    }

    public function getMediaListAttribute(){

        $productMediaList = ProductMedia::where(['product_id' => $this->id])->get();

        return $productMediaList;
        //return (array) json_decode($this->medias);
    }

    public function getSellerAttribute(){
        return Customer::where(['id' => $this->customer_id])->first();
    }

    public function getConditionAttribute(){
        return ProductCondition::where(['id' => $this->condition_id])->first();
    }

    public function getFavoriteAttribute(){
        return FavoriteProduct::where(['product_id' => $this->id])->count();
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }

    public function hasOrders(){

        $orderIds = Collection::make([]);

        $ordersItems = OrderItem::where(['meta_data_id' => $this->id])->get();

        if(count($ordersItems) == 0){

            return false;

        }else{

            foreach ($ordersItems as $ordersItem){

                if($orderIds->contains($ordersItem->order_id)){

                }else{
                    $orderIds->push($ordersItem->order_id);
                }

            }

            return count($orderIds) >0;

        }

    }

    public function getOrders(){

        $orders = Collection::make([]);
        $orderIds = Collection::make([]);

        $ordersItems = OrderItem::where(['meta_data_id' => $this->id])->get();

        if(count($ordersItems) == 0){

            return [];


        }else{

            foreach ($ordersItems as $ordersItem){

                if($orderIds->contains($ordersItem->order_id)){

                }else{
                    $orderIds->push($ordersItem->order_id);
                    $orders->push(Order::where(['id' => $ordersItem->order_id])->first());
                }

            }

            return $orders;

        }

    }

public function orderitem(){
        return $this->hasMany('App\Models\OrderItem');
    }

}
