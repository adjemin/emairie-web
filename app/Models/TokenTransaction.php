<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="TokenTransaction",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="source_id",
 *          description="source_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="source_name",
 *          description="source_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transaction_type",
 *          description="transaction_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="receiver_id",
 *          description="receiver_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="receiver_name",
 *          description="receiver_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="designation",
 *          description="designation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class TokenTransaction extends Model
{
    use SoftDeletes;

    public $table = 'token_transactions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'amount',
        'source_id',
        'source_name',
        'transaction_type',
        'receiver_id',
        'receiver_name',
        'designation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'amount' => 'integer',
        'source_id' => 'integer',
        'source_name' => 'string',
        'transaction_type' => 'string',
        'receiver_id' => 'integer',
        'receiver_name' => 'string',
        'designation' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
