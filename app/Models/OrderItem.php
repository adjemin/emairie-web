<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Order;
use App\Models\Product;

/**
 * @SWG\Definition(
 *      definition="OrderItem",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="meta_data",
 *          description="meta_data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="quantity",
 *          description="quantity",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="quantity_unit",
 *          description="quantity_unit",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="unit_price",
 *          description="unit_price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="total_amount",
 *          description="total_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class OrderItem extends Model
{
    use SoftDeletes;

    public $table = 'order_items';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'meta_data_id',
        'meta_data',
        'quantity',
        'quantity_unit',
        'unit_price',
        'currency_code',
        'total_amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order_id' => 'integer',
        'meta_data_id' => 'integer',
        'meta_data' => 'string',
        'quantity' => 'integer',
        'quantity_unit' => 'string',
        'unit_price' => 'string',
        'currency_code' => 'string',
        'total_amount' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function order(){
        return $this->belongsTo('App\Models\Order');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product','meta_data_id');
    }
}
