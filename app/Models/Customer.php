<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Product;
use App\Models\Customer;
use App\Models\Order;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Customer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone_number",
 *          description="phone_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="dial_code",
 *          description="dial_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="country_code",
 *          description="country_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo_id",
 *          description="photo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="photo_url",
 *          description="photo_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo_mime",
 *          description="photo_mime",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo_height",
 *          description="photo_height",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo_width",
 *          description="photo_width",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo_length",
 *          description="photo_length",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gender",
 *          description="gender",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bio",
 *          description="bio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birthday",
 *          description="birthday",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="language",
 *          description="language",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="facebook_id",
 *          description="facebook_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="twitter_id",
 *          description="twitter_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_pro_seller",
 *          description="is_pro_seller",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';


    protected $dates = ['deleted_at'];

    protected $appends = ['followers', 'sold_products'];


    public $fillable = [
        'first_name',
        'last_name',
        'name',
        'phone',
        'phone_number',
        'dial_code',
        'country_code',
        'email',
        'photo_id',
        'photo_url',
        'photo_mime',
        'photo_height',
        'photo_width',
        'photo_length',
        'gender',
        'bio',
        'birthday',
        'language',
        'facebook_id',
        'twitter_id',
        'is_pro_seller'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'last_name' => 'string',
        'name' => 'string',
        'phone' => 'string',
        'phone_number' => 'string',
        'dial_code' => 'string',
        'country_code' => 'string',
        'email' => 'string',
        'photo_id' => 'integer',
        'photo_url' => 'string',
        'photo_mime' => 'string',
        'photo_height' => 'string',
        'photo_width' => 'string',
        'photo_length' => 'string',
        'gender' => 'string',
        'bio' => 'string',
        'birthday' => 'string',
        'language' => 'string',
        'facebook_id' => 'string',
        'twitter_id' => 'string',
        'is_pro_seller' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getFollowersAttribute(){
        return Follow::where(['followed' => $this->id])->count();
    }

    public function getSoldProductsAttribute(){
        return Product::where(['customer_id' => $this->id, 'is_sold' => true])->count();
    }

    public function product(){
        return $this->hasMany('App\Models\Product');
    }

    public function order(){
        return $this->hasMany('App\Models\Order');
    }


}
