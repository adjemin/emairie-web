<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Message",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sender_id",
 *          description="sender_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="conversation_id",
 *          description="conversation_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_read",
 *          description="is_read",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_received",
 *          description="is_received",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_sent",
 *          description="is_sent",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="content_type",
 *          description="content_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Message extends Model
{
    use SoftDeletes;

    public $table = 'messages';


    protected $dates = ['deleted_at'];

    protected $appends = ['sender'];


    public $fillable = [
        'content',
        'sender_id',
        'conversation_id',
        'is_read',
        'is_received',
        'is_sent',
        'content_type',
        'client_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'content' => 'string',
        'sender_id' => 'integer',
        'conversation_id' => 'integer',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'is_sent' => 'boolean',
        'content_type' => 'string',
        'client_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getSenderAttribute(){
        return Customer::where(['id' => $this->sender_id])->first();
    }

    public function getConversation(){
        return Conversation::where(['id' => $this->conversation_id])->first();
    }


}
