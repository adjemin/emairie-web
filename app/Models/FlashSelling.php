<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="FlashSelling",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="product_id",
 *          description="product_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="flash_start_date",
 *          description="flash_start_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="flash_end_date",
 *          description="flash_end_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="delay",
 *          description="delay",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_started",
 *          description="is_started",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_ended",
 *          description="is_ended",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class FlashSelling extends Model
{
    use SoftDeletes;

    public $table = 'flash_sellings';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'flash_start_date',
        'flash_end_date',
        'delay',
        'is_started',
        'is_ended'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'delay' => 'string',
        'is_started' => 'boolean',
        'is_ended' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
