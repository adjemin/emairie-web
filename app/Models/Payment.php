<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Payment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="method_payment_id",
 *          description="method_payment_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_id",
 *          description="payment_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type_payment",
 *          description="type_payment",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_phone_prefix",
 *          description="cpm_phone_prefix",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cel_phone_num",
 *          description="cel_phone_num",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_ipn_ack",
 *          description="cpm_ipn_ack",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_currency",
 *          description="cpm_currency",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_transfer",
 *          description="is_transfer",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="cpm_result",
 *          description="cpm_result",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="commission",
 *          description="commission",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payid",
 *          description="payid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="buyername",
 *          description="buyername",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="transstatus",
 *          description="transstatus",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="signature",
 *          description="signature",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_designation",
 *          description="cpm_designation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_error_message",
 *          description="cpm_error_message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_trans_date",
 *          description="cpm_trans_date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_payment_date",
 *          description="cpm_payment_date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cpm_payment_time",
 *          description="cpm_payment_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sms",
 *          description="sms",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_by",
 *          description="created_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="updated_by",
 *          description="updated_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="deleted_by",
 *          description="deleted_by",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="source_id",
 *          description="source_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="source_name",
 *          description="source_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'method_payment_id',
        'payment_id',
        'type_payment',
        'amount',
        'cpm_phone_prefix',
        'cel_phone_num',
        'cpm_ipn_ack',
        'cpm_currency',
        'status',
        'is_transfer',
        'cpm_result',
        'commission',
        'payid',
        'buyername',
        'transstatus',
        'signature',
        'cpm_designation',
        'cpm_error_message',
        'cpm_trans_date',
        'cpm_payment_date',
        'cpm_payment_time',
        'sms',
        'created_by',
        'updated_by',
        'deleted_by',
        'source_id',
        'source_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'method_payment_id' => 'string',
        'payment_id' => 'string',
        'type_payment' => 'string',
        'amount' => 'string',
        'cpm_phone_prefix' => 'string',
        'cel_phone_num' => 'string',
        'cpm_ipn_ack' => 'string',
        'cpm_currency' => 'string',
        'status' => 'string',
        'is_transfer' => 'boolean',
        'cpm_result' => 'string',
        'commission' => 'string',
        'payid' => 'string',
        'buyername' => 'string',
        'transstatus' => 'string',
        'signature' => 'string',
        'cpm_designation' => 'string',
        'cpm_error_message' => 'string',
        'cpm_trans_date' => 'string',
        'cpm_payment_date' => 'string',
        'cpm_payment_time' => 'string',
        'sms' => 'string',
        'created_by' => 'integer',
        'updated_by' => 'integer',
        'deleted_by' => 'integer',
        'source_id' => 'integer',
        'source_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
