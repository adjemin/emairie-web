<?php

namespace App\Repositories;

use App\Models\CustomerDevice;
use App\Repositories\BaseRepository;

/**
 * Class CustomerDeviceRepository
 * @package App\Repositories
 * @version October 5, 2019, 10:16 am UTC
*/

class CustomerDeviceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'firebase_id',
        'device_id',
        'device_model',
        'device_os',
        'device_os_version',
        'device_model_type',
        'device_meta_data'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerDevice::class;
    }
}
