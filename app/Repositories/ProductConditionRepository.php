<?php

namespace App\Repositories;

use App\Models\ProductCondition;
use App\Repositories\BaseRepository;

/**
 * Class ProductConditionRepository
 * @package App\Repositories
 * @version October 4, 2019, 4:41 am UTC
*/

class ProductConditionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'language'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductCondition::class;
    }
}
