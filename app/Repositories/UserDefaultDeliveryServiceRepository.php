<?php

namespace App\Repositories;

use App\Models\UserDefaultDeliveryService;
use App\Repositories\BaseRepository;

/**
 * Class UserDefaultDeliveryServiceRepository
 * @package App\Repositories
 * @version October 4, 2019, 6:05 am UTC
*/

class UserDefaultDeliveryServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'delivery_service_id',
        'default_cost',
        'cost_type',
        'priority'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserDefaultDeliveryService::class;
    }
}
