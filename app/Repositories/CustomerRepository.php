<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Repositories\BaseRepository;

/**
 * Class CustomerRepository
 * @package App\Repositories
 * @version October 4, 2019, 4:57 am UTC
*/

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'last_name',
        'name',
        'phone_number',
        'dial_code',
        'country_code',
        'email',
        'photo_id',
        'photo_url',
        'photo_mime',
        'photo_height',
        'photo_width',
        'photo_length',
        'gender',
        'bio',
        'birthday',
        'language',
        'facebook_id',
        'twitter_id',
        'is_pro_seller'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
