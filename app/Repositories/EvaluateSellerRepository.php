<?php

namespace App\Repositories;

use App\Models\EvaluateSeller;
use App\Repositories\BaseRepository;

/**
 * Class EvaluateSellerRepository
 * @package App\Repositories
 * @version October 5, 2019, 5:08 pm UTC
*/

class EvaluateSellerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'note',
        'note_description',
        'note_by'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EvaluateSeller::class;
    }
}
