<?php

namespace App\Repositories;

use App\Models\FlashSelling;
use App\Repositories\BaseRepository;

/**
 * Class FlashSellingRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:37 am UTC
*/

class FlashSellingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'flash_start_date',
        'flash_end_date',
        'delay',
        'is_started',
        'is_ended'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FlashSelling::class;
    }
}
