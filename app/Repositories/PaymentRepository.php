<?php

namespace App\Repositories;

use App\Models\Payment;
use App\Repositories\BaseRepository;

/**
 * Class PaymentRepository
 * @package App\Repositories
 * @version October 5, 2019, 11:43 am UTC
*/

class PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'methode_payment_id',
        'payment_id',
        'type_payment',
        'payment_method',
        'amount',
        'cpm_phone_prefixe',
        'cel_phone_num',
        'cpm_ipn_ack',
        'cpm_currency',
        'status',
        'is_transfer',
        'cpm_result',
        'commission',
        'payid',
        'buyername',
        'transstatus',
        'signature',
        'cpm_designation',
        'cpm_error_message',
        'cpm_trans_date',
        'cpm_payment_date',
        'cpm_payment_time',
        'sms',
        'is_deleted',
        'created_by',
        'updated_by',
        'deleted_by',
        'source_id',
        'source_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Payment::class;
    }
}
