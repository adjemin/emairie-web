<?php

namespace App\Repositories;

use App\Models\NotificationType;
use App\Repositories\BaseRepository;

/**
 * Class NotificationTypeRepository
 * @package App\Repositories
 * @version October 5, 2019, 11:00 am UTC
*/

class NotificationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NotificationType::class;
    }
}
