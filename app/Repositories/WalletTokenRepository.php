<?php

namespace App\Repositories;

use App\Models\WalletToken;
use App\Repositories\BaseRepository;

/**
 * Class WalletTokenRepository
 * @package App\Repositories
 * @version October 5, 2019, 2:26 pm UTC
*/

class WalletTokenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'amount',
        'is_actived'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WalletToken::class;
    }
}
