<?php

namespace App\Repositories;

use App\Models\InvoicePayment;
use App\Repositories\BaseRepository;

/**
 * Class InvoicePaymentRepository
 * @package App\Repositories
 * @version November 10, 2019, 10:53 am UTC
 *
 * @method InvoicePayment findWithoutFail($id, $columns = ['*'])
 * @method InvoicePayment find($id, $columns = ['*'])
 * @method InvoicePayment first($columns = ['*'])
*/
class InvoicePaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'invoice_id',
        'payment_method',
        'payment_reference',
        'amount',
        'currency_code',
        'coupon',
        'creator_id',
        'creator_name',
        'creator'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }
    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoicePayment::class;
    }
}
