<?php

namespace App\Repositories;

use App\Models\TokenTransaction;
use App\Repositories\BaseRepository;

/**
 * Class TokenTransactionRepository
 * @package App\Repositories
 * @version October 5, 2019, 5:02 pm UTC
*/

class TokenTransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'source_id',
        'source_name',
        'transaction_type',
        'receiver_id',
        'receiver_name',
        'designation'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TokenTransaction::class;
    }
}
