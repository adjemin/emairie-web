<?php

namespace App\Repositories;

use App\Models\OrderAssignment;
use App\Repositories\BaseRepository;

/**
 * Class OrderAssignmentRepository
 * @package App\Repositories
 * @version October 4, 2019, 6:18 am UTC
*/

class OrderAssignmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'delivery_service_username',
        'is_accepted',
        'acceptation_time',
        'rejection_time',
        'is_waiting_acceptation',
        'creator_id',
        'creator',
        'delivery_fees',
        'task_id',
        'currency_code'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderAssignment::class;
    }
}
