<?php

namespace App\Repositories;

use App\Models\PaymentSource;
use App\Repositories\BaseRepository;

/**
 * Class PaymentSourceRepository
 * @package App\Repositories
 * @version October 5, 2019, 11:45 am UTC
*/

class PaymentSourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PaymentSource::class;
    }
}
