<?php

namespace App\Repositories;

use App\Models\ProductMedia;
use App\Repositories\BaseRepository;

/**
 * Class ProductMediaRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:26 am UTC
*/

class ProductMediaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'content_type',
        'mime',
        'url',
        'thumbnail',
        'duration',
        'width',
        'height',
        'length'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductMedia::class;
    }
}
