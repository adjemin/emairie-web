<?php

namespace App\Repositories;

use App\Models\ProductMediaType;
use App\Repositories\BaseRepository;

/**
 * Class ProductMediaTypeRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:27 am UTC
*/

class ProductMediaTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductMediaType::class;
    }
}
