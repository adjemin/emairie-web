<?php

namespace App\Repositories;

use App\Models\OrderHistory;
use App\Repositories\BaseRepository;

/**
 * Class OrderHistoryRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:54 am UTC
*/

class OrderHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'status',
        'creator',
        'creator_id',
        'creator_name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderHistory::class;
    }
}
