<?php

namespace App\Repositories;

use App\Models\Follow;
use App\Repositories\BaseRepository;

/**
 * Class FollowRepository
 * @package App\Repositories
 * @version October 5, 2019, 9:50 am UTC
*/

class FollowRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'follower',
        'followed'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Follow::class;
    }
}
