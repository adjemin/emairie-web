<?php

namespace App\Repositories;

use App\Models\ContactInvitation;
use App\Repositories\BaseRepository;

/**
 * Class ContactInvitationRepository
 * @package App\Repositories
 * @version October 5, 2019, 2:23 pm UTC
*/

class ContactInvitationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contact_id',
        'message',
        'is_sms',
        'is_sent',
        'receiver'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactInvitation::class;
    }
}
