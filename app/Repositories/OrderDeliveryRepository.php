<?php

namespace App\Repositories;

use App\Models\OrderDelivery;
use App\Repositories\BaseRepository;

/**
 * Class OrderDeliveryRepository
 * @package App\Repositories
 * @version October 4, 2019, 6:20 am UTC
*/

class OrderDeliveryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'delivery_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderDelivery::class;
    }
}
