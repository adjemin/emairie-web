<?php

namespace App\Repositories;

use App\Models\Sponsoring;
use App\Repositories\BaseRepository;

/**
 * Class SponsoringRepository
 * @package App\Repositories
 * @version October 5, 2019, 11:30 am UTC
*/

class SponsoringRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'sponsoring_type',
        'start_date',
        'end_date',
        'cost',
        'currency_code',
        'percentage',
        'percentage_fees',
        'visilibity',
        'visibility_location'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sponsoring::class;
    }
}
