<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\BaseRepository;

/**
 * Class ProductRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:19 am UTC
*/

class ProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'description_metadata',
        'medias',
        'cover_id',
        'cover_type',
        'cover_url',
        'cover_thumbnail',
        'cover_duration',
        'cover_width',
        'cover_height',
        'location_name',
        'location_address',
        'location_lat',
        'location_lng',
        'category_id',
        'category_meta',
        'customer_id',
        'is_sold',
        'sold_at',
        'initiale_count',
        'is_firm_price',
        'condition_id',
        'published_at',
        'is_published',
        'deleted_by',
        'deleted_creator',
        'sold_count'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Product::class;
    }
}
