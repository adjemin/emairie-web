<?php

namespace App\Repositories;

use App\Models\CustomerSession;
use App\Repositories\BaseRepository;

/**
 * Class CustomerSessionRepository
 * @package App\Repositories
 * @version October 5, 2019, 10:44 am UTC
*/

class CustomerSessionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'token',
        'customer_id',
        'is_active',
        'location_address',
        'location_latitude',
        'location_longitude',
        'battery',
        'version',
        'device',
        'ip_address',
        'network',
        'isMobile',
        'isTesting',
        'deviceId',
        'devicesOSVersion',
        'devicesName',
        'w',
        'h',
        'ms',
        'idapp'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerSession::class;
    }
}
