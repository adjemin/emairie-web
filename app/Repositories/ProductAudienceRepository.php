<?php

namespace App\Repositories;

use App\Models\ProductAudience;
use App\Repositories\BaseRepository;

/**
 * Class ProductAudienceRepository
 * @package App\Repositories
 * @version October 5, 2019, 5:49 pm UTC
*/

class ProductAudienceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'view_count',
        'last_read_date',
        'like_count',
        'order_count',
        'conversation_count',
        'issue_count'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ProductAudience::class;
    }
}
