<?php

namespace App\Repositories;

use App\Models\CustomerIssue;
use App\Repositories\BaseRepository;

/**
 * Class CustomerIssueRepository
 * @package App\Repositories
 * @version October 5, 2019, 5:17 pm UTC
*/

class CustomerIssueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'created_by',
        'description',
        'issue_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerIssue::class;
    }
}
