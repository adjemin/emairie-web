<?php

namespace App\Repositories;

use App\Models\SponsoringType;
use App\Repositories\BaseRepository;

/**
 * Class SponsoringTypeRepository
 * @package App\Repositories
 * @version October 5, 2019, 11:08 am UTC
*/

class SponsoringTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SponsoringType::class;
    }
}
