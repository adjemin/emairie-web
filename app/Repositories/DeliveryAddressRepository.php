<?php

namespace App\Repositories;

use App\Models\DeliveryAddress;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryAddressRepository
 * @package App\Repositories
 * @version October 4, 2019, 5:01 am UTC
*/

class DeliveryAddressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'address_name',
        'latitude',
        'longitude',
        'place',
        'town',
        'address_typed',
        'address_type',
        'customer_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryAddress::class;
    }
}
