<?php

namespace App\Repositories;

use App\Models\FavoriteProduct;
use App\Repositories\BaseRepository;

/**
 * Class FavoriteProductRepository
 * @package App\Repositories
 * @version October 5, 2019, 9:31 am UTC
*/

class FavoriteProductRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'customer_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FavoriteProduct::class;
    }
}
