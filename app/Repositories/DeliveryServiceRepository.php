<?php

namespace App\Repositories;

use App\Models\DeliveryService;
use App\Repositories\BaseRepository;

/**
 * Class DeliveryServiceRepository
 * @package App\Repositories
 * @version October 4, 2019, 6:09 am UTC
*/

class DeliveryServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'smartlivraison_username',
        'email',
        'logo',
        'delivery_level',
        'phone'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DeliveryService::class;
    }
}
