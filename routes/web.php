<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test',function(){
  return view('test');
});

Route::get('/', function(){
  return view('customer/auth/register');
});

Route::get('home','CustomerController@index')->name('home');

Route::get('payment', function(){
  return view('payments.indexx');
});

Route::get('deconnexion','CustomerController@deco')->name('deconnexion');

Route::put('modifier/{id}','CustomerController@modifier');

Route::get('supprimeraddrese/{lieu}','DeliveryAddressController@supprimer');

Route::get('choix','OrderController@choix');

Route::get('commande/{id}','OrderController@montrer');

Route::get('moi/{id}','CustomerController@profil');

Route::get('vente/{id}','OrderController@vente');

Route::get('lieu/{type}','DeliveryAddressController@details');

Route::get('resultat','ProductController@resultat')->name('resultat');

Route::get('recherche', 'ProductController@recherche')->name('recherche');

Route::get('categorie/{cat}', 'ProductController@cat')->name('categorie');

Auth::routes();

Route::get('prix','ProductController@prix')->name('prix');

Route::get('prixbas','CustomerController@prixbas')->name('prixbas');

Route::get('prixhaut','CustomerController@prixhaut')->name('prixhaut');

Route::get('recent','CustomerController@recent')->name('recent');

Route::get('vieux','CustomerController@vieux')->name('vieux');

Route::get('/admin', 'HomeController@index');

Route::resource('productCategories', 'ProductCategoryController');

Route::resource('productConditions', 'ProductConditionController');

Route::resource('countries', 'CountryController');

Route::resource('currencies', 'CurrencyController');

Route::resource('customers', 'CustomerController');

Route::resource('deliveryAddresses', 'DeliveryAddressController');

Route::resource('products', 'ProductController');

Route::resource('photos', 'PhotoController');

Route::resource('productMedia', 'ProductMediaController');

Route::resource('productMediaTypes', 'ProductMediaTypeController');

Route::resource('flashSellings', 'FlashSellingController');

Route::resource('orderItems', 'OrderItemController');

Route::resource('orders', 'OrderController');

Route::resource('orderHistories', 'OrderHistoryController');

Route::resource('userDefaultDeliveryServices', 'UserDefaultDeliveryServiceController');

Route::resource('deliveryServices', 'DeliveryServiceController');

Route::resource('orderAssignments', 'OrderAssignmentController');

Route::resource('orderDeliveries', 'OrderDeliveryController');

Route::resource('paymentMethods', 'PaymentMethodController');

Route::resource('invoices', 'InvoiceController');

Route::resource('favoriteProducts', 'FavoriteProductController');

Route::resource('follows', 'FollowController');

Route::resource('customerNotifications', 'CustomerNotificationController');

Route::resource('customerDevices', 'CustomerDeviceController');

Route::resource('customerSessions', 'CustomerSessionController');

Route::resource('coupons', 'CouponController');

Route::resource('conversations', 'ConversationController');

Route::resource('messages', 'MessageController');

Route::resource('notificationTypes', 'NotificationTypeController');

Route::resource('sponsoringTypes', 'SponsoringTypeController');

Route::resource('sponsorings', 'SponsoringController');

Route::resource('payments', 'PaymentController');

Route::resource('paymentSources', 'PaymentSourceController');

Route::resource('contacts', 'ContactController');

Route::resource('contactInvitations', 'ContactInvitationController');

Route::resource('walletTokens', 'WalletTokenController');

Route::resource('tokenTransactions', 'TokenTransactionController');

Route::resource('evaluateSellers', 'EvaluateSellerController');

Route::resource('customerIssues', 'CustomerIssueController');

Route::resource('productAudiences', 'ProductAudienceController');
Route::post('notify', 'InvoiceController@notify');
Route::get('cancel/{transactionId}', 'InvoiceController@cancelTransaction');

Route::resource('users', 'UserController');
Route::group(['prefix' => 'customer'], function () {
  Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'CustomerAuth\LoginController@login');
  Route::post('/logout', 'CustomerAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'CustomerAuth\RegisterController@register');

  Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');
});
