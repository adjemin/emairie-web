<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('product_categories', 'ProductCategoryAPIController');

Route::resource('product_conditions', 'ProductConditionAPIController');

Route::resource('countries', 'CountryAPIController');

Route::resource('currencies', 'CurrencyAPIController');

Route::resource('customers', 'CustomerAPIController');
Route::post('customers_login', 'CustomerAPIController@login');
Route::get('customers/find-by-phone/{phone_with_prefix}', 'CustomerAPIController@findByPhone');

Route::resource('delivery_addresses', 'DeliveryAddressAPIController');
Route::post('delivery_addresses_pricing', 'DeliveryAddressAPIController@pricing');

Route::resource('products', 'ProductAPIController');
Route::get('timeline', 'ProductAPIController@findTimeline');

Route::resource('photos', 'PhotoAPIController');

Route::resource('product_media', 'ProductMediaAPIController');

Route::resource('product_media_types', 'ProductMediaTypeAPIController');

Route::resource('flash_sellings', 'FlashSellingAPIController');

Route::resource('order_items', 'OrderItemAPIController');

Route::resource('orders', 'OrderAPIController');
Route::get('sellings/{id}', 'OrderAPIController@sellings');

Route::resource('order_histories', 'OrderHistoryAPIController');

Route::resource('user_default_delivery_services', 'UserDefaultDeliveryServiceAPIController');

Route::resource('delivery_services', 'DeliveryServiceAPIController');

Route::resource('order_assignments', 'OrderAssignmentAPIController');

Route::resource('order_deliveries', 'OrderDeliveryAPIController');

Route::resource('payment_methods', 'PaymentMethodAPIController');

Route::resource('invoices', 'InvoiceAPIController');

Route::resource('favorite_products', 'FavoriteProductAPIController');
Route::post('unfavorite_products', 'FavoriteProductAPIController@unFavorite');
Route::post('is_favorite_products', 'FavoriteProductAPIController@isFavorite');

Route::get('favorite_products/customers/{customerId}', 'FavoriteProductAPIController@findProductByCustomer');

Route::resource('follows', 'FollowAPIController');
Route::post('unfollows', 'FollowAPIController@unFollow');
Route::post('check-following', 'FollowAPIController@checkFollowing');

Route::resource('customer_notifications', 'CustomerNotificationAPIController');
Route::get('customer_notifications_read/{id}', 'CustomerNotificationAPIController@makeRead');
Route::get('customer_notifications_receive/(id}', 'CustomerNotificationAPIController@makeReceive');

Route::resource('customer_devices', 'CustomerDeviceAPIController');

Route::resource('customer_sessions', 'CustomerSessionAPIController');

Route::resource('coupons', 'CouponAPIController');

Route::post('open_conversation', 'ConversationAPIController@openConversation');

Route::resource('conversations', 'ConversationAPIController');
Route::get('conversations/customers/{customerId}', 'ConversationAPIController@findByCustomer');

Route::resource('messages', 'MessageAPIController');

Route::get('messages/conversations/{conversationId}', 'MessageAPIController@findByConversation');

Route::get('messages-set-read/{id}', 'MessageAPIController@makeRead');
Route::get('messages-set-receive/{id}', 'MessageAPIController@makeReceive');

Route::resource('notification_types', 'NotificationTypeAPIController');

Route::resource('sponsoring_types', 'SponsoringTypeAPIController');

Route::resource('sponsorings', 'SponsoringAPIController');

Route::resource('payments', 'PaymentAPIController');

Route::resource('payment_sources', 'PaymentSourceAPIController');

Route::resource('contacts', 'ContactAPIController');

Route::resource('contact_invitations', 'ContactInvitationAPIController');

Route::resource('wallet_tokens', 'WalletTokenAPIController');

Route::resource('token_transactions', 'TokenTransactionAPIController');

Route::resource('evaluate_sellers', 'EvaluateSellerAPIController');

Route::resource('customer_issues', 'CustomerIssueAPIController');

Route::resource('product_audiences', 'ProductAudienceAPIController');

Route::resource('users', 'UserAPIController');

Route::post('subscribe', 'UserAPIController@subscribe');

Route::resource('invoice_payments', 'InvoicePaymentAPIController');
Route::post('invoice_payments/notify', 'InvoicePaymentAPIController@notify');
Route::post('invoice_payments/cancel', 'InvoicePaymentAPIController@cancel');
