<?php namespace Tests\Repositories;

use App\Models\CustomerNotification;
use App\Repositories\CustomerNotificationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerNotificationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerNotificationRepository
     */
    protected $customerNotificationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerNotificationRepo = \App::make(CustomerNotificationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->make()->toArray();

        $createdCustomerNotification = $this->customerNotificationRepo->create($customerNotification);

        $createdCustomerNotification = $createdCustomerNotification->toArray();
        $this->assertArrayHasKey('id', $createdCustomerNotification);
        $this->assertNotNull($createdCustomerNotification['id'], 'Created CustomerNotification must have id specified');
        $this->assertNotNull(CustomerNotification::find($createdCustomerNotification['id']), 'CustomerNotification with given id must be in DB');
        $this->assertModelData($customerNotification, $createdCustomerNotification);
    }

    /**
     * @test read
     */
    public function test_read_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();

        $dbCustomerNotification = $this->customerNotificationRepo->find($customerNotification->id);

        $dbCustomerNotification = $dbCustomerNotification->toArray();
        $this->assertModelData($customerNotification->toArray(), $dbCustomerNotification);
    }

    /**
     * @test update
     */
    public function test_update_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();
        $fakeCustomerNotification = factory(CustomerNotification::class)->make()->toArray();

        $updatedCustomerNotification = $this->customerNotificationRepo->update($fakeCustomerNotification, $customerNotification->id);

        $this->assertModelData($fakeCustomerNotification, $updatedCustomerNotification->toArray());
        $dbCustomerNotification = $this->customerNotificationRepo->find($customerNotification->id);
        $this->assertModelData($fakeCustomerNotification, $dbCustomerNotification->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_notification()
    {
        $customerNotification = factory(CustomerNotification::class)->create();

        $resp = $this->customerNotificationRepo->delete($customerNotification->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerNotification::find($customerNotification->id), 'CustomerNotification should not exist in DB');
    }
}
