<?php namespace Tests\Repositories;

use App\Models\WalletToken;
use App\Repositories\WalletTokenRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class WalletTokenRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var WalletTokenRepository
     */
    protected $walletTokenRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->walletTokenRepo = \App::make(WalletTokenRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->make()->toArray();

        $createdWalletToken = $this->walletTokenRepo->create($walletToken);

        $createdWalletToken = $createdWalletToken->toArray();
        $this->assertArrayHasKey('id', $createdWalletToken);
        $this->assertNotNull($createdWalletToken['id'], 'Created WalletToken must have id specified');
        $this->assertNotNull(WalletToken::find($createdWalletToken['id']), 'WalletToken with given id must be in DB');
        $this->assertModelData($walletToken, $createdWalletToken);
    }

    /**
     * @test read
     */
    public function test_read_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();

        $dbWalletToken = $this->walletTokenRepo->find($walletToken->id);

        $dbWalletToken = $dbWalletToken->toArray();
        $this->assertModelData($walletToken->toArray(), $dbWalletToken);
    }

    /**
     * @test update
     */
    public function test_update_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();
        $fakeWalletToken = factory(WalletToken::class)->make()->toArray();

        $updatedWalletToken = $this->walletTokenRepo->update($fakeWalletToken, $walletToken->id);

        $this->assertModelData($fakeWalletToken, $updatedWalletToken->toArray());
        $dbWalletToken = $this->walletTokenRepo->find($walletToken->id);
        $this->assertModelData($fakeWalletToken, $dbWalletToken->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();

        $resp = $this->walletTokenRepo->delete($walletToken->id);

        $this->assertTrue($resp);
        $this->assertNull(WalletToken::find($walletToken->id), 'WalletToken should not exist in DB');
    }
}
