<?php namespace Tests\Repositories;

use App\Models\EvaluateSeller;
use App\Repositories\EvaluateSellerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class EvaluateSellerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var EvaluateSellerRepository
     */
    protected $evaluateSellerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->evaluateSellerRepo = \App::make(EvaluateSellerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->make()->toArray();

        $createdEvaluateSeller = $this->evaluateSellerRepo->create($evaluateSeller);

        $createdEvaluateSeller = $createdEvaluateSeller->toArray();
        $this->assertArrayHasKey('id', $createdEvaluateSeller);
        $this->assertNotNull($createdEvaluateSeller['id'], 'Created EvaluateSeller must have id specified');
        $this->assertNotNull(EvaluateSeller::find($createdEvaluateSeller['id']), 'EvaluateSeller with given id must be in DB');
        $this->assertModelData($evaluateSeller, $createdEvaluateSeller);
    }

    /**
     * @test read
     */
    public function test_read_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();

        $dbEvaluateSeller = $this->evaluateSellerRepo->find($evaluateSeller->id);

        $dbEvaluateSeller = $dbEvaluateSeller->toArray();
        $this->assertModelData($evaluateSeller->toArray(), $dbEvaluateSeller);
    }

    /**
     * @test update
     */
    public function test_update_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();
        $fakeEvaluateSeller = factory(EvaluateSeller::class)->make()->toArray();

        $updatedEvaluateSeller = $this->evaluateSellerRepo->update($fakeEvaluateSeller, $evaluateSeller->id);

        $this->assertModelData($fakeEvaluateSeller, $updatedEvaluateSeller->toArray());
        $dbEvaluateSeller = $this->evaluateSellerRepo->find($evaluateSeller->id);
        $this->assertModelData($fakeEvaluateSeller, $dbEvaluateSeller->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();

        $resp = $this->evaluateSellerRepo->delete($evaluateSeller->id);

        $this->assertTrue($resp);
        $this->assertNull(EvaluateSeller::find($evaluateSeller->id), 'EvaluateSeller should not exist in DB');
    }
}
