<?php namespace Tests\Repositories;

use App\Models\FavoriteProduct;
use App\Repositories\FavoriteProductRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FavoriteProductRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FavoriteProductRepository
     */
    protected $favoriteProductRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->favoriteProductRepo = \App::make(FavoriteProductRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->make()->toArray();

        $createdFavoriteProduct = $this->favoriteProductRepo->create($favoriteProduct);

        $createdFavoriteProduct = $createdFavoriteProduct->toArray();
        $this->assertArrayHasKey('id', $createdFavoriteProduct);
        $this->assertNotNull($createdFavoriteProduct['id'], 'Created FavoriteProduct must have id specified');
        $this->assertNotNull(FavoriteProduct::find($createdFavoriteProduct['id']), 'FavoriteProduct with given id must be in DB');
        $this->assertModelData($favoriteProduct, $createdFavoriteProduct);
    }

    /**
     * @test read
     */
    public function test_read_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();

        $dbFavoriteProduct = $this->favoriteProductRepo->find($favoriteProduct->id);

        $dbFavoriteProduct = $dbFavoriteProduct->toArray();
        $this->assertModelData($favoriteProduct->toArray(), $dbFavoriteProduct);
    }

    /**
     * @test update
     */
    public function test_update_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();
        $fakeFavoriteProduct = factory(FavoriteProduct::class)->make()->toArray();

        $updatedFavoriteProduct = $this->favoriteProductRepo->update($fakeFavoriteProduct, $favoriteProduct->id);

        $this->assertModelData($fakeFavoriteProduct, $updatedFavoriteProduct->toArray());
        $dbFavoriteProduct = $this->favoriteProductRepo->find($favoriteProduct->id);
        $this->assertModelData($fakeFavoriteProduct, $dbFavoriteProduct->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();

        $resp = $this->favoriteProductRepo->delete($favoriteProduct->id);

        $this->assertTrue($resp);
        $this->assertNull(FavoriteProduct::find($favoriteProduct->id), 'FavoriteProduct should not exist in DB');
    }
}
