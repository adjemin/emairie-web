<?php namespace Tests\Repositories;

use App\Models\Follow;
use App\Repositories\FollowRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FollowRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FollowRepository
     */
    protected $followRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->followRepo = \App::make(FollowRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_follow()
    {
        $follow = factory(Follow::class)->make()->toArray();

        $createdFollow = $this->followRepo->create($follow);

        $createdFollow = $createdFollow->toArray();
        $this->assertArrayHasKey('id', $createdFollow);
        $this->assertNotNull($createdFollow['id'], 'Created Follow must have id specified');
        $this->assertNotNull(Follow::find($createdFollow['id']), 'Follow with given id must be in DB');
        $this->assertModelData($follow, $createdFollow);
    }

    /**
     * @test read
     */
    public function test_read_follow()
    {
        $follow = factory(Follow::class)->create();

        $dbFollow = $this->followRepo->find($follow->id);

        $dbFollow = $dbFollow->toArray();
        $this->assertModelData($follow->toArray(), $dbFollow);
    }

    /**
     * @test update
     */
    public function test_update_follow()
    {
        $follow = factory(Follow::class)->create();
        $fakeFollow = factory(Follow::class)->make()->toArray();

        $updatedFollow = $this->followRepo->update($fakeFollow, $follow->id);

        $this->assertModelData($fakeFollow, $updatedFollow->toArray());
        $dbFollow = $this->followRepo->find($follow->id);
        $this->assertModelData($fakeFollow, $dbFollow->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_follow()
    {
        $follow = factory(Follow::class)->create();

        $resp = $this->followRepo->delete($follow->id);

        $this->assertTrue($resp);
        $this->assertNull(Follow::find($follow->id), 'Follow should not exist in DB');
    }
}
