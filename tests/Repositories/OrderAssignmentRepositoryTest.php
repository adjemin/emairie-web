<?php namespace Tests\Repositories;

use App\Models\OrderAssignment;
use App\Repositories\OrderAssignmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderAssignmentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderAssignmentRepository
     */
    protected $orderAssignmentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderAssignmentRepo = \App::make(OrderAssignmentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->make()->toArray();

        $createdOrderAssignment = $this->orderAssignmentRepo->create($orderAssignment);

        $createdOrderAssignment = $createdOrderAssignment->toArray();
        $this->assertArrayHasKey('id', $createdOrderAssignment);
        $this->assertNotNull($createdOrderAssignment['id'], 'Created OrderAssignment must have id specified');
        $this->assertNotNull(OrderAssignment::find($createdOrderAssignment['id']), 'OrderAssignment with given id must be in DB');
        $this->assertModelData($orderAssignment, $createdOrderAssignment);
    }

    /**
     * @test read
     */
    public function test_read_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();

        $dbOrderAssignment = $this->orderAssignmentRepo->find($orderAssignment->id);

        $dbOrderAssignment = $dbOrderAssignment->toArray();
        $this->assertModelData($orderAssignment->toArray(), $dbOrderAssignment);
    }

    /**
     * @test update
     */
    public function test_update_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();
        $fakeOrderAssignment = factory(OrderAssignment::class)->make()->toArray();

        $updatedOrderAssignment = $this->orderAssignmentRepo->update($fakeOrderAssignment, $orderAssignment->id);

        $this->assertModelData($fakeOrderAssignment, $updatedOrderAssignment->toArray());
        $dbOrderAssignment = $this->orderAssignmentRepo->find($orderAssignment->id);
        $this->assertModelData($fakeOrderAssignment, $dbOrderAssignment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();

        $resp = $this->orderAssignmentRepo->delete($orderAssignment->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderAssignment::find($orderAssignment->id), 'OrderAssignment should not exist in DB');
    }
}
