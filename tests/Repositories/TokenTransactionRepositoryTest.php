<?php namespace Tests\Repositories;

use App\Models\TokenTransaction;
use App\Repositories\TokenTransactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TokenTransactionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TokenTransactionRepository
     */
    protected $tokenTransactionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tokenTransactionRepo = \App::make(TokenTransactionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->make()->toArray();

        $createdTokenTransaction = $this->tokenTransactionRepo->create($tokenTransaction);

        $createdTokenTransaction = $createdTokenTransaction->toArray();
        $this->assertArrayHasKey('id', $createdTokenTransaction);
        $this->assertNotNull($createdTokenTransaction['id'], 'Created TokenTransaction must have id specified');
        $this->assertNotNull(TokenTransaction::find($createdTokenTransaction['id']), 'TokenTransaction with given id must be in DB');
        $this->assertModelData($tokenTransaction, $createdTokenTransaction);
    }

    /**
     * @test read
     */
    public function test_read_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();

        $dbTokenTransaction = $this->tokenTransactionRepo->find($tokenTransaction->id);

        $dbTokenTransaction = $dbTokenTransaction->toArray();
        $this->assertModelData($tokenTransaction->toArray(), $dbTokenTransaction);
    }

    /**
     * @test update
     */
    public function test_update_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();
        $fakeTokenTransaction = factory(TokenTransaction::class)->make()->toArray();

        $updatedTokenTransaction = $this->tokenTransactionRepo->update($fakeTokenTransaction, $tokenTransaction->id);

        $this->assertModelData($fakeTokenTransaction, $updatedTokenTransaction->toArray());
        $dbTokenTransaction = $this->tokenTransactionRepo->find($tokenTransaction->id);
        $this->assertModelData($fakeTokenTransaction, $dbTokenTransaction->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();

        $resp = $this->tokenTransactionRepo->delete($tokenTransaction->id);

        $this->assertTrue($resp);
        $this->assertNull(TokenTransaction::find($tokenTransaction->id), 'TokenTransaction should not exist in DB');
    }
}
