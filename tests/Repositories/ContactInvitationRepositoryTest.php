<?php namespace Tests\Repositories;

use App\Models\ContactInvitation;
use App\Repositories\ContactInvitationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ContactInvitationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactInvitationRepository
     */
    protected $contactInvitationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->contactInvitationRepo = \App::make(ContactInvitationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->make()->toArray();

        $createdContactInvitation = $this->contactInvitationRepo->create($contactInvitation);

        $createdContactInvitation = $createdContactInvitation->toArray();
        $this->assertArrayHasKey('id', $createdContactInvitation);
        $this->assertNotNull($createdContactInvitation['id'], 'Created ContactInvitation must have id specified');
        $this->assertNotNull(ContactInvitation::find($createdContactInvitation['id']), 'ContactInvitation with given id must be in DB');
        $this->assertModelData($contactInvitation, $createdContactInvitation);
    }

    /**
     * @test read
     */
    public function test_read_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();

        $dbContactInvitation = $this->contactInvitationRepo->find($contactInvitation->id);

        $dbContactInvitation = $dbContactInvitation->toArray();
        $this->assertModelData($contactInvitation->toArray(), $dbContactInvitation);
    }

    /**
     * @test update
     */
    public function test_update_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();
        $fakeContactInvitation = factory(ContactInvitation::class)->make()->toArray();

        $updatedContactInvitation = $this->contactInvitationRepo->update($fakeContactInvitation, $contactInvitation->id);

        $this->assertModelData($fakeContactInvitation, $updatedContactInvitation->toArray());
        $dbContactInvitation = $this->contactInvitationRepo->find($contactInvitation->id);
        $this->assertModelData($fakeContactInvitation, $dbContactInvitation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();

        $resp = $this->contactInvitationRepo->delete($contactInvitation->id);

        $this->assertTrue($resp);
        $this->assertNull(ContactInvitation::find($contactInvitation->id), 'ContactInvitation should not exist in DB');
    }
}
