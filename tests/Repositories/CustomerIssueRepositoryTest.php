<?php namespace Tests\Repositories;

use App\Models\CustomerIssue;
use App\Repositories\CustomerIssueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerIssueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerIssueRepository
     */
    protected $customerIssueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerIssueRepo = \App::make(CustomerIssueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->make()->toArray();

        $createdCustomerIssue = $this->customerIssueRepo->create($customerIssue);

        $createdCustomerIssue = $createdCustomerIssue->toArray();
        $this->assertArrayHasKey('id', $createdCustomerIssue);
        $this->assertNotNull($createdCustomerIssue['id'], 'Created CustomerIssue must have id specified');
        $this->assertNotNull(CustomerIssue::find($createdCustomerIssue['id']), 'CustomerIssue with given id must be in DB');
        $this->assertModelData($customerIssue, $createdCustomerIssue);
    }

    /**
     * @test read
     */
    public function test_read_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();

        $dbCustomerIssue = $this->customerIssueRepo->find($customerIssue->id);

        $dbCustomerIssue = $dbCustomerIssue->toArray();
        $this->assertModelData($customerIssue->toArray(), $dbCustomerIssue);
    }

    /**
     * @test update
     */
    public function test_update_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();
        $fakeCustomerIssue = factory(CustomerIssue::class)->make()->toArray();

        $updatedCustomerIssue = $this->customerIssueRepo->update($fakeCustomerIssue, $customerIssue->id);

        $this->assertModelData($fakeCustomerIssue, $updatedCustomerIssue->toArray());
        $dbCustomerIssue = $this->customerIssueRepo->find($customerIssue->id);
        $this->assertModelData($fakeCustomerIssue, $dbCustomerIssue->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();

        $resp = $this->customerIssueRepo->delete($customerIssue->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerIssue::find($customerIssue->id), 'CustomerIssue should not exist in DB');
    }
}
