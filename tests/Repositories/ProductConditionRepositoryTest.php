<?php namespace Tests\Repositories;

use App\Models\ProductCondition;
use App\Repositories\ProductConditionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductConditionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductConditionRepository
     */
    protected $productConditionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productConditionRepo = \App::make(ProductConditionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->make()->toArray();

        $createdProductCondition = $this->productConditionRepo->create($productCondition);

        $createdProductCondition = $createdProductCondition->toArray();
        $this->assertArrayHasKey('id', $createdProductCondition);
        $this->assertNotNull($createdProductCondition['id'], 'Created ProductCondition must have id specified');
        $this->assertNotNull(ProductCondition::find($createdProductCondition['id']), 'ProductCondition with given id must be in DB');
        $this->assertModelData($productCondition, $createdProductCondition);
    }

    /**
     * @test read
     */
    public function test_read_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();

        $dbProductCondition = $this->productConditionRepo->find($productCondition->id);

        $dbProductCondition = $dbProductCondition->toArray();
        $this->assertModelData($productCondition->toArray(), $dbProductCondition);
    }

    /**
     * @test update
     */
    public function test_update_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();
        $fakeProductCondition = factory(ProductCondition::class)->make()->toArray();

        $updatedProductCondition = $this->productConditionRepo->update($fakeProductCondition, $productCondition->id);

        $this->assertModelData($fakeProductCondition, $updatedProductCondition->toArray());
        $dbProductCondition = $this->productConditionRepo->find($productCondition->id);
        $this->assertModelData($fakeProductCondition, $dbProductCondition->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();

        $resp = $this->productConditionRepo->delete($productCondition->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductCondition::find($productCondition->id), 'ProductCondition should not exist in DB');
    }
}
