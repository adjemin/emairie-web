<?php namespace Tests\Repositories;

use App\Models\SponsoringType;
use App\Repositories\SponsoringTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SponsoringTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SponsoringTypeRepository
     */
    protected $sponsoringTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sponsoringTypeRepo = \App::make(SponsoringTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->make()->toArray();

        $createdSponsoringType = $this->sponsoringTypeRepo->create($sponsoringType);

        $createdSponsoringType = $createdSponsoringType->toArray();
        $this->assertArrayHasKey('id', $createdSponsoringType);
        $this->assertNotNull($createdSponsoringType['id'], 'Created SponsoringType must have id specified');
        $this->assertNotNull(SponsoringType::find($createdSponsoringType['id']), 'SponsoringType with given id must be in DB');
        $this->assertModelData($sponsoringType, $createdSponsoringType);
    }

    /**
     * @test read
     */
    public function test_read_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();

        $dbSponsoringType = $this->sponsoringTypeRepo->find($sponsoringType->id);

        $dbSponsoringType = $dbSponsoringType->toArray();
        $this->assertModelData($sponsoringType->toArray(), $dbSponsoringType);
    }

    /**
     * @test update
     */
    public function test_update_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();
        $fakeSponsoringType = factory(SponsoringType::class)->make()->toArray();

        $updatedSponsoringType = $this->sponsoringTypeRepo->update($fakeSponsoringType, $sponsoringType->id);

        $this->assertModelData($fakeSponsoringType, $updatedSponsoringType->toArray());
        $dbSponsoringType = $this->sponsoringTypeRepo->find($sponsoringType->id);
        $this->assertModelData($fakeSponsoringType, $dbSponsoringType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();

        $resp = $this->sponsoringTypeRepo->delete($sponsoringType->id);

        $this->assertTrue($resp);
        $this->assertNull(SponsoringType::find($sponsoringType->id), 'SponsoringType should not exist in DB');
    }
}
