<?php namespace Tests\Repositories;

use App\Models\CustomerDevice;
use App\Repositories\CustomerDeviceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerDeviceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerDeviceRepository
     */
    protected $customerDeviceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerDeviceRepo = \App::make(CustomerDeviceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->make()->toArray();

        $createdCustomerDevice = $this->customerDeviceRepo->create($customerDevice);

        $createdCustomerDevice = $createdCustomerDevice->toArray();
        $this->assertArrayHasKey('id', $createdCustomerDevice);
        $this->assertNotNull($createdCustomerDevice['id'], 'Created CustomerDevice must have id specified');
        $this->assertNotNull(CustomerDevice::find($createdCustomerDevice['id']), 'CustomerDevice with given id must be in DB');
        $this->assertModelData($customerDevice, $createdCustomerDevice);
    }

    /**
     * @test read
     */
    public function test_read_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();

        $dbCustomerDevice = $this->customerDeviceRepo->find($customerDevice->id);

        $dbCustomerDevice = $dbCustomerDevice->toArray();
        $this->assertModelData($customerDevice->toArray(), $dbCustomerDevice);
    }

    /**
     * @test update
     */
    public function test_update_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();
        $fakeCustomerDevice = factory(CustomerDevice::class)->make()->toArray();

        $updatedCustomerDevice = $this->customerDeviceRepo->update($fakeCustomerDevice, $customerDevice->id);

        $this->assertModelData($fakeCustomerDevice, $updatedCustomerDevice->toArray());
        $dbCustomerDevice = $this->customerDeviceRepo->find($customerDevice->id);
        $this->assertModelData($fakeCustomerDevice, $dbCustomerDevice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();

        $resp = $this->customerDeviceRepo->delete($customerDevice->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerDevice::find($customerDevice->id), 'CustomerDevice should not exist in DB');
    }
}
