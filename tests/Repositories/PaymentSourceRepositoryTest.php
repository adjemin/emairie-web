<?php namespace Tests\Repositories;

use App\Models\PaymentSource;
use App\Repositories\PaymentSourceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PaymentSourceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PaymentSourceRepository
     */
    protected $paymentSourceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->paymentSourceRepo = \App::make(PaymentSourceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->make()->toArray();

        $createdPaymentSource = $this->paymentSourceRepo->create($paymentSource);

        $createdPaymentSource = $createdPaymentSource->toArray();
        $this->assertArrayHasKey('id', $createdPaymentSource);
        $this->assertNotNull($createdPaymentSource['id'], 'Created PaymentSource must have id specified');
        $this->assertNotNull(PaymentSource::find($createdPaymentSource['id']), 'PaymentSource with given id must be in DB');
        $this->assertModelData($paymentSource, $createdPaymentSource);
    }

    /**
     * @test read
     */
    public function test_read_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();

        $dbPaymentSource = $this->paymentSourceRepo->find($paymentSource->id);

        $dbPaymentSource = $dbPaymentSource->toArray();
        $this->assertModelData($paymentSource->toArray(), $dbPaymentSource);
    }

    /**
     * @test update
     */
    public function test_update_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();
        $fakePaymentSource = factory(PaymentSource::class)->make()->toArray();

        $updatedPaymentSource = $this->paymentSourceRepo->update($fakePaymentSource, $paymentSource->id);

        $this->assertModelData($fakePaymentSource, $updatedPaymentSource->toArray());
        $dbPaymentSource = $this->paymentSourceRepo->find($paymentSource->id);
        $this->assertModelData($fakePaymentSource, $dbPaymentSource->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();

        $resp = $this->paymentSourceRepo->delete($paymentSource->id);

        $this->assertTrue($resp);
        $this->assertNull(PaymentSource::find($paymentSource->id), 'PaymentSource should not exist in DB');
    }
}
