<?php namespace Tests\Repositories;

use App\Models\ProductAudience;
use App\Repositories\ProductAudienceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductAudienceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductAudienceRepository
     */
    protected $productAudienceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productAudienceRepo = \App::make(ProductAudienceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->make()->toArray();

        $createdProductAudience = $this->productAudienceRepo->create($productAudience);

        $createdProductAudience = $createdProductAudience->toArray();
        $this->assertArrayHasKey('id', $createdProductAudience);
        $this->assertNotNull($createdProductAudience['id'], 'Created ProductAudience must have id specified');
        $this->assertNotNull(ProductAudience::find($createdProductAudience['id']), 'ProductAudience with given id must be in DB');
        $this->assertModelData($productAudience, $createdProductAudience);
    }

    /**
     * @test read
     */
    public function test_read_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();

        $dbProductAudience = $this->productAudienceRepo->find($productAudience->id);

        $dbProductAudience = $dbProductAudience->toArray();
        $this->assertModelData($productAudience->toArray(), $dbProductAudience);
    }

    /**
     * @test update
     */
    public function test_update_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();
        $fakeProductAudience = factory(ProductAudience::class)->make()->toArray();

        $updatedProductAudience = $this->productAudienceRepo->update($fakeProductAudience, $productAudience->id);

        $this->assertModelData($fakeProductAudience, $updatedProductAudience->toArray());
        $dbProductAudience = $this->productAudienceRepo->find($productAudience->id);
        $this->assertModelData($fakeProductAudience, $dbProductAudience->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();

        $resp = $this->productAudienceRepo->delete($productAudience->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductAudience::find($productAudience->id), 'ProductAudience should not exist in DB');
    }
}
