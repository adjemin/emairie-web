<?php namespace Tests\Repositories;

use App\Models\Sponsoring;
use App\Repositories\SponsoringRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SponsoringRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SponsoringRepository
     */
    protected $sponsoringRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->sponsoringRepo = \App::make(SponsoringRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->make()->toArray();

        $createdSponsoring = $this->sponsoringRepo->create($sponsoring);

        $createdSponsoring = $createdSponsoring->toArray();
        $this->assertArrayHasKey('id', $createdSponsoring);
        $this->assertNotNull($createdSponsoring['id'], 'Created Sponsoring must have id specified');
        $this->assertNotNull(Sponsoring::find($createdSponsoring['id']), 'Sponsoring with given id must be in DB');
        $this->assertModelData($sponsoring, $createdSponsoring);
    }

    /**
     * @test read
     */
    public function test_read_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();

        $dbSponsoring = $this->sponsoringRepo->find($sponsoring->id);

        $dbSponsoring = $dbSponsoring->toArray();
        $this->assertModelData($sponsoring->toArray(), $dbSponsoring);
    }

    /**
     * @test update
     */
    public function test_update_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();
        $fakeSponsoring = factory(Sponsoring::class)->make()->toArray();

        $updatedSponsoring = $this->sponsoringRepo->update($fakeSponsoring, $sponsoring->id);

        $this->assertModelData($fakeSponsoring, $updatedSponsoring->toArray());
        $dbSponsoring = $this->sponsoringRepo->find($sponsoring->id);
        $this->assertModelData($fakeSponsoring, $dbSponsoring->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();

        $resp = $this->sponsoringRepo->delete($sponsoring->id);

        $this->assertTrue($resp);
        $this->assertNull(Sponsoring::find($sponsoring->id), 'Sponsoring should not exist in DB');
    }
}
