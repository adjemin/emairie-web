<?php namespace Tests\Repositories;

use App\Models\UserDefaultDeliveryService;
use App\Repositories\UserDefaultDeliveryServiceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class UserDefaultDeliveryServiceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserDefaultDeliveryServiceRepository
     */
    protected $userDefaultDeliveryServiceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->userDefaultDeliveryServiceRepo = \App::make(UserDefaultDeliveryServiceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->make()->toArray();

        $createdUserDefaultDeliveryService = $this->userDefaultDeliveryServiceRepo->create($userDefaultDeliveryService);

        $createdUserDefaultDeliveryService = $createdUserDefaultDeliveryService->toArray();
        $this->assertArrayHasKey('id', $createdUserDefaultDeliveryService);
        $this->assertNotNull($createdUserDefaultDeliveryService['id'], 'Created UserDefaultDeliveryService must have id specified');
        $this->assertNotNull(UserDefaultDeliveryService::find($createdUserDefaultDeliveryService['id']), 'UserDefaultDeliveryService with given id must be in DB');
        $this->assertModelData($userDefaultDeliveryService, $createdUserDefaultDeliveryService);
    }

    /**
     * @test read
     */
    public function test_read_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();

        $dbUserDefaultDeliveryService = $this->userDefaultDeliveryServiceRepo->find($userDefaultDeliveryService->id);

        $dbUserDefaultDeliveryService = $dbUserDefaultDeliveryService->toArray();
        $this->assertModelData($userDefaultDeliveryService->toArray(), $dbUserDefaultDeliveryService);
    }

    /**
     * @test update
     */
    public function test_update_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();
        $fakeUserDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->make()->toArray();

        $updatedUserDefaultDeliveryService = $this->userDefaultDeliveryServiceRepo->update($fakeUserDefaultDeliveryService, $userDefaultDeliveryService->id);

        $this->assertModelData($fakeUserDefaultDeliveryService, $updatedUserDefaultDeliveryService->toArray());
        $dbUserDefaultDeliveryService = $this->userDefaultDeliveryServiceRepo->find($userDefaultDeliveryService->id);
        $this->assertModelData($fakeUserDefaultDeliveryService, $dbUserDefaultDeliveryService->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();

        $resp = $this->userDefaultDeliveryServiceRepo->delete($userDefaultDeliveryService->id);

        $this->assertTrue($resp);
        $this->assertNull(UserDefaultDeliveryService::find($userDefaultDeliveryService->id), 'UserDefaultDeliveryService should not exist in DB');
    }
}
