<?php namespace Tests\Repositories;

use App\Models\CustomerSession;
use App\Repositories\CustomerSessionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CustomerSessionRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomerSessionRepository
     */
    protected $customerSessionRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->customerSessionRepo = \App::make(CustomerSessionRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->make()->toArray();

        $createdCustomerSession = $this->customerSessionRepo->create($customerSession);

        $createdCustomerSession = $createdCustomerSession->toArray();
        $this->assertArrayHasKey('id', $createdCustomerSession);
        $this->assertNotNull($createdCustomerSession['id'], 'Created CustomerSession must have id specified');
        $this->assertNotNull(CustomerSession::find($createdCustomerSession['id']), 'CustomerSession with given id must be in DB');
        $this->assertModelData($customerSession, $createdCustomerSession);
    }

    /**
     * @test read
     */
    public function test_read_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();

        $dbCustomerSession = $this->customerSessionRepo->find($customerSession->id);

        $dbCustomerSession = $dbCustomerSession->toArray();
        $this->assertModelData($customerSession->toArray(), $dbCustomerSession);
    }

    /**
     * @test update
     */
    public function test_update_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();
        $fakeCustomerSession = factory(CustomerSession::class)->make()->toArray();

        $updatedCustomerSession = $this->customerSessionRepo->update($fakeCustomerSession, $customerSession->id);

        $this->assertModelData($fakeCustomerSession, $updatedCustomerSession->toArray());
        $dbCustomerSession = $this->customerSessionRepo->find($customerSession->id);
        $this->assertModelData($fakeCustomerSession, $dbCustomerSession->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();

        $resp = $this->customerSessionRepo->delete($customerSession->id);

        $this->assertTrue($resp);
        $this->assertNull(CustomerSession::find($customerSession->id), 'CustomerSession should not exist in DB');
    }
}
