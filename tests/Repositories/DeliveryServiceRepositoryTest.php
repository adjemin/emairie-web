<?php namespace Tests\Repositories;

use App\Models\DeliveryService;
use App\Repositories\DeliveryServiceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryServiceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryServiceRepository
     */
    protected $deliveryServiceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryServiceRepo = \App::make(DeliveryServiceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->make()->toArray();

        $createdDeliveryService = $this->deliveryServiceRepo->create($deliveryService);

        $createdDeliveryService = $createdDeliveryService->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryService);
        $this->assertNotNull($createdDeliveryService['id'], 'Created DeliveryService must have id specified');
        $this->assertNotNull(DeliveryService::find($createdDeliveryService['id']), 'DeliveryService with given id must be in DB');
        $this->assertModelData($deliveryService, $createdDeliveryService);
    }

    /**
     * @test read
     */
    public function test_read_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();

        $dbDeliveryService = $this->deliveryServiceRepo->find($deliveryService->id);

        $dbDeliveryService = $dbDeliveryService->toArray();
        $this->assertModelData($deliveryService->toArray(), $dbDeliveryService);
    }

    /**
     * @test update
     */
    public function test_update_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();
        $fakeDeliveryService = factory(DeliveryService::class)->make()->toArray();

        $updatedDeliveryService = $this->deliveryServiceRepo->update($fakeDeliveryService, $deliveryService->id);

        $this->assertModelData($fakeDeliveryService, $updatedDeliveryService->toArray());
        $dbDeliveryService = $this->deliveryServiceRepo->find($deliveryService->id);
        $this->assertModelData($fakeDeliveryService, $dbDeliveryService->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();

        $resp = $this->deliveryServiceRepo->delete($deliveryService->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryService::find($deliveryService->id), 'DeliveryService should not exist in DB');
    }
}
