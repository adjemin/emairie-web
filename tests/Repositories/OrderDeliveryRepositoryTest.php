<?php namespace Tests\Repositories;

use App\Models\OrderDelivery;
use App\Repositories\OrderDeliveryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderDeliveryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderDeliveryRepository
     */
    protected $orderDeliveryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderDeliveryRepo = \App::make(OrderDeliveryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->make()->toArray();

        $createdOrderDelivery = $this->orderDeliveryRepo->create($orderDelivery);

        $createdOrderDelivery = $createdOrderDelivery->toArray();
        $this->assertArrayHasKey('id', $createdOrderDelivery);
        $this->assertNotNull($createdOrderDelivery['id'], 'Created OrderDelivery must have id specified');
        $this->assertNotNull(OrderDelivery::find($createdOrderDelivery['id']), 'OrderDelivery with given id must be in DB');
        $this->assertModelData($orderDelivery, $createdOrderDelivery);
    }

    /**
     * @test read
     */
    public function test_read_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();

        $dbOrderDelivery = $this->orderDeliveryRepo->find($orderDelivery->id);

        $dbOrderDelivery = $dbOrderDelivery->toArray();
        $this->assertModelData($orderDelivery->toArray(), $dbOrderDelivery);
    }

    /**
     * @test update
     */
    public function test_update_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();
        $fakeOrderDelivery = factory(OrderDelivery::class)->make()->toArray();

        $updatedOrderDelivery = $this->orderDeliveryRepo->update($fakeOrderDelivery, $orderDelivery->id);

        $this->assertModelData($fakeOrderDelivery, $updatedOrderDelivery->toArray());
        $dbOrderDelivery = $this->orderDeliveryRepo->find($orderDelivery->id);
        $this->assertModelData($fakeOrderDelivery, $dbOrderDelivery->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();

        $resp = $this->orderDeliveryRepo->delete($orderDelivery->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderDelivery::find($orderDelivery->id), 'OrderDelivery should not exist in DB');
    }
}
