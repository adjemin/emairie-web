<?php namespace Tests\Repositories;

use App\Models\ProductMediaType;
use App\Repositories\ProductMediaTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductMediaTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductMediaTypeRepository
     */
    protected $productMediaTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productMediaTypeRepo = \App::make(ProductMediaTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->make()->toArray();

        $createdProductMediaType = $this->productMediaTypeRepo->create($productMediaType);

        $createdProductMediaType = $createdProductMediaType->toArray();
        $this->assertArrayHasKey('id', $createdProductMediaType);
        $this->assertNotNull($createdProductMediaType['id'], 'Created ProductMediaType must have id specified');
        $this->assertNotNull(ProductMediaType::find($createdProductMediaType['id']), 'ProductMediaType with given id must be in DB');
        $this->assertModelData($productMediaType, $createdProductMediaType);
    }

    /**
     * @test read
     */
    public function test_read_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();

        $dbProductMediaType = $this->productMediaTypeRepo->find($productMediaType->id);

        $dbProductMediaType = $dbProductMediaType->toArray();
        $this->assertModelData($productMediaType->toArray(), $dbProductMediaType);
    }

    /**
     * @test update
     */
    public function test_update_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();
        $fakeProductMediaType = factory(ProductMediaType::class)->make()->toArray();

        $updatedProductMediaType = $this->productMediaTypeRepo->update($fakeProductMediaType, $productMediaType->id);

        $this->assertModelData($fakeProductMediaType, $updatedProductMediaType->toArray());
        $dbProductMediaType = $this->productMediaTypeRepo->find($productMediaType->id);
        $this->assertModelData($fakeProductMediaType, $dbProductMediaType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();

        $resp = $this->productMediaTypeRepo->delete($productMediaType->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductMediaType::find($productMediaType->id), 'ProductMediaType should not exist in DB');
    }
}
