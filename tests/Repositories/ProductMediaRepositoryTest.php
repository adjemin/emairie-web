<?php namespace Tests\Repositories;

use App\Models\ProductMedia;
use App\Repositories\ProductMediaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ProductMediaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ProductMediaRepository
     */
    protected $productMediaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->productMediaRepo = \App::make(ProductMediaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_product_media()
    {
        $productMedia = factory(ProductMedia::class)->make()->toArray();

        $createdProductMedia = $this->productMediaRepo->create($productMedia);

        $createdProductMedia = $createdProductMedia->toArray();
        $this->assertArrayHasKey('id', $createdProductMedia);
        $this->assertNotNull($createdProductMedia['id'], 'Created ProductMedia must have id specified');
        $this->assertNotNull(ProductMedia::find($createdProductMedia['id']), 'ProductMedia with given id must be in DB');
        $this->assertModelData($productMedia, $createdProductMedia);
    }

    /**
     * @test read
     */
    public function test_read_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();

        $dbProductMedia = $this->productMediaRepo->find($productMedia->id);

        $dbProductMedia = $dbProductMedia->toArray();
        $this->assertModelData($productMedia->toArray(), $dbProductMedia);
    }

    /**
     * @test update
     */
    public function test_update_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();
        $fakeProductMedia = factory(ProductMedia::class)->make()->toArray();

        $updatedProductMedia = $this->productMediaRepo->update($fakeProductMedia, $productMedia->id);

        $this->assertModelData($fakeProductMedia, $updatedProductMedia->toArray());
        $dbProductMedia = $this->productMediaRepo->find($productMedia->id);
        $this->assertModelData($fakeProductMedia, $dbProductMedia->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();

        $resp = $this->productMediaRepo->delete($productMedia->id);

        $this->assertTrue($resp);
        $this->assertNull(ProductMedia::find($productMedia->id), 'ProductMedia should not exist in DB');
    }
}
