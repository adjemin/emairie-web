<?php namespace Tests\Repositories;

use App\Models\FlashSelling;
use App\Repositories\FlashSellingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FlashSellingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FlashSellingRepository
     */
    protected $flashSellingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->flashSellingRepo = \App::make(FlashSellingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->make()->toArray();

        $createdFlashSelling = $this->flashSellingRepo->create($flashSelling);

        $createdFlashSelling = $createdFlashSelling->toArray();
        $this->assertArrayHasKey('id', $createdFlashSelling);
        $this->assertNotNull($createdFlashSelling['id'], 'Created FlashSelling must have id specified');
        $this->assertNotNull(FlashSelling::find($createdFlashSelling['id']), 'FlashSelling with given id must be in DB');
        $this->assertModelData($flashSelling, $createdFlashSelling);
    }

    /**
     * @test read
     */
    public function test_read_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();

        $dbFlashSelling = $this->flashSellingRepo->find($flashSelling->id);

        $dbFlashSelling = $dbFlashSelling->toArray();
        $this->assertModelData($flashSelling->toArray(), $dbFlashSelling);
    }

    /**
     * @test update
     */
    public function test_update_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();
        $fakeFlashSelling = factory(FlashSelling::class)->make()->toArray();

        $updatedFlashSelling = $this->flashSellingRepo->update($fakeFlashSelling, $flashSelling->id);

        $this->assertModelData($fakeFlashSelling, $updatedFlashSelling->toArray());
        $dbFlashSelling = $this->flashSellingRepo->find($flashSelling->id);
        $this->assertModelData($fakeFlashSelling, $dbFlashSelling->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();

        $resp = $this->flashSellingRepo->delete($flashSelling->id);

        $this->assertTrue($resp);
        $this->assertNull(FlashSelling::find($flashSelling->id), 'FlashSelling should not exist in DB');
    }
}
