<?php namespace Tests\Repositories;

use App\Models\OrderHistory;
use App\Repositories\OrderHistoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OrderHistoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderHistoryRepository
     */
    protected $orderHistoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->orderHistoryRepo = \App::make(OrderHistoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->make()->toArray();

        $createdOrderHistory = $this->orderHistoryRepo->create($orderHistory);

        $createdOrderHistory = $createdOrderHistory->toArray();
        $this->assertArrayHasKey('id', $createdOrderHistory);
        $this->assertNotNull($createdOrderHistory['id'], 'Created OrderHistory must have id specified');
        $this->assertNotNull(OrderHistory::find($createdOrderHistory['id']), 'OrderHistory with given id must be in DB');
        $this->assertModelData($orderHistory, $createdOrderHistory);
    }

    /**
     * @test read
     */
    public function test_read_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();

        $dbOrderHistory = $this->orderHistoryRepo->find($orderHistory->id);

        $dbOrderHistory = $dbOrderHistory->toArray();
        $this->assertModelData($orderHistory->toArray(), $dbOrderHistory);
    }

    /**
     * @test update
     */
    public function test_update_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();
        $fakeOrderHistory = factory(OrderHistory::class)->make()->toArray();

        $updatedOrderHistory = $this->orderHistoryRepo->update($fakeOrderHistory, $orderHistory->id);

        $this->assertModelData($fakeOrderHistory, $updatedOrderHistory->toArray());
        $dbOrderHistory = $this->orderHistoryRepo->find($orderHistory->id);
        $this->assertModelData($fakeOrderHistory, $dbOrderHistory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();

        $resp = $this->orderHistoryRepo->delete($orderHistory->id);

        $this->assertTrue($resp);
        $this->assertNull(OrderHistory::find($orderHistory->id), 'OrderHistory should not exist in DB');
    }
}
