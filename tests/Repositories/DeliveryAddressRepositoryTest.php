<?php namespace Tests\Repositories;

use App\Models\DeliveryAddress;
use App\Repositories\DeliveryAddressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DeliveryAddressRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DeliveryAddressRepository
     */
    protected $deliveryAddressRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->deliveryAddressRepo = \App::make(DeliveryAddressRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->make()->toArray();

        $createdDeliveryAddress = $this->deliveryAddressRepo->create($deliveryAddress);

        $createdDeliveryAddress = $createdDeliveryAddress->toArray();
        $this->assertArrayHasKey('id', $createdDeliveryAddress);
        $this->assertNotNull($createdDeliveryAddress['id'], 'Created DeliveryAddress must have id specified');
        $this->assertNotNull(DeliveryAddress::find($createdDeliveryAddress['id']), 'DeliveryAddress with given id must be in DB');
        $this->assertModelData($deliveryAddress, $createdDeliveryAddress);
    }

    /**
     * @test read
     */
    public function test_read_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();

        $dbDeliveryAddress = $this->deliveryAddressRepo->find($deliveryAddress->id);

        $dbDeliveryAddress = $dbDeliveryAddress->toArray();
        $this->assertModelData($deliveryAddress->toArray(), $dbDeliveryAddress);
    }

    /**
     * @test update
     */
    public function test_update_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();
        $fakeDeliveryAddress = factory(DeliveryAddress::class)->make()->toArray();

        $updatedDeliveryAddress = $this->deliveryAddressRepo->update($fakeDeliveryAddress, $deliveryAddress->id);

        $this->assertModelData($fakeDeliveryAddress, $updatedDeliveryAddress->toArray());
        $dbDeliveryAddress = $this->deliveryAddressRepo->find($deliveryAddress->id);
        $this->assertModelData($fakeDeliveryAddress, $dbDeliveryAddress->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();

        $resp = $this->deliveryAddressRepo->delete($deliveryAddress->id);

        $this->assertTrue($resp);
        $this->assertNull(DeliveryAddress::find($deliveryAddress->id), 'DeliveryAddress should not exist in DB');
    }
}
