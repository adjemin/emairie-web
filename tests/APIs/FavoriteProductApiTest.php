<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FavoriteProduct;

class FavoriteProductApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/favorite_products', $favoriteProduct
        );

        $this->assertApiResponse($favoriteProduct);
    }

    /**
     * @test
     */
    public function test_read_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/favorite_products/'.$favoriteProduct->id
        );

        $this->assertApiResponse($favoriteProduct->toArray());
    }

    /**
     * @test
     */
    public function test_update_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();
        $editedFavoriteProduct = factory(FavoriteProduct::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/favorite_products/'.$favoriteProduct->id,
            $editedFavoriteProduct
        );

        $this->assertApiResponse($editedFavoriteProduct);
    }

    /**
     * @test
     */
    public function test_delete_favorite_product()
    {
        $favoriteProduct = factory(FavoriteProduct::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/favorite_products/'.$favoriteProduct->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/favorite_products/'.$favoriteProduct->id
        );

        $this->response->assertStatus(404);
    }
}
