<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderDelivery;

class OrderDeliveryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_deliveries', $orderDelivery
        );

        $this->assertApiResponse($orderDelivery);
    }

    /**
     * @test
     */
    public function test_read_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_deliveries/'.$orderDelivery->id
        );

        $this->assertApiResponse($orderDelivery->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();
        $editedOrderDelivery = factory(OrderDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_deliveries/'.$orderDelivery->id,
            $editedOrderDelivery
        );

        $this->assertApiResponse($editedOrderDelivery);
    }

    /**
     * @test
     */
    public function test_delete_order_delivery()
    {
        $orderDelivery = factory(OrderDelivery::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_deliveries/'.$orderDelivery->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_deliveries/'.$orderDelivery->id
        );

        $this->response->assertStatus(404);
    }
}
