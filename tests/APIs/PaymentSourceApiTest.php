<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PaymentSource;

class PaymentSourceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/payment_sources', $paymentSource
        );

        $this->assertApiResponse($paymentSource);
    }

    /**
     * @test
     */
    public function test_read_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/payment_sources/'.$paymentSource->id
        );

        $this->assertApiResponse($paymentSource->toArray());
    }

    /**
     * @test
     */
    public function test_update_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();
        $editedPaymentSource = factory(PaymentSource::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/payment_sources/'.$paymentSource->id,
            $editedPaymentSource
        );

        $this->assertApiResponse($editedPaymentSource);
    }

    /**
     * @test
     */
    public function test_delete_payment_source()
    {
        $paymentSource = factory(PaymentSource::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/payment_sources/'.$paymentSource->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/payment_sources/'.$paymentSource->id
        );

        $this->response->assertStatus(404);
    }
}
