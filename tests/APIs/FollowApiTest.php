<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Follow;

class FollowApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_follow()
    {
        $follow = factory(Follow::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/follows', $follow
        );

        $this->assertApiResponse($follow);
    }

    /**
     * @test
     */
    public function test_read_follow()
    {
        $follow = factory(Follow::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/follows/'.$follow->id
        );

        $this->assertApiResponse($follow->toArray());
    }

    /**
     * @test
     */
    public function test_update_follow()
    {
        $follow = factory(Follow::class)->create();
        $editedFollow = factory(Follow::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/follows/'.$follow->id,
            $editedFollow
        );

        $this->assertApiResponse($editedFollow);
    }

    /**
     * @test
     */
    public function test_delete_follow()
    {
        $follow = factory(Follow::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/follows/'.$follow->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/follows/'.$follow->id
        );

        $this->response->assertStatus(404);
    }
}
