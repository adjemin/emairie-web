<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerIssue;

class CustomerIssueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_issues', $customerIssue
        );

        $this->assertApiResponse($customerIssue);
    }

    /**
     * @test
     */
    public function test_read_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_issues/'.$customerIssue->id
        );

        $this->assertApiResponse($customerIssue->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();
        $editedCustomerIssue = factory(CustomerIssue::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_issues/'.$customerIssue->id,
            $editedCustomerIssue
        );

        $this->assertApiResponse($editedCustomerIssue);
    }

    /**
     * @test
     */
    public function test_delete_customer_issue()
    {
        $customerIssue = factory(CustomerIssue::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_issues/'.$customerIssue->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_issues/'.$customerIssue->id
        );

        $this->response->assertStatus(404);
    }
}
