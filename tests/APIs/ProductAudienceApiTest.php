<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductAudience;

class ProductAudienceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_audiences', $productAudience
        );

        $this->assertApiResponse($productAudience);
    }

    /**
     * @test
     */
    public function test_read_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_audiences/'.$productAudience->id
        );

        $this->assertApiResponse($productAudience->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();
        $editedProductAudience = factory(ProductAudience::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_audiences/'.$productAudience->id,
            $editedProductAudience
        );

        $this->assertApiResponse($editedProductAudience);
    }

    /**
     * @test
     */
    public function test_delete_product_audience()
    {
        $productAudience = factory(ProductAudience::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_audiences/'.$productAudience->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_audiences/'.$productAudience->id
        );

        $this->response->assertStatus(404);
    }
}
