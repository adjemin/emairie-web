<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductCondition;

class ProductConditionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_conditions', $productCondition
        );

        $this->assertApiResponse($productCondition);
    }

    /**
     * @test
     */
    public function test_read_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_conditions/'.$productCondition->id
        );

        $this->assertApiResponse($productCondition->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();
        $editedProductCondition = factory(ProductCondition::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_conditions/'.$productCondition->id,
            $editedProductCondition
        );

        $this->assertApiResponse($editedProductCondition);
    }

    /**
     * @test
     */
    public function test_delete_product_condition()
    {
        $productCondition = factory(ProductCondition::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_conditions/'.$productCondition->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_conditions/'.$productCondition->id
        );

        $this->response->assertStatus(404);
    }
}
