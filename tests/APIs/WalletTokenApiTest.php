<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\WalletToken;

class WalletTokenApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/wallet_tokens', $walletToken
        );

        $this->assertApiResponse($walletToken);
    }

    /**
     * @test
     */
    public function test_read_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/wallet_tokens/'.$walletToken->id
        );

        $this->assertApiResponse($walletToken->toArray());
    }

    /**
     * @test
     */
    public function test_update_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();
        $editedWalletToken = factory(WalletToken::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/wallet_tokens/'.$walletToken->id,
            $editedWalletToken
        );

        $this->assertApiResponse($editedWalletToken);
    }

    /**
     * @test
     */
    public function test_delete_wallet_token()
    {
        $walletToken = factory(WalletToken::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/wallet_tokens/'.$walletToken->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/wallet_tokens/'.$walletToken->id
        );

        $this->response->assertStatus(404);
    }
}
