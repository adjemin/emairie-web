<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryAddress;

class DeliveryAddressApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_addresses', $deliveryAddress
        );

        $this->assertApiResponse($deliveryAddress);
    }

    /**
     * @test
     */
    public function test_read_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_addresses/'.$deliveryAddress->id
        );

        $this->assertApiResponse($deliveryAddress->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();
        $editedDeliveryAddress = factory(DeliveryAddress::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_addresses/'.$deliveryAddress->id,
            $editedDeliveryAddress
        );

        $this->assertApiResponse($editedDeliveryAddress);
    }

    /**
     * @test
     */
    public function test_delete_delivery_address()
    {
        $deliveryAddress = factory(DeliveryAddress::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_addresses/'.$deliveryAddress->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_addresses/'.$deliveryAddress->id
        );

        $this->response->assertStatus(404);
    }
}
