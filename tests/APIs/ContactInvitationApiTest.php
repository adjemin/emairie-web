<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ContactInvitation;

class ContactInvitationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/contact_invitations', $contactInvitation
        );

        $this->assertApiResponse($contactInvitation);
    }

    /**
     * @test
     */
    public function test_read_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/contact_invitations/'.$contactInvitation->id
        );

        $this->assertApiResponse($contactInvitation->toArray());
    }

    /**
     * @test
     */
    public function test_update_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();
        $editedContactInvitation = factory(ContactInvitation::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/contact_invitations/'.$contactInvitation->id,
            $editedContactInvitation
        );

        $this->assertApiResponse($editedContactInvitation);
    }

    /**
     * @test
     */
    public function test_delete_contact_invitation()
    {
        $contactInvitation = factory(ContactInvitation::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/contact_invitations/'.$contactInvitation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/contact_invitations/'.$contactInvitation->id
        );

        $this->response->assertStatus(404);
    }
}
