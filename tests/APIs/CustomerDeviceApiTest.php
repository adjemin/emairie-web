<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerDevice;

class CustomerDeviceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_devices', $customerDevice
        );

        $this->assertApiResponse($customerDevice);
    }

    /**
     * @test
     */
    public function test_read_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_devices/'.$customerDevice->id
        );

        $this->assertApiResponse($customerDevice->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();
        $editedCustomerDevice = factory(CustomerDevice::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_devices/'.$customerDevice->id,
            $editedCustomerDevice
        );

        $this->assertApiResponse($editedCustomerDevice);
    }

    /**
     * @test
     */
    public function test_delete_customer_device()
    {
        $customerDevice = factory(CustomerDevice::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_devices/'.$customerDevice->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_devices/'.$customerDevice->id
        );

        $this->response->assertStatus(404);
    }
}
