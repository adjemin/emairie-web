<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DeliveryService;

class DeliveryServiceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/delivery_services', $deliveryService
        );

        $this->assertApiResponse($deliveryService);
    }

    /**
     * @test
     */
    public function test_read_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/delivery_services/'.$deliveryService->id
        );

        $this->assertApiResponse($deliveryService->toArray());
    }

    /**
     * @test
     */
    public function test_update_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();
        $editedDeliveryService = factory(DeliveryService::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/delivery_services/'.$deliveryService->id,
            $editedDeliveryService
        );

        $this->assertApiResponse($editedDeliveryService);
    }

    /**
     * @test
     */
    public function test_delete_delivery_service()
    {
        $deliveryService = factory(DeliveryService::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/delivery_services/'.$deliveryService->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/delivery_services/'.$deliveryService->id
        );

        $this->response->assertStatus(404);
    }
}
