<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\EvaluateSeller;

class EvaluateSellerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/evaluate_sellers', $evaluateSeller
        );

        $this->assertApiResponse($evaluateSeller);
    }

    /**
     * @test
     */
    public function test_read_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/evaluate_sellers/'.$evaluateSeller->id
        );

        $this->assertApiResponse($evaluateSeller->toArray());
    }

    /**
     * @test
     */
    public function test_update_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();
        $editedEvaluateSeller = factory(EvaluateSeller::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/evaluate_sellers/'.$evaluateSeller->id,
            $editedEvaluateSeller
        );

        $this->assertApiResponse($editedEvaluateSeller);
    }

    /**
     * @test
     */
    public function test_delete_evaluate_seller()
    {
        $evaluateSeller = factory(EvaluateSeller::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/evaluate_sellers/'.$evaluateSeller->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/evaluate_sellers/'.$evaluateSeller->id
        );

        $this->response->assertStatus(404);
    }
}
