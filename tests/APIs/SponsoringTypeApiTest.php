<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SponsoringType;

class SponsoringTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sponsoring_types', $sponsoringType
        );

        $this->assertApiResponse($sponsoringType);
    }

    /**
     * @test
     */
    public function test_read_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/sponsoring_types/'.$sponsoringType->id
        );

        $this->assertApiResponse($sponsoringType->toArray());
    }

    /**
     * @test
     */
    public function test_update_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();
        $editedSponsoringType = factory(SponsoringType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sponsoring_types/'.$sponsoringType->id,
            $editedSponsoringType
        );

        $this->assertApiResponse($editedSponsoringType);
    }

    /**
     * @test
     */
    public function test_delete_sponsoring_type()
    {
        $sponsoringType = factory(SponsoringType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sponsoring_types/'.$sponsoringType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sponsoring_types/'.$sponsoringType->id
        );

        $this->response->assertStatus(404);
    }
}
