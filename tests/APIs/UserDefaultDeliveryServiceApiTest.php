<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\UserDefaultDeliveryService;

class UserDefaultDeliveryServiceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/user_default_delivery_services', $userDefaultDeliveryService
        );

        $this->assertApiResponse($userDefaultDeliveryService);
    }

    /**
     * @test
     */
    public function test_read_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/user_default_delivery_services/'.$userDefaultDeliveryService->id
        );

        $this->assertApiResponse($userDefaultDeliveryService->toArray());
    }

    /**
     * @test
     */
    public function test_update_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();
        $editedUserDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/user_default_delivery_services/'.$userDefaultDeliveryService->id,
            $editedUserDefaultDeliveryService
        );

        $this->assertApiResponse($editedUserDefaultDeliveryService);
    }

    /**
     * @test
     */
    public function test_delete_user_default_delivery_service()
    {
        $userDefaultDeliveryService = factory(UserDefaultDeliveryService::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/user_default_delivery_services/'.$userDefaultDeliveryService->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/user_default_delivery_services/'.$userDefaultDeliveryService->id
        );

        $this->response->assertStatus(404);
    }
}
