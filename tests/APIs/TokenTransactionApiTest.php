<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TokenTransaction;

class TokenTransactionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/token_transactions', $tokenTransaction
        );

        $this->assertApiResponse($tokenTransaction);
    }

    /**
     * @test
     */
    public function test_read_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/token_transactions/'.$tokenTransaction->id
        );

        $this->assertApiResponse($tokenTransaction->toArray());
    }

    /**
     * @test
     */
    public function test_update_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();
        $editedTokenTransaction = factory(TokenTransaction::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/token_transactions/'.$tokenTransaction->id,
            $editedTokenTransaction
        );

        $this->assertApiResponse($editedTokenTransaction);
    }

    /**
     * @test
     */
    public function test_delete_token_transaction()
    {
        $tokenTransaction = factory(TokenTransaction::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/token_transactions/'.$tokenTransaction->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/token_transactions/'.$tokenTransaction->id
        );

        $this->response->assertStatus(404);
    }
}
