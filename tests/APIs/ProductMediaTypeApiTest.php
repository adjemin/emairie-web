<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductMediaType;

class ProductMediaTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_media_types', $productMediaType
        );

        $this->assertApiResponse($productMediaType);
    }

    /**
     * @test
     */
    public function test_read_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_media_types/'.$productMediaType->id
        );

        $this->assertApiResponse($productMediaType->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();
        $editedProductMediaType = factory(ProductMediaType::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_media_types/'.$productMediaType->id,
            $editedProductMediaType
        );

        $this->assertApiResponse($editedProductMediaType);
    }

    /**
     * @test
     */
    public function test_delete_product_media_type()
    {
        $productMediaType = factory(ProductMediaType::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_media_types/'.$productMediaType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_media_types/'.$productMediaType->id
        );

        $this->response->assertStatus(404);
    }
}
