<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderHistory;

class OrderHistoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_histories', $orderHistory
        );

        $this->assertApiResponse($orderHistory);
    }

    /**
     * @test
     */
    public function test_read_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_histories/'.$orderHistory->id
        );

        $this->assertApiResponse($orderHistory->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();
        $editedOrderHistory = factory(OrderHistory::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_histories/'.$orderHistory->id,
            $editedOrderHistory
        );

        $this->assertApiResponse($editedOrderHistory);
    }

    /**
     * @test
     */
    public function test_delete_order_history()
    {
        $orderHistory = factory(OrderHistory::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_histories/'.$orderHistory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_histories/'.$orderHistory->id
        );

        $this->response->assertStatus(404);
    }
}
