<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FlashSelling;

class FlashSellingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/flash_sellings', $flashSelling
        );

        $this->assertApiResponse($flashSelling);
    }

    /**
     * @test
     */
    public function test_read_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/flash_sellings/'.$flashSelling->id
        );

        $this->assertApiResponse($flashSelling->toArray());
    }

    /**
     * @test
     */
    public function test_update_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();
        $editedFlashSelling = factory(FlashSelling::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/flash_sellings/'.$flashSelling->id,
            $editedFlashSelling
        );

        $this->assertApiResponse($editedFlashSelling);
    }

    /**
     * @test
     */
    public function test_delete_flash_selling()
    {
        $flashSelling = factory(FlashSelling::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/flash_sellings/'.$flashSelling->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/flash_sellings/'.$flashSelling->id
        );

        $this->response->assertStatus(404);
    }
}
