<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CustomerSession;

class CustomerSessionApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/customer_sessions', $customerSession
        );

        $this->assertApiResponse($customerSession);
    }

    /**
     * @test
     */
    public function test_read_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/customer_sessions/'.$customerSession->id
        );

        $this->assertApiResponse($customerSession->toArray());
    }

    /**
     * @test
     */
    public function test_update_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();
        $editedCustomerSession = factory(CustomerSession::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/customer_sessions/'.$customerSession->id,
            $editedCustomerSession
        );

        $this->assertApiResponse($editedCustomerSession);
    }

    /**
     * @test
     */
    public function test_delete_customer_session()
    {
        $customerSession = factory(CustomerSession::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/customer_sessions/'.$customerSession->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/customer_sessions/'.$customerSession->id
        );

        $this->response->assertStatus(404);
    }
}
