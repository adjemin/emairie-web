<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Sponsoring;

class SponsoringApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sponsorings', $sponsoring
        );

        $this->assertApiResponse($sponsoring);
    }

    /**
     * @test
     */
    public function test_read_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/sponsorings/'.$sponsoring->id
        );

        $this->assertApiResponse($sponsoring->toArray());
    }

    /**
     * @test
     */
    public function test_update_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();
        $editedSponsoring = factory(Sponsoring::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sponsorings/'.$sponsoring->id,
            $editedSponsoring
        );

        $this->assertApiResponse($editedSponsoring);
    }

    /**
     * @test
     */
    public function test_delete_sponsoring()
    {
        $sponsoring = factory(Sponsoring::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sponsorings/'.$sponsoring->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sponsorings/'.$sponsoring->id
        );

        $this->response->assertStatus(404);
    }
}
