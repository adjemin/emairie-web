<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ProductMedia;

class ProductMediaApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_product_media()
    {
        $productMedia = factory(ProductMedia::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/product_media', $productMedia
        );

        $this->assertApiResponse($productMedia);
    }

    /**
     * @test
     */
    public function test_read_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/product_media/'.$productMedia->id
        );

        $this->assertApiResponse($productMedia->toArray());
    }

    /**
     * @test
     */
    public function test_update_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();
        $editedProductMedia = factory(ProductMedia::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/product_media/'.$productMedia->id,
            $editedProductMedia
        );

        $this->assertApiResponse($editedProductMedia);
    }

    /**
     * @test
     */
    public function test_delete_product_media()
    {
        $productMedia = factory(ProductMedia::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/product_media/'.$productMedia->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/product_media/'.$productMedia->id
        );

        $this->response->assertStatus(404);
    }
}
