<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderAssignment;

class OrderAssignmentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/order_assignments', $orderAssignment
        );

        $this->assertApiResponse($orderAssignment);
    }

    /**
     * @test
     */
    public function test_read_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/order_assignments/'.$orderAssignment->id
        );

        $this->assertApiResponse($orderAssignment->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();
        $editedOrderAssignment = factory(OrderAssignment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/order_assignments/'.$orderAssignment->id,
            $editedOrderAssignment
        );

        $this->assertApiResponse($editedOrderAssignment);
    }

    /**
     * @test
     */
    public function test_delete_order_assignment()
    {
        $orderAssignment = factory(OrderAssignment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/order_assignments/'.$orderAssignment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/order_assignments/'.$orderAssignment->id
        );

        $this->response->assertStatus(404);
    }
}
